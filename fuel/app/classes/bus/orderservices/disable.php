<?php

namespace Bus;

/**
 * Disable/Enable list Order Service
 *
 * @package Bus
 * @created 2015-05-25
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class OrderServices_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'       => array(1, 11),
        'order_id' => array(1, 11),
        'service_id'  => array(1, 11),
        'disable'  => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'service_id',
        'disable'
    );

    /**
     * Call function disable() from model Order Service
     *
     * @author KhoaTX
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Service::disable($data);
            return $this->result(\Model_Order_Service::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
