<?php

namespace Bus;

/**
 * Get all Order Service (without array count)
 *
 * @package Bus
 * @created 2015-05-25
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class OrderServices_All extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id'
    );

    /**
     * Call function get_all() from model Order Service
     *
     * @author KhoaTX
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Service::get_all($data);
            return $this->result(\Model_Order_Service::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
