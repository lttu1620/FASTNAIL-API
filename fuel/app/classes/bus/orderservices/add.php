<?php

namespace Bus;

/**
 * Add info for Order Service
 *
 * @package Bus
 * @created 2015-05-25
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class OrderServices_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id',
        'service_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11),
        'service_id'  => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'service_id'
    );

    /**
     * Call function add() from model Order Service
     *
     * @author KhoaTX
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Service::add($data);
            return $this->result(\Model_Order_Service::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
