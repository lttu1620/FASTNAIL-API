<?php

namespace Bus;

/**
 * <ShopGroups_AddUpdate - API to get list of ShopGroups>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class ShopGroups_AddUpdate extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'name',
        'address',
    );
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11),
        'name' => array(0,64),
        'address' => array(0,256)
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'disable',
    );
    /**
     * Call function operateDB() from model ShopGroup
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_Shop_Group::add_update($data);
            return $this->result(\Model_Shop_Group::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
