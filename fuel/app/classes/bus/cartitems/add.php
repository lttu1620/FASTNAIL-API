<?php

namespace Bus;

/**
 * Add info for Cart Item
 *
 * @package Bus
 * @created 2015-03-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CartItems_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'cart_id',
        'item_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'cart_id' => array(1, 11),
        'item_id'  => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'cart_id',
        'item_id'
    );

    /**
     * Call function add() from model Cart Item
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Cart_Item::add($data);
            return $this->result(\Model_Cart_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
