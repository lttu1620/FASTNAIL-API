<?php

namespace Bus;

/**
 * <Keywords_All - Model to operate to Keywords's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Keywords_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Keyword::get_all($data);
            return $this->result(\Model_Keyword::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
