<?php

namespace Bus;

/**
 * Update info for Sub order
 *
 * @package Bus
 * @created 2015-09-15
 * @version 1.0
 * @author DienNVT
 * @copyright Oceanize INC
 */
class SubOrders_Update extends BusAbstract
{
    /** @var array $_requires Require of fields */
    protected $_required = array(
        'id',
        'order_id',
        'order_start_date',
        'order_end_date',
        'hf_section'
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'     => array(1, 11),
        'order_id'     => array(1, 11),
        'hf_section'   => array(1, 32),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'order_id',     
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'order_start_date' => 'Y-m-d H:i',
        'order_end_date'   => 'Y-m-d H:i',
    );

    /**
     * Call function add() from model Sub Order
     *
     * @author DienNVT
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Sub_Order::update_sub_order($data);
            return $this->result(\Model_Sub_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
