<?php

namespace Bus;

/**
 * Get all sub order of a order
 *
 * @package Bus
 * @created 2015-09-15
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class SubOrders_All extends BusAbstract
{
    /** @var array $_requires Require of fields */
    protected $_required = array(
        'order_id',     
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'     => array(1, 11),      
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',     
    );
    
    /**
     * Call function get_all() from model Sub Order
     *
     * @author thailh
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Sub_Order::get_all($data);
            return $this->result(\Model_Sub_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
