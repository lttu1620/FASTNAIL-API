<?php

namespace Bus;

/**
 * Disable/Enable list Order Daily Limit
 *
 * @package Bus
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderDailyLimits_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    /**
     * Call function disable() from model Order Daily Limit
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Daily_Limit::disable($data);
            return $this->result(\Model_Order_Daily_Limit::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
