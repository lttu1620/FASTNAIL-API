<?php

namespace Bus;

/**
 * Get list Order Daily Limit
 *
 * @package Bus
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderDailyLimits_List extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'shop_id',
        'date'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Order Daily Limit
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Daily_Limit::get_list($data);
            return $this->result(\Model_Order_Daily_Limit::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
