<?php

namespace Bus;

/**
 * Get all Holograms (without array count)
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Holograms_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    /**
     * Call function get_all() from model Holograms
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Hologram::get_all();
            return $this->result(\Model_Hologram::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
