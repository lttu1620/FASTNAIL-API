<?php

namespace Bus;

/**
 * Update info for User
 *
 * @package Bus
 * @created 2015-06-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_UpdateProfile extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(1, 64),
        'kana' => array(1, 64),       
        'sex' => 1,
        'phone' => array(1, 64),
        'post_code' => array(1, 50),
        'prefecture_id' => array(1, 11),
        'address1' => array(1, 128),
        'address2' => array(1, 128),
        'is_magazine' => 1,
        'visit_element' => array(1, 11),
        'is_point_member' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'sex',
        'prefecture_id',
        'is_magazine',
        'visit_element',
        'is_point_member',
    );

    /** @var array $_date_format date */
    protected $_date_format = array(
        'birthday' => 'Y-m-d',
    );
    
    /** @var array $_kana_format  */
    protected $_kana_format = array(
        'kana'
    );    

    /**
     * Call function update_profile() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::update_profile($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
