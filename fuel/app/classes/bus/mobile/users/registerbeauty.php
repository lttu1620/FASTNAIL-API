<?php

namespace Bus;

/**
 * Register Beauty
 *
 * @package Bus
 * @created 2015-08-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_RegisterBeauty extends BusAbstract
{
    /** @var array $_required require of fields */
    protected $_required = array(
        'login_user_id',        
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_user_id' => array(1, 11),        
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'login_user_id',       
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'is_point_member' => 1
    );

    /**
     * Call function register_beauty() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::register_beauty($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
