<?php

namespace Bus;

/**
 * Forget password for user
 *
 * @package Bus
 * @created 2015-08-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_ForgetPassword extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function forget_password_for_app() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::forget_password_for_app($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
