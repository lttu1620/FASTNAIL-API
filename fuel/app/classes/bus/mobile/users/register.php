<?php

namespace Bus;

use Lib\Util;

/**
 * Register User
 *
 * @package Bus
 * @created 2015-06-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_Register extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
        'password'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'email'         => array(1, 255),
        'password'      => array(4, 255),
        'name'          => array(1, 64),
        'kana'          => array(1, 64),
        'sex'           => 1,
        'phone'         => array(1, 64),
        'post_code'     => array(1, 50),
        'prefecture_id' => array(1, 11),
        'address1'      => array(1, 128),
        'address2'      => array(1, 128),
        'is_magazine'   => 1,
        'visit_element' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(        
        'sex',
        'prefecture_id',
        'is_magazine',
        'visit_element'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'sex' => 0
    );

    /** @var array $_date_format date */
    protected $_date_format = array(
        'birthday' => 'Y-m-d',
    );

    /** @var array $_kana_format  */
    protected $_kana_format = array(
        'kana'
    );   

    /**
     * Call function register_by_mobile() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::register_by_mobile($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
