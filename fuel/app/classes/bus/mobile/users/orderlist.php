<?php

namespace Bus;

/**
 * Get list Order (with array count)
 *
 * @package Bus
 * @created 2015-05-04
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Mobile_Users_OrderList extends BusAbstract
{
	/** @var array $_required require of fields */
	protected $_required = array(
        'login_user_id'
	);
	
    /** @var array $_length Length of fields */
    protected $_length = array(    
        'login_user_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );

    /**
     * Call function get_user_order_list() from model Order
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $response = \Model_Order::get_user_order_list($data);
            $this->_response = array(
            		'total' => !empty($response[0]) ? $response[0] : 0,
            		'data' => !empty($response[1]) ? $response[1] : array()
            );
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
