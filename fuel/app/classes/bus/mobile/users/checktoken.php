<?php

namespace Bus;

/**
 * Check token for User
 *
 * @package Bus
 * @created 2015-08-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_CheckToken extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'token'
    );

    /** @var array $_default_value default value */
    protected $_default_value = array(
        'regist_type' => 'forget_password'
    );

    /**
     * Call function check_token_for_app() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::check_token_for_app($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
