<?php

namespace Bus;

/**
 * Search Nails
 *
 * @package Bus
 * @created 2015-06-19
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Mobile_Nails_Search extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'hf_section' => array(1, 11),
        'design_id' => array(1, 11),
        'price' => array(1, 11),
        'scene_id' => array(1, 11),
        'genre_id' => array(1, 11),
        'color_id' => array(1, 11),
        'keyword_id' => array(1, 11),     
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'hf_section',
        'design_id',
        'price',
        'scene_id',
        'genre_id',
        'color_id',
        'keyword_id',
    );

    /**
     * Call function search() from model Nail
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {        
        try {
            $response = \Model_Nail::search($data);
            $this->_response = array(
                'total' => !empty($response[0]) ? $response[0] : 0,
                'data' => !empty($response[1]) ? $response[1] : array()
            );
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
