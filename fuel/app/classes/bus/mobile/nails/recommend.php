<?php

namespace Bus;

/**
 * Get Nail Recommend
 *
 * @package Bus
 * @created 2015-06-09
 * @version 1.0
 * @author CaoLP
 * @copyright Oceanize INC
 */
class Mobile_Nails_Recommend extends BusAbstract
{
    /**
     * Call function get_mobile_recommend() from model Nails
     *
     * @author CaoLP
     * @param array $data Input data
     * @return array recommend
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail::get_mobile_recommend($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
