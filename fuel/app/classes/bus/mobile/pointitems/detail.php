<?php

namespace Bus;

/**
 * Get list Item Point (using array count)
 *
 * @package Bus
 * @created 2015-06-23
 * @version 1.0
 * @author Caolp
 * @copyright Oceanize INC
 */
class Mobile_PointItems_Detail extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
    );
    /**
     * Call function get_list_for_mobile() from model Item Point
     *
     * @author Caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Point_Item::get_detail_for_mobile($data);
            return $this->result(\Model_Point_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
