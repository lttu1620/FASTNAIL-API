<?php

namespace Bus;

/**
 * Get list User Point Log (using array count)
 *
 * @package Bus
 * @created 2015-07-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_UserPointLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'type' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'type'
    );

    /**
     * Call function get_list_for_mobile() from model User Point Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Point_Log::get_list_for_mobile($data);
            return $this->result(\Model_User_Point_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
