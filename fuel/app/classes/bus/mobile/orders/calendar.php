<?php

namespace Bus;

/**
 * <Stones_All - Model to operate to Stones's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Mobile_Orders_Calendar extends BusAbstract 
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'shop_id', //shop_id
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'shop_id' => array(0, 11),
        'year' => array(0, 4),
        'month' => array(0, 2),
        'date_from' => array(0, 2),
        'date_to' => array(0, 2)
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'shop_id',
        'year',
        'month',
        'date_from',
        'date_to'
    );
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $data = \Model_Order::calendar($data);
            // custom response for mobile
            $response = array();
            foreach ($data as $date => $value) {
                $block = array();
                foreach ($value as $time => $status) {
                    $block[] = array(
                        'time' => $time,
                        'status' => $status
                    );
                }
                $response[] = array(
                    'date' => $date,                    
                    'block' => $block,                    
                );
            }            
            $this->_response = $response;
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
