<?php

namespace Bus;

/**
 * Get Nail Recommend
 *
 * @package Bus
 * @created 2015-06-10
 * @version 1.0
 * @author CaoLP
 * @copyright Oceanize INC
 */
class Mobile_Orders_Element extends BusAbstract
{
    /**
     * Call function get_mobile_element() from model Nails
     *
     * @author CaoLP
     * @param array $data Input data
     * @return array element
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_mobile_element($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
