<?php

namespace Bus;

/**
 * Get list info for Order
 *
 * @package Bus
 * @created 2015-04-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Orders_Confirm extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id'    => array(1, 11),
        'shop_id'    => array(1, 11),
        'nailist_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'shop_id',
        'nailist_id'
    );

    /**
     * Call function get_confirm_for_frontend() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_confirm_for_mobile($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
