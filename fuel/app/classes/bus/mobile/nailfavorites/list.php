<?php

namespace Bus;

/**
 * Get list Nail Favorite (using array count)
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_NailFavorites_List extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'login_user_id',       
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(        
        'login_user_id' => array(1, 11),
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',      
        'login_user_id',
        'disable'
    );

    /**
     * Call function get_list() from model Nail Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {           
            if (!empty($data['login_user_id'])) {
                $data['user_id'] = $data['login_user_id'];
            }
            $data['disable'] = 0;
            $response = \Model_Nail_Favorite::get_list($data);
            $this->_response = array(
                'total' => $response[0],
                'data' => $response[1],
            );           
            return $this->result(\Model_Nail_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
