<?php

namespace Bus;

/**
 * Disable/Enable User Beauty View Log
 *
 * @package Bus
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBeautyViewLogs_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'beauty_id',
    );

    /** @var array $_length Define check length */
    protected $_length = array(
        'user_id'   => array(1, 11),
        'beauty_id' => array(1, 11),
    );

    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'user_id',
        'beauty_id',
    );

    /**
     * Call function disable() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Beauty_View_Log::disable($data);
            return $this->result(\Model_User_Beauty_View_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
