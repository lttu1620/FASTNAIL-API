<?php

namespace Bus;

/**
 * Add info for User Beauty View Log
 *
 * @package Bus
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBeautyViewLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'login_user_id',
        'beauty_id',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_user_id' => array(1, 11),
        'beauty_id'     => array(1, 11),
        'image_url'     => array(1, 256),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'login_user_id',
        'beauty_id',

    );

    /**
     * Call function add() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Beauty_View_Log::add($data);
            return $this->result(\Model_User_Beauty_View_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
