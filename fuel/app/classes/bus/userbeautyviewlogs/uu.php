<?php

namespace Bus;

/**
 * Get unique user from User Beauty View Log
 *
 * @package Bus
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBeautyViewLogs_Uu extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'beauty_id'     => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'beauty_id',

    );
    /** @var array $_date_format*/
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );
    
    /**
     * Call function get_uu() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Beauty_View_Log::get_uu($data);
            return $this->result(\Model_User_Beauty_View_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
