<?php

namespace Bus;

/**
 * Get list User Beauty View Log (using array count)
 *
 * @package Bus
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBeautyViewLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'   => array(1, 11),
        'beauty_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'user_id',
        'beauty_id',
    );

    /**
     * Call function get_list() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Beauty_View_Log::get_list($data);
            return $this->result(\Model_User_Beauty_View_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
