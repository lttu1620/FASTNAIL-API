<?php

namespace Bus;

/**
 * Get all Color Jell (without array count)
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ColorJells_All extends BusAbstract
{
    /**
     * Call function get_all() from model Color Jell
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Color_Jell::get_all();
            return $this->result(\Model_Color_Jell::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
