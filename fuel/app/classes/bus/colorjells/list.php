<?php

namespace Bus;

/**
 * Get list Color Jell (using array count)
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ColorJells_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'    => array(1, 128),
        'phone'   => array(1, 45),
        'address' => array(1, 128),
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable'
    );

    /**
     * Call function get_list() from model Color Jell
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Color_Jell::get_list($data);
            return $this->result(\Model_Color_Jell::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
