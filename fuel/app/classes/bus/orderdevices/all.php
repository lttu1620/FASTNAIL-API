<?php

namespace Bus;

/**
 * Get all Order Device (without array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderDevices_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'device_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'    => array(1, 11),
        'device_id' => array(1, 11)
    );

    /**
     * Call function get_all() from model Order Device
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Device::get_all($data);
            return $this->result(\Model_Order_Device::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
