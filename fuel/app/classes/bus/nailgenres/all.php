<?php

namespace Bus;

/**
 * get list genre
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailGenres_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'genre_id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'genre_id' => array (1, 11),
        'disable' => 1
    );

    /**
     * call function get_all() from model Nail Genre
     *
     * @created 2015-03-20
     * @updated 2015-03-20
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Genre::get_all($data);
            return $this->result(\Model_Nail_Genre::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
