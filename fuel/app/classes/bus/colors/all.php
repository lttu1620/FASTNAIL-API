<?php

namespace Bus;

/**
 * <Colors_All - Model to operate to Colors's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Colors_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Color::get_all($data);
            return $this->result(\Model_Color::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
