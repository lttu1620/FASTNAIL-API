<?php

namespace Bus;

/**
 * Get list Survey (using array count)
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Surveys_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'    => array(1, 11),
        'title'   => array(1, 45),
        'url' => array(1, 128),
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'order_id'
    );

    /**
     * Check date format
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool True if successful, otherwise false
     */
    public function checkDateFormat($data)
    {
        $value = array(
            'Y-m-d',
            'Y-m-d H:i:s'
        );
        $fields = array(
            'date_from',
            'date_to'
        );
        foreach ($fields as $field) {
            if (!empty($data[$field])) {
                $dt = \DateTime::createFromFormat($value[0], $data[$field]);
                if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
                    $dt = \DateTime::createFromFormat($value[1], $data[$field]);
                    if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
                        $this->_addError(self::ERROR_CODE_FIELD_FORMAT_DATE, $field, $data[$field]);
                        $this->_invalid_parameter = $field;
                        return false;
                    }
                }
            }
        }
        if (!empty($data[$fields[0]]) && !empty($data[$fields[1]])) {
            if (strtotime($data[$fields[0]]) > strtotime($data[$fields[1]])) {
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, 'date');
                $this->_invalid_parameter = 'date';
                return false;
            }
        }
        return true;
    }

    /**
     * Call function get_list() from model Survey
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Survey::get_list($data);
            return $this->result(\Model_Survey::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
