<?php

namespace Bus;

/**
 * Get all Survey (without array count)
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Surveys_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'    => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_all() from model Survey
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Survey::get_all();
            return $this->result(\Model_Survey::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
