<?php

namespace Bus;

/**
 * Add or update info for Survey
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Surveys_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'       => array(1, 11),
        'order_id' => array(1, 11),
        'title'    => array(1, 45),
        'url'      => array(1, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'order_id'
    );

    /**
     * Check date format
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool True if successful, otherwise false
     */
    public function checkDateFormat($data)
    {
        $value = array(
            'Y-m-d',
            'Y-m-d H:i:s'
        );
        $fields = array(
            'started',
            'finished'
        );
        foreach ($fields as $field) {
            if (!empty($data[$field])) {
                $dt = \DateTime::createFromFormat($value[0], $data[$field]);
                if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
                    $dt = \DateTime::createFromFormat($value[1], $data[$field]);
                    if (!($dt !== false && !array_sum($dt->getLastErrors()))) {
                        $this->_addError(self::ERROR_CODE_FIELD_FORMAT_DATE, $field, $data[$field]);
                        $this->_invalid_parameter = $field;
                        return false;
                    }
                }
            }
        }
        if (strtotime($data[$fields[0]]) > strtotime($data[$fields[1]])) {
            $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, 'date');
            $this->_invalid_parameter = 'date';
            return false;
        }
        return true;
    }

    /**
     * Call function add_update() from model Survey
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Survey::add_update($data);
            return $this->result(\Model_Survey::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
