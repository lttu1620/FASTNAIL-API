<?php

namespace Bus;

/**
 * Execute is_plus list Shop
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Shops_Plus extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'is_plus'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'is_plus' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'is_plus'
    );

    /**
     * Call function plus() from model Shop
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shop::plus($data);
            return $this->result(\Model_Shop::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
