<?php

namespace Bus;

/**
 * Get list Shop's customer
 *
 * @package Bus
 * @created 2015-04-09
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class Shops_Customer extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'    => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'shop_id',
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_customer() from model Shop
     *
     * @author KhoaTX
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shop::get_customer($data);
            return $this->result(\Model_Shop::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
