<?php

namespace Bus;

/**
 * Add or update info for Shop
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Shops_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                   => array(1, 11),
        'shop_group_id'        => array(1, 11),
        'area_id'              => array(1, 11),
        'prefecture_id'        => array(1, 11),
        'admin_id'             => array(1, 11),
        'reservation_interval' => array(1, 11),
        'name'                 => array(1, 45),
        'phone'                => array(0, 45),
        'address'              => array(1, 128),    
        'open_time'            => array(1, 11),
        'close_time'           => array(1, 11),
        'is_designate'         => 1,
        'is_plus'              => 1,
        'order_daily_limit'    => array(1, 11),
        'max_seat'             => array(1, 11),
        'hp_max_seat'          => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'shop_group_id',
        'area_id',
        'prefecture_id',
        'admin_id',
        'reservation_interval',
        'is_designate',
        'is_plus',
        'open_time',
        'close_time',
        'order_daily_limit',
        'max_seat',
        'hp_max_seat'
    );

    /**
     * Call function add_update() from model Shop
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shop::add_update($data);
            return $this->result(\Model_Shop::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
