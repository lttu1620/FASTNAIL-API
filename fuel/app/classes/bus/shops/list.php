<?php

namespace Bus;

/**
 * Get list Shop (using array count)
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Shops_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'       => array(1, 128),
        'phone'      => array(1, 45),
        'address'    => array(1, 128),
        'open_time'  => array(1, 11),
        'close_time' => array(1, 11),
        'disable'    => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'open_time',
        'close_time'
    );

    /**
     * Call function get_list() from model Shop
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shop::get_list($data);
            return $this->result(\Model_Shop::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
