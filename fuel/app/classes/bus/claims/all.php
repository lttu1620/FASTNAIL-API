<?php

namespace Bus;

/**
 * Get all Claim (without array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Claims_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /**
     * Call function get_all() from model Claim
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Claim::get_all();
            return $this->result(\Model_Claim::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
