<?php

namespace Bus;

/**
 * Add or update info for Claim
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Claims_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'         => array(1, 11),
        'user_id'    => array(1, 11),
        'nailist_id' => array(1, 11),
        'shop_id'    => array(1, 11),
        'order_id'   => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
        'nailist_id',
        'shop_id',
        'order_id'
    );

    /**
     * Call function add_update() from model Claim
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Claim::add_update($data);
            return $this->result(\Model_Claim::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
