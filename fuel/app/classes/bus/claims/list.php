<?php

namespace Bus;

/**
 * Get list Claim (using array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Claims_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'      => array(1, 11),
        'order_id'     => array(1, 11),
        'user_name'    => array(1, 64),
        'nailist_name' => array(1, 64),
        'disable'      => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'shop_id',
        'order_id'
    );

    /**
     * Call function get_list() from model Claim
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Claim::get_list($data);
            return $this->result(\Model_Claim::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
