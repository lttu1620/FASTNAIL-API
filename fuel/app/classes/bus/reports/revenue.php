<?php

namespace Bus;

/**
 * <Colors_Detail - API to get detail of Colors>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Reports_Revenue extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        //'shop_id', //shop_id
        //'nailist_id', //shop_id
        'limit',
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'shop_id' => array(0, 11),
        'nailist_id' => array(0, 11),
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'shop_id',
        'nailist_id',
        'limit',
    );
    /**
     * Call function operateDB() from model Color
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
           
            $this->_response = \Model_Order::get_revenue($data);
            
            return $this->result(\Model_Order::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
