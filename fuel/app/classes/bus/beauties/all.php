<?php

namespace Bus;

/**
 * <Beautys_All - Model to operate to Beauty's functions>
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauties_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author diennvt
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Beauty::get_all($data);
            return $this->result(\Model_Beauty::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
