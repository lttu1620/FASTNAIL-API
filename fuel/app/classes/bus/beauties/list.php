<?php

namespace Bus;

/**
 * <Beauties_List - API to get list of Beauties>
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauties_List extends BusAbstract
{

	/**
     * Call function operateDB() 
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
   
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Beauty::get_list($data);
            return $this->result(\Model_Beauty::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
