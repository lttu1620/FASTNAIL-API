<?php

namespace Bus;

/**
 * <Beauties_Detail - API to get detail of Beauties>
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauties_Detail extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'id', 
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(1, 11),
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
    );
    /**
     * Call function operateDB() from model Beauty
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Beauty::get_detail($data);
            return $this->result(\Model_Beauty::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
