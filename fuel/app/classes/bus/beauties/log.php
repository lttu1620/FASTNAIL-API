<?php

namespace Bus;

/**
 * Log view
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauties_Log extends BusAbstract
{
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(1, 11),        
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',    	
    );

    /**
     * Call function operateDB() from model Beauty
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_User_Beauty_View_Log::addLog(array(
                'login_user_id' => $data['login_user_id'],
                'beauty_id' => $data['id'],
            ));
            return $this->result(\Model_User_Beauty_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
