<?php

namespace Bus;

/**
 * Get random beauty
 *
 * @package Bus
 * @created 2015-06-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Beauties_OneRandom extends BusAbstract
{
    /**
     * Call function get_random() from model Beauty
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Beauty::get_random($data);
            return $this->result(\Model_Beauty::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
