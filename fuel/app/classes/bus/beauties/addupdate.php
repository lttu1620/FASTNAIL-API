<?php

namespace Bus;

/**
 * <Beautys_AddUpdate - API to get list of Beautys>
 *
 * @package Bus
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Beauties_AddUpdate extends BusAbstract
{
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(1, 11),
        'title' => array(0,128),
    	'point' => array(1,11)	
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
    	'point',
        'display_no',
        'disable',
    );
    
    /** @var array $_date_format field date */
    protected $_date_format = array(       
        'start_date' => 'Y-m-d',      
        'end_date' => 'Y-m-d',       
    );
    
    /** @var array $_default_value field default */
    protected $_default_value = array(
        'mission_type' => 0,
        'segment_device' => 0,
        'distribution_limit' => 0,
        'display_no' => 0
    );

    /**
     * Validate list with format: d,d,d,d
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        //Validate id's format
        $fields = array(
            'segment_prefecture',
            'segment_user_generation'
        );
        foreach ($fields as $field) {
            if (!empty($data[$field])
                && !preg_match ('/^\d{1,2}(?:,\d{1,2})*$/', $data[$field])) {
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }
        return true;
    }

    /**
     * Call function operateDB() from model Beauty
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_Beauty::add_update($data);
            return $this->result(\Model_Beauty::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
