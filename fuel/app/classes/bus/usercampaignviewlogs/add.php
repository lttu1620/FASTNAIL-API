<?php

namespace Bus;

/**
 * Add info for User Campaign View Log
 *
 * @package Bus
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserCampaignViewLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'login_user_id',
        'campaign_id',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_user_id' => array(1, 11),
        'campaign_id'   => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'login_user_id',
        'campaign_id',

    );

    /**
     * Call function add() from model User Campaign View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Campaign_View_Log::add($data);
            return $this->result(\Model_User_Campaign_View_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
