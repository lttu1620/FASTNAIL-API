<?php

namespace Bus;

/**
 * <Paints_AddUpdate - API to get list of Paints>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Paints_AddUpdate extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'name',
    );
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11),
        'name' => array(0, 128)
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'disable',
    );
    /**
     * Call function operateDB() from model Paint
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_Paint::add_update($data);
            return $this->result(\Model_Paint::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
