<?php

namespace Bus;

/**
 * <Paints_List - API to get list of Paints>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Paints_List extends BusAbstract
{

    /** @var array $_required Define check required */
    protected $_required = array(
        'limit',
    );
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'limit',
    );
    /**
     * Call function operateDB() 
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */
   
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Paint::get_list($data);
            return $this->result(\Model_Paint::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
