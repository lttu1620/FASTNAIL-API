<?php

namespace Bus;

/**
 * <Paints_All - Model to operate to Paints's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Paints_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Paint::get_all($data);
            return $this->result(\Model_Paint::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
