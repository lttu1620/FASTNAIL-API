<?php

namespace Bus;

/**
 * Get all Nailist (without array count)
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Nailists_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /**
     * Call function get_all() from model Nailist
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nailist::get_all($data);
            return $this->result(\Model_Nailist::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
