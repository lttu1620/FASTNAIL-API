<?php

namespace Bus;

/**
 * Add or update info for Nailist
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Nailists_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'      => array(1, 11),
        'code'    => array(1, 64),
        'name'    => array(0, 64),
        'shop_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'shop_id'
    );

    /**
     * Call function add_update() from model Nailist
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nailist::add_update($data);
            return $this->result(\Model_Nailist::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
