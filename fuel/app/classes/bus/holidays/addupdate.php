<?php

namespace Bus;

/**
 * Add or update info for Holidays
 *
 * @package Bus
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Holidays_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'date',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(0, 255),
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d',
    );

    /**
     * Call function add_update() from model Holiday
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Holiday::add_update($data);
            return $this->result(\Model_Holiday::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
