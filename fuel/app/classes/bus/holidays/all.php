<?php

namespace Bus;

/**
 * Get all Holiday (without array count)
 *
 * @package Bus
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Holidays_All extends BusAbstract
{
    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d',
    );

    /**
     * Call function get_all() from model Holiday
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Holiday::get_all($data);
            return $this->result(\Model_Holiday::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
