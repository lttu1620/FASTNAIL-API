<?php

namespace Bus;

/**
 * Disable/Enable list Holiday
 *
 * @package Bus
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Holidays_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'date',
        'disable',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d',
    );

    /**
     * Call function disable() from model Holiday
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Holiday::disable($data);
            return $this->result(\Model_Holiday::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
