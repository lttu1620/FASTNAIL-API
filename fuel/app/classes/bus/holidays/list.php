<?php

namespace Bus;

/**
 * Get list Holiday (using array count)
 *
 * @package Bus
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Holidays_List extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'date'
    );
    
    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Holiday
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Holiday::get_list($data);
            return $this->result(\Model_Holiday::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
