<?php

namespace Bus;

/**
 * Get list Recommend Reaction Log (with array count)
 *
 * @package Bus
 * @created 2015-05-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class RecommendReactionLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'         => array(1, 11),
        'order_id'        => array(1, 11),
        'action_type'     => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'order_id',
        'action_type',
        'page',
        'limit'
    );

    /**
     * Call function get_list() from model Recommend Reaction Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Recommend_Reaction_Log::get_list($data);
            return $this->result(\Model_Recommend_Reaction_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
