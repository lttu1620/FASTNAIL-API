<?php

namespace Bus;

/**
 * Get list User Point Log (using array count)
 *
 * @package Bus
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserPointLogs_Used extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'login_user_id',        
    ); 

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',      
    );

    /**
     * Call function get_list() from model User Point Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {           
            $data['user_id'] = $data['login_user_id'];
            $data['type'] = array(
                \Config::get('user_point_logs.type.point_item'),
                \Config::get('user_point_logs.type.point_used_by_order'),
            );
            $this->_response = \Model_User_Point_Log::get_list_for_mobile($data);
            return $this->result(\Model_User_Point_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
