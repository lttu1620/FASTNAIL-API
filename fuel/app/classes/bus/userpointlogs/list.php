<?php

namespace Bus;

/**
 * Get list User Point Log (using array count)
 *
 * @package Bus
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserPointLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'  => array(1, 11),
        'username' => array(1, 64),
        'email'    => array(1, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'user_id'
    );

    /** @var array $_date_format date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_list() from model User Point Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Point_Log::get_list($data);
            return $this->result(\Model_User_Point_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
