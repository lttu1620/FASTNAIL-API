<?php

namespace Bus;

/**
 * get list tag
 *
 * @package Bus
 * @created 2015-03-25
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class NailTags_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'tag_id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'tag_id' => array (1, 11),
        'disable' => 1
    );

    /**
     * call function get_all() from model Nail Tag
     *
     * @created 2015-03-25
     * @updated 2015-03-25
     * @access public
     * @author Hoang Gia Thong
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Tag::get_all($data);
            return $this->result(\Model_Nail_Tag::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
