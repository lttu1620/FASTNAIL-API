<?php

namespace Bus;

/**
 * Get all Shells (without array count)
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Shells_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    /**
     * Call function get_all() from model Shells
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shell::get_all();
            return $this->result(\Model_Shell::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
