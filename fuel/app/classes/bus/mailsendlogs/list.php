<?php

namespace Bus;

/**
 * get list mail's log
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class MailSendLogs_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id',
        'user_id',
        'nailist_id',
        'page',
        'limit'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id' => array (1, 11),
        'user_id' => array (1, 11),
        'nailist_id' => array (1, 11),
        'title' => array (3, 36),
    );

    /**
     * call function get_list() from model Nail Design
     *
     * @created 2015-03-19
     * @updated 2015-03-19
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Mail_Send_Log::get_list($data);
            return $this->result(\Model_Mail_Send_Log::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
