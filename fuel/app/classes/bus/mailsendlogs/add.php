<?php

namespace Bus;

/**
 * Add info for Mail Send Log
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class MailSendLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'shop_id',
        'nailist_id',
        'type',
        'title',
        'content'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'    => array(1, 11),
        'shop_id'    => array(1, 11),
        'nailist_id' => array(1, 11),
        'type'       => 1,
        'title'      => array(3, 255),
        'to_email'   => array(0, 255),
        'status'     => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'shop_id',
        'nailist_id',
        'type',
        'status'
    );


    /**
     * Call function add() from model Mail Send Log
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return int Id of new log's record or error
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Mail_Send_Log::add($data);
            return $this->result(\Model_Mail_Send_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
