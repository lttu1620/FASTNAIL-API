<?php

namespace Bus;

/**
 * get all Campaigns
 *
 * @package Bus
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Campaigns_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1
    );

    /**
     * call function get_all() from model Campaign
     *
     * @access public
     * @author diennvt
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campaign::get_all($data);
            return $this->result(\Model_Campaign::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}