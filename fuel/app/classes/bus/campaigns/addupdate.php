<?php

namespace Bus;

/**
 * Add or update info for Campaigns
 *
 * @package Bus
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Campaigns_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'name' => array(1, 128),
        'tag_name' => array(1, 512)
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );
    
    /** @var array $_date_format field number */
    protected $_date_format = array(
    	'start_date' => 'Y-m-d',
    	'end_date' => 'Y-m-d',
    );

    /**
     * Call function add_update() from model Campaigns
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campaign::add_update($data);
            return $this->result(\Model_Campaign::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
