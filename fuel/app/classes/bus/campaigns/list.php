<?php

namespace Bus;

/**
 * get list Campaign
 *
 * @package Bus
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Campaigns_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );
    
    /** @var array $_date_format field number */
    protected $_date_format = array(
        'start_date' => 'Y-m-d',
        'end_date' => 'Y-m-d',
    );

    /**
     * call function get_list() from model Campaign
     *
     * @access public
     * @author diennvt
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campaign::get_list($data);
            return $this->result(\Model_Campaign::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
