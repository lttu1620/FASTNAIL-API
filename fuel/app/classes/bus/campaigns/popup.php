<?php

namespace Bus;

/**
 * get list Campaign
 *
 * @package Bus
 * @created 2015-07-06
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class Campaigns_Popup extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
       
    );
    /**
     * call function get_campaign_popup() from model Campaign
     *
     * @access public
     * @author caolp
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campaign::get_popup($data);
            return $this->result(\Model_Campaign::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
