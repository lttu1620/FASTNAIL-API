<?php

namespace Bus;

/**
 * Add info for Nail Rank Log
 *
 * @package Bus
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NailRankLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'admin_id',
        'nail_id',
        'rank'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'admin_id' => array(1, 11),
        'nail_id'  => array(1, 11),
        'rank'     => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'admin_id',
        'nail_id',
        'rank'
    );

    /**
     * Call function add() from model Nail Rank Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Rank_Log::add($data);
            return $this->result(\Model_Nail_Rank_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
