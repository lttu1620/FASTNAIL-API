<?php

namespace Bus;

/**
 * Get list Nail Rank Log (using array count)
 *
 * @package Bus
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NailRankLogs_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id'  => array(1, 11),
        'photo_cd' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'nail_id',
        'photo_cd'
    );

    /** @var array $_date_format date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Nail Rank Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Rank_Log::get_list($data);
            return $this->result(\Model_Nail_Rank_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
