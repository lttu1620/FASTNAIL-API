<?php

namespace Bus;

/**
 * Add and update info for Order Timely Limit
 *
 * @package Bus
 * @created 2015-05-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderTimelyLimits_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'  => array(1, 11),
        'limit'    => array(1, 11),
        'hp_limit' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id',
        'limit',
        'hp_limit'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'time' => 'Y-m-d H:i:s'
    );

    /**
     * Call function add_update() from model Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Timely_Limit::add_update($data);
            return $this->result(\Model_Order_Timely_Limit::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
