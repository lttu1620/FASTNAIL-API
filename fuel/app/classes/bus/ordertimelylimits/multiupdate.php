<?php

namespace Bus;

/**
 * Multi update info for Order Timely Limit
 *
 * @package Bus
 * @created 2015-05-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderTimelyLimits_MultiUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'shop_id',
        'data_json'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'  => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /**
     * Call function multi_update() from model Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Timely_Limit::multi_update($data);
            return $this->result(\Model_Order_Timely_Limit::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
