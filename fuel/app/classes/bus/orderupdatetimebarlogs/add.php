<?php

namespace Bus;

/**
 * Add info for Order Update Timebar Logs
 *
 * @package Bus
 * @created 2015-08-24
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class OrderUpdateTimebarLogs_Add extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'admin_id' => array(1, 11),
        'order_id'     => array(1, 11),
        'before_seat_id'     => array(1, 11),
        'after_seat_id'     => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'admin_id',
        'order_id',
        'before_seat_id',
        'after_seat_id',
    );

    /**
     * Call function add() from model Order_Update_Timebar_Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Update_Timebar_Log::add($data);
            return $this->result(\Model_Order_Update_Timebar_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
