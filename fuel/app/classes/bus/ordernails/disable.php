<?php

namespace Bus;

/**
 * Disable/Enable list Order Nail
 *
 * @package Bus
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderNails_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'       => array(1, 11),
        'order_id' => array(1, 11),
        'nail_id'  => array(1, 11),
        'disable'  => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'nail_id',
        'disable'
    );

    /**
     * Call function disable() from model Order Nail
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Nail::disable($data);
            return $this->result(\Model_Order_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
