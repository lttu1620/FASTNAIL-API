<?php

namespace Bus;

/**
 * <Powders_Detail - API to get detail of Powders>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Powders_Detail extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'id', //stone_id
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11),
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
    );
    /**
     * Call function operateDB() from model Powder
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
            
            $this->_response = \Model_Powder::get_detail($data);
            
            return $this->result(\Model_Powder::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
