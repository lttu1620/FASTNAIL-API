<?php

namespace Bus;

/**
 * Update order when select nail for medicalchart
 *
 * @package Bus
 * @created 2015-05-28
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Fe_Orders_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'login_user_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'login_user_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'login_user_id',        
    );

    /**
     * Call function fe_update() from model Order
     *
     * @author thailh
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::fe_update($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
