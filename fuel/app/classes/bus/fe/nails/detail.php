<?php

namespace Bus;

/**
 * Get list Nails (using array count)
 *
 * @package Bus
 * @created 2015-04-06
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Fe_Nails_Detail extends BusAbstract
{

    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),        
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',        
    );

    /**
     * Call function get_top() from model Nail
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $data['get_relation'] = 1;
            $data['get_tag'] = 1;            
            //$data['get_color_jell'] = 1;
            $data['get_color'] = 1;
            $data['get_design'] = 1;
            $key = 'nails_detail_' . $data['id'];
            $result = \Lib\Cache::get($key);
            if ($result === false) {  
                $result = \Model_Nail::get_detail($data);
                if ($result && empty(\Model_Nail::error())) {
                    $this->_response = $result;
                    \Lib\Cache::set($key, $result);
                }                     
            }
            $data['nail_id'] = array_merge(array($data['id']), \Lib\Arr::field($result['relations'], 'id'));
            $is_favorite = \Model_Nail::is_favorite($data);    
            if (!empty($is_favorite)) {
                $result['is_favorite'] = (string) (in_array($data['id'], $is_favorite) ? 1 : 0);
                if (!empty($result['relations'])) {
                    foreach ($result['relations'] as &$nail) {
                        $nail['is_favorite'] = (string) (in_array($nail['id'], $is_favorite) ? 1 : 0);
                    }
                    unset($nail);
                }
            }
            $this->_response = $result;
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
    
}
