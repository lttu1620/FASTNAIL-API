<?php

namespace Bus;

/**
 * <Materials_AddUpdate - API to get list of Materials>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Materials_AddUpdate extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'name',
        'material_section',
        'material_cd'
    );
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11),
        'material_section',array(0, 11),
        'material_cd',array(0, 11),
        'name' => array(0,128)
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'material_section',
        'material_cd',
        'disable',
    );
    /**
     * Call function operateDB() from model Material
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_Material::add_update($data);
            return $this->result(\Model_Material::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
