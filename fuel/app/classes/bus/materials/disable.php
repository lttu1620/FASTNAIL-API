<?php

namespace Bus;

/**
 * <Materials_Disable - API to disable or enable a Materials>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Materials_Disable extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'id',
        'disable',
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'disable' => 1,
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'disable',
    );
    /**
     * Call function operateDB() 
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Material::disable($data);
            return $this->result(\Model_Material::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
