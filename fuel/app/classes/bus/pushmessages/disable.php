<?php

namespace Bus;

/**
 * Enable/Disable push messages
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PushMessages_Disable extends BusAbstract
{
    protected $_required = array(
        'id'
    );
    //check length
    protected $_length = array(
        'disable' => 1
    );
    //check number
    protected $_number_format = array(
        'disable'
    );
    /**
     * call function disable() from model Push Messages
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Push_Message::disable($data);
            return $this->result(\Model_Push_Message::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
