<?php

namespace Bus;

/**
 * get detail push messages
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PushMessages_GetListPushMessage extends BusAbstract {

    /**
     * get detail push messages by id
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Push_Message::get_list_push_message_for_task();
            return $this->result(\Model_Push_Message::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
