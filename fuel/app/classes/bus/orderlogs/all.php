<?php

namespace Bus;

/**
 * get list order_log
 *
 * @package Bus
 * @created 2015-03-26
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class OrderLogs_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'nailist_id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array (1, 11),
        'nailist_id' => array (1, 11),
        'disable' => 1
    );

    /**
     * call function get_all() from model Order Log
     *
     * @access public
     * @author diennvt
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Log::get_all($data);
            return $this->result(\Model_Order_Log::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
