<?php

namespace Bus;

/**
 * Update all orderlogs
 *
 * @package Bus
 * @created 2015-24-04
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class OrderLogs_AllForUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id',
        'shop_id'
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'shop_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array (1, 11),
        'shop_id' => array (1, 11)
    );

    /**
     * Get all nailists by order.
     * 
     * @param array $param Input data.
     * @author truongnn
     * @return array List nailists
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Log::get_all_nailist_by_order($data);
            return $this->result(\Model_Order_Log::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
