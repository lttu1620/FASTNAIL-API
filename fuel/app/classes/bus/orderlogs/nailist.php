<?php

namespace Bus;

/**
 * Get detail Nailist
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orderlogs_Nailist extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id',
       
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11),

    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
       
    );

    /**
     * Call function get_detail() from model Nailist
     *
     * @author Vulth
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Log::get_nailist($data);
            return $this->result(\Model_Order_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
