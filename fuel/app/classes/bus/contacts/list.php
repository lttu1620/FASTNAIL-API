<?php

namespace Bus;

/**
 * Get list Contact (using array count)
 *
 * @package Bus
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Contacts_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'   => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'user_id',
    );

    /**
     * Call function get_list() from model Contact
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Contact::get_list($data);
            return $this->result(\Model_Contact::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
