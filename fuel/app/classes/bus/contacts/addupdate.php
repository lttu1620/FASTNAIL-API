<?php

namespace Bus;

/**
 * Add and update info for Contact
 *
 * @package Bus
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Contacts_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'      => array(1, 11),
        'user_id' => array(1, 11),
        'name'    => array(1, 40),
        'email'   => array(1, 255),
        'tel'     => array(1, 40),
        'subject' => array(1, 50),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id',
    );

    /**
     * Call function add_update() from model Contact
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Contact::add_update($data);
            return $this->result(\Model_Contact::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
