<?php

namespace Bus;

/**
 * <UserSelectedNailLogs_AddUpdate - API to get list of UserSelectedNailLogs>
 *
 * @package Bus
 * @created 2015-05-28
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserSelectedNailLogs_Add extends BusAbstract {

    /** @var array $_required Define check required */
    protected $_required = array(
        'login_user_id',
        'nail_id',
        'unselected_nail_id'
    );

    /** @var array $_length Define check length */
    protected $_length = array(
        'login_user_id' => array(1, 11),
        'nail_id' => array(1, 11),
        'unselected_nail_id' => array(1, 11)
    );

    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'login_user_id',
        'nail_id',
        'unselected_nail_id'
    );

    /**
     * Call function operateDB() from model UserSelectedNailLog
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data) {
        try {
            if (!empty($data['login_user_id'])) {
                $data['user_id'] = $data['login_user_id'];
            }
            $this->_response = \Model_User_Selected_Nail_Log::add($data);
            return $this->result(\Model_User_Selected_Nail_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
