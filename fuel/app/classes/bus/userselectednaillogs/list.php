<?php

namespace Bus;

/**
 * <UserSelectedNailLogs_List - API to get list of UserSelectedNailLogs>
 *
 * @package Bus
 * @created 2015-05-28
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserSelectedNailLogs_List extends BusAbstract
{

    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'user_id',
        'nail_id',
        'unselected_nail_id'
    );
     /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );
    /**
     * Call function operateDB() 
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
   
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Selected_Nail_Log::get_list($data);
            return $this->result(\Model_User_Selected_Nail_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
