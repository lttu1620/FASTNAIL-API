<?php

namespace Bus;

/**
 * Get all Item Point (without array count)
 *
 * @package Bus
 * @created 2015-06-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PointItems_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'item_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'item_id'
    );

    /**
     * Call function get_all() from model Item Point
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Point_Item::get_all($data);
            return $this->result(\Model_Point_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
