<?php

namespace Bus;

/**
 * Add info for Item
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Items_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'     => array(1, 11),
        'item_cd' => array(1, 45),       
        'item_section' => array(1, 45),       
        'name' => array(1, 45),       
        'price' => array(1, 11),       
        'tax_price' => array(1, 11),       
        'is_sale' => 1,       
        'is_discount' => 1,       
        'is_tax' => 1,       
        'priority' => array(1, 11),       
        'is_discount_combi' => 1,       
        'receipt_coupon' => 1,       
        'discount_price' => array(1, 11),             
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',     
        'price',     
        'tax_price',     
        'is_sale',     
        'is_discount',     
        'is_tax',
        'priority',
        'is_discount_combi',
        'receipt_coupon',
        'discount_price'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'is_sale' => '0',
        'is_discount_combi'=> '0',
        'receipt_coupon' => '0'
    );

    /**
     * Call function add_update() from model Item
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Item::add_update($data);
            return $this->result(\Model_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
