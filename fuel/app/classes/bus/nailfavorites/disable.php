<?php

namespace Bus;

/**
 * Disable a Nail
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NailFavorites_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
        'nail_id' => array(1, 11),
        'user_id'     => array(1, 11),
        'disable'     => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'user_id',
        'disable'
    );

    /**
     * Call function disable() from model Nail Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['login_user_id'])) {
                $data['user_id'] = $data['login_user_id'];
            }
            $this->_response = \Model_Nail_Favorite::disable($data);
            return $this->result(\Model_Nail_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
