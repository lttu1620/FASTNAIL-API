<?php

namespace Bus;

/**
 * Get list Devices (using array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Devices_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'   => array(1, 11),        
        'name'   => array(1, 128),
        'device_type' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id',
        'device_type',
        'page',
        'limit',
        'disable'
    );

    /**
     * Call function get_list() from model Device
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Device::get_list($data);
            return $this->result(\Model_Device::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
