<?php

namespace Bus;

/**
 * Get all Devices (without array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Devices_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable'     => 1,
        'shop_id'     => array(1, 11),
        'device_type' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
        'shop_id',
        'device_type'
    );

    /**
     * Call function get_all() from model Devices
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Device::get_all($data);
            return $this->result(\Model_Device::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
