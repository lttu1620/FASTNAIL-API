<?php

namespace Bus;

/**
 * Add info for Survey Log
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class SurveyLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'survey_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'  => array(1, 11),
        'survey_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'survey_id'
    );

    /**
     * Call function add() from model Survey Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Survey_Log::add($data);
            return $this->result(\Model_Survey_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
