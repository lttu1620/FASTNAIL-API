<?php

namespace Bus;

/**
 * get list powder
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailPowders_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'powder_id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'powder_id' => array (1, 11),
        'disable' => 1
    );

    /**
     * call function get_all() from model Nail Powder
     *
     * @created 2015-03-20
     * @updated 2015-03-20
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Powder::get_all($data);
            return $this->result(\Model_Nail_Powder::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
