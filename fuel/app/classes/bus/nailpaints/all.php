<?php

namespace Bus;

/**
 * get list paint
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailPaints_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'paint_id',
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'paint_id' => array (1, 11),
        'disable' => 1
    );

    /**
     * call function get_all() from model Nail Paint
     *
     * @created 2015-03-20
     * @updated 2015-03-20
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Paint::get_all($data);
            return $this->result(\Model_Nail_Paint::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
