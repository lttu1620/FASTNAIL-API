<?php

namespace Bus;

/**
 * <Services_All - Model to operate to Services's functions>
 *
 * @package Bus
 * @created 2015-05-25
 * @updated 2015-05-25
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Services_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author truongnn
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Service::get_all($data);
            return $this->result(\Model_Service::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
