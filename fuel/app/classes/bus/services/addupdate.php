<?php

namespace Bus;

/**
 * <Services_AddUpdate - API to get list of Services>
 *
 * @package Bus
 * @created 2015-05-25
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Services_AddUpdate extends BusAbstract
{
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11),
        'name' => array(0,45),
        'time' => array(0,2),
        'device_type' => 1
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'time',
        'disable',
    );
    /**
     * Call function operateDB() from model Service
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {   
            $this->_response = \Model_Service::add_update($data);
            return $this->result(\Model_Service::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
