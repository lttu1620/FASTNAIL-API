<?php

namespace Bus;

/**
 * <Services_Disable - API to disable or enable a Services>
 *
 * @package Bus
 * @created 2015-05-25
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Services_Disable extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'id',
        'disable',
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'disable' => 1,
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'disable',
    );
    /**
     * Call function operateDB() 
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Service::disable($data);
            return $this->result(\Model_Service::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
