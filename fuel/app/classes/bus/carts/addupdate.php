<?php

namespace Bus;

/**
 * <Carts_AddUpdate - API to get list of Carts>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Carts_AddUpdate extends BusAbstract
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'shop_id',
    );
    /** @var array $_length Define check length */
    protected $_length = array(
        'id' => array(0, 11)
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'disable',
    );
    /**
     * Call function operateDB() from model Cart
     *
     * @author VuLTH
     * @param array $data Input data
     * @return bool Success or otherwise
     */

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_Cart::add_update($data);
            return $this->result(\Model_Cart::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
