<?php

namespace Bus;

/**
 * Add and update info for Nail Stone
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailStones_AddUpdateByNailId extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'nail_id'
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id'         => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id'
    );

    /**
     * Validate stone_id list format: d,d,d,d
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data) 
    {
        //Validate stone_id's format
        $field = 'stone_id';
        if (!empty($data[$field]) 
            && !preg_match ('/^[\d,]+$/', $data[$field])) {//stone_id's format: digital,digital,digital
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
        }
        
        return true;
    }
    
    /**
     * Call function add_update_by_nail_id() from model Nail Stone
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    { 
        try {
            $this->_response = \Model_Nail_Stone::add_update_by_nail_id($data);
            return $this->result(\Model_Nail_Stone::error());
        } catch (\Exception $e) {
            $this->_exception = $e;         
        }
        return false;
    }
}
