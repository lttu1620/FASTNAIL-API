<?php
namespace Bus;
/**
 * run batch
 *
 * @package Bus
 * @created 2015-01-14
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Systems_UpdateShopCounterCode extends BusAbstract {

    /**
     * run batch
     *
     * @created 2015-01-14
     * @updated 2015-01-14
     * @access public
     * @author thailh
     * @param $data
     * @return bool
     */
    public function operateDB($param) {
        try 
        {  
            $data = \DB::query("
                SELECT DATE_FORMAT(FROM_UNIXTIME(reservation_date), '%Y-%m-%d') AS reservation, shop_counter_code, GROUP_CONCAT(id) AS id, COUNT(id) AS cnt
                FROM orders
                WHERE disable = 0 AND shop_counter_code IS NOT NULL
                AND reservation_date >= UNIX_TIMESTAMP(UTC_DATE())
                AND user_id = 0
                GROUP BY DATE_FORMAT(FROM_UNIXTIME(reservation_date), '%Y-%m-%d'), shop_counter_code 
                HAVING cnt > 1
            ")
            ->execute()
            ->as_array();
            if (isset($param['view_only'])) {
                $this->_response = $data;            
                return true;
            }
            $result = array();
            foreach ($data as $item) {
                $ids = explode(',', $item['id']);
                foreach ($ids as $id) {
                    $order = \Model_Order::find($id);
                    if (!empty($order)) {                        
                        $order->set('shop_counter_code', \Model_Shop_Counter_Code::get_code(true));
                        $order->save();
                        $result[$id] = 'OK';
                    }
                }
            }           
            $this->_response = $result;            
            return true;
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
