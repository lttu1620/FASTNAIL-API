<?php

namespace Bus;

/**
 * Add and update info for Nail Shell
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailShells_AddUpdateByShellId extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'shell_id'
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'shell_id'         => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shell_id'
    );
    
    /**
     * Validate nail_id list format: d,d,d,d
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data) 
    {
        //Validate nail_id's format
        $field = 'nail_id';
        if (!empty($data[$field]) 
            && !preg_match ('/^[\d,]+$/', $data[$field])) {//nail_id's format: digital,digital,digital
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
        }
        
        return true;
    }


    /**
     * Call function add_update_by_shell_id() from model Nail Shell
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    { 
        try {
            $this->_response = \Model_Nail_Shell::add_update_by_shell_id($data);
            return $this->result(\Model_Nail_Shell::error());
        } catch (\Exception $e) {
            $this->_exception = $e;         
        }
        return false;
    }
}
