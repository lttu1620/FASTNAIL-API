<?php

namespace Bus;

/**
 * Add and update info for Cart Nailist
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CartNailists_AddUpdateByCartId extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'cart_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'cart_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'cart_id'
    );

    /**
     * Validate nailist_id list format: d,d,d,d
     *
     * @author diennnvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        //Validate nailist_id's format
        $field = 'nailist_id';
        if (!empty($data[$field])
            && !preg_match('/^\d(?:,\d)*$/', $data[$field])
        ) {//nailist_id's format: digital,digital,digital
            $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
            $this->_invalid_parameter = $field;
            return false;
        }
        return true;
    }

    /**
     * Call function add_update_by_cart_id() from model Cart Nailist
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Cart_Nailist::add_update_by_cart_id($data);
            return $this->result(\Model_Cart_Nailist::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
