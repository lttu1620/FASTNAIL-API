<?php

namespace Bus;

/**
 * Get all Cart Nailist (without array count)
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CartNailists_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'cart_id',
        'nailist_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'cart_id'    => array(1, 11),
        'nailist_id' => array(1, 11)
    );

    /**
     * Call function get_all() from model Cart Nailist
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Cart_Nailist::get_all($data);
            return $this->result(\Model_Cart_Nailist::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
