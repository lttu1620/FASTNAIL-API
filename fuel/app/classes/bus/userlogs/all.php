<?php

namespace Bus;

/**
 * <Userlogs_All - Model to operate to Userlogs's functions>
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Userlogs_All extends BusAbstract 
{
    /**
     * Call function operateDB()
     *     
     * @author truongnn
     * @param array $data Input array
     * @return bool True if add/update successfully or otherwise.   
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Log::get_all($data);
            return $this->result(\Model_User_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
