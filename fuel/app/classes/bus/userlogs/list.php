<?php

namespace Bus;

/**
 * <Userlogs_List - API to get list of Userlogs>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Userlogs_List extends BusAbstract
{

     /** @var array $_length Define check length */
    protected $_length = array(
        'user_id' => array(1, 11),
        'nailist_id' => array(1,11)
    );
    
     /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'id',
        'user_id',
        'nailist_id'
    );
    /**
     * Call function operateDB() 
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Log::get_list($data);
            return $this->result(\Model_User_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
