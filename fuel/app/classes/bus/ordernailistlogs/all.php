<?php

namespace Bus;

/**
 * Get all Order Nailist Log (without array count)
 *
 * @package Bus
 * @created 2015-05-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderNailistLogs_all extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'   => array(1, 11),
        'admin_id'   => array(1, 11),
        'nailist_id' => array(1, 11),
        'type'       => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
        'admin_id',
        'nailist_id',
        'type'
    );

    /**
     * Call function get_all() from model Order Nailist Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Nailist_Log::get_all($data);
            return $this->result(\Model_Order_Nailist_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
