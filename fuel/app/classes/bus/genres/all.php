<?php

namespace Bus;

/**
 * <Genres_All - Model to operate to Genres's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Genres_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Genre::get_all($data);
            return $this->result(\Model_Genre::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
