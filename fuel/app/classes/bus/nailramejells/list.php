<?php

namespace Bus;

/**
 * get list rame_jell
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailRameJells_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'rame_jell_id',
        'page',
        'limit'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'rame_jell_id' => array (1, 11),
    );

    /**
     * call function get_list() from model Nail RameJell
     *
     * @created 2015-03-19
     * @updated 2015-03-19
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Rame_Jell::get_list($data);
            return $this->result(\Model_Nail_Rame_Jell::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
