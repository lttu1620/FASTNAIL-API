<?php

namespace Bus;

/**
 * Scan QR
 *
 * @package Bus
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBoughtItems_ScanQR extends BusAbstract
{
    /**
     * Call function scan_qr() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Bought_Item::scan_qr($data);
            return $this->result(\Model_User_Bought_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
