<?php

namespace Bus;

/**
 * Get list User Beauty View Log (using array count)
 *
 * @package Bus
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBoughtItems_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'         => array(1, 11),
        'user_name'       => array(1, 64),
        'point_item_id'   => array(1, 11),
        'point_item_name' => array(1, 256),
        'scanqr'          => 1,
        'completed'       => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'user_id',
        'point_item_id',
        'scanqr',
        'completed',
    );

    /**
     * Call function get_list() from model User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Bought_Item::get_list($data);
            return $this->result(\Model_User_Bought_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
