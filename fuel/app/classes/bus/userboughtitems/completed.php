<?php

namespace Bus;

/**
 * Complete process
 *
 * @package Bus
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBoughtItems_Completed extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 15),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Call function completed() from model User Bought Item
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Bought_Item::completed($data);
            return $this->result(\Model_User_Bought_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
