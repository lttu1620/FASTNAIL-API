<?php

namespace Bus;

/**
 * Add and update info for User Bought Item
 *
 * @package Bus
 * @created 2015-08-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserBoughtItems_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(        
        'login_user_id',
        'point_item_id',
        'quantity',
    );   

    /** @var array $_number_format field number */
    protected $_number_format = array(       
        'point_item_id',        
        'quantity',
    );

    /**
     * Call function add_update() from model User Bought Item
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Bought_Item::add_update($data);
            return $this->result(\Model_User_Bought_Item::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
