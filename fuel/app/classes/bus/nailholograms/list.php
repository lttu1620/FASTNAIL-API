<?php

namespace Bus;

/**
 * get list hologram
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class NailHolograms_List extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'hologram_id',
        'page',
        'limit'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array (1, 11),
        'hologram_id' => array (1, 11),
    );

    /**
     * call function get_list() from model Nail Hologram
     *
     * @created 2015-03-19
     * @updated 2015-03-19
     * @access public
     * @author Tran Xuan Khoa
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail_Hologram::get_list($data);
            return $this->result(\Model_Nail_Hologram::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
