<?php

namespace Bus;

/**
 * get list Bannerrecommends
 *
 * @package Bus
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Bannerrecommends_All extends BusAbstract
{
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1
    );

    /**
     * call function get_all() from model Bannerrecommend
     *
     * @access public
     * @author diennvt
     * @param $data
     * @return array
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Bannerrecommend::get_all($data);
            return $this->result(\Model_Bannerrecommend::error());

        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
