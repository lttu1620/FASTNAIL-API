<?php

namespace Bus;

/**
 * Get list PointGet (using array count)
 *
 * @package Bus
 * @created 2015-08-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PointGets_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'    => array(1, 255),
        'point'   => array(1, 11),
        'disable' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'point',
    );

    /**
     * Call function get_list() from model Point Get
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Point_Get::get_list($data);
            return $this->result(\Model_Point_Get::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
