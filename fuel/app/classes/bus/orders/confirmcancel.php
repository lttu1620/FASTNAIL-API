<?php

namespace Bus;

/**
 * Cancel Order
 *
 * @package Bus
 * @created 2015-08-26
 * @version 1.0
 * @author Caolp
 * @copyright Oceanize INC
 */
class Orders_ConfirmCancel extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'cancel'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'cancel' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'cancel'
    );

    /**
     * Call function cancel() from model Order
     *
     * @author Caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['login_user_id'])) {
                $data['user_id'] = $data['login_user_id'];
            }
            $data['confirm'] = 1;
            $this->_response = \Model_Order::cancel($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
