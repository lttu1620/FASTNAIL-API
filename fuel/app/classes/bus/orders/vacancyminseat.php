<?php

namespace Bus;

/**
 * Find min vacancy
 *
 * @package Bus
 * @created 2015-05-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Orders_VacancyMinSeat extends BusAbstract
{

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'              => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id',
    );
    
    /** @var array $_date_format field date */
    protected $_date_format = array(
    		'order_start_date' => 'Y-m-d H:i',
    		'order_end_date'   => 'Y-m-d H:i',
    );

    /**
     * Call function get_available_vacancy() from model Order
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_min_available_vacancy($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
