<?php

namespace Bus;

/**
 * Get list Order (using array count)
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_customid' => array(1, 11),
        'shop_id'        => array(1, 11),
        'user_id'        => array(1, 11),
        'user_name'      => array(1, 64),
        'address'        => array(0, 255),
        'sex'            => 1,
        'email'          => array(0, 255),
        'phone'          => array(0, 64),
        'disable'        => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'shop_id',
        'user_id',
        'sex'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to'   => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_list($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
