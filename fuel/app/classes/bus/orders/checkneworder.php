<?php

namespace Bus;

/**
 * Check new order for calendar
 *
 * @package Bus
 * @created 2015-04-10
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Orders_CheckNewOrder extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(        
        'shop_id'              => array(1, 11),   
        'admin_id'             => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(        
        'shop_id',        
        'admin_id',
    );

    /**
     * Call function check_neworder() from model Timebar
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {             
        try {
            $this->_response = \Model_Order::check_neworder($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
