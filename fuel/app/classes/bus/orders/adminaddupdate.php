<?php

namespace Bus;

/**
 * Add or update info for Order
 *
 * @package Bus
 * @created 2015-05-27
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class Orders_AdminAddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                   => array(1, 11),
        'shop_id'              => array(1, 11),
        'cart_id'              => array(1, 11),
        'admin_id'             => array(1, 11),
        'user_id'              => array(1, 11),
        'total_price'          => array(0, 11),
        'total_tax_price'      => array(0, 11),
        'user_name'            => array(1, 64),
        'reservation_type'     => array(1, 11),
        'off_nails'            => array(0, 11),
        'nail_length'          => array(0, 11),
        'nail_type'            => array(0, 11),
        'nail_add_length'      => array(0, 11),
        'pay_type'             => array(1, 11),
        'photo_code'           => array(0, 11),
        'sales_code'           => array(0, 11),
        'rating_1'             => array(0, 11),
        'rating_2'             => array(0, 11),
        'rating_3'             => array(0, 11),
        'rating_4'             => array(0, 11),
        'rating_5'             => array(0, 11),
        'reservation_type_old' => array(1, 11),
        'is_mail'              => 1,
        'is_designate'         => 1,
        'is_next_reservation'  => 1,
        'is_next_designate'    => 1,
        'welfare_id'           => array(0, 11),
        'kana'                 => array(0, 64),
        'prefecture_id'        => array(0, 11),
        'address1'             => array(0, 255),
        'address2'             => array(0, 255),
        'sex'                  => 1,
        'email'                 => array(0, 255),
        'phone'                => array(0, 64)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'shop_id',
        'cart_id',
        'admin_id',
        'user_id',
        'total_price',
        'total_tax_price',
        'visit_section',
        'visit_element',
        'reservation_type',
        'off_nails',
        'nail_length',
        'nail_type',
        'nail_add_length',
        'pay_type',
        'photo_code',
        'sales_code',
        'rating_1',
        'rating_2',
        'rating_3',
        'rating_4',
        'rating_5',
        'reservation_type_old',
        'is_mail',
        'is_designate',
        'is_next_reservation',
        'is_next_designate',
        'welfare_id',
        'prefecture_id',
        'sex'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'reservation_date' => 'Y-m-d H:i',
        'start_date'       => 'Y-m-d H:i',
        'order_start_date' => 'Y-m-d H:i',
        'order_end_date'   => 'Y-m-d H:i',
        'sr_start_date'    => 'Y-m-d H:i',
        'sr_end_date'      => 'Y-m-d H:i',
        'birthday'         => 'Y-m-d'
    );

    /**
     * Call function add_update() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::admin_add_update($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
