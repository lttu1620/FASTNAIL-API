<?php

namespace Bus;

/**
 * Undo Stopwatch
 *
 * @package Bus
 * @created 2015-06-22
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_UndoStopwatch extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id',
        'undo'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11),
        'is_paid'  => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'is_paid'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
//         'start_date'         => 'Y-m-d H:i',
//         'consult_start_date' => 'Y-m-d H:i',
//         'consult_end_date'   => 'Y-m-d H:i',
//         'off_start_date'     => 'Y-m-d H:i',
//         'off_end_date'       => 'Y-m-d H:i',
//         'sr_start_date'      => 'Y-m-d H:i',
//         'sr_end_date'        => 'Y-m-d H:i'
    );

    /**
     * Call function undo_stopwatch() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::undo_stopwatch($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
