<?php

namespace Bus;

/**
 * Calendar data
 *
 * @package Bus
 * @created 2015-08-17
 * @updated 2015-08-17
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Orders_DateCalendarData extends BusAbstract 
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'shop_id',       
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'shop_id' => array(0, 11),
        'date' => array(0, 10),    
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'shop_id',            
    );
    /**
     * call function operateDB()
     *     
     * @author thailh
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {     
        try {
            $this->_response = \Model_Order::date_calendar_for_seat($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
