<?php

namespace Bus;

/**
 * <Stones_All - Model to operate to Stones's functions>
 *
 * @package Bus
 * @created 2015-03-30
 * @updated 2015-03-30
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Orders_DateCalendar extends BusAbstract 
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'shop_id',       
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'shop_id' => array(0, 11),
        'date' => array(0, 10),    
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'shop_id',            
    );
    /**
     * call function operateDB()
     *     
     * @author Hoang Gia Thong
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {     
        try {
            $this->_response = \Model_Order::date_calendar($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
