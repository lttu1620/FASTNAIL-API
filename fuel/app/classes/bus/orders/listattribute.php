<?php

namespace Bus;

/**
 * Add or update info for Timebar
 *
 * @package Bus
 * @created 2015-04-03
 * @version 1.0
 * @author Cao Dinh Tuan
 * @copyright Oceanize INC
 */
class Orders_ListAttribute extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id'             => array(1, 11),   
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id',
    );

    /**
     * Call function add_update() from model Timebar
     *
     * @author Cao Dinh Tuan
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {      
        try {
            $this->_response = \Model_Order::get_nail_attributes($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
