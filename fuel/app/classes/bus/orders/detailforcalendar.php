<?php

namespace Bus;

/**
 * Get detail order for calendar
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_DetailForCalendar extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id'
    );

    /**
     * Call function get_detail_for_calendar() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_detail_for_calendar($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
