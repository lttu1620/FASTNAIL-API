<?php

namespace Bus;

/**
 * Get detail to view all informations of order.
 *
 * @package Bus
 * @created 2015-05-05
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Orders_DetailForView extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Get detail to view all informations of order.
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_detailforview($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
