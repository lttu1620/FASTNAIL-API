<?php

namespace Bus;

/**
 * Export order
 *
 * @package Bus
 * @created 2015-09-14
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_Export extends BusAbstract
{
    /**
     * Call function get_export() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_export($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
