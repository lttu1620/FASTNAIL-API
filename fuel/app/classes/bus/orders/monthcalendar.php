<?php

namespace Bus;

/**
 * Get list booking of month
 *
 * @package Bus
 * @created 2015-03-30
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_MonthCalendar extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'date'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d'
    );

    /**
     * Call function get_month_booking() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_month_booking($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
