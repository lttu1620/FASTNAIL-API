<?php

namespace Bus;

/**
 * Get all Order by date
 *
 * @package Bus
 * @created 2015-03-31
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Orders_AllByDate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'date'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'   => array(1, 11),
        'date' => array(8, 10),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'date' => 'Y-m-d'
    );
    /**
     * Call function get_all_by_date() from model Order
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return array string
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::get_all_by_date($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
