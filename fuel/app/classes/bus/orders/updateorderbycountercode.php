<?php

namespace Bus;

/**
 * Update order by counter_code
 *
 * @package Bus
 * @created 2015-05-21
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Orders_UpdateOrderByCounterCode extends BusAbstract {

    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'shop_counter_code'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );

    /**
     * Call function update_order_by_counter_code() from model Order
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Order::update_order_by_counter_code($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
