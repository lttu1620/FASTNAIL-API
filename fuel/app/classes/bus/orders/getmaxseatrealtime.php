<?php

namespace Bus;

/**
 * get max_seat realtime by Date and shop_id
 *
 * @package Bus
 * @created 2015-04-10
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Orders_GetMaxSeatRealTime extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(        
        'shop_id'   => array(1, 11),   
        'date'      => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(        
        'shop_id',        
    );

    /**
     * Call function check_neworder() from model Timebar
     *
     * @author Cao Dinh Tuan
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {             
        try {
            $this->_response = \Model_Order::get_max_seat_realtime($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
