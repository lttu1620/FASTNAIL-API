<?php

namespace Bus;

/**
 * Stopwatch Order
 *
 * @package Bus
 * @created 2015-04-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Orders_Stopwatch extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id'
    );

    /** @var array $_date_format field date */
    /*protected $_date_format = array(
        'order_start_date' => 'Y-m-d H:i',
        'order_end_date'   => 'Y-m-d H:i',
        'sr_start_date'    => 'Y-m-d H:i',
        'sr_end_date'      => 'Y-m-d H:i',
        'start_date'       => 'Y-m-d H:i'
    );*/

    /** @var array $_only_one_field input only 1 param */
    protected $_only_n_parameter = array(
        'number' => 1,
        'data'   => array(
            'off_start_date',
            'off_end_date',
            'sr_start_date',
            'sr_end_date',
            'f_off_start_date',
            'f_off_end_date',
            'f_sr_start_date',
            'f_sr_end_date',
            'start_date',
            'is_paid',
        	'consult_start_date',	
        	'consult_end_date',
            
        )
    );

    /**
     * Validate input only 1 param
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        if ($this->_only_n_parameter['number'] > 0) {
            $numberParam = 0;
            foreach ($this->_only_n_parameter['data'] as $field) {
                if (isset($data[$field]) && $data[$field] != '') {
                    $numberParam = $numberParam + 1;
                }
                if ($numberParam > $this->_only_n_parameter['number']) {
                    $this->_addError(self::ERROR_CODE_ONLY_N_PARAMETER, $this->_only_n_parameter['number']);
                    $this->_invalid_parameter = $field;
                    return false;
                }
            }
            if ($numberParam == $this->_only_n_parameter['number']) {
                return true;
            }
            if ($numberParam < $this->_only_n_parameter['number']) {
                $this->_addError(self::ERROR_CODE_ONLY_N_PARAMETER, $this->_only_n_parameter['number']);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Call function stopwatch() from model Order
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::stopwatch($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
