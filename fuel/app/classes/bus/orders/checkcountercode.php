<?php

namespace Bus;

/**
 * Check valid counter_code
 *
 * @package Bus
 * @created 2015-09-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Orders_CheckCounterCode extends BusAbstract {

    /** @var array $_required field require */
    protected $_required = array(
        'shop_counter_code'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
    );

    /**
     * Call function update_order_by_counter_code() from model Order
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data) {
        try {
            $result = \Model_Order::check_exist_order_by_couter_code($data);
            $this->_response = $result;
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
