<?php

namespace Bus;

/**
 * Add or update info for Order for calendar
 *
 * @package Bus
 * @created 2015-06-02
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Orders_AddUpdateCalendar extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                   => array(1, 11),
        'shop_id'              => array(1, 11),
        'admin_id'             => array(1, 11),
        'user_id'              => array(1, 11),
        'user_name'            => array(1, 64),       
        'reservation_type'     => array(1, 11),       
        'sex'                  => 1,
        'email'                 => array(0, 255),
        'phone'                => array(0, 64)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'shop_id',
        'cart_id',
        'admin_id',
        'user_id',
        'visit_section',
        'visit_element',
        'reservation_type',
        'sex'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'reservation_date' => 'Y-m-d H:i',
        'order_start_date' => 'Y-m-d H:i',
        'order_end_date'   => 'Y-m-d H:i',
    );

    /**
     * Call function add_update() from model Order
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::add_update_calendar($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
