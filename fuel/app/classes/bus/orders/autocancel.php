<?php

namespace Bus;

/**
 * Cancel Order
 *
 * @package Bus
 * @created 2015-05-11
 * @version 1.0
 * @author Cao Dinh Tuan
 * @copyright Oceanize INC
 */
class Orders_AutoCancel extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'autocancel'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'autocancel' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'autocancel'
    );

    /**
     * Call function autocancel() from model Order
     *
     * @author Cao Dinh Tuan
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::autocancel($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
