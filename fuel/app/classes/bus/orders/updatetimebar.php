<?php

namespace Bus;

/**
 * Add or update info for Timebar
 *
 * @package Bus
 * @created 2015-04-03
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Orders_UpdateTimebar extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'                   => array(1, 11),
        'shop_id'              => array(1, 11),   
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'shop_id', 
        'reservation_date', 
        'start_date', 
        'order_start_date', 
        'order_end_date', 
    );

    /**
     * Call function add_update() from model Timebar
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {      
        try {
            $this->_response = \Model_Order::update_timebar($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
