<?php

namespace Bus;

/**
 * Import order from email of HP
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Orders_Import extends BusAbstract
{
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'total_price'          => array(0, 11),
        'user_name'            => array(1, 64),
        'hp_order_code'        => array(0, 12),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'total_price'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'reservation_date' => 'Y-m-d H:i',
        'created'       => 'Y-m-d'
    );
    
    /**
     * Call function get_import() from model Order
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::import($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
