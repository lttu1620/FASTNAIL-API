<?php

namespace Bus;

/**
 * <Orders_Confirm - Model to operate to Stones's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Orders_Confirm extends BusAbstract 
{
    /** @var array $_required Define check required */
    protected $_required = array(
        'cart_id', //cart_id
    );
    
    /** @var array $_length Define check length */
    protected $_length = array(
        'cart_id' => array(0, 11),
        'user_id' => array(0, 4)
    );
    
    /** @var array $_number_format Define check number */
    protected $_number_format = array(
        'cart_id',
        'user_id'
    );
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order::confirm($data);
            return $this->result(\Model_Order::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
