<?php

namespace Bus;

/**
 * Get list Item Point (using array count)
 *
 * @package Bus
 * @created 2015-06-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ItemPoints_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'item_id'   => array(1, 11),
        'item_type' => 1,
        'name'      => array(0, 255),
        'disable'   => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'item_id',
        'item_type'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'start_date' => 'Y-m-d',
        'end_date'   => 'Y-m-d'
    );

    /**
     * Call function get_list() from model Item Point
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Item_Point::get_list($data);
            return $this->result(\Model_Item_Point::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
