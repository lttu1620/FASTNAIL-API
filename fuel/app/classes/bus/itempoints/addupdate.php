<?php

namespace Bus;

/**
 * Add or update info for Item Point
 *
 * @package Bus
 * @created 2015-06-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ItemPoints_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'        => array(1, 11),
        'item_id'   => array(1, 11),
        'item_type' => 1,
        'name'      => array(0, 255),
        'image_url' => array(0, 255),
        'point'     => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'item_id',
        'item_type'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'start_date' => 'Y-m-d',
        'end_date'   => 'Y-m-d'
    );

    /**
     * Call function add_update() from model Nailist
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Item_Point::add_update($data);
            return $this->result(\Model_Item_Point::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
