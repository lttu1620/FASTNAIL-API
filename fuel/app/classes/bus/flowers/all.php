<?php

namespace Bus;

/**
 * <Flowers_All - Model to operate to Flowers's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class Flowers_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Flower::get_all($data);
            return $this->result(\Model_Flower::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
