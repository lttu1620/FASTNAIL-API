<?php

namespace Bus;

/**
 * Get detail User
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_Point extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        
    );

    /**
     * Call function get_detail() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            if (empty($data['id']) && !empty($data['login_user_id'])) {
                $data['id'] = $data['login_user_id'];
            }
            $user = \Model_User::get_detail($data); 
            if (empty(\Model_User::error())) {
                $this->_response = $user->get('point');
            }
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
