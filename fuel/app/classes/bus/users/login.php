<?php

namespace Bus;

/**
 * Login User
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_Login extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
        'password'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'email'    => array(1, 255),
        'password' => array(4, 64)
    );

    /**
     * Validate katakana name
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        //Validate kana's format
        $field = 'email';

        if (!empty($data[$field])) {           
            $pattern = "/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/";
            if (!preg_match($pattern, $data[$field])) {
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }

        return true;
    }
    
    /**
     * Call function get_login() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {            
            if (!empty($data['email']) && !empty($data['password'])) {
                $this->_response = \Model_User::get_login($data);
            }
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
