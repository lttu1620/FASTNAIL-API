<?php

namespace Bus;

/**
 * Add and update info for User
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'        => array(1, 11),
        'code'      => array(1, 64),
        'email'    => array(1, 255),
        'first_name' => array(1, 64),
        'last_name' => array(1, 64),
        'address1' => array(1, 128),
        'sex' => 1,
        'phone' => array(1, 64),
        'address2' => array(1, 128),        
        'password' => array(4, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'sex'
    );

    
    /** @var array $_date_format date */
    protected $_date_format = array(
        'birthday' => 'Y-m-d',
    );

   /**
     * Validate katakana name
     *
     * @author Tran Xuan Khoa
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        //Validate kana's format
        $field = 'email';

        if (!empty($data[$field])) {           
            $pattern = "/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/";
            if (!preg_match($pattern, $data[$field])) {
                $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
                $this->_invalid_parameter = $field;
                return false;
            }
        }

        return true;
    }
    
    /**
     * Call function add_update() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::add_update($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
