<?php

namespace Bus;

/**
 * Add info for Nail
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Nails_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'           => array(1, 11),
        'photo_cd'     => array(1, 11),
        'show_section' => array(1, 11),
        'hf_section'   => array(1, 11),
        'menu_section' => array(1, 11),
        'price'        => array(1, 11),
        'tax_price'    => array(1, 11),
        'time'         => array(1, 4),
        'print'        => array(1, 4),
        'limited'      => array(1, 4),
        'rank'         => array(1, 11),
        'hp_coupon'    => array(1, 4),
        'coupon_memo'  => array(1, 255),
        'is_change'    => array(1, 4),
        'coupon_type'  => 1,
        'attention'    => array(1, 255)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'photo_cd',
        'show_section',
        'hf_section',
        'menu_section',
        'price',
        'tax_price',
        'time',
        'print',
        'limited',
        'rank',
        'hp_coupon',
        'is_change',
        'coupon_type'
    );

    /**
     * Call function add_update() from model Nail
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail::add_update($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
