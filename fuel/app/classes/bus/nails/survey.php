<?php

namespace Bus;

/**
 * Get Nail Survey
 *
 * @package Bus
 * @created 2015-04-17
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class Nails_Survey extends BusAbstract
{
    /**
     * Call function get_survey() from model Nails
     *
     * @author KhoaTX
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail::get_survey($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
