<?php

namespace Bus;

/**
 * Get list Nails (using array count)
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Nails_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id'       => array(1, 11),
        'price'         => array(1, 11),
        'time'          => array(1, 11),
        'rank'          => array(1, 11),
        'show_section'  => array(1, 11),
        'print'         => array(1, 4),
        'limited'       => array(1, 4),
        'hp_coupon'     => array(1, 4),
        'login_user_id' => array(1, 11),
        'disable'       => 1,
        'coupon_type'   => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
        'price',
        'login_user_id',
        'page',
        'limit',
        'disable',
        'coupon_type',
    );

    /**
     * Call function get_list() from model Nail
     *
     * @author Hoang Gia Thong
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Nail::get_list($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }

        return false;
    }
}
