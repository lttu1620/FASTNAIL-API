<?php

namespace Bus;

/**
 * Get nail price
 *
 * @package Bus
 * @created 2015-04-06
 * @version 1.0
 * @author Thai Lai
 * @copyright Oceanize INC
 */
class Nails_Price extends BusAbstract
{   
    /**
     * Get nail price from config
     *
     * @author Thai Lai
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Config::get('nail_pice');
            return true;
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
