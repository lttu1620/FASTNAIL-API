<?php

namespace Bus;

/**
 * Get attribute of nails (using for admin page)
 *
 * @package Bus
 * @created 2015-03-23
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Nails_Attribute extends BusAbstract {

    /** @var array $_required Required fields */
    protected $_required = array(
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',
    );

    /**
     * Call function get_attribute() from model Nail
     *
     * @author diennvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Nail::get_attribute($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
