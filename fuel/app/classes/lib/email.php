<?php
/**
 * Support functions for Email
 *
 * @package Lib
 * @created 2014-11-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
namespace Lib;

use Fuel\Core\Config;

class Email {

    public static function beforeSend() {
        $send = Config::get('send_email', true);
        if ($send == false) {
            return false;
        }
        return true;
    }

    /**
     * Send email to test email (For testing)
     *
     * @author thailh
     * @param string email Email
     * @return string Real email | test email
     */
    public static function to($email) {
        $test_email = Config::get('test_email', '');
        return !empty($test_email) ? $test_email : $email;
    }
    
    /**
     * Send email if forgetting password
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $to = $param['email'];
        $subject = 'パスワード再設定手続きのお知らせ';

        $emailConfig = Config::get('system_email');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject($subject);

        $data['url']= Config::get('fe_url') . "pages/newpassword/{$param['token']}";
        $data['user_name']= $param['name'];
        $body = \View::forge('email/pc/forget_password', $data);
        //$email->body(mb_convert_encoding($body, 'jis'));
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_FORGET_PASSWORD,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }

    /**
     * Send email if forgetting password for mobile only
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmailForMobile($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $to = $param['email'];
        $subject = 'パスワード再設定手続きのお知らせ';

        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject($subject);

        $data['user_name'] = $param['name'];
        $data['token'] = $param['token'];
        $body = \View::forge('email/mobile/forget_password', $data);
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        }
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_FORGET_PASSWORD,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok,
            'shop_id' => '0',
            'nailist_id' => '0'
        ));
        return (boolean) $ok;
    }

     /**
     * Send create user
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendCreateUser($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $subject = 'Fastnail 仮登録';
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject($subject);
        $data['url']= Config::get('fe_url') . "login";
        $data['password'] = $param['password'];
        $body = \View::forge('email/pc/create_user', $data);
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        }
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_ADMIN_CREATE_USER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }

     /**
     * Send create new order.
     *
     * @author truongnn
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendCreateOrder($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        switch (date('D', $param['reservation_date'])) {
            case "Sun":
                $dayOfWeek = '日';
                break;
            case "Mon":
                $dayOfWeek = '月';
                break;
            case "Tue":
                $dayOfWeek = '火';
                break;
            case "Wed":
                $dayOfWeek = '水';
                break;
            case "Thu":
                $dayOfWeek = '木';
                break;
            case "Fri":
                $dayOfWeek = '金';
                break;
            case "Sat":
                $dayOfWeek = '土';
                break;
        }
        $day = date('Y年m月d日', $param['reservation_date']);
        $hour = date('H:i', $param['reservation_date']);
        $param['reservation_date'] = $day.'（'.$dayOfWeek.'）'.$hour;
        $subject = 'ファストネイルご予約ありがとうございます。';
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');  
        $ok = 0;
        try {
            $shop = \Model_shop::find($param['shop_id']);
            $param['shop_name'] = $shop->get('name');
            $param['shop_address'] = $shop->get('address');
            $param['shop_map_url'] = $shop->get('map_url');
            $param['shop_phone'] = $shop->get('phone');
            if (empty($param['service'])) {
                $param['order_service'] = 'メニューなし';
            } else {
                $param['order_service'] = \Config::get('order_service')[$param['service']];
            } 
            $param['nailist_name'] = '';
            if (!empty($param['nailist_id'])) {
                $nailist = \Model_Nailist::find($param['nailist_id']);
                if (!empty($nailist->get('name'))) {
                    $param['nailist_name'] = $nailist->get('name');
                }                
            }
            if (empty($param['nailist_name'])) {
                $param['nailist_name'] = '指名なし';
            }
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            $email->subject($subject);
            $body = \View::forge('email/pc/create_order', $param);
            $email->html_body($body);
            $email->to(self::to($to));
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);            
        }
        \Model_Mail_Send_Log::add(
            array(
                'order_id' => $param['order_id'],
                'user_id' => $param['user_id'],
                'shop_id' => $param['shop_id'],
                'nailist_id' => !empty($param['nailist_id']) ? $param['nailist_id'] : 0,
                'type' => \Model_Mail_Send_Log::TYPE_EMAIL_CREATE_ORDER,
                'title' => $subject,
                'content' => $body,
                'to_email' => $to,
                'status' => $ok
            )
        );
        return (boolean) $ok;
    }

    /**
     * Send register email
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendRegisterEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $subject = '会員登録頂きまして誠に有難うございました。';
    	$to = $param['email'];
    	$email = \Email::forge('jis');
    	$email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
    	$email->subject($subject);
    	$body = \View::forge('email/pc/register', $param);
    	$email->html_body($body);
    	$email->to(self::to($to));
        $ok = 0;
    	try {
    		\LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
    	} catch (\EmailSendingFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	} catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(            
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'order_id' => 0,
            'shop_id' => 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_REGISTER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }

    /**
     * Send register email
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendThanksEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
    	$to = $param['email'];
    	$email = \Email::forge('jis');
    	$email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
    	$email->subject(\Config::get('titleMail_Thanks'));
    	$body = \View::forge('email/pc/thanks',$param);
    	$email->body($body);
    	$email->to(self::to($to));
        $ok = 0;
    	try {
    		\LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
    	} catch (\EmailSendingFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	} catch (\EmailValidationFailedException $e) {
    		\LogLib::warning($e, __METHOD__, $param);
    	}
        \Model_Mail_Send_Log::add(array(
            'order_id'=> $param['id'],
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => !empty($param['shop_id']) ? $param['shop_id'] : 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_THANKS,
            'title' => \Config::get('titleMail_Thanks'),
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }

     /**
     * Send Reminder
     *
     * @author Cao Dinh Tuan
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendReminderEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        if (empty($param['nailists'])) {
            $param['nailists'] = '指名なし';
        }
        if (empty($param['order_service'])) {
            $param['order_service'] = 'メニューなし';
        }   
        if (empty($param['request'])) {
            $param['request'] = 'メニューなし';
        }        
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject(\Config::get('titleMail_Reminder'));
        $body = \View::forge('email/pc/reminders', $param);
        $email->body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        }
        \Model_Mail_Send_Log::add(array(
            'order_id'=> $param['order_id'],
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => !empty($param['shop_id']) ? $param['shop_id'] : 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_REMINDER,
            'title' => \Config::get('titleMail_Reminder'),
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
    
     /**
     * Send when user quit
     *
     * @author Cao Dinh Tuan
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendUserQuitEmail($param) {
        if (self::beforeSend() == false) {
            return true;
        }
        $subject = '退会処理が完了しました';
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject($subject);
        $param['name'] = !empty($param['name']) ? $param['name'] : '';
        $body = \View::forge('email/pc/cancel_user', $param);
        $email->html_body($body);
        $email->to(self::to($to));
        $ok = 0;
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            if ($email->send()) {
                $ok = 1;
            }
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
        }
        \Model_Mail_Send_Log::add(array(
            'user_id' => !empty($param['user_id']) ? $param['user_id'] : 0,
            'shop_id' => 0,
            'nailist_id' => 0,
            'type' => \Model_Mail_Send_Log::TYPE_EMAIL_QUIT_USER,
            'title' => $subject,
            'content' => $body,
            'to_email' => $to,
            'status' => $ok
        ));
        return (boolean) $ok;
    }
    
     /**
     * Send when user quite
     *
     * @author Cao Dinh Tuan
     * @param object $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function resendEmail($param) {
        if (self::beforeSend() == false ) {
            return true;
        }
        if(empty($param->get('to_email'))){
             \LogLib::warning('Email is null or empty', __METHOD__, $param->get('to_email'));
            return false;
        }
        $to = $param->get('to_email');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Fastnail No reply');
        $email->subject( $param->get('title'));
        $body = $param->get('content');
        $email->html_body($body);

        $email->to(self::to($to));
        try {
            \LogLib::info("Resent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }
    
    /**
     * Send test
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendTest($param) {
        if (self::beforeSend() == false ) {
            return true;
        }
        $to = !empty($param['to']) ? $param['to'] : '';
        if (empty($to)) {
            \LogLib::warning('Email is null or empty', __METHOD__, $to);
            return false;
        }
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), '[Test] Fastnail No reply');
        $email->subject('Test at ' . date('Y-m-d H:i'));
        $body  = 'This is message that sent from Fastnail.town.<br/><br/>';       
        $email->html_body($body);
        $email->to(self::to($to));
        try {
            \LogLib::info("Resent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Execute to send email after added contact successfully
     *
     * @author truongnn
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendContactEmail($param) {
        $emailConfig = Config::get('system_email');
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from($emailConfig['noreply'], 'Fastnail No reply');
        $email->subject('【fastnail.town】お問い合わせありがとうございます。');
        $body = \View::forge('email/pc/contact', $param);
        $email->html_body($body);
        $email->to($to);
        $email->bcc($emailConfig['support']);

        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }
    
    /**
     * Send report when bug
     *
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendReportBug($content) {
        if (self::beforeSend() == false ) {
            return true;
        }
        $to = Config::get('report_bug_email', '');
        if (empty($to)) {            
            return true;
        }      
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), '[Report bugs] Fastnail No reply');
        $email->subject('Report bugs at ' . date('Y-m-d H:i'));       
        $email->html_body($content);
        $email->to($to);
        try {
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $content);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $content);
            return false;
        }
    }
    
}
