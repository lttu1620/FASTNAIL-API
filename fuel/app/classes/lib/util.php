<?php

/**
 * class Util - support functions for Util
 *
 * @package Lib
 * @created 2014-11-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
namespace Lib;

use Fuel\Core\Image;
use Fuel\Core\Config;
use Fuel\Core\Input;
use Fuel\Core\Crypt;

class Util {

    /**
     * Method getShortUrl - get short url  
     *  
     * @author thailh
     * @param string $longUrl Long url
     * @return string/bool
     */
    public static function getShortUrl($longUrl = '') {
        $bitlyConfig = Config::get('bitly');
        $postFields = $bitlyConfig['auth'];
        $postFields['longUrl'] = $longUrl;
        $ch = curl_init($bitlyConfig['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, $bitlyConfig['timeout']);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        if ($response['status_code'] == 200 && isset($response['data']['url'])) {
            curl_close($ch);
            return $response['data']['url'];
        } else {
            \LogLib::warning(curl_error($ch), __METHOD__, $response);
            curl_close($ch);
        }
        return false;
    }
    
    /**
     * Method uploadImage - upload image  
     *  
     * @author thailh
     * @param array $selection
     * @return array
     */
    public static function uploadImage($selection = array()) {
        $thumb = Input::param('thumb');
        $thumbConfig = Config::get('thumbs');
        if (!empty($thumb) && !empty($thumbConfig[$thumb])) {
            $thumbSize = $thumbConfig[$thumb];
        }
        try {
            if ($selection) {
                Config::set('upload.selection', $selection);
            }
            $uploadConfig = Config::load('upload', true);
            $uploadConfig['ext_whitelist'] = Config::get('image_ext');
            AppUpload::process($uploadConfig);
            if (!AppUpload::is_valid()) {
                return array('status' => 400, 'error' => AppUpload::get_errors());
            } else {
                AppUpload::save();
                $result = array();
                foreach (AppUpload::get_files() as $file) {
                    $savedAs = explode('.', $file['saved_as']);
                    $fileName = $savedAs[0];
                    if (!empty($thumbSize)) {
                        foreach ($thumbSize as $size) {
                            list($w, $h) = explode('x', $size);
                            if (!empty($w) && !empty($h)) {
                                Image::load($file['saved_to'] . '/' . $fileName . '.' . $file['extension'])
                                    ->crop_resize($w, $h)
                                    ->save($file['saved_to'] . '/' . $fileName . "_{$size}." . $file['extension']);
                            }
                        }
                    }
                    $result[$file['field']] = Config::get('img_url') . date('Y/m/d') . '/' . $fileName . '.' . $file['extension'];
                }
                return array('status' => 200, 'body' => $result);
            }
        } catch (Exception $e) {
            \LogLib::error(sprintf("Upload image Exception\n"
                    . " - Message : %s\n"
                    . " - Code : %s\n"
                    . " - File : %s\n"
                    . " - Line : %d\n"
                    . " - Stack trace : \n"
                    . "%s", $e->getMessage(), $e->getCode(), $e->getFile(), $e->getLine(), $e->getTraceAsString()), __METHOD__, $_FILES);
            return array('status' => 500, 'error' => $e->getTraceAsString());
        }
    }

    /**
     * Method uploadVideo - upload video   
     *  
     * @author thailh
     * @param array $selection
     * @return array
     */
    public static function uploadVideo($selection = array()) {
        try {
            if ($selection) {
                Config::set('upload.selection', $selection);
            }
            $uploadConfig = Config::load('upload', true);
            $uploadConfig['ext_whitelist'] = Config::get('video_ext');
            AppUpload::process($uploadConfig);
            if (!AppUpload::is_valid()) {
                return array('status' => 400, 'error' => AppUpload::get_errors());
            } else {
                AppUpload::save(array('video'));
                $result = array();
                foreach (AppUpload::get_files() as $file) {
                    $savedAs = explode('.', $file['saved_as']);
                    $fileName = $savedAs[0];
                    $result[$file['field']] = date('Y/m/d') . '/' . $fileName . '.' . $file['extension'];
                }
                return array('status' => 200, 'body' => $result);
            }
        } catch (Exception $e) {
            \LogLib::error(sprintf("Upload video Exception\n"
                    . " - Message : %s\n"
                    . " - Code : %s\n"
                    . " - File : %s\n"
                    . " - Line : %d\n"
                    . " - Stack trace : \n"
                    . "%s", $e->getMessage(), $e->getCode(), $e->getFile(), $e->getLine(), $e->getTraceAsString()), __METHOD__, $_FILES);
            return array('status' => 500, 'error' => $e->getTraceAsString());
        }
    }

     /**
     * Method os - get operating system type   
     *  
     * @author thailh
     * @return string Type name of OS
     */
    public static function os() {
        preg_match("/iPhone|Android|iPad|iPod|webOS/", Input::user_agent(), $matches);
        $os = current($matches);
        switch ($os) {
            case 'iPhone':
            case 'iPad':
            case 'iPod':
                $os = Config::get('os')['ios'];
                break;
            case 'Android':
                $os = Config::get('os')['android'];
                break;
            default:
                $os = Config::get('os')['webos'];
                break;
        }
        return $os;
    }

    /**
     * Method deviceId - get device type   
     *  
     * @author thailh
     * @param string $os Name of operating system
     * @return int Type of device 
     */
    public static function deviceId($os) 
    {
        if (isset($os))
        {
            if ($os == \Config::get('os')['ios'])
            {
                return 1; //ios
            }
            elseif ($os == \Config::get('os')['android'])
            {
                return 2; //android
            }
            else
            {
                return 3; // webos
            }
        }
        return 0;
    }
    
    /**
     * Method authUserId - Fetch User-Id from the HTTP request headers   
     *  
     * @author thailh
     * @return array Information of User-Id
     */
    public static function authUserId() 
    {
        return Input::headers('User-Id');
    }

    /**
     * Method authToken - Fetch Authorization from the HTTP request headers   
     *  
     * @author thailh
     * @return array Information of Authorization
     */
    public static function authToken() {
        return Input::headers('Authorization');
    }

    /*
      public static function cropImage() {
      try {
      $param = Input::param();
      if (empty($param['image_url'])) {
      return -1; // array('status' => 200, 'body' => false);
      }
      // Get full image_url and crop image
      $uploadConfig = Config::load('upload', true);
      $imageUrl = base64_decode($param['image_url']);
      $imagePath = str_replace(Config::get('img_url'), $uploadConfig['img_dir'], $imageUrl);
      $x1 = !empty($param['x1']) ? intval($param['x1']) : 0;
      $y1 = !empty($param['y1']) ? intval($param['y1']) : 0;
      $x2 = !empty($param['x2']) ? intval($param['x2']) : 0;
      $y2 = !empty($param['y2']) ? intval($param['y2']) : 0;
      if ($x1 + $y1 + $x2 + $y2 > 0) {
      $imgDir = dirname($imagePath);
      $imgFileName = basename($imagePath);
      $originDir = $imgDir . '/origin';
      if (!file_exists($originDir . '/' . $imgFileName)) {
      @mkdir($originDir, 0777, true);
      Image::load($imagePath)->save($originDir . '/' . $imgFileName);
      }
      Image::load($imagePath)->crop($x1, $y1, $x2, $y2)->save($imagePath);
      }
      return 1;
      } catch (\Exception $e) {
      \LogLib::error(sprintf("Upload image Exception\n"
      . " - Message : %s\n"
      . " - Code : %s\n"
      . " - File : %s\n"
      . " - Line : %d\n"
      . " - Stack trace : \n"
      . "%s", $e->getMessage(), $e->getCode(), $e->getFile(), $e->getLine(), $e->getTraceAsString()), __METHOD__, null);
      return $e->getMessage();
      }
      }
     */

    /**
     * Upload from image url   
     *  
     * @author thailh
     * @param string $sUrl Image url
     * @return array 
     */
    public static function uploadFromImageUrl($sUrl = '') {
        if (empty($sUrl))
            return false;
        $uploadConfig = Config::load('upload', true);
        $sUploadDir = $uploadConfig['path'];
        //Ecore_Function::mkDirectory($sUploadDir);
        $fImage = @file_get_contents($sUrl, FILE_USE_INCLUDE_PATH);
        $aFileInfo = pathinfo($sUrl);
        if (is_array($aFileInfo) && count($aFileInfo) >= 4) {
            $sExt = strtolower(strrchr($aFileInfo["basename"], '.'));
            $sImage = uniqid() . time();
            $sFilename = $sUploadDir . $sImage . $sExt;
            @mkdir($sUploadDir, 0777, true);
            if (file_put_contents($sFilename, $fImage) !== false) {
                $aSize = @getimagesize($sFilename);
                if (is_array($aSize) && count($aSize)) {
                    $img_url = Config::get('img_url');
                    $img_url = str_replace($uploadConfig['img_dir'], $img_url, $sFilename);
                    return array($sFilename, $img_url);
                }
            }
        }
        return array();
    }

    /**
     * Check if url is valid   
     *  
     * @author thailh
     * @param string $url An url
     * @return bool Return true if successful ortherwise return false
     */
    public static function url_exists($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        } 
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($code == 200);
    }

    /**
     * Encode password 
     *  
     * @author thailh
     * @param string $pwd Password
     * @param string $email Email
     * @return string Password after encoding
     */
    public static function encodePassword($pwd, $email = '') {
        return Crypt::encode($email . ':;' . $pwd);
    }

    /**
     * Decode password 
     *  
     * @author thailh
     * @param string $pwd Password
     * @return string Password after decoding
     */
    public static function decodePassword($pwd) {
        $pwd_decode = explode(':;', Crypt::decode($pwd));
        if (count($pwd_decode) == 2) {
            return $pwd_decode[1];
        }
        return $pwd_decode;
    }
    
     /**
     * Check if is mobie
     *  
     * @author thailh
     * @return bool Return true if mobile ortherwise return false
     */
    public static function isMobile() {
        $os = static::os();        
        if ($os == \Config::get('os')['ios'] || $os == \Config::get('os')['android']) {
            return true;
        }
        return false;       
    }   
    
    /**
     * Get time
     *  
     * @author thailh
     * @param string $d string of time
     * @return int A timestamp on success, false otherwise
     */
    public static function gmtime($d) {
        if (empty($d)) 
        {
            return false;
        }
        return strtotime(gmdate("M d Y H:i:s", strtotime($d)));
    }    

}
