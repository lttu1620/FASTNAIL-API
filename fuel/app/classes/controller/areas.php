<?php

/**
 * Controller_Areas - Controller for actions on Areas
 *
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_Areas extends \Controller_App {

    /**
     *  Get list areas by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\Areas_List::getInstance()->execute();
    }
    
    /**
     *  Get all areas
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\Areas_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new an area
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Areas_AddUpdate::getInstance()->execute();
    }
    
    /**
     *  Disable list of areas
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Areas_Disable::getInstance()->execute();
    }
    
    /**
     *  Get detail info of an area
     * 
     * @return array Detail information of an area
     */
    public function action_detail() {
        return \Bus\Areas_Detail::getInstance()->execute();
    }

}
