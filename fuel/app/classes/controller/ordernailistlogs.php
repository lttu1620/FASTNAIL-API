<?php

/**
 * Controller for actions on Order Nailist Log
 *
 * @package Controller
 * @created 2015-05-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderNailistLogs extends \Controller_App
{
    /**
     * Add info for Order Nailist Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\OrderNailistLogs_Add::getInstance()->execute();
    }

    /**
     * Get all Order Nailist Log (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderNailistLogs_all::getInstance()->execute();
    }
}
