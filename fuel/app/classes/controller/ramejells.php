<?php

/**
 * Controller for actions on RameJells
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_RameJells extends \Controller_App
{
    /**
     * Add and update info for RameJells
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\RameJells_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list RameJells (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\RameJells_List::getInstance()->execute();
    }

    /**
     * Get all RameJells (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\RameJells_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list RameJells
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\RameJells_Disable::getInstance()->execute();
    }

    /**
     * Get detail RameJells
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\RameJells_Detail::getInstance()->execute();
    }    
}
