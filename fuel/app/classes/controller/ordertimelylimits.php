<?php

/**
 * Controller for actions on Order Timely Limit
 *
 * @package Controller
 * @created 2015-05-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderTimelyLimits extends \Controller_App
{
    /**
     * Add and update info for Order Timely Limit
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\OrderTimelyLimits_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Order Timely Limit
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\OrderTimelyLimits_List::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order Timely Limit
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\OrderTimelyLimits_Disable::getInstance()->execute();
    }

    /**
     * Decrease field limit for Order Timely Limit
     *
     * @return boolean
     */
    public function action_decreaseLimit()
    {
        return \Bus\OrderTimelyLimits_DecreaseLimit::getInstance()->execute();
    }
    
    /**
     * Increase field limit for Order Timely Limit
     *
     * @return boolean
     */
    public function action_increaseLimit()
    {
    	return \Bus\OrderTimelyLimits_IncreaseLimit::getInstance()->execute();
    }

    /**
     * Multi update info for Order Timely Limit
     *
     * @return boolean
     */
    public function action_MultiUpdate()
    {
        return \Bus\OrderTimelyLimits_MultiUpdate::getInstance()->execute();
    }
}
