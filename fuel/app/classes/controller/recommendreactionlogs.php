<?php

/**
 * Controller for actions on Recommend Reaction Log
 *
 * @package Controller
 * @created 2015-05-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_RecommendReactionLogs extends \Controller_App
{
    /**
     * Add info for Recommend Reaction Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\RecommendReactionLogs_Add::getInstance()->execute();
    }

    /**
     * Get list Recommend Reaction Log (with array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\RecommendReactionLogs_List::getInstance()->execute();
    }
}
