<?php

/**
 * Controller_Stones - Controller for actions on Stones
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Stones extends \Controller_App {

    /**
     *  Get list stone by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Stones_List::getInstance()->execute();
    }

    /**
     *  Get detail of stone
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Stones_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for stone
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Stones_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new stone
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Stones_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all stone
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Stones_All::getInstance()->execute();
    }
}
