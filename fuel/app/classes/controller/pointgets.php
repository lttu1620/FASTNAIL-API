<?php

/**
 * Controller for actions on Point Get
 *
 * @package Controller
 * @created 2015-08-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_PointGets extends \Controller_App
{
    /**
     * Add or update info for Point Get
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\PointGets_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Point Get (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\PointGets_List::getInstance()->execute();
    }

    /**
     * Get all Point Get (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\PointGets_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Point Get
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\PointGets_Disable::getInstance()->execute();
    }

    /**
     * Get detail Point Get
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\PointGets_Detail::getInstance()->execute();
    }
}
