<?php

/**
 * Controller_Scenes - Controller for actions on Scenes
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Scenes extends \Controller_App {

    /**
     *  Get list scene by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Scenes_List::getInstance()->execute();
    }

    /**
     *  Get detail of scene
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Scenes_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for scene
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Scenes_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new scene
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Scenes_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all scene
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Scenes_All::getInstance()->execute();
    }
}
