<?php

/**
 * Controller for actions on Shells
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Shells extends \Controller_App
{
    /**
     * Add and update info for User
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Shells_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list Question (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Shells_List::getInstance()->execute();
    }

    /**
     * Get all Question (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Shells_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Question
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Shells_Disable::getInstance()->execute();
    }

    /**
     * Get detail Question
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Shells_Detail::getInstance()->execute();
    }    
}
