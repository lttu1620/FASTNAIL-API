<?php

/**
 * Controller_Designs - Controller for actions on Designs
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Designs extends \Controller_App {

    /**
     *  Get list design by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Designs_List::getInstance()->execute();
    }

    /**
     *  Get detail of design
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Designs_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for design
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Designs_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new design
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Designs_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all design
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Designs_All::getInstance()->execute();
    }
}
