<?php

/**
 * Controller for actions on Order Service
 *
 * @package Controller
 * @created 2015-05-25
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class Controller_OrderServices extends \Controller_App
{
    /**
     * Add info for Order Service
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\OrderServices_Add::getInstance()->execute();
    }

    /**
     * Get all Order Service (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderServices_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order Service
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\OrderServices_Disable::getInstance()->execute();
    }
}
