<?php

/**
 * Controller for actions on Search Nails
 *
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Fe_Nails extends \Controller_App
{  
    /**
     * Search Nails (using for admin page)
     *
     * @return boolean
     */
    public function action_search()
    {      
        return \Bus\Fe_Nails_Search::getInstance()->execute();
    }

    /**
     * Get top Nails (using array count)
     *
     * @return boolean
     */
    public function action_top()
    {
        return \Bus\Fe_Nails_Top::getInstance()->execute();
    }
    
    /**
     * Get nail detail
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Fe_Nails_Detail::getInstance()->execute();
    }
    
    /**
     * Get nail detail
     *
     * @return boolean
     */
    public function action_attribute()
    {
        return \Bus\Fe_Nails_Attribute::getInstance()->execute();
    }
}
