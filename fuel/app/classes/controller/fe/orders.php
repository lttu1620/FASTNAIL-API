<?php

/**
 * Controller for actions on Order
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Fe_Orders extends \Controller_App
{
    /**
     * Get list info for Order
     *
     * @return boolean
     */
    public function action_confirm()
    {
        return \Bus\Fe_Orders_Confirm::getInstance()->execute();
    }
    
    /**
     * Update order when select nail for medicalchart
     *
     * @return boolean
     */
    public function action_update()
    {
        return \Bus\Fe_Orders_Update::getInstance()->execute();
    }
    
}
