<?php

/**
 * Controller for actions on Contact
 *
 * @package Controller
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Contacts extends \Controller_App
{
    /**
     * Add and update info for Contact
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Contacts_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Contact (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Contacts_List::getInstance()->execute();
    }

    /**
     * Get detail Contact
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Contacts_Detail::getInstance()->execute();
    }
}