<?php

/**
 * Controller for actions on Nail Rank Log
 *
 * @package Controller
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NailRankLogs extends \Controller_App
{
    /**
     * Add info for Nail Rank Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\NailRankLogs_Add::getInstance()->execute();
    }

    /**
     * Get list Nail Rank Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\NailRankLogs_List::getInstance()->execute();
    }
}
