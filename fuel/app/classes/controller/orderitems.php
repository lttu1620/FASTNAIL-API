<?php

/**
 * Controller for actions on Order Item
 *
 * @package Controller
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderItems extends \Controller_App
{
    /**
     * Add info for Order Item
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\OrderItems_Add::getInstance()->execute();
    }

    /**
     * Get all Order Item (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderItems_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order Item
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\OrderItems_Disable::getInstance()->execute();
    }
}
