<?php

/**
 * Controller_Campaigns - Controller for actions on Campaigns
 *
 * @package Controller
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Campaigns extends \Controller_App {

    /**
     *  Get list Campaigns by condition
     * 
     * @return array 
     */
    public function action_list() { 
        return \Bus\Campaigns_List::getInstance()->execute();
    }
    
    /**
     *  Get all Campaigns
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\Campaigns_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new an Campaign
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Campaigns_AddUpdate::getInstance()->execute();
    }
    
    /**
     *  Disable list of Campaigns
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Campaigns_Disable::getInstance()->execute();
    }
    
    /**
     *  Get detail info of an Campaign
     * 
     * @return array Detail information of an Campaign
     */
    public function action_detail() {
        return \Bus\Campaigns_Detail::getInstance()->execute();
    }
    /**
     *  Get Campaign Popup
     *
     * @return array Detail information of an Campaign
     */
    public function action_popup() {
        return \Bus\Campaigns_Popup::getInstance()->execute();
    }
}
