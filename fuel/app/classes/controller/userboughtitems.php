<?php

/**
 * Controller for actions on User Bought Item
 *
 * @package Controller
 * @created 2015-08-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserBoughtItems extends \Controller_App
{
    /**
     * Add and update info for User Beauty View Log
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\UserBoughtItems_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list User Beauty View Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\UserBoughtItems_List::getInstance()->execute();
    }

    /**
     * Scan QR
     *
     * @return boolean
     */
    public function action_scanQR($token)
    {   
        $param = Input::param();
        $param['token'] = $token;
        $result = \Model_User_Bought_Item::scan_qr($param);
        $errors = \Model_User_Bought_Item::error();       
        return Response::forge(View::forge('userboughtitems/scanqr', array('result' => $result, 'errors' => $errors)));       
    }

    /**
     * Complete process
     *
     * @return boolean
     */
    public function action_completed()
    {
        return \Bus\UserBoughtItems_Completed::getInstance()->execute();
    }

    /**
     * Confirm Cancel
     *
     * @return boolean
     */
    public function action_confirmcancel()
    {
        return \Bus\UserBoughtItems_ConfirmCancel::getInstance()->execute();
    }

    /**
     * Cancel
     *
     * @return boolean
     */
    public function action_cancel()
    {
        return \Bus\UserBoughtItems_Cancel::getInstance()->execute();
    }

}