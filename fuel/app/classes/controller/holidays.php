<?php

/**
 * Controller for actions on Holiday
 *
 * @package Controller
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Holidays extends \Controller_App
{
    /**
     * Add info for Holiday
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Holidays_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Holiday (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Holidays_List::getInstance()->execute();
    }

    /**
     * Get all Holiday (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Holidays_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Holiday
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Holidays_Disable::getInstance()->execute();
    }

    /**
     * Get detail Holiday
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Holidays_Detail::getInstance()->execute();
    }
}
