<?php

/**
 * Controller for actions on Item Point
 *
 * @package Controller
 * @created 2015-06-23
 * @version 1.0
 * @author Caolp
 * @copyright Oceanize INC
 */
class Controller_Mobile_PointItems extends \Controller_App
{
    /**
     * Get list Item Point (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Mobile_PointItems_List::getInstance()->execute();
    }
    
    /**
     * Get list Item Point (using array count)
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Mobile_PointItems_Detail::getInstance()->execute();
    }

}
