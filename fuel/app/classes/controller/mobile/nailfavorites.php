<?php

/**
 * Controller for actions on Nail Favorite
 *
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Mobile_NailFavorites extends \Controller_App
{   

    /**
     * Get list Nail Favorite (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Mobile_NailFavorites_List::getInstance()->execute();
    }
    
}
