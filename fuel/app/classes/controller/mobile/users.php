<?php

/**
 * Controller for actions on User
 *
 * @package Controller
 * @created 2015-06-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Mobile_Users extends \Controller_App
{
    /**
     * Register User
     *
     * @return boolean
     */
    public function action_register()
    {
        return \Bus\Mobile_Users_Register::getInstance()->execute();
    }

    /**
     * Update info for User
     *
     * @return boolean
     */
    public function action_updateProfile()
    {
        return \Bus\Mobile_Users_UpdateProfile::getInstance()->execute();
    }

    /**
     * Get order list
     *
     * @return boolean
     */
    public function action_orderlist()
    {
        return \Bus\Mobile_Users_OrderList::getInstance()->execute();
    }

    /**
     * Forget password for User
     *
     * @return boolean
     */
    public function action_forgetPassword()
    {
        return \Bus\Mobile_Users_ForgetPassword::getInstance()->execute();
    }

    /**
     * Check token for User
     *
     * @return boolean
     */
    public function action_checkToken()
    {
        return \Bus\Mobile_Users_CheckToken::getInstance()->execute();
    }

    /**
     * Update password for User
     *
     * @return boolean
     */
    public function action_updatePassword()
    {
        return \Bus\Mobile_Users_UpdatePassword::getInstance()->execute();
    }

    /**
     * Register Beauty
     *
     * @return boolean
     */
    public function action_registerBeauty()
    {
        return \Bus\Mobile_Users_RegisterBeauty::getInstance()->execute();
    }
}
