<?php

/**
 * Controller for actions on UserSettings
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author Dien Nguyen
 * @copyright Oceanize INC
 */
class Controller_Mobile_UserSettings extends \Controller_App {

    /**
     *  Get all setting of user by action POST
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Mobile_UserSettings_All::getInstance()->execute();
    }   
    
}
