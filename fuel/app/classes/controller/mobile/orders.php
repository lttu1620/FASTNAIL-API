<?php

/**
 * Controller for actions of Orders for mobile
 *
 * @package Controller
 * @created 2015-06-10
 * @version 1.0
 * @author CaoLP
 * @copyright Oceanize INC
 */
class Controller_Mobile_Orders extends \Controller_App
{
    /**
     * Element Orders (using for mobile)
     *
     * @return boolean
     */
    public function action_element()
    {
        return \Bus\Mobile_Orders_Element::getInstance()->execute();
    }
    
    /**
     *  Get list material by condition
     *
     * @return boolean
     */
    public function action_calendar()
    {
        return \Bus\Mobile_Orders_Calendar::getInstance()->execute();
    }
    
    /**
     * Get list info for Order confrim
     *
     * @return boolean
     */
    public function action_confirm()
    {
        return \Bus\Mobile_Orders_Confirm::getInstance()->execute();
    }
    
}
