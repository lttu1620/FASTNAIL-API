<?php

/**
 * Controller_Powders - Controller for actions on Powders
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Powders extends \Controller_App {

    /**
     *  Get list powder by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Powders_List::getInstance()->execute();
    }

    /**
     *  Get detail of powder
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Powders_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for powder
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Powders_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new powder
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Powders_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all powder
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Powders_All::getInstance()->execute();
    }
}
