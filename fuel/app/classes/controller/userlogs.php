<?php

/**
 * Controller_Userlogs - Controller for actions on Userlogs
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Userlogs extends \Controller_App {

    /**
     *  Get list material by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Userlogs_List::getInstance()->execute();
    }

    /**
     *  Get detail of material
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Userlogs_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for material
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Userlogs_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new material
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Userlogs_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all material
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Userlogs_All::getInstance()->execute();
    }
}
