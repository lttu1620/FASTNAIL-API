<?php

/**
 * Controller_NailFlowers - Controller for actions on Nail Flowers
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailFlowers extends \Controller_App {

    /**
     *  Get list nail's flowers by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailFlowers_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's flowers
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailFlowers_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for flower
     * 
     * @return boolean 
     */
    public function action_addupdatebyflowerid() {
        return \Bus\NailFlowers_AddUpdateByFlowerId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for flower
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailFlowers_AddUpdateByNailId::getInstance()->execute();
    }

}
