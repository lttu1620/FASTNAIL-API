<?php

/**
 * Controller for actions on Nailist
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Nailists extends \Controller_App
{
    /**
     * Add or update info for Nailist
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Nailists_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Nailist (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Nailists_List::getInstance()->execute();
    }

    /**
     * Get all Nailist (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Nailists_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Nailist
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Nailists_Disable::getInstance()->execute();
    }

    /**
     * Get detail Nailist
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Nailists_Detail::getInstance()->execute();
    }
}
