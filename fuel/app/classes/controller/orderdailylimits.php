<?php

/**
 * Controller for actions on Order Daily Limit
 *
 * @package Controller
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderDailyLimits extends \Controller_App
{
    /**
     * Add and update info for Order Daily Limit
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\OrderDailyLimits_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Order Daily Limit
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\OrderDailyLimits_List::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order Daily Limit
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\OrderDailyLimits_Disable::getInstance()->execute();
    }
}
