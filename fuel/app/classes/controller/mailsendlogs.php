<?php

/**
 * Controller_MailSendLogs - Controller for actions on Mail Logs
 *
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_MailSendLogs extends \Controller_App {

    /**
     *  Get list mail's logs by condition
     *
     * @return array
     */
    public function action_list() {
        return \Bus\MailSendLogs_List::getInstance()->execute();
    }

    /**
     *  Update or add new log for an email sent
     *
     * @return boolean
     */
    public function action_add() {
        return \Bus\MailSendLogs_Add::getInstance()->execute();
    }

}
