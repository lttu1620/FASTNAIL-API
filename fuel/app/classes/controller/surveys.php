<?php

/**
 * Controller for actions on Survey
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Surveys extends \Controller_App
{
    /**
     * Add or update info for Survey
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Surveys_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Survey (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Surveys_List::getInstance()->execute();
    }

    /**
     * Get all Survey (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Surveys_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Survey
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Surveys_Disable::getInstance()->execute();
    }

    /**
     * Get detail Survey
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Surveys_Detail::getInstance()->execute();
    }
}
