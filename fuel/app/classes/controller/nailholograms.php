<?php

/**
 * Controller_NailHolograms - Controller for actions on Nail Holograms
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailHolograms extends \Controller_App {

    /**
     *  Get list nail's holograms by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailHolograms_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's holograms
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailHolograms_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for hologram
     * 
     * @return boolean 
     */
    public function action_addupdatebyhologramid() {
        return \Bus\NailHolograms_AddUpdateByHologramId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for hologram
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailHolograms_AddUpdateByNailId::getInstance()->execute();
    }

}
