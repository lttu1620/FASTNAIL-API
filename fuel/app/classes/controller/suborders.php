<?php

/**
 * Controller for actions on SubOrder
 *
 * @package Controller
 * @created 2015-09-15
 * @version 1.0
 * @author DienNVT
 * @copyright Oceanize INC
 */
class Controller_SubOrders extends \Controller_App
{
    /**
     * Add info for SubOrder
     *
     * @return boolean
     */
    public function action_add()
    { 
        return \Bus\SubOrders_Add::getInstance()->execute();
    }

    /**
     * Update info for SubOrder
     *
     * @return boolean
     */
    public function action_update()
    {
        return \Bus\SubOrders_Update::getInstance()->execute();
    }
    
   /**
     * Get all sub order of a order
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\SubOrders_All::getInstance()->execute();
    }
    
    
}
