<?php

/**
 * Controller_NailGenres - Controller for actions on Nail Genres
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailGenres extends \Controller_App {

    /**
     *  Get list nail's genres by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailGenres_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's genres
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailGenres_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for genre
     * 
     * @return boolean 
     */
    public function action_addupdatebygenreid() {
        return \Bus\NailGenres_AddUpdateByGenreId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for genre
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailGenres_AddUpdateByNailId::getInstance()->execute();
    }

}
