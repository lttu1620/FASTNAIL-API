<?php

/**
 * Controller for actions on Holograms
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Holograms extends \Controller_App
{
    /**
     * Add and update info for Holograms
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Holograms_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list Holograms (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Holograms_List::getInstance()->execute();
    }

    /**
     * Get all Holograms (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Holograms_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Holograms
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Holograms_Disable::getInstance()->execute();
    }

    /**
     * Get detail Holograms
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Holograms_Detail::getInstance()->execute();
    }    
}
