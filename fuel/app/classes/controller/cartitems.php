<?php

/**
 * Controller for actions on Cart Item
 *
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_CartItems extends \Controller_App
{
    /**
     * Add info for Cart Item
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\CartItems_Add::getInstance()->execute();
    }

    /**
     * Get all Cart Item (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\CartItems_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Cart Item
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\CartItems_Disable::getInstance()->execute();
    }
}
