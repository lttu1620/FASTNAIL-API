<?php

/**
 * Controller_NailTags - Controller for actions on Nail Tags
 *
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_NailTags extends \Controller_App {

    /**
     *  Get list nail's tags by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailTags_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's tags
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailTags_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for tag
     * 
     * @return boolean 
     */
    public function action_addupdatebytagid() {
        return \Bus\NailTags_AddUpdateByTagId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for tag
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailTags_AddUpdateByNailId::getInstance()->execute();
    }

}
