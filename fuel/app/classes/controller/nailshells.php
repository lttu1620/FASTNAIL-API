<?php

/**
 * Controller_NailShells - Controller for actions on Nail Shells
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailShells extends \Controller_App {

    /**
     *  Get list nail's shells by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailShells_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's shells
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailShells_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for shell
     * 
     * @return boolean 
     */
    public function action_addupdatebyshellid() {
        return \Bus\NailShells_AddUpdateByShellId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for shell
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailShells_AddUpdateByNailId::getInstance()->execute();
    }

}
