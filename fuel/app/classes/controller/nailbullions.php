<?php

/**
 * Controller_NailBullions - Controller for actions on Nail Bullions
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailBullions extends \Controller_App {

    /**
     *  Get list nail's bullions by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailBullions_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's bullions
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailBullions_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for bullion
     * 
     * @return boolean 
     */
    public function action_addupdatebybullionid() {
        return \Bus\NailBullions_AddUpdateByBullionId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for bullion
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailBullions_AddUpdateByNailId::getInstance()->execute();
    }

}
