<?php

/**
 * Controller for actions on Nails
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Nails extends \Controller_App
{
    /**
     * Add and update info for Nails
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Nails_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Nails (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Nails_List::getInstance()->execute();
    }

    /**
     * Get all Nails (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Nails_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Nails
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Nails_Disable::getInstance()->execute();
    }

    /**
     * Get detail Nails
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Nails_Detail::getInstance()->execute();
    }

    /**
     * Get nails's attribute
     *
     * @return boolean
     */
    public function action_attribute()
    {
        return \Bus\Nails_Attribute::getInstance()->execute();
    }

    /**
     * Get nails's relation
     *
     * @return boolean
     */
    public function action_relation()
    {
        return \Bus\Nails_Relation::getInstance()->execute();
    }

    /**
     * Get all Nails (without array count)
     *
     * @return boolean
     */
    public function action_price()
    {
        return \Bus\Nails_Price::getInstance()->execute();
    }

    /**
     * Get nail survey
     *
     * @return boolean
     */
    public function action_survey()
    {
        return \Bus\Nails_Survey::getInstance()->execute();
    }
}
