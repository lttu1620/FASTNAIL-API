<?php

/**
 * Controller for actions on Cart Nailist
 *
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_CartNailists extends \Controller_App
{
    /**
     * Add or update info for Cart Nailist
     *
     * @return boolean
     */
    public function action_addUpdateByCartId()
    {
        return \Bus\CartNailists_AddUpdateByCartId::getInstance()->execute();
    }

    /**
     * Get all Cart Nailist (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\CartNailists_All::getInstance()->execute();
    }

}
