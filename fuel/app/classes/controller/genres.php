<?php

/**
 * Controller_Genres - Controller for actions on Genres
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Genres extends \Controller_App {

    /**
     *  Get list genre by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Genres_List::getInstance()->execute();
    }

    /**
     *  Get detail of genre
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Genres_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for genre
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Genres_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new genre
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Genres_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all genre
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Genres_All::getInstance()->execute();
    }
}
