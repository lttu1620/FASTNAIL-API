<?php

/**
 * Controller_NailColors - Controller for actions on Nail Color
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailColors extends \Controller_App {

    /**
     *  Get list nail's colors by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailColors_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's colors
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailColors_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for color
     * 
     * @return boolean 
     */
    public function action_addupdatebycolorid() {
        return \Bus\NailColors_AddUpdateByColorId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for color
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailColors_AddUpdateByNailId::getInstance()->execute();
    }

}
