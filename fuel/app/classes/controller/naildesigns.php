<?php

/**
 * Controller_NailDesigns - Controller for actions on Nail Designs
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailDesigns extends \Controller_App {

    /**
     *  Get list nail's designs by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailDesigns_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's designs
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailDesigns_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_addupdatebydesignid() {
        return \Bus\NailDesigns_AddUpdateByDesignId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailDesigns_AddUpdateByNailId::getInstance()->execute();
    }

}
