<?php

/**
 * Controller for actions on User Campaign View Log
 *
 * @package Controller
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserCampaignViewLogs extends \Controller_App
{
    /**
     * Add info for User Campaign View Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\UserCampaignViewLogs_Add::getInstance()->execute();
    }

    /**
     * Get list User Campaign View Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\UserCampaignViewLogs_List::getInstance()->execute();
    }
}