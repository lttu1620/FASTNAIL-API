<?php

/**
 * Controller_NailStones - Controller for actions on Stones
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailStones extends \Controller_App {

    /**
     *  Get list nail's stones by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailStones_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's stones
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailStones_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for stone
     * 
     * @return boolean 
     */
    public function action_addupdatebystoneid() {
        return \Bus\NailStones_AddUpdateByStoneId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for stone
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailStones_AddUpdateByNailId::getInstance()->execute();
    }

}
