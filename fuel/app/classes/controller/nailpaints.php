<?php

/**
 * Controller_NailPaints - Controller for actions on Nail Paints
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailPaints extends \Controller_App {

    /**
     *  Get list nail's paints by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailPaints_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's paints
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailPaints_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for paint
     * 
     * @return boolean 
     */
    public function action_addupdatebypaintid() {
        return \Bus\NailPaints_AddUpdateByPaintId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for paint
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailPaints_AddUpdateByNailId::getInstance()->execute();
    }

}
