<?php

/**
 * Controller_NailPowders - Controller for actions on Nail Powders
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailPowders extends \Controller_App {

    /**
     *  Get list nail's powders by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailPowders_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's powders
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailPowders_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for powder
     * 
     * @return boolean 
     */
    public function action_addupdatebypowderid() {
        return \Bus\NailPowders_AddUpdateByPowderId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for powder
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailPowders_AddUpdateByNailId::getInstance()->execute();
    }

}
