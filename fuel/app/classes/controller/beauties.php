<?php

/**
 * Controller_Beauties - Controller for actions on Beauties
 *
 * @package Controller
 * @created 2015-06-16
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Beauties extends \Controller_App {

    /**
     *  Get list Beauty by condition
     *
     * @return boolean
     */
    public function action_list() {
        return \Bus\Beauties_List::getInstance()->execute();
    }

    /**
     *  Get detail of Beauty
     *
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Beauties_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for Beauty
    *
     * @return boolean
     */
    public function action_disable() {
        return \Bus\Beauties_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new Beauty
     *
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Beauties_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all Beauty
     *
     * @return boolean
     */
    public function action_all() {
        return \Bus\Beauties_All::getInstance()->execute();
    }

    /**
     * Get random beauty
     *
     * @return boolean
     */
    public function action_oneRandom()
    {
        return \Bus\Beauties_OneRandom::getInstance()->execute();
    }
    
    /**
     * Write log view
     *
     * @return boolean
     */
    public function action_log()
    {
        return \Bus\Beauties_Log::getInstance()->execute();
    }
    
}
