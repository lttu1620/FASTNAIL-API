<?php

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2014 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  Controller
 * @extends  Controller
 */
use Fuel\Core\Controller;
use Fuel\Core\Response;
use Fuel\Core\View;
use Fuel\Core\ViewModel;
use Facebook\FacebookSession;
use Facebook\FacebookSDKException;

class Controller_Welcome extends \Controller
{

    /**
     * The basic welcome message
     *
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        /*
          include(APPPATH . '/classes/lib/simple_html_dom.php');
          $url = "http://vnexpress.net/rss/oto-xe-may.rss";
          $str = file_get_contents($url);
          $html = str_get_html($str); //var_dump($html); exit;
          $result = array();
          foreach ($html->find('item') as $article) {
          $result[] = array(
          'title' => html_by_tag($article->innertext, '<title>', '</title>'),
          'link' => html_by_tag($article->innertext, '<link>', '</link>'),
          'description' => html_by_tag($article->innertext, '<description>', '</description>'),
          'pubDate' => html_by_tag($article->innertext, '<pubDate>', '</pubDate>'),
          );
          }
          print_r($result); exit;
         * 
         */
        return Response::forge(View::forge('welcome/index'));
    }

    /**
     * A typical "Hello, Bob!" type example.  This uses a Presenter to
     * show how to use them.
     *
     * @access  public
     * @return  Response
     */
    public function action_hello()
    {
        return Response::forge(Presenter::forge('welcome/hello'));
    }

    /**
     * The 404 action for the application.
     *
     * @access  public
     * @return  Response
     */
    public function action_404()
    {
        return Response::forge(Presenter::forge('welcome/404'), 404);
    }

    /**
     * Mission
     *
     * @access  public
     * @return  Response
     */
    public function action_mission() {
        $request = Input::param();
        if (!empty($request) && !empty($request['user_id']) && !empty($request['mission_id'])) {
            \LogLib::info('Mission request', __METHOD__, $request);
            $mission_id = $request['mission_id'];
            $beauty = Model_Beauty::get_detail(array('id' => $mission_id));
            if ($beauty['mission_type'] != \Config::get('beauties.type.answer_survey')//answer_survey == 2
                && $beauty['mission_type'] != \Config::get('beauties.type.register_from_other_site')) { //register_from_other_site ==  4
                \LogLib::info('Invalid mission type', __METHOD__, $beauty['mission_type']);
                return View::forge('welcome/mission');
            }
            $cookie_name = "mission_beauty";
            if (!empty($beauty['detail_page_url'])) {
                \Fuel\Core\Cookie::set($cookie_name, json_encode($request));
                \LogLib::info('Redirect to survey monkey', __METHOD__, $beauty);
                return Response::redirect($beauty['detail_page_url'], 'refresh');
            } else {
                \LogLib::info('No redirect url', __METHOD__, $beauty);
            }
        } else {
            \LogLib::info('No request or missing parameter', __METHOD__, $request);
        }
    }

    /**
     * Thank you
     *
     * @access  public
     * @return  Response
     */
    public function action_thankyou()
    {
        $cookie_name = "mission_beauty";
        $request = Input::param();
        $token = !empty($request['token']) ? $request['token'] : '';        
        $mission = \Fuel\Core\Cookie::get($cookie_name);
        $mission = (array) json_decode($mission);
        if(!empty($mission)) 
        {
            \LogLib::info('Have cookie mission', __METHOD__, $mission);
            $beauty = Model_Beauty::get_detail(array('id' => $mission['mission_id']));
        }
        if (!empty($mission) && ($beauty['token'] === $token))
        {
            // if mission was used then go to thank you page
            $beauty = Model_Beauty::get_detail(array(
                'login_user_id' => $mission['user_id'],
                'id' => $mission['mission_id'],
                'for_mission' => 1
            ));    
            if (!empty($beauty['is_used'])) {
                \LogLib::info('Go to thank you (mission was used)', __METHOD__, $mission);
                \Fuel\Core\Cookie::delete($cookie_name);
                return View::forge('welcome/thankyou');
            }

            // Record log after doing mission then go to thank you page
            $result = Model_User_Beauty_View_Log::add(array(
                'login_user_id' => $mission['user_id'],
                'beauty_id' => $mission['mission_id']
            ));
            \LogLib::info('Result of Model_User_Beauty_View_Log::add = ', __METHOD__, array($result));
            if ($result)
            {
                \LogLib::info('Go to thank you', __METHOD__, $mission);
                \Fuel\Core\Cookie::delete($cookie_name);
                return View::forge('welcome/thankyou');
            } else
            {
                \LogLib::info('Model_User_Beauty_View_Log::add Failed ', __METHOD__, Model_User_Beauty_View_Log::error());
            }
        } else
        {
            \LogLib::info('NO MISSION COOKIE OR VALID TOKEN', __METHOD__, array());
        }
    }

}
