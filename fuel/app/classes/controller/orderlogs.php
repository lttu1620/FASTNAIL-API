<?php

/**
 * Controller_OrderLogs - Controller for actions on Order Log
 *
 * @package Controller
 * @created 2015-03-26
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_OrderLogs extends \Controller_App {

    /**
     *  Get all order's nailist
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\OrderLogs_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_addupdatebyorderid() {
        return \Bus\OrderLogs_AddUpdateByOrderId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_allforupdate() {
        return \Bus\OrderLogs_AllForUpdate::getInstance()->execute();
    }
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_nailist() {
        return \Bus\OrderLogs_Nailist::getInstance()->execute();
    }
}
