<?php

/**
 * Controller for actions on User Beauty View Log
 *
 * @package Controller
 * @created 2015-08-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserBeautyViewLogs extends \Controller_App
{
    /**
     * Add info for User Beauty View Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\UserBeautyViewLogs_Add::getInstance()->execute();
    }

    /**
     * Get list User Beauty View Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\UserBeautyViewLogs_List::getInstance()->execute();
    }

    /**
     * Disable/Enable User Beauty View Log
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\UserBeautyViewLogs_Disable::getInstance()->execute();
    }
    
    /**
     * Get page view User Beauty View Log
     *
     * @return boolean
     */
    public function action_pv()
    {
        return \Bus\UserBeautyViewLogs_Pv::getInstance()->execute();
    }
    
    /**
     * Get unique user from User Beauty View Log
     *
     * @return boolean
     */
    public function action_uu()
    {
        return \Bus\UserBeautyViewLogs_Uu::getInstance()->execute();
    }
    
    /**
     * Get cvr from User Beauty View Log
     *
     * @return boolean
     */
    public function action_cvr()
    {
        return \Bus\UserBeautyViewLogs_Cvr::getInstance()->execute();
    }
}