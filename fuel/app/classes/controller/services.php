<?php

/**
 * Controller_Services - Controller for actions on Services
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Services extends \Controller_App {

    /**
     *  Get list material by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Services_List::getInstance()->execute();
    }

   /**
     *  Update disable field for material
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Services_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new material
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Services_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all material
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Services_All::getInstance()->execute();
    }
    
    /**
     *  Get service's detail
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Services_Detail::getInstance()->execute();
    }
}
