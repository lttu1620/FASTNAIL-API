<?php

/**
 * Controller_UserSelectedNailLogs - Controller for actions on user_selected_nail_log
 *
 * @package Controller
 * @created 2015-03-25
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_UserSelectedNailLogs extends \Controller_App {

    /**
     *  Get list nail's tags by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\UserSelectedNailLogs_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's tags
     * 
     * @return array 
     */
    public function action_add() {
        return \Bus\UserSelectedNailLogs_Add::getInstance()->execute();
    }
}
