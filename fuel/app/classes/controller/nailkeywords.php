<?php

/**
 * Controller_NailKeywords - Controller for actions on Nail Keywords
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailKeywords extends \Controller_App {

    /**
     *  Get list nail's keywords by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailKeywords_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's keywords
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailKeywords_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for keyword
     * 
     * @return boolean 
     */
    public function action_addupdatebykeywordid() {
        return \Bus\NailKeywords_AddUpdateByKeywordId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for keyword
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailKeywords_AddUpdateByNailId::getInstance()->execute();
    }

}
