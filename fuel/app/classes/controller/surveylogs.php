<?php

/**
 * Controller for actions on Survey Log
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_SurveyLogs extends \Controller_App
{
    /**
     * Add info for Survey Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\SurveyLogs_Add::getInstance()->execute();
    }

    /**
     * Get list Survey Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\SurveyLogs_List::getInstance()->execute();
    }
}
