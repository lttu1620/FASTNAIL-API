<?php

/**
 * Controller_Flowers - Controller for actions on Flowers
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Flowers extends \Controller_App {

    /**
     *  Get list flower by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Flowers_List::getInstance()->execute();
    }

    /**
     *  Get detail of flower
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Flowers_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for flower
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Flowers_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new flower
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Flowers_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all flower
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Flowers_All::getInstance()->execute();
    }
}
