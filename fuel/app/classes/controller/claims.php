<?php

/**
 * Controller for actions on Claim
 *
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Claims extends \Controller_App
{
    /**
     * Add or update info for Claim
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Claims_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Claim (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Claims_List::getInstance()->execute();
    }

    /**
     * Get all Claim (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Claims_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Claim
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Claims_Disable::getInstance()->execute();
    }

    /**
     * Get detail Claim
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Claims_Detail::getInstance()->execute();
    }
}
