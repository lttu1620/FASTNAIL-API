<?php

/**
 * Controller for actions on Push Message
 *
 * @package Controller
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_PushMessages extends \Controller_App
{
    /**
     *  Get detail PushMessages
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\PushMessages_Detail::getInstance()->execute();
    }

    /**
     *  Get list PushMessages
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\PushMessages_List::getInstance()->execute();
    }

    /**
     *  Update disable field for PushMessages
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\PushMessages_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new PushMessage
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\PushMessages_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get list PushMessage
     *
     * @return boolean
     */
    public function action_getlistpushmessage()
    {
        return \Bus\PushMessages_GetListPushMessage::getInstance()->execute();
    }

}
