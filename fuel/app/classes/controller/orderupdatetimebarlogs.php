<?php

/**
 * Controller for actions on Order Update Timebar Logs
 *
 * @package Controller
 * @created 2015-08-24
 * @version 1.0
 * @author Diennvt
 * @copyright Oceanize INC
 */
class Controller_OrderUpdateTimebarLogs extends \Controller_App
{
    /**
     * Add info for Order Update Timebar Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\OrderUpdateTimebarLogs_Add::getInstance()->execute();
    }

    /**
     * Get all Order Update Timebar Log (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderUpdateTimebarLogs_All::getInstance()->execute();
    }
}