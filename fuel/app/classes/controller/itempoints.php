<?php

/**
 * Controller for actions on Item Point
 *
 * @package Controller
 * @created 2015-06-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_ItemPoints extends \Controller_App
{
    /**
     * Add info for Item Point
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\ItemPoints_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Item Point (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\ItemPoints_List::getInstance()->execute();
    }

    /**
     * Get all Item Point (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\ItemPoints_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Item Point
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\ItemPoints_Disable::getInstance()->execute();
    }

    /**
     * Get detail Item Point
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\ItemPoints_Detail::getInstance()->execute();
    }
}
