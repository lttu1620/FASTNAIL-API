<?php

/**
 * Controller_ShopGroups - Controller for actions on ShopGroups
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_ShopGroups extends \Controller_App {

    /**
     *  Get list material by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\ShopGroups_List::getInstance()->execute();
    }

    /**
     *  Get detail of material
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\ShopGroups_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for material
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\ShopGroups_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new material
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\ShopGroups_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all material
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\ShopGroups_All::getInstance()->execute();
    }
}
