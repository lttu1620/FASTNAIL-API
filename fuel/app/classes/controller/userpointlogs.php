<?php

/**
 * Controller for actions on User Point Log
 *
 * @package Controller
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserPointLogs extends \Controller_App
{
    /**
     * Add info for User Point Log
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\UserPointLogs_Add::getInstance()->execute();
    }

    /**
     * Get list User Point Log (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\UserPointLogs_List::getInstance()->execute();
    }
    
    /**
     * Get list used point
     *
     * @return boolean
     */
    public function action_used()
    {
        return \Bus\UserPointLogs_Used::getInstance()->execute();
    }
    
    /**
     * Get list saved point
     *
     * @return boolean
     */
    public function action_saved()
    {
        return \Bus\UserPointLogs_Saved::getInstance()->execute();
    }
    
}
