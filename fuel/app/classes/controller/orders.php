<?php

/**
 * Controller for actions on Order
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Orders extends \Controller_App
{
    /**
     * Add info for Order
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\Orders_Add::getInstance()->execute();
    }

    /**
     * Add or update info for Order
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Orders_AddUpdate::getInstance()->execute();
    }
    
    /**
     * Add or update info for Order
     *
     * @return boolean
     */
    public function action_addUpdateCalendar()
    {
    	return \Bus\Orders_AddUpdateCalendar::getInstance()->execute();
    }

    /**
     * Get list Order (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Orders_List::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Orders_Disable::getInstance()->execute();
    }

    /**
     * Get detail Order
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Orders_Detail::getInstance()->execute();
    }

    /**
     *  Get list material by condition
     *
     * @return boolean
     */
    public function action_calendar()
    {
        return \Bus\Orders_Calendar::getInstance()->execute();
    }

    /**
     *  Get list material by condition
     *
     * @return boolean
     */
    public function action_confirm()
    {
        return \Bus\Orders_Confirm::getInstance()->execute();
    }

    /**
     * Get list booking of month
     *
     * @return boolean
     */
    public function action_monthCalendar()
    {
        return \Bus\Orders_MonthCalendar::getInstance()->execute();
    }

    /** Get list booking of calendar
     *
     * @return boolean
    */
    public function action_dateCalendar()
    {
        return \Bus\Orders_DateCalendar::getInstance()->execute();
    }

    /**
     *  Get list material by condition
     *
     * @return boolean
     */
    public function action_allByDate()
    {
        return \Bus\Orders_AllByDate::getInstance()->execute();
    }

    /**
     * Get detail order for calendar
     *
     * @return boolean
     */
    public function action_detailForCalendar()
    {
        return \Bus\Orders_DetailForCalendar::getInstance()->execute();
    }

    /**
     * Get detail order for calendar
     *
     * @return boolean
     */
    public function action_updateTimebar()
    {
        return \Bus\Orders_UpdateTimebar::getInstance()->execute();
    }

    /**
     * Check new order for calendar
     *
     * @return boolean
     */
    public function action_checkNewOrder()
    {
        return \Bus\Orders_CheckNewOrder::getInstance()->execute();
    }

    /**
     * Stopwatch Order
     *
     * @return boolean
     */
    public function action_stopwatch()
    {
        return \Bus\Orders_Stopwatch::getInstance()->execute();
    }

    /**
     * Get detail to view all information of order.
     *
     * @return boolean
     */
    public function action_detailForView()
    {
        return \Bus\Orders_DetailForView::getInstance()->execute();
    }

    /**
     * Confirm Cancel order
     *
     * @return boolean
     */
    public function action_confirmcancel()
    {
        return \Bus\Orders_ConfirmCancel::getInstance()->execute();
    }

    /**
     * Cancel order
     *
     * @return boolean
     */
    public function action_cancel()
    {
        return \Bus\Orders_Cancel::getInstance()->execute();
    }

     /**
     * Add info for Order
     *
     * @return boolean
     */
    public function action_import()
    {
        return \Bus\Orders_Import::getInstance()->execute();
    }

    /**
     * Cancel order
     *
     * @return boolean
     */
    public function action_autocancel()
    {
        return \Bus\Orders_AutoCancel::getInstance()->execute();
    }

    /**
     * get vacancy
     *
     * @return boolean
     */
    public function action_vacancyminseat()
    {
    	return \Bus\Orders_VacancyMinSeat::getInstance()->execute();
    }

     /**
     * get available vacancy
     *
     * @return boolean
     */
    public function action_availablevacancy()
    {
    	return \Bus\Orders_AvailableVacancy::getInstance()->execute();
    }

     /**
     * Update order by counter_code
     *
     * @return boolean
     */
    public function action_updateorderbycountercode()
    {
    	return \Bus\Orders_UpdateOrderByCounterCode::getInstance()->execute();
    }

      /**
     * Update order by counter_code
     *
     * @return boolean
     */
    public function action_getmaxseatrealtime()
    {
        return \Bus\Orders_GetMaxSeatRealTime::getInstance()->execute();
    }

     /**
     * Update user info
     *
     * @return boolean
     */
    public function action_adminaddupdate()
    {
        return \Bus\Orders_AdminAddUpdate::getInstance()->execute();
    }

    /**
     * Undo Stopwatch
     *
     * @return boolean
     */
    public function action_undoStopwatch()
    {
        return \Bus\Orders_UndoStopwatch::getInstance()->execute();
    }
    /**
     * get Nail attribute
     *
     * @return boolean
     */
    public function action_listAttribute()
    {
        return \Bus\Orders_ListAttribute::getInstance()->execute();
    }
    
    /**
     * Update nail attribute
     *
     * @return boolean
     */
    public function action_updateAttribute()
    {
        return \Bus\Orders_UpdateAttribute::getInstance()->execute();
    }
    
    
    /**
     * Get calendar data nail attribute
     *
     * @return boolean
     */
    public function action_datecalendardata()
    {
        return \Bus\Orders_DateCalendarData::getInstance()->execute();
    }
    
    /**
     * Check valid counter code
     *
     * @return boolean
     */
    public function action_checkcountercode()
    {
        return \Bus\Orders_CheckCounterCode::getInstance()->execute();
    }

    /**
     * Export order
     *
     * @return boolean
     */
    public function action_export()
    {
        return \Bus\Orders_Export::getInstance()->execute();
    }
}
