<?php

/**
 * Controller_NailScenes - Controller for actions on Nail Scenes
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailScenes extends \Controller_App {

    /**
     *  Get list nail's scenes by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailScenes_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's scenes
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailScenes_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for scene
     * 
     * @return boolean 
     */
    public function action_addupdatebysceneid() {
        return \Bus\NailScenes_AddUpdateBySceneId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for scene
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailScenes_AddUpdateByNailId::getInstance()->execute();
    }

}
