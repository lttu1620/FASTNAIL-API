<?php

/**
 * Controller for actions on Bullions
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Bullions extends \Controller_App
{
    /**
     * Add and update info for Bullions
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Bullions_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list Bullions (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Bullions_List::getInstance()->execute();
    }

    /**
     * Get all Bullions (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Bullions_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Bullions
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Bullions_Disable::getInstance()->execute();
    }

    /**
     * Get detail Bullions
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Bullions_Detail::getInstance()->execute();
    }    
}
