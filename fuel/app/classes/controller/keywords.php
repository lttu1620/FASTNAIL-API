<?php

/**
 * Controller_Keywords - Controller for actions on Keywords
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Keywords extends \Controller_App {

    /**
     *  Get list keyword by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Keywords_List::getInstance()->execute();
    }

    /**
     *  Get detail of keyword
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Keywords_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for keyword
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Keywords_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new keyword
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Keywords_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all keyword
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Keywords_All::getInstance()->execute();
    }
}
