<?php

/**
 * Controller_Materials - Controller for actions on Materials
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Materials extends \Controller_App {

    /**
     *  Get list material by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Materials_List::getInstance()->execute();
    }

    /**
     *  Get detail of material
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Materials_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for material
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Materials_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new material
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Materials_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all material
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Materials_All::getInstance()->execute();
    }
}
