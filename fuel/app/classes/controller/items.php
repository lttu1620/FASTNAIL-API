<?php

/**
 * Controller for actions on Items
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Items extends \Controller_App
{
    /**
     * Add and update info for Items
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Items_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list Items (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Items_List::getInstance()->execute();
    }

    /**
     * Get all Items (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Items_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Items
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Items_Disable::getInstance()->execute();
    }

    /**
     * Get detail Items
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Items_Detail::getInstance()->execute();
    }    
}
