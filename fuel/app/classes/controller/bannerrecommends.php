<?php

/**
 * Controller_Bannerrecommends - Controller for actions on Bannerrecommends
 *
 * @package Controller
 * @created 2015-07-06
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Bannerrecommends extends \Controller_App {

    /**
     *  Get list Bannerrecommends by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\Bannerrecommends_List::getInstance()->execute();
    }
    
    /**
     *  Get all Bannerrecommends
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\Bannerrecommends_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new an Bannerrecommend
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Bannerrecommends_AddUpdate::getInstance()->execute();
    }
    
    /**
     *  Disable list of Bannerrecommends
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Bannerrecommends_Disable::getInstance()->execute();
    }
    
    /**
     *  Get detail info of an Bannerrecommend
     * 
     * @return array Detail information of an Bannerrecommend
     */
    public function action_detail() {
        return \Bus\Bannerrecommends_Detail::getInstance()->execute();
    }

}
