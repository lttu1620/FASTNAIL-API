<?php

/**
 * Controller for actions on Order Device
 *
 * @package Controller
 * @created 2015-04-14
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderDevices extends \Controller_App
{
    /**
     * Add info for Order Device
     *
     * @return boolean
     */
    public function action_addUpdateByOrderId()
    {
        return \Bus\OrderDevices_AddUpdateByOrderId::getInstance()->execute();
    }

    /**
     * Get all Order Device (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderDevices_All::getInstance()->execute();
    }
}
