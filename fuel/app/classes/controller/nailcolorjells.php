<?php

/**
 * Controller_NailColorJells - Controller for actions on Nail ColorJells
 *
 * @package Controller
 * @created 2015-03-19
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailColorJells extends \Controller_App {

    /**
     *  Get list nail's designs by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailColorJells_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's designs
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailColorJells_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_addupdatebycolorjellid() {
        return \Bus\NailColorJells_AddUpdateByColorJellId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for design
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailColorJells_AddUpdateByNailId::getInstance()->execute();
    }

}
