<?php

/**
 * Controller_Colors - Controller for actions on Colors
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Colors extends \Controller_App {

    /**
     *  Get list material by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Colors_List::getInstance()->execute();
    }

    /**
     *  Get detail of material
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Colors_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for material
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Colors_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new material
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Colors_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all material
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Colors_All::getInstance()->execute();
    }
}
