<?php

/**
 * Controller for actions on Report
 *
 * @package Controller
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Reports extends \Controller_App
{
    /**
     * Get top nail best-selling (using array count)
     *
     * @return boolean
     */
    public function action_topOrder()
    {
        return \Bus\Reports_TopOrder::getInstance()->execute();
    }
    /**
     *  Get detail of material
     *
     * @author Vulth
     * @return boolean 
     */
    public function action_revenue() {
        return \Bus\Reports_Revenue::getInstance()->execute();
    }
    /**
     *  Get detail of material
     *
     * @author Vulth
     * @return boolean 
     */
    public function action_general() {
        return \Bus\Reports_General::getInstance()->execute();
    }
}