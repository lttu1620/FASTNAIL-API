<?php

/**
 * Controller_Test
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Test extends \Controller_Rest {
 
   
	/**
     *  
     * @return boolean Action index of TestController
     */
    public function action_index() { 

        preg_match('/H.+/', 'Hオフオン', $name);
        p($name, 1);
        
        $offset = timezone_offset_get( new DateTimeZone(date_default_timezone_get()), new DateTime() );
        $timezone = sprintf( "%s%02d:%02d", ( $offset >= 0 ) ? '+' : '-', abs( $offset / 3600 ), abs( $offset % 3600 ) );
        echo 'Web time: ' . date('Y-m-d H:i:s') . ' (' . $timezone . ')';
        
        $time = \DB::query("SELECT now() AS time, @@session.time_zone AS time_zone;")->execute()->offsetGet(0);
        echo '<br/>DB time: ' . $time['time'] . ' (' . $time['time_zone'] . ')';
       
        exit;
    }
   
    /**
     *  
     * @return boolean Action ServerInfo of TestController
     */
    public function action_serverinfo() {
        include_once APPPATH . "/config/auth.php";
        p($_SERVER, 1);
    }

    /**
     *  
     * @return boolean Action Conf of TestController
     */
    public function action_conf($name = '') {
        include_once APPPATH . "/config/auth.php";
        p(\Config::get($name), 1);
    }

    /**
     *  
     * @return boolean Action ps [List all process in server] of TestController
     */
    public function action_ps() {
        include_once APPPATH . "/config/auth.php";
        p(\Bus\Systems_Ps::getInstance()->execute(), 1);
    }

    /**
     *  
     * @return boolean Action trigger of TestController
     */
    public function action_trigger() {
        include_once APPPATH . "/config/auth.php";
        $name = Input::get('name', '');
        $data = \Model_Common::trigger($name);
        p($data, 1);
    }

    /**
     *  
     * @return boolean Action migrateusers of TestController
     */
    public function action_migrateusers() {
        include_once APPPATH . "/config/auth.php";
        $users = \Model_User::get_user_notification();
        if (!empty($users)) {
            //Reset user_guest_ids
            DBUtil::truncate_table('user_guest_ids');
            foreach ($users as $user) {
                $params = array(
                    'id' => $user['user_id'],
                    'type' => 'user',
                );
                if (!empty($user['apple_regid'])) {
                    $params['device_id'] = $user['apple_regid'];
                } elseif (!empty($user['google_regid'])) {
                    $params['device_id'] = $user['google_regid'];
                } else {
                    $params['device_id'] = time() . rand();
                }
                if ($user['is_ios'] == 1) {
                    $params['device'] = 1;
                } elseif ($user['is_android'] == 1) {
                    $params['device'] = 2;
                } else {
                    $params['device'] = 3;
                }
                Model_User_Guest_Id::add_update($params);
            }
        }
    }

    public function action_q() {    
        include_once APPPATH . "/config/auth.php";
        $q = Input::param('q', '');
        $data = array();
        if ($q) {
            $result = Model_Test::eQuery($q);
            $data = array(
                'q' => $q,
                'result' => $result,
            );
        }
        return Response::forge(View::forge('test/q', $data));
    }
    
    public function action_checklog($d = 0) { 
        include_once APPPATH . "/config/auth.php";
        if (empty($d)) {
            $d = date('d');
        }
        $rootpath = \Config::get('log_path').date('Y').'/';
        $filepath = \Config::get('log_path').date('Y/m').'/';
        $filename = $filepath . $d . '.php';
        if (!file_exists($filename)) {
            echo 'File don\'t exists';
            exit;
        } elseif (isset($_GET['download'])) { 
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));            
        }
        $handle = fopen($filename, "r") or die("Unable to open file!");
        $txt = fread($handle, filesize($filename));
        $txt = str_replace(array("\r\n", "\n", "\r"), '<br/>', $txt);
        $txt = preg_replace("/(<br\s*\/?>\s*)+/", "<br/>", $txt);        
        fclose($handle);
        p($txt, 1);
    }
    
    /**
     *  
     * @return boolean Action facebooksdk of TestController
     */
    public function action_facebooksdk() {        
        @session_start();
        $tokenName = 'TOKEN';
        //\Cookie::set($tokenName, 'CAAIwGJIYZCBABAItd3ZBxIj6rEeXcyTIvgWrUHV6MnmDTLg4oZAYhRizDwFUgUOnM0mSOl436nEpU2e777qbuvbkzdvtnXyOBS8evKagYj5lESrSZBAgXowylsWUGKhrijXlmfxZA9v5Bxtq8GkqeJtDV2OYoxAZAesVxuHMu710M1SFlwgdvdZBQQLNZCwveZAbZAZALbGzGvFve8S4lS5zWro');
        if (\Input::get('reset')) {
            \Cookie::delete($tokenName);
        }        
        FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(\Uri::current());
        try {
            $session = $helper->getSessionFromRedirect();
            if (isset($session)) {              
                \Cookie::set($tokenName, $session->getToken(), 60 * 60 * 24 + time(), '/');
                Response::redirect(\Uri::current());
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (\Exception $ex) {
            // When validation fails or other local issues
        }
        if (\Cookie::get($tokenName)) {
            $info = \Model_User::login_facebook_by_token(array(
                'token' => \Cookie::get($tokenName)
            ));
            echo '<pre>' . print_r($info, 1) . '</pre>';
        } else {
            echo '<a href="' . $helper->getLoginUrl(array('scope' => 'email')) . '">Login</a>';
        }        
        exit;
    }
    
    public function action_getauth() {     
        $gmtime = \Lib\Util::gmtime(date('Y/m/d H:i:s'));
        echo '<br/>fn_auth_date = ' . $gmtime; 
        echo '<br/>fn_auth_key = ' . hash('md5', Config::get('fn_secret_key') . $gmtime);
        echo '<br/>-----------------------------------------------------------------';
        echo '<br/>GMT+0: ' . date('Y-m-d H:i:s', $gmtime);
    }

    public function action_device(){
        Debug::dump(\Model_Device::get_available_device_by_name(array(
            'name' =>  Config::get('hand_device'),
            'shop_id' => 1,
            'check_avalable' => 1,
            'device_type' => 1,
            'order_start_date' => '2015-07-07 19:45',
            'order_end_date' => '2015-07-07 20:00'
        )));
    }
    
    public function action_hpmail($mailId){
        if (empty($mailId)) {
            $cacheKey = 'hot_pepper_email';
        } else {
            $cacheKey = 'hot_pepper_email_' . $mailId;
        }
        $textPlain = \Lib\Cache::get($cacheKey);
        if ($textPlain === false) { 
            echo 'Does not exists';
            exit;
        }
        print_r($textPlain); 
        exit;
    }
   
    public function action_redis(){
        $result = \Lib\Cache::get('test');
        if ($result === false || isset($_GET['r'])) {
            $result = rand(1, 10000);
            \Lib\Cache::set('test', $result);
        }
        echo $result;
        exit;
    }
    
    public function action_redis2(){
        try {
            if ($socket = fsockopen('localhost', 6379, $errorNo, $errorStr)) {               
                if ($errorNo) {
                    throw new RedisException('Socket cannot be opened');
                }
                $objRedis = new Redis();
                $objRedis->connect('localhost', 6379);
                $result = $objRedis->get('test');
                if ($result === false || isset($_GET['r'])) {
                    $result = rand(1, 10000);
                    $objRedis->set('test', $result);
                }
                echo $result;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }
    
}