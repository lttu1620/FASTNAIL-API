<?php
/**
 * Controller for actions on poscheckapi
 *
 * @package Controller
 * @created 2015-05-18
 * @updated 2015-08-09
 * @version 2.1
 * @author Haino Hiromu , Kinoshita Shogo
 * @copyright Oceanize INC
 */
class Controller_poscheckapi extends Controller_Rest {


	protected $format = 'json';

	/**
	 * ログイン認証
	 * @param string key
	 * @param string app_id
	 * @param string date
	 * @return false or Object
	 */
	protected function login($key, $app_id, $date) {
		try{

			//認証(セッション)
			#$secretKey = Config::get('security.salt');
			$secretKey ="fastnailsalt";
			$authKey = hash('md5', "{$secretKey}{$app_id}{$date}");
			if ($authKey !== $key) throw new Exception('key did not much');

			//認証(ユーザー)
			#$where = compact("app_id");
			#$user = Model_User::find('first', compact("where"));
			#if (empty($user)) return false;
			if($app_id != "91505102vn") throw new Exception('app_id did not much');;

			//時間
			if( strtotime( $date ) < strtotime("-10minutes") ) throw new Exception('out of time');
			if( strtotime( $date ) > strtotime("+10minutes") ) throw new Exception('out of time');
			if( $date == "now" ) throw new Exception('when is now?');;
		
		return array("success" => true);
		
		}catch (Exception $e) {

			return array("success" => false,
				"error" => $e->getMessage() );
		}
		
	}


	/**
	 * 注文取得API
	 * ユーザーコード(ユニーク)で取得
	 *
	 */
	public function action_get_user_order() {
		
		//必須項目
		$user_code = Input::post('user_code');
		$shop_id = Input::post('shop_id');
		$secretkey = Input::post('key');
		$date = Input::post('date');
		$app_id = Input::post('app_id');

		try {
			$disable = false;
			$auth_result = $this->login($secretkey, $app_id, $date);
			if(!$auth_result["success"])throw new Exception("user auth error : ".$auth_result["error"]);
			
			//まずは必須確認
			if (empty($user_code)) throw new Exception("Empty user_code");
			if (empty($shop_id)) throw new Exception("Empty shop_id");
			// --------------------------------------------

			# orderの取得
			$query = Model_Order::query()->where('reservation_date','>', strtotime('today'))
										 ->where('reservation_date','<', strtotime('today +1day'))
			                             ->where('user_code',$user_code)
			                             ->where('disable',0)
			                             ->where('shop_id',$shop_id)
			                             ->order_by('reservation_date', 'desc');
			$Order = $query->get_one();			

			// 当日データが確認できない場合は最新追加データを返す
			if(empty($Order)){
				$order_by = array('created' => 'desc');
				$where = compact("user_code", "shop_id", "disable");
				
				$Order = Model_Order::find('first', compact("where", "order_by"));
				if(empty($Order)) throw new Exception("オーダーが見つかりませんでした");

			}

			// --------------------------------------------
			
			# prefectures の取得
			if(!is_null($Order->get('prefecture_id') && $Order->get('prefecture_id') != 0 ) ){
				$where = array("id"=> $Order->get('prefecture_id'));
				$prefectures = Model_Prefecture::find('first',compact("where"));
			}

			// --------------------------------------------

 			# service staff codeの取得
			$order_id = $Order->get('id');
			$type = 4; #4=stop watch start nail service
			$where = compact("order_id","type");
			$Nailist_Log = Model_Order_Nailist_Log::find('first', compact("where"));

			if(!empty($Nailist_Log)){
				$where = array("id"=> $Nailist_Log->get('nailist_id'));
				$Staff_service = Model_Nailist::find('first',compact("where"));
			}
			
			// --------------------------------------------

			# consult staff codeの取得
			$order_id = $Order->get('id');
			$type = 7; #4=stop watch start nail service
			$where = compact("order_id","type");
			$Nailist_Log = Model_Order_Nailist_Log::find('first', compact("where"));

			if(!empty($Nailist_Log)){
				$where = array("id"=> $Nailist_Log->get('nailist_id'));
				$Staff_consult = Model_Nailist::find('first',compact("where"));
			}

			// --------------------------------------------

			# option itemの取得
			$order_id = $Order->get('id');
			$disable = 0;
			$where = compact("order_id","disable");
			$Order_items = Model_Order_Item::find('all', compact("where"));
			
			// --------------------------------------------

			//送信データの作成
			$order = $this->_init_order();		

			$order["shop_id"] = $Order->get('shop_id');
			$order["user_code"] = $Order->get('user_code');
			$order["reservation_date"] = Date("Ymd H:i",$Order->get('reservation_date'));
			$order["user_name"] = $Order->get('user_name');

			# visit_section 
			#️ 1:初めて,2:リピート
			$order["visit_section"] = (int)$Order->get('visit_section');
			if(empty($Order->get('visit_section'))) $order["visit_section"] = 2;			

			// visit_element
			// 1:PCホームページ,2:携帯ホームページ,3:スマホホームページ,4:ご友人紹介,
			// 5:店舗看板,6:テレビCM,7:新聞折込,8:商品券,9:ホットペッパー,10:クーポンランド,
			// 11:電車・バス広告,12:シティリビング,13:手配りチラシ,14:その他,15:既存会員
			if(empty($Order->get('visit_section')) ){
				$order["visit_element"] = $this->_whenLastTimeCome($Order);
			}else{
				$visitElement = $Order->get('visit_element');
				if($visitElement == 4) {
					$order["visit_element"] = 10; # 10=ご友人紹介
				} elseif($visitElement == 5) {
					$order["visit_element"] = 11; # 11=店舗看板
				} elseif($visitElement == 6) {
					$order["visit_element"] = 12; # 12=テレビCM
				} elseif($visitElement == 7) {
					$order["visit_element"] = 13; # 13=新聞折込
				} elseif($visitElement == 8) {
					$order["visit_element"] = 4; # 4=商品券
				} elseif($visitElement == 9) {
					$order["visit_element"] = 5; # 5=ホットペッパー
				} elseif($visitElement == 10) {
					$order["visit_element"] = 6; # 6=クーポンランド
				} elseif($visitElement == 11) {
					$order["visit_element"] = 7; # 7=電車・バス広告
				} elseif($visitElement == 12) {
					$order["visit_element"] = 8; # 8=シティリビング
				} elseif($visitElement == 13) {
					$order["visit_element"] = 9; # 9=手配りチラシ
				} elseif($visitElement == 15) {
					$order["visit_element"] = $this->_whenLastTimeCome($Order);
				} else {
					$order["visit_element"] = (int)$visitElement;
				}
			}
			if($Order->get('hf_section') == 1) {
				$order["hf_section"] = "H";
			} elseif($Order->get('hf_section') == 2) {
				$order["hf_section"] = "F";
			}			

			# nail off
			# 1:なし,2:ソフトジェル,3:ハードジェル,4:マニキュア,5:アクリル
			# -> pos 1:なし,2:ハンドジェル,3:ハンドマニキュア,4:ハンドアクリル,5:フットジェル,6:フットマニキュア
			if($Order->get('off_nails') == 2 || $Order->get('off_nails') == 3) {
				if($Order->get('hf_section') == 1) $order["off_nails"] = 2; # ハンドジェル
				if($Order->get('hf_section') == 2) $order["off_nails"] = 5; # フットジェル
			}elseif($Order->get('off_nails') == 4) {
				if($Order->get('hf_section') == 1) $order["off_nails"] = 3; # ハンドマニキュア
				if($Order->get('hf_section') == 2) $order["off_nails"] = 6; # フットマニキュア
			}elseif($Order->get('off_nails') == 5) {
				if($Order->get('hf_section') == 1) $order["off_nails"] = 4; # ハンドアクリル
				if($Order->get('hf_section') == 2) $order["off_nails"] = 7; # フットアクリル
			}else{
				$order["off_nails"] = 1;
			}

			$order["nail_length"] = $Order->get('nail_length');
			if(empty($Order->get('nail_length'))) $order["nail_length"] = 1;

			$order["nail_type"] = $Order->get('nail_type');
			if(empty($Order->get('nail_type'))) $order["nail_type"] = 1;

			$order["nail_add_length"] = $Order->get('nail_add_length');
			if(empty($Order->get('nail_add_length'))) $order["nail_add_length"] = 1;

			$order["problem"] = $Order->get('problem');
			$order["pay_type"] = $Order->get('pay_type');
			$order["photo_code"] = $Order->get('photo_code');
			if($Order->get('start_date') >0) $order["st_dt"] = Date("Ymd H:i",$Order->get('start_date')); #受付時間
			if($Order->get('start_date') >0) $order["or_st_dt"] = Date("Ymd H:i",$Order->get('start_date')); #受付時間
			if($Order->get('consult_start_date') >0) $order["or_end_dt"] = Date("Ymd H:i",$Order->get('consult_start_date')); #コンサル開始時間
			if($Order->get('sr_start_date') >0) $order["sr_st_dt"] = Date("Ymd H:i",$Order->get('sr_start_date'));#サービス開始時間
			if($Order->get('sr_end_date') >0) $order["sr_end_dt"] = Date("Ymd H:i",$Order->get('sr_end_date'));#サービス終了時間
			
			$order["sales_code"] = $Order->get('sales_code');
			$order["rating_1"] = $Order->get('rating_1');
			$order["rating_2"] = $Order->get('rating_2');
			$order["rating_3"] = $Order->get('rating_3');
			$order["rating_4"] = $Order->get('rating_4');
			$order["rating_5"] = $Order->get('rating_5');
			$order["first_name"] = NULL;
			$order["last_name"] = NULL;
			$order["reservation_type"] = $Order->get('reservation_type');
			$order["reservation_type_old"] = $Order->get('reservation_type_old');
			$order["is_mail"] = $Order->get('is_mail');
			$order["is_designate"] = $Order->get('is_designate');
			$order["is_next_reservation"] = $Order->get('is_next_reservation');
			$order["is_next_designate"] = $Order->get('is_next_designate');
			$order["welfare_id"] = $Order->get('welfare_id');
			if(is_null($Order->get('problem')) || $Order->get('problem') == "Imported order"){
				$order["problem"] = 1;
			}else{
				$order["problem"] = 2;
				}
			if(isset($prefectures)){
				$order["prefecture"] = $prefectures->get('name');	
			}
			
			$order["addr"] = $Order->get('address1').$Order->get('address2');
			$order["sex"] = $Order->get('sex');
			if($Order->get('birthday')>0) $order["birthday"] = Date("Ymd",$Order->get('birthday'));
			$order["mail"] = $Order->get('email');
			$order["phone"] = $Order->get('phone');
			$order["created"] = Date("Ymd",$Order->get('created'));
			$order["updated"] = Date("Ymd",$Order->get('updated'));
			$order["disable"] = $Order->get('disable');
			$order["r_point"] = $Order->get('r_point');
			$order["fn_point"] = $Order->get('fn_point');
			
			if(!empty($Staff_consult) ){
				$order["consult_staff"] = $Staff_consult->get('code');
			}
			if(!empty($Staff_service) ){
				$order["service_staff"] = $Staff_service->get('code');
			}

			if(!empty($Order_items) ){
				$num=0;
				$order["option_items"]["item_num"] = count($Order_items);
				foreach($Order_items as $key=>$val){
					$val['tmp'] = Model_Item::query()->where('id',$val->get('item_id'))->select('item_cd')->get_one();
					$num++;
					$order["option_items"]["item_cd_$num"] = $val['tmp']['item_cd'];
					$order["option_items"]["item_name_$num"] = $val->get('item_name');
					$order["option_items"]["item_quantity_$num"] = $val->get('item_quantity');
					$order["option_items"]["item_price_$num"] = $val->get('item_price');
					$order["option_items"]["item_tax_price_$num"] = $val->get('item_tax_price');
					
					// $order["option_items"][] = array(
					// 	'item_cd'        => $val['tmp']['item_cd'],
					// 	'item_name'      => $val->get('item_name'),
					// 	'item_quantity'  => $val->get('item_quantity'),
					// 	'item_price'     => $val->get('item_price'),
					// 	'item_tax_price' => $val->get('item_tax_price') );
				}
			}			

			$result = array('success'=>true, 'order'=>$order);
		} catch (Exception $e) {
			$result = array("success"=>false, "error"=>$e->getMessage());
		}
		$this->response($result);
	}

	/**********************************************************************
	 * 
	 **********************************************************************/
	private function _whenLastTimeCome($orderData){
		$vist_element = 0;
		// Order beforeの取得
		$query = Model_Order::query()->where('reservation_date','<', $orderData->get('reservation_date'))
		                             ->where('user_id',$orderData->get('user_id'))
		                             ->where('disable',0)
		                             ->where('is_cancel',0)
		                             ->where('is_autocancel',0)
		                             ->order_by('reservation_date', 'desc');
		$Order_before = $query->get_one();
		
		if(!empty($Order_before)){
			$last_time = $Order_before->get('reservation_date');	
			if(strtotime('-3 week') <= $last_time){
				$vist_element = 51; // 51	3週間以内
			}elseif(strtotime('-1 month') <= $last_time){
				$vist_element = 52; // 52	1ヶ月以内
			}elseif(strtotime('-2 month') <= $last_time){
				$vist_element = 53; // 53	2ヶ月以内
			}else{
				$vist_element = 54; // 54	それ以上
			}
		}
		return $vist_element;
	}


	/**********************************************************************
	 * Responseデータの初期化
	 **********************************************************************/
	private function _init_order(){
		$order = array();
		$order['shop_id'] = "";
		$order['user_code'] = "";
		$order['reservation_date'] = "";
		$order['user_name'] = "";
		$order['visit_section'] = "";
		$order['visit_element'] = "";
		$order['hf_section'] = "";
		$order['off_nails'] = "";
		$order['nail_length'] = "";
		$order['nail_type'] = "";
		$order['nail_add_length'] = "";
		$order['problem'] = "";
		$order['pay_type'] = "";
		$order['photo_code'] = "";
		$order['st_dt'] = "";
		$order['or_st_dt'] = "";
		$order['or_end_dt'] = "";
		$order['sr_st_dt'] = "";
		$order['sr_end_dt'] = "";
		$order['sales_code'] = "";
		$order['rating_1'] = "";
		$order['rating_2'] = "";
		$order['rating_3'] = "";
		$order['rating_4'] = "";
		$order['rating_5'] = "";
		$order['first_name'] = "";
		$order['last_name'] = "";
		$order['reservation_type'] = "";
		$order['reservation_type_old'] = "";
		$order['is_mail'] = "";
		$order['is_designate'] = "";
		$order['is_next_reservation'] = "";
		$order['is_next_designate'] = "";
		$order['welfare_id'] = "";
		$order['prefecture'] = "";
		$order['addr'] = "";
		$order['sex'] = "";
		$order['birthday'] = "";
		$order['mail'] = "";
		$order['phone'] = "";
		$order['created'] = "";
		$order['updated'] = "";
		$order['disable'] = "";
		$order['r_point'] = "";
		$order['fn_point'] = "";
		$order['consult_staff'] = "";
		$order['service_staff'] = "";
		$order['option_items'] = array('item_num'=>0);
		return $order;
	}

}