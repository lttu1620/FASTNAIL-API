<?php

/**
 * Controller for actions on Color Jell
 *
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_ColorJells extends \Controller_App
{
    /**
     * Add or update info for Color Jell
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\ColorJells_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Color Jell (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\ColorJells_List::getInstance()->execute();
    }

    /**
     * Get all Color Jell (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\ColorJells_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Color Jell
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\ColorJells_Disable::getInstance()->execute();
    }

    /**
     * Get detail Color Jell
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\ColorJells_Detail::getInstance()->execute();
    }
}
