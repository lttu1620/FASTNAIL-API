<?php

/**
 * Any query in Model Recommend Reaction Log
 *
 * @package Model
 * @created 2015-05-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Recommend_Reaction_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'recommend_order',
        'order_id',
        'nail_id',
        'action_type',
        'created'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'recommend_reaction_logs';

    /**
     * Add info for Recommend Reaction Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Recommend Reaction Log id or false if error
     */
    public static function add($param)
    {
        $options['where'] = array(
            'user_id'         => $param['user_id'],
            'recommend_order' => $param['recommend_order'],
            'order_id'        => $param['order_id'],
            'nail_id'         => $param['nail_id'],
            'action_type'     => $param['action_type']
        );
        $log = self::find('first', $options);
        if ($log) {
            \LogLib::info('Duplicate recommend reaction log', __METHOD__, $param);
            return true;
        }
        $log = new self;
        // set value
        if (!empty($param['user_id'])) {
            $log->set('user_id', $param['user_id']);
        }
        if (isset($param['recommend_order']) && $param['recommend_order'] != '') {
            $log->set('recommend_order', $param['recommend_order']);
        }
        if (isset($param['order_id']) && $param['order_id'] != '') {
            $log->set('order_id', $param['order_id']);
        }
        if (!empty($param['nail_id'])) {
            $log->set('nail_id', $param['nail_id']);
        }
        if (!empty($param['action_type'])) {
            $log->set('action_type', $param['action_type']);
        }
        // save to database
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list Recommend Reaction Log (with array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Recommend Reaction Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id');
        // filter by keyword
        if (!empty($param['user_name'])) {
            $query->where('users.user_name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name . '.order_id', '=', $param['order_id']);
        }
        if (!empty($param['action_type'])) {
            $query->where(self::$_table_name . '.action_type', '=', $param['action_type']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
