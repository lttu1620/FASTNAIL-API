<?php
use Fuel\Core\DB;
use Lib\Util;

/**
 * Any query in Model User
 *
 * @package Model
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'code',
        'password',
        'name',
        'kana',
        'first_name',
        'last_name',
        'sex',
        'phone',
        'email',
        'birthday',
        'prefecture_id',
        'address1',
        'address2',
        'is_magazine',
        'visit_element',
        'point',
        'is_ios',
        'is_android',
        'is_web',
        'created',
        'updated',
        'disable',
        'disable_by_user',
        'post_code',
        'phone_search',
        'is_point_member',
        'reason',
        'last_login'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load',                
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'users';

    /**
     * Add or update info for User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User id or false if error
     */
    public static function add_update($param)
    {
        $is_new = false;
        $id = !empty($param['id']) ? $param['id'] : 0;
        $user = new self;
        // check exist in case of updating
        if (!empty($id)) {
            $user = self::find($id);
            if (empty($user)) {
                self::errorNotExist('user_id');
                return false;
            }
        } else {            
            $is_new = true;
            //check email if exist in case of adding new
            $option['where'] = array(
                'email' => $param['email']
            );
            $profile = self::find('first', $option);
            if (!empty($profile)) {
                \LogLib::info('Duplicate email in users', __METHOD__, $param);
                self::errorDuplicate('email', $param['email']);
                return false;
            }
            if (empty($param['is_point_member'])) {
                $param['is_point_member'] = 0;
            }
            $user->set('is_point_member', $param['is_point_member']);
            $user->set('is_ios', '0');
            $user->set('is_android', '0');
            $user->set('is_web', '0');
        }
        // set value
        if (empty($param['is_magazine'])) {
            $param['is_magazine'] = 0;
        }                
        if (empty($param['password']) && $is_new) {
            //generate password with 6 characters
            $param['password'] = Lib\Str::generate_password();
            $user->set('password', Lib\Util::encodePassword($param['password'], $param['email']));
        } elseif (isset($param['password']) && $param['password'] != '') {
            $user->set('password', Lib\Util::encodePassword($param['password'], $param['email']));
        }

        if (!empty($param['name'])) {
            $user->set('name', $param['name']);
        }
        if (isset($param['kana']) && $param['kana'] != '') {
            $user->set('kana', $param['kana']);
        }

        if (isset($param['sex']) && $param['sex'] != '') {
            $user->set('sex', $param['sex']);
        }else{
             $user->set('sex', 0);
        }
        if (isset($param['birthday']) && $param['birthday'] != '') {
            $user->set('birthday', self::time_to_val($param['birthday']));
        }
        if (isset($param['phone']) && $param['phone'] != '') {
            $user->set('phone', $param['phone']);
        }
        if (isset($param['email']) && $param['email'] != '') {
            $user->set('email', $param['email']);
        }
        if (isset($param['prefecture_id']) && $param['prefecture_id'] != '') {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1']) && $param['address1'] != '') {
            $user->set('address1', $param['address1']);
        }
        if (isset($param['address2']) && $param['address2'] != '') {
            $user->set('address2', $param['address2']);
        }        
        $user->set('is_magazine', $param['is_magazine']);
        
        if (empty($param['visit_element'])) {
            $param['visit_element'] = 0;
        }
        $user->set('visit_element', $param['visit_element']);
        if (isset($param['point']) && $param['point'] != '') {
            $user->set('point', $param['point']);
        }

        // save to database
        if ($user->save()) {
            if (empty($user->id)) {
                $user->id = self::cached_object($user)->_original['id'];
            }
            if (isset($param['order_id']) && $param['order_id'] != '') {
                $order = new Model_Order();
                $order = $order::find($param['order_id']);
                $order->set('user_id', $user->id);
                $order->set('user_code', $user->code);
                $order->set('user_name', $user->name);
                $order->set('kana', $user->kana);
                $order->set('email', $user->email);
                $order->set('sex', $user->sex);
                $order->set('birthday', $user->birthday);
                $order->set('phone', $user->phone);
                $order->set('prefecture_id', $user->prefecture_id);
                $order->set('address1', $user->address1);
                $order->set('address2', $user->address2);
                $order->set('visit_element', $user->visit_element);
                if (!$order->save()) {
                    \LogLib::warning('Can not orders.user_id', __METHOD__, $param);
                    return false;
                }
            }
            //sending email if add new
            if ($is_new == true) {
                $info = array(
                    'user_id' => $user->id,
                    'email' => $param['email'],
                    'password' => $param['password']
                );
                if (!\Lib\Email::sendCreateUser($info)) {
                    \LogLib::warning('Can not resend create user email', __METHOD__, $info);
                    //return false;
                }
            }
            return !empty($user->id) ? $user->id : 0;
        }
        return false;
    }

    /**
     * Update info for User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User id or false if error
     */
    public static function update_profile($param)
    {
        $user = self::find($param['login_user_id']);
        if (empty($user)) {
            self::errorNotExist('login_user_id');
            return false;
        }
        $param['password'] = Lib\Util::decodePassword($user->get('password'));
        if (isset($param['email']) && $param['email'] != $user->get('email')) {
            $query = DB::select()
                ->from(self::$_table_name)
                ->where(self::$_table_name . '.id', '<>', $param['login_user_id'])
                ->where(self::$_table_name . '.email', '=', $param['email']);
            $checkEmail = $query->execute()->offsetGet(0);
            if (!empty($checkEmail)) {
                \LogLib::info('Check Duplicate email in users', __METHOD__, $param);
                self::errorDuplicate('email');
                return false;
            }
            $user->set('email', $param['email']);
            $user->set('password', Lib\Util::encodePassword($param['password'], $param['email']));          
        }
        if (isset($param['email']) && $param['email'] !== '') {
            $user->set('email', $param['email']);
        }        
        if (isset($param['name']) && $param['name'] !== '') {
            $user->set('name', $param['name']);
        }
        if (isset($param['kana']) && $param['kana'] !== '') {
            $user->set('kana', $param['kana']);
        }
        if (isset($param['sex']) && $param['sex'] !== '') {
            $user->set('sex', $param['sex']);
        }
        if (isset($param['phone']) && $param['phone'] !== '') {
            $user->set('phone', $param['phone']);
        }
        if (isset($param['birthday']) && $param['birthday'] !== '') {
            $user->set('birthday', self::time_to_val($param['birthday']));
        }
        if (isset($param['prefecture_id'])) {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1'])) {
            $user->set('address1', $param['address1']);
        }
        if (isset($param['address2'])) {
            $user->set('address2', $param['address2']);
        }
        if (isset($param['is_magazine']) && $param['is_magazine'] !== '') {
            $user->set('is_magazine', $param['is_magazine']);
        }
        if (isset($param['visit_element']) && $param['visit_element'] !== '') {
            $user->set('visit_element', $param['visit_element']);
        }
        if (isset($param['post_code']) && $param['post_code'] !== '') {
            $user->set('post_code', $param['post_code']);
        }
        if (isset($param['is_point_member']) && $param['is_point_member'] !== '') {
            $user->set('is_point_member', $param['is_point_member']);
        }        
        if ($user->update()) {
            \LogLib::info('Save users information', __METHOD__, $user);
            
            return self::get_login(array(
                'email' => $user->get('email'),
                'password' => $param['password']
            ));
        }
        return false;
    }

    /**
     * Get list User (with array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User
     */
    public static function get_list($param)
    {
        $query = DB::select(
            'id',
            'name',
            'code',
            'kana',
            'sex',
            'phone',
            'email',
            'birthday',
            'prefecture_id',
            'address1',
            'address2',
            'is_magazine',
            'point',
            'is_ios',
            'is_android',
            'is_web',
            'created',
            'shop_name',
            'disable',
            'disable_by_user'    
        )
            ->from(self::$_table_name)
            ->join(DB::expr("(
                SELECT  orders.user_id,
                        GROUP_CONCAT(shops.name SEPARATOR  ', ') AS shop_name
                FROM (  SELECT DISTINCT user_id, shop_id
                        FROM orders
                        WHERE orders.disable = 0
                        AND user_id > 0
                        ORDER BY id DESC
                    ) orders
                JOIN shops ON orders.shop_id = shops.id
                WHERE shops.disable = 0
                GROUP BY orders.user_id
            ) AS order_shops"), 'LEFT')
            /*
            ->join(DB::expr("(
                SELECT  orders.user_id,
                        orders.shop_id,
                        shops.name AS shop_name
                FROM orders JOIN shops ON orders.shop_id = shops.id
                            JOIN (  SELECT user_id, max(id) AS id
                                    FROM orders
                                    WHERE disable = 0
                                    AND user_id > 0
                                    GROUP BY user_id
                                 ) AS max_orders
                            ON orders.user_id = max_orders.user_id AND orders.id = max_orders.id
            ) AS order_shops"), 'LEFT')
             *
             */
            ->on(self::$_table_name . '.id', '=', "order_shops.user_id");
        // filter by keyword
        if (!empty($param['id'])) {
            $query->where('id', $param['id']);
        }
        if (!empty($param['code'])) {
            $query->where('code', 'like', "%{$param['code']}%");
        }
        if (!empty($param['name'])) {
            $query->where('name', 'like', "%{$param['name']}%");
        }
        if (!empty($param['kana'])) {
            $query->where('kana', 'like', "%{$param['kana']}%");
        }
        if (isset($param['sex_id']) && $param['sex_id'] != '') {
            $query->where('sex', '=', $param['sex_id']);
        }
        if (!empty($param['phone'])) {
            $query->where('phone', '=', $param['phone']);
        }
        if (!empty($param['email'])) {
            $query->where('email', '=', $param['email']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
         if (isset($param['disable_by_user']) && $param['disable_by_user'] != '') {
            $query->where(self::$_table_name . '.disable_by_user', '=', $param['disable_by_user']);
        }
        if (!empty($param['shop_id'])) {
            $query->where(DB::expr("users.id IN (SELECT user_id FROM orders WHERE shop_id = {$param['shop_id']} AND disable = 0)"));
        }
        if (!empty($param['reservation_date'])) {
            $query->where(DB::expr("users.id IN (SELECT user_id FROM orders WHERE reservation_date BETWEEN " .
                self::date_from_val($param['reservation_date']) . " AND " . self::date_to_val($param['reservation_date']) . " AND disable = 0)"));
        }

        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get list User (with array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $user = self::find($id);
            if ($user) {
                $user->set('disable', $param['disable']);
                if (!$user->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('user_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail User or false if error
     */
    public static function get_detail($param)
    {
        if (empty($param['id'])) {
            self::errorParamInvalid('user_id');
            return false;
        }
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('user_id');
            return false;
        }
        return $data;
    }

    /**
     * Login User
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail User or false if error
     */
    public static function get_login($param)
    {        
        $options['where'] = array(
            'email' => $param['email'],
            'password' => Lib\Util::encodePassword($param['password'], $param['email']),
        );
        $options['from_cache'] = false;
        \LogLib::info('Login', __METHOD__, $param);
        $login = self::find('first', $options);
        if ($login) {
            if ($login->get('disable') == 0) {
                $login->set('token', \Model_Authenticate::addupdate(array(
                        'user_id' => $login->get('id'),
                        'regist_type' => 'user'
                )));
                if (empty($login->get('code'))) {
                    $user = DB::select('code')
                        ->from(self::$_table_name)
                        ->where('id', $login->get('id'))
                        ->execute()
                        ->offsetGet(0);
                    if (!empty($user['code'])) {
                        $login->set('code', $user['code']);
                    }
                }
                $login->set('nail_favorites', Model_Nail_Favorite::get_all_nail_id($login->get('id')));
                $login->set('last_login', time());
                $param['os'] = \Lib\Util::os();                
                if ($login->get('is_ios') != '1' && $param['os'] == \Config::get('os')['ios']) {
                    $login->set('is_ios', '1');
                } elseif ($login->get('is_android') != '1' && $param['os'] == \Config::get('os')['android']) {
                    $login->set('is_android', '1');
                } elseif ($login->get('is_web') != '1' && $param['os'] == \Config::get('os')['webos']) {
                    $login->set('is_web', '1');
                }
                $login->save();
                return $login;
            }
            static::errorOther(static::ERROR_CODE_OTHER_1, 'User is disabled');
            return false;
        }
        static::errorOther(static::ERROR_CODE_AUTH_ERROR, 'Email/Password');
        return false;
    }

    /**
     * Function to register user.
     *
     * @author Hoang Gia Thong
     * @return array Returns the array.
     */
    public static function register($param)
    {
        //check email if exist in user_profiles (not for user_recruiter)
        $option['where'] = array(
            'email' => $param['email']
        );
        $profile = self::find('first', $option);
        if (!empty($profile)) {
            \LogLib::info('[Register user] Duplicate email in users', __METHOD__, $param);
            self::errorDuplicate('email', $param['email']);
            return false;
        }
        if (empty($param['is_point_member'])) {
            $param['is_point_member'] = 0;
        }
        if (empty($param['point'])) {
            $param['point'] = 0;
        }
        $user = new self;
        // set value
        if (!empty($param['name'])) {
            $user->set('name', $param['name']);
        }
        if (isset($param['kana']) && $param['kana'] != '') {
            $user->set('kana', $param['kana']);
        }
        if (isset($param['sex']) && $param['sex'] != '') {
            $user->set('sex', $param['sex']);
        }else{
             $user->set('sex',0);
        }
        if (isset($param['birthday']) && $param['birthday'] != '') {
            $user->set('birthday', self::time_to_val($param['birthday']));
        }
        if (isset($param['phone']) && $param['phone'] != '') {
            $user->set('phone', $param['phone']);
        }
        if (isset($param['email']) && $param['email'] != '') {
            $user->set('email', $param['email']);
        }
        if (isset($param['prefecture_id']) && $param['prefecture_id'] != '') {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1']) && $param['address1'] != '') {
            $user->set('address1', $param['address1']);
        }
        if (isset($param['address2']) && $param['address2'] != '') {
            $user->set('address2', $param['address2']);
        }
        if (isset($param['password']) && $param['password'] != '') {
            $user->set('password', Util::encodePassword($param['password'], $param['email']));
        }
        if (isset($param['is_magazine']) && $param['is_magazine'] != '') {
            $user->set('is_magazine', $param['is_magazine']);
        }
        if (isset($param['visit_element'])) {
            $user->set('visit_element', $param['visit_element']);            
        }
        $user->set('is_point_member', $param['is_point_member']);
        $user->set('point', $param['point']);
        // save to database
        if ($user->save()) {
            if (empty($user->id)) {
                $user->id = self::cached_object($user)->_original['id'];
                $user->email = self::cached_object($user)->_original['email'];
                $user->name = self::cached_object($user)->_original['name'];//added by KhoaTX on 2015-05-22
            }

            // send email
            if (!\Lib\Email::sendRegisterEmail(array(
                'email' => $user->email,
                'user_name' => $user->name//added by KhoaTX on 2015-05-22
            ))) {
            	\LogLib::warning('Can not send register email', __METHOD__, $param);
            }

            //return !empty($user->id) ? $user->id : 0;
            // Return user login information
            \LogLib::info('[Register user] register ok', __METHOD__, $user->to_array());
            return self::get_login(array(
                'email'     => $user->get('email'),
                'password'  => $param['password']
            ));
        }
        return false;
    }

    /**
     * Login facebook
     *
     * @author diennvt
     * @param array $facebookInfo Input data.
     * @param int $isCompany Input data or not input data.
     * @return bool Returns the boolean.
     */
    public static function login_facebook($facebookInfo)
    {        
        if (empty($facebookInfo['email']) && empty($facebookInfo['id'])) {
            self::errorNotExist('facebook_id_and_email');
            return false;
        }
        $param['facebook_email'] = isset($facebookInfo['email']) ? $facebookInfo['email'] : '';
        $param['facebook_id'] = isset($facebookInfo['id']) ? $facebookInfo['id'] : '';
        $param['facebook_name'] = isset($facebookInfo['name']) ? $facebookInfo['name'] : '';
        $param['facebook_first_name'] = isset($facebookInfo['first_name']) ? $facebookInfo['first_name'] : '';
        $param['facebook_last_name'] = isset($facebookInfo['last_name']) ? $facebookInfo['last_name'] : '';
        $param['facebook_username'] = isset($facebookInfo['username']) ? $facebookInfo['username'] : '';
        $param['facebook_gender'] = isset($facebookInfo['gender']) ? $facebookInfo['gender'] : '';
        $param['facebook_link'] = isset($facebookInfo['link']) ? $facebookInfo['link'] : '';
        $param['os'] = isset($facebookInfo['os']) ? $facebookInfo['os'] : '';
        if (!empty($param['facebook_email'])) {
            $facebook = \Model_User_Facebook_Information::get_detail(array(
                    'facebook_email' => $param['facebook_email'],
                    'disable' => 0
                )
            );
        } elseif (!empty($param['facebook_id'])) {
            $facebook = \Model_User_Facebook_Information::get_detail(array(
                    'facebook_id' => $param['facebook_id'],
                    'disable' => 0
                )
            );
        }
        if (!empty($facebook['facebook_id']) && $facebook['facebook_id'] != $param['facebook_id']) {
            if (\Model_User_Facebook_Information::add_update(array(
                'id' => $facebook['id'],
                'facebook_id' => $param['facebook_id'],
            ))
            ) {
                $facebook['facebook_id'] = $param['facebook_id'];
                \LogLib::info('Update facebook_id', __METHOD__, $param);
            }
        }

        $isNewUser = false;//use to differ first new login facebook or not
        if (!empty($facebook['user_id']) && !empty($facebook['facebook_id'])) {
            \LogLib::info('User used to login with facebook', __METHOD__, $facebook);
            $userId = $facebook['user_id'];
        } elseif (!empty($facebook['user_id']) && empty($facebook['facebook_id'])) {
            \LogLib::info('User used to login without facebook', __METHOD__, $facebook);
            $param['user_id'] = $facebook['user_id'];
            if (\Model_User_Facebook_Information::add_update($param)) {
                \LogLib::info('Update facebook info', __METHOD__, $param);
                $userId = $facebook['user_id'];
            }
        } else {
            $isNewUser = true;
            \LogLib::info('First login using facebook', __METHOD__, $param);
            $param['email'] = $param['facebook_email'];
            $param['password'] = '';
            $param['name'] = $param['facebook_name'];           
            $param['sex'] = $param['facebook_gender'] == 'male' ? 1 : 2;
            $param['image_url'] = "http://graph.facebook.com/{$param['facebook_id']}/picture?type=large";
            $param['is_magazine'] = 0;
            $param['visit_element'] = 0;
            $userId = Model_User::add_update($param);
            if ($userId > 0) {
                // Add user_facebook_information
                $param['user_id'] = $userId;
                if (\Model_User_Facebook_Information::add_update($param)) {
                    \LogLib::info('Add facebook info', __METHOD__, $param);
                }
            }
        }
        if (!empty($userId)) {
            \LogLib::info('Return user info', __METHOD__, $param);           
            $options['where']= array(
                'disable' => 0,
                'id' => $userId
            );
            $data = self::find('first', $options);
            if (!empty($data)) {
                $param['os'] = \Lib\Util::os();
                $data['is_new_user'] = $isNewUser;
                if ($data->get('is_ios') != '1' && $param['os'] == \Config::get('os')['ios']) {
                    $data->set('is_ios', '1');
                    $data->save();
                } elseif ($data->get('is_android') != '1' && $param['os'] == \Config::get('os')['android']) {
                    $data->set('is_android', '1');
                    $data->save();
                } elseif ($data->get('is_web') != '1' && $param['os'] == \Config::get('os')['webos']) {
                    $data->set('is_web', '1');
                    $data->save();
                }
                return $data;
            }
        }
        \LogLib::info('User info unavailable', __METHOD__, $param);
        self::errorNotExist('fb_user_information');
        return false;
    }

    /**
     * Login facebook by token.
     *
     * @author diennvt
     * @param array $param Input data.
     * @param int $isCompany Input data or not input data.
     * @return bool Returns the boolean.
     */
    public static function login_facebook_by_token($param)
    {
        @session_start();
        try
        {            
            FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));   
            \LogLib::info('login_facebook_by_token - Get token from cookie', __METHOD__, $param);
            $session = new FacebookSession($param['token']);   
            if (isset($session))
            {   
                \LogLib::info('login_facebook_by_token - Session is OK', __METHOD__, $param);
                $request = new FacebookRequest($session, 'GET', '/me');
                $response = $request->execute();
                $facebookInfo = (array) $response->getResponse();
                if (!empty($facebookInfo))
                {                    
                    \LogLib::info('login_facebook_by_token - call login_facebook', __METHOD__, $facebookInfo);
                    $loginInfo = self::login_facebook($facebookInfo);
                    $loginInfo['fb_token'] = $param['token'];
                    return $loginInfo;
                }
            }
            else
            {
                \LogLib::info('login_facebook_by_token - Session is not OK', __METHOD__, $param);
                return false;
            }            
        } 
        catch (FacebookRequestException $ex)
        {   
            // When Facebook returns an error
            \LogLib::warning($ex->getRawResponse(), __METHOD__, $param);
            static::errorOther(self::ERROR_CODE_OTHER_1, '', $ex->getRawResponse());
            return false;
        } 
        catch (\Exception $ex)
        {
            // When validation fails or other local issues
            \LogLib::warning($ex->getMessage(), __METHOD__, $param);
            static::errorOther(self::ERROR_CODE_OTHER_2, '', $ex->getMessage());
            return false;
        }
        \LogLib::info('login_facebook_by_token - There is no token from cookie', __METHOD__, $param);
        return false;
    }

    /**
     * Function to processing forget password user.
     *
     * @author tuancd
     * @return array|bool Returns the array or the boolean.
     */
    public static function forget_password($param)
    {
        $conditions['where'] = array(
            'email' => $param['email'],
            'disable' => 0
        );
        $user = self::find('first', $conditions); 
        if (!empty($user)) {
            // get token for sending email and add/update            
			$param['user_id'] = $user->get('id'); 
			$param['name'] = $user->get('name'); 
            $option['where'] = array(
                'email' => $param['email'],
                'regist_type' => \Config::get('user_activations_type')['forget_password'],
            );
            //Check user is request forget password ??
            $user_activation = Model_User_Activation::find('first', $option);          
            if (!empty($user_activation)) {
                $param['token'] = $user_activation->get('token');               
                $user_activation->set('expire_date', \Config::get('register_token_expire'));
                $user_activation->set('disable', '0');
            } else {
                $param['token'] = \Lib\Str::generate_token();
                $user_activation = new Model_User_Activation();
                $user_activation->set('user_id', $user->get('id'));
                $user_activation->set('email', $user->get('email'));
                $user_activation->set('disable', '0');
                $user_activation->set('token', $param['token']);
                $user_activation->set('regist_type', \Config::get('user_activations_type')['forget_password']);
                $user_activation->set('expire_date', \Config::get('register_token_expire'));
            }
            if (!$user_activation->save()) {
                \LogLib::warning('Can not insert/update user_activations', __METHOD__, $param);                
                return false;                
            }             
            \Lib\Email::sendForgetPasswordEmail($param);
            return true;
        }
        // Return email not exists in system
        static::errorNotExist('email', $param['email']);
        return false;        
    }

    /**
     * Resend register email.
     *
     * @author tuancd
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function resend_forget_password($param)
    {
        $option['where'] = array(
            'email' => $param['email'],
            'disable' => '0',
            'regist_type' => \Config::get('user_activations_type')['forget_password']
        );
        $userActivation = \Model_User_Activation::find('first', $option);
        if (!empty($userActivation)) {
            $token = $userActivation->get('token');
            // update new expire_date
            $userActivation->set('expire_date', \Config::get('register_token_expire'));
            if (!$userActivation->update()) {
                \LogLib::info('Can not update user_activations', __METHOD__, $param);
                return false;
            }
            $param = array(
                'email' => $param['email'],
                'token' => $token,
            );
            if (!\Lib\Email::sendForgetPasswordEmail($param)) {
                \LogLib::warning('Can not resend forgetpassword email', __METHOD__, $param);
                return false;
            }
        } else {
            \LogLib::info('Not exist email in user_activations', __METHOD__, $param);
            self::errorNotExist('email', $param['email']);
            return false;
        }
        return true;
    }

    /**
     * Change password by user_id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function change_password($param)
    {
        $options['where'] = array(
                'id'        => $param['id']
            );

        if (!(!empty($param['regist_type']) && $param['regist_type'] == 'admin')) {
            if (empty($param['email'])) {
                static::errorOther('1000','email', __('The email is required and must contain a value'));
            }

            if (empty($param['password_old'])) {
                static::errorOther('1000','password_old', __('The password_old is required and must contain a value'));
            }

            if (!empty(\Model_Abstract::$error_code_validation)) {
                return false;
            }

            $options['where']['password']  = Util::encodePassword($param['password_old'], $param['email']);
        }

        $user = self::find('first',$options);
        if (empty($user)) {
            static::errorOther('1021','password', __('Password not match. Please try again'));
            return false;
        } else {
            $user->set('password', Util::encodePassword($param['password'], $user['email']));
            if ($user->update()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Update password by token.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_password($param)
    {
        $query = DB::select(
            array('user_activations.email', 'activation_email'),
            array('user_activations.token', 'token'),
            array('user_activations.regist_type', 'regist_type'),
            array('user_activations.disable', 'activation_disable'),
            array('user_activations.id', 'activation_id'),
            self::$_table_name . '.*'
        )
            ->from('user_activations')
            ->join(self::$_table_name, 'LEFT')
            ->on('user_activations.email', '=', self::$_table_name . '.email')
            ->where('user_activations.token', '=', $param['token'])
            ->where('user_activations.regist_type', '=', $param['regist_type'])
            ->where('user_activations.disable', '=', '0');
        $data = $query->execute()->as_array();
        if ($data) {
            if (isset($data[0]['id']) && $data[0]['id']) {
                $user = self::find($data[0]['id']);
                if ($user) {
                    $user->set('password', Util::encodePassword($param['password'], $user->get('email')));
                    if ($user->update()) {
                        if (!\Model_User_Activation::disable(array(
                            'id' => $data[0]['activation_id'],
                            'disable' => '1'
                        ))
                        ) {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Update password by token.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int Returns 1 If exsit user, 0 otherwise .
     */
    public static function check_password($param)
    {   
        $options['where'] = array(
            'email' => $param['email'],
            'password' =>  Util::encodePassword($param['password'], $param['email'])
        );
        return !empty(Model_User::find('first', $options)) ? 1 : 0;
    }    
    
    /**
     * Cancel user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool Returns True if update successfully or false if otherwise.
     */
    public static function quit($param)
    {
        $user = self::find($param['login_user_id']);
        if (empty($user)) {
            self::errorNotExist('user_id');
            return false;
        }
        $param['email'] = $user->get('email');
        $param['name'] = $user->get('name');
        
        $user->set('email', md5($user->get('email')) . date('Ymd') . '@fastnail.town');
        $user->set('disable_by_user', 1);
        $user->set('disable', 1);
        $user->set('code', '');
        $user->set('password', '');
        $user->set('name', '');
        $user->set('kana', '');
        $user->set('phone', '');
        $user->set('birthday', null);
        $user->set('address1', '');
        $user->set('address2', '');
        $user->set('sex', 0);
        $user->set('visit_element', 0);
        $user->set('is_magazine', 0);
        $user->set('prefecture_id', 0);
        if (isset($param['reason'])) {
            $user->set('reason', $param['reason']);
        }
        if ($user->update()) {
            // Sending email if cancel user successfully.
            if (!\Lib\Email::sendUserQuitEmail($param)) {
                \LogLib::warning('Can not resend cancel user email', __METHOD__);
                return false;
            }
        }
        return true;
    }

    /**
     * Register user by mobile
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User id or false if error
     */
    public static function register_by_mobile($param)
    {
        $option['where'] = array(
            'email' => $param['email']
        );
        $profile = self::find('first', $option);
        if (!empty($profile)) {
            \LogLib::info('Duplicate email in users', __METHOD__, $param);
            self::errorDuplicate('email', $param['email']);
            return false;
        }
        if (empty($param['is_point_member'])) {
            $param['is_point_member'] = 0;
        }
        $user = new self;
        $user->set('email', $param['email']);
        $user->set('password', Util::encodePassword($param['password'], $param['email']));
        if (isset($param['name']) && $param['name'] !== '') {
            $user->set('name', $param['name']);
        }
        if (isset($param['kana']) && $param['kana'] !== '') {
            $user->set('kana', $param['kana']);
        }
        if (isset($param['sex']) && $param['sex'] !== '') {
            $user->set('sex', $param['sex']);
        }
        if (isset($param['phone']) && $param['phone'] !== '') {
            $user->set('phone', $param['phone']);
        }
        if (!empty($param['birthday'])) {
            $user->set('birthday', self::time_to_val($param['birthday']));
        }
        if (isset($param['prefecture_id'])) {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1'])) {
            $user->set('address1', $param['address1']);
        }
        if (isset($param['address2'])) {
            $user->set('address2', $param['address2']);
        }
        if (isset($param['is_magazine']) && $param['is_magazine'] !== '') {
            $user->set('is_magazine', $param['is_magazine']);
        }
        if (isset($param['visit_element']) && $param['visit_element'] !== '') {
            $user->set('visit_element', $param['visit_element']);
        }
        $user->set('is_point_member', $param['is_point_member']);
        $user->set('is_ios', '0');
        $user->set('is_android', '0');
        $user->set('is_web', '0');
        // save to database
        if ($user->save()) {
            if (empty($user->id)) {
                $user->id = self::cached_object($user)->_original['id'];
                $user->email = self::cached_object($user)->_original['email'];
                $user->name = self::cached_object($user)->_original['name'];
            }
            if (isset($param['is_magazine']) && $param['is_magazine'] !== '') {
                $user->set('is_magazine', $param['is_magazine']);
                Model_User_Setting::set_update(
                    array(
                        'name'          => 'send_magazine_email',
                        'value'         => $param['is_magazine'],
                        'login_user_id' => $user->id,
                    )
                );
            }
            /*
            if (!empty($param['is_point_member'])) {
                if (!Model_User_Point_Log::add(array(
                    'user_id' => $user->id,
                    'point' => \Config::get('user_point_logs.point.register'),
                    'type' => \Config::get('user_point_logs.type.register'),
                    'item_point_id' => 0,                    
                ))) {
                    \LogLib::warning('Can not add point', __METHOD__, $param);
                    return false;
                }
            }
            */
            
            // send email
            if (!\Lib\Email::sendRegisterEmail(array(
                'email' => $user->email,
                'user_name' => $user->name
            ))) {
                \LogLib::warning('Can not send register email', __METHOD__, $param);
            }

            \LogLib::info('get user login information', __METHOD__, $user);
            return self::get_login(array(
                'email' => $user->get('email'),
                'password' => $param['password'],               
            ));
        }
        return false;
    }

    /**
     * Search info user
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User's info
     */
    public static function search_info($param)
    {
        if (!empty($param['phone'])) {
            $param['phone'] = str_replace('-', '', $param['phone']);
        }
        if (empty($param['page'])) {
            $param['page'] = 1;
        }
        
        $strSelect = "DISTINCT users.name, users.phone, users.email";
        if (!empty($param['is_franchise']) && !empty($param['shop_id'])) {
            $strSelect .= ", orders.shop_id";
        }
        
        $query = DB::select(
                DB::expr("{$strSelect}")
            )
            ->from(self::$_table_name);

        if (!empty($param['is_franchise']) && !empty($param['shop_id'])) {
            $query->join(Model_Order::table(), 'LEFT')
            ->on(self::table().".id", '=', Model_Order::table().".user_id")
            ->where("orders.shop_id","=",$param['shop_id'])
            ->where(Model_Order::table().'.disable', '=', '0');
        }

        $query->where(self::table().'.disable', '=', '0')
            ->where(DB::expr("
                IFNULL(disable_by_user, 0) = 0
            "));
        
        // filter by keyword
        if (isset($param['name']) && $param['name'] !== '') {
            $query->where(self::table().'.name', 'like', "%{$param['name']}%");
        }
        if (isset($param['phone']) && $param['phone'] !== '') {
            $query->where(self::table().'.phone_search', 'like', "%{$param['phone']}%");
        }
        if (isset($param['email']) && $param['email'] !== '') {
            $query->where(self::table().'.email', 'like', "%{$param['email']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
    
    /**
     * Check email
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int Returns 1 If exsit user, 0 otherwise .
     */
    public static function check_email($param)
    {   
        $options['where'] = array(
            'email' => $param['email']           
        );
        return !empty(Model_User::find('first', $options)) ? 1 : 0;
    }
    
    /**
     * Update point
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_point($param)
    {       
        if (empty($param['user_id'])) {
            self::errorParamInvalid('user_id');
            return false;
        }
        if (empty($param['point'])) {
            $param['point'] = 0;
        }
        $option['where'] = array(
            'id' => $param['user_id'],
            'disable' => '0'
        );
        $user = self::find('first', $option);
        if (empty($user)) {
            self::errorNotExist('user_id');
            return false;
        }       
        $user->set('point', $param['point']);
        if (!$user->update()) {
            return false;
        }
        return true;
    }

    /**
     * Forget password for user
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Success or otherwise
     */
    public static function forget_password_for_app($param)
    {
        $options['where'] = array(
            'email'   => $param['email'],
            'disable' => 0,
        );
        $user = self::find('first', $options);
        if (!empty($user)) {
            // get token for sending email and add/update
            $param['user_id'] = $user->get('id');
            $param['name'] = $user->get('name');
            $option['where'] = array(
                'email'       => $param['email'],
                'regist_type' => \Config::get('user_activations_type')['forget_password'],
            );
            //Check user is request forget password ??
            $user_activation = Model_User_Activation::find('first', $option);
            if (!empty($user_activation)) {
                $param['token'] = $user_activation->get('token');
                $user_activation->set('expire_date', \Config::get('register_token_expire'));
                $user_activation->set('disable', '0');
            } else {
                $param['token'] = \Lib\Str::generate_token();
                $user_activation = new Model_User_Activation();
                $user_activation->set('user_id', $user->get('id'));
                $user_activation->set('email', $user->get('email'));
                $user_activation->set('disable', '0');
                $user_activation->set('regist_type', \Config::get('user_activations_type')['forget_password']);
                $user_activation->set('expire_date', \Config::get('register_token_expire'));
            }
            if (isset($param['os']) && $param['os'] != 'webos') { // get for mobile
                $param['token'] = \Lib\Str::generate_token_forget_password_for_mobile();
            }
            if (!empty($param['token'])) {
                $user_activation->set('token', $param['token']);
            }
            if (!$user_activation->save()) {
                \LogLib::warning('Can not insert/update user_activations', __METHOD__, $param);
                return false;
            }
            if (isset($param['os']) && $param['os'] != 'webos') {
                \Lib\Email::sendForgetPasswordEmailForMobile($param);
            } else {
                \Lib\Email::sendForgetPasswordEmail($param);
            }
            return true;
        }
        // Return email not exists in system
        static::errorNotExist('email', $param['email']);
        return false;
    }

    /**
     * Check token for app
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int Returns the integer.
     */
    public static function check_token_for_app($param)
    {
        $query = DB::select()
            ->from('user_activations')
            ->where('token', '=', $param['token'])
            ->where('regist_type', '=', $param['regist_type']);
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            self::errorNotExist('token');
            return false;
        }
        if (!empty($data['disable']) && $data['disable'] == '1') {
            self::errorOther(self::ERROR_CODE_OTHER_1, 'token', 'Token has already been used');
            return false;
        }
        if (intval($data['expire_date']) < time()) {
            self::errorOther(self::ERROR_CODE_OTHER_2, 'token', 'Token has been expired');
            return false;
        }
        return true;
    }

    /**
     * Update password for User
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_password_for_app($param)
    {
        $options['where'] = array(
            'email'   => $param['email'],
            'disable' => 0,
        );
        $user = self::find('first', $options);
        if (!empty($user)) {
            $options['where'] = array(
                'token'       => $param['token'],
                'email'       => $param['email'],
                'regist_type' => 'forget_password',
            );
            $activation = Model_User_Activation::find('first', $options);
            if ($activation) {
                if ($activation->get('disable') == '1') {
                    self::errorOther(self::ERROR_CODE_OTHER_1, 'token', 'Token has already been used');
                    return false;
                }
                if (intval($activation->get('expire_date')) < time()) {
                    self::errorOther(self::ERROR_CODE_OTHER_2, 'token', 'Token has been expired');
                    return false;
                }
                $user->set('password', Util::encodePassword($param['password'], $user->get('email')));
                if ($user->update()) {
                    $activation->set('disable', '1');
                    if ($activation->save()) {
                        return self::get_login(
                            array(
                                'email'    => $user->get('email'),
                                'password' => $param['password'],
                            )
                        );
                    }
                }
                return false;
            } else {
                self::errorNotExist('token');
                return false;
            }
        } else {
            static::errorNotExist('email', $param['email']);
            return false;
        }
    }

    /**
     * Register Beauty
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Beauty id or false if error
     */
    public static function register_beauty($param) {

        $user = self::find($param['login_user_id']);
        if (empty($user)) {
            self::errorNotExist('login_user_id');
            return false;
        }
        if ($user->get('is_point_member') == 1) { // registed beauty
            self::errorDuplicate('is_point_member');
            return false;
        }       
        $point_get = Model_Point_Get::find(\Config::get('point_gets.id.register_beauty'));
        if (empty($point_get)) {
            self::errorNotExist('point_gets');
            return false;
        }
        $user->set('is_point_member', '1');
        if (isset($param['sex']) && $param['sex'] !== '') {
            $user->set('sex', $param['sex']);
        }
        if (isset($param['birthday']) && $param['birthday'] !== '') {
            $user->set('birthday', self::time_to_val($param['birthday']));
        }
        if (isset($param['post_code']) && $param['post_code'] !== '') {
            $user->set('post_code', $param['post_code']);
        }
        if (isset($param['prefecture_id'])) {
            $user->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1'])) {
            $user->set('address1', $param['address1']);
        }
        if (isset($param['address2'])) {
            $user->set('address2', $param['address2']);
        }
        if ($user->save()) {
            $param['type'] = \Config::get('user_point_logs.type.register_beauty');
            $param['point'] = $point_get->get('point');
            $param['point_get_id'] = $point_get->get('id');
            $param['point_item_id'] = 0;
            $param['get_point'] = true;
            $result = Model_User_Point_Log::add($param);
            if (!$result) {
                \LogLib::warning("Can not write user_point_logs", __METHOD__, $param);  
                return false;
            }
            return $result;
//            $user_point = 0;
//            if (!empty($user->get('point'))) {
//                $user_point = $user->get('point');
//            }
//            return $user_point + $point_get->get('point');
        }
        return false;
    }

}
