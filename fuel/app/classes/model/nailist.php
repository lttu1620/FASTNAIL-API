<?php

/**
 * Any query in Model Nailist
 *
 * @package Model
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Nailist extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'code',
        'name',
        'shop_id',
        'image_url',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load', 
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'nailists';

    /**
     * Add or update info for Nailist
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Nailist id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $nailist = new self;
        // check exist
        if (!empty($id)) {
            $nailist = self::find($id);
            if (empty($nailist)) {
                self::errorNotExist('nailist_id');
                return false;
            }
        }
        // set value
        if (!empty($param['code'])) {
            $nailist->set('code', $param['code']);
        }
        if (!empty($param['name'])) {
            $nailist->set('name', $param['name']);
        }
        if (!empty($param['shop_id'])) {
            $nailist->set('shop_id', $param['shop_id']);
        }
        if (isset($param['image_url']) && is_string($param['image_url'])) {
            $nailist->set('image_url', $param['image_url']);
        }
        // save to database
        if ($nailist->save()) {
            if (empty($nailist->id)) {
                $nailist->id = self::cached_object($nailist)->_original['id'];
            }
            return !empty($nailist->id) ? $nailist->id : 0;
        }
        return false;
    }

    /**
     * Get list Nailist (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nailist
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('shops.name', 'shop_name'),
            DB::expr("IFNULL(IF(nailists.image_url='',NULL,nailists.image_url), '" . \Config::get('no_image_nailist') . "') AS nailist_image_url")
        )
            ->from(self::$_table_name)
            ->join('shops')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id');
        // filter by keyword
        if (!empty($param['id'])) {
            $query->where(self::$_table_name.'.id', $param['id']);
        }
        
        if (!empty($param['code'])) {
            $query->where('code', '=', $param['code']);
        }
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['shop_id'])) {
            $query->where('shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['shop_name'])) {
            $query->where('shops.name', 'LIKE', "%{$param['shop_name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.id', 'ASC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Nailist (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Nailist
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . '.*',
                array('shops.name', 'shop_name'),
                DB::expr("IFNULL(IF(nailists.image_url='',NULL,nailists.image_url), '" . \Config::get('no_image_nailist') . "') AS image_url")
            )
            ->from(self::$_table_name)
            ->join('shops')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->where(self::$_table_name . '.disable', '=', '0');
        // filter by keyword
        if (!empty($param['shop_id'])) {
            $query->where('shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['is_designate'])) {
            $query->where('is_designate', '=', $param['is_designate']);
        }
        $query->order_by(self::$_table_name . '.id', 'ASC');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Nailist
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $nailist = self::find($id);
            if ($nailist) {
                $nailist->set('disable', $param['disable']);
                if (!$nailist->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('nailist_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Nailist
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Nailist or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('nailist_id');
            return false;
        }
        return $data;
    }
}
