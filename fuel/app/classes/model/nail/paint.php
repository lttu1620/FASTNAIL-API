<?php

class Model_Nail_Paint extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'paint_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_paints';

    /**
     * Get list of nail's paints
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Paint Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['paint_id'])) {
            $query->where('paint_id', "{$param['paint_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's paints
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Paint Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['paint_id'])) {
            $query->where('paint_id', "{$param['paint_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's paint
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Paint Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['paint_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('paint_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'paint_id' => $param['paint_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_paints = self::find('first', $options);       
        if (empty($nail_paints)) {
            $nail_paints = new self;           
        }        
        $nail_paints->set('paint_id', $param['paint_id']);
        $nail_paints->set('nail_id', $param['nail_id']);       
        $nail_paints->set('disable', $param['disable']);       
        if ($nail_paints->save()) {
            if (empty($nail_paints->id)) {
                $nail_paints->id = self::cached_object($nail_paints)->_original['id'];
            }
            return !empty($nail_paints->id) ? $nail_paints->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a paint
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_paint_id($param)
    {
        //Paint id
        $paint_id = $param['paint_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: paint's id
        if (empty(Model_Paint::find($paint_id))) {
            self::errorNotExist('paint_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current paint's nails
        $options['where'] = array(
            'paint_id' => $paint_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_paints = self::find('all', $options);  
       
        //Get current paint's nails' id
        $array_nail_paints_id = array ();
        
        foreach ($current_nail_paints as $obj_nail_paint) {
            $array_nail_paints_id[] = $obj_nail_paint->get('nail_id');
        }
        
        //New nails's id
        $new_nail_paints = array_diff($nail_ids, $array_nail_paints_id);
        
        //Nails's id to remove
        $remove_nail_paints = array_diff($array_nail_paints_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_paints as $new_nail_id) {
            $data[] = array (
                'paint_id' => $paint_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_paints as $remove_nail_id) {
            $data[] = array (
                'paint_id' => $paint_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update paints for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of paint's id
        $paint_ids = !empty($param['paint_id']) ? 
                array_filter(array_unique(explode(',', $param['paint_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: paint's ids
        if (!empty($paint_ids)) {
            //Count paint's ids from db
            $count_paint = Model_Paint::query()
            ->where('id', 'in', $paint_ids)
            ->count();        

            //Do compare
            if ($count_paint != count($paint_ids)) {
                self::errorNotExist('paint_id');
                return false;
            }
        }
        
        //Get current nail's paints
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_paints_nail = self::find('all', $options);  
       
        //Get current nail's paint' id
        $array_paints_nail_id = array ();
        
        foreach ($current_paints_nail as $obj_paint_nail) {
            $array_paints_nail_id[] = $obj_paint_nail->get('paint_id');
        }
        
        //New paint's id
        $new_paint_nails = array_diff($paint_ids, $array_paints_nail_id);
        
        //Paints's id to remove
        $remove_paint_nails = array_diff($array_paints_nail_id, $paint_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_paint_nails as $new_paint_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'paint_id' => $new_paint_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_paint_nails as $remove_paint_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'paint_id' => $remove_paint_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
       
}
