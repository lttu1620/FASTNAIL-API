<?php

/**
 * Any query in Model Nail Favorite
 *
 * @package Model
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Nail_Favorite extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'nail_id',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(               
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'nail_favorites';

    /**
     * Add info for Nail Favorite
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Nail Favorite id or false if error
     *
     */
    public static function add($param)
    {
        $query = DB::select(
            array('nails.id', 'nail_id'),
            array('nail_favorites.id', 'favorite_id'),
            array('nail_favorites.user_id', 'user_id'),
            array('nail_favorites.disable', 'favorite_disable')
        )
            ->from('nails')
            ->join(
                DB::expr(
                    "(SELECT * FROM nail_favorites
                     WHERE user_id = {$param['user_id']}) AS nail_favorites"
                ),
                'LEFT'
            )
            ->on('nails.id', '=', 'nail_favorites.nail_id')
            ->where('nails.id', '=', $param['nail_id'])
            ->where('nails.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);
        if (empty($data['nail_id'])) {
            static::errorNotExist('nail_id');
            return false;
        }
        if (!empty($data['user_id']) && $data['favorite_disable'] == 0) {
            static::errorDuplicate('user_id');
            return false;
        }
        $new = false;
        if (!empty($data['user_id']) && $data['favorite_disable'] == 1) {
            $dataUpdate = array(
                'id'      => $data['favorite_id'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'nail_id' => $data['nail_id'],
                'user_id' => $param['user_id']
            );
        }
        $favorite = new self($dataUpdate, $new);
        if ($favorite->save()) {
            if (Model_Recommend_Reaction_Log::add(array(
                'user_id'         => $param['user_id'],
                'recommend_order' => '0',
                'order_id'        => '0',
                'nail_id'         => $data['nail_id'],
                'action_type'     => '2'
            ))
            ) {
                if ($new == true) {
                    $favorite->id = self::cached_object($favorite)->_original['id'];
                }
                \Lib\Cache::delete('nails_detail_' . $data['nail_id']);
                return $favorite->id;
            }
        }
        return false;
    }

    /**
     * Get list Nail Favorite (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nail
     */
    public static function get_list($param)
    {
        $query = DB::select(
                array('users.name', 'user_name'),
                array('users.kana', 'user_kana'),
                array('nails.id', 'id'),
                array('nails.photo_cd', 'photo_cd'),
                array('nails.show_section', 'show_section'),
                array('nails.hf_section', 'hf_section'),
                array('nails.menu_section', 'menu_section'),
                array('nails.price', 'price'),
                array('nails.tax_price', 'tax_price'),
                array('nails.time', 'time'),
                array('nails.print', 'print'),
                array('nails.limited', 'limited'),
                array('nails.rank', 'rank'),
                array('nails.hp_coupon', 'hp_coupon'),
                DB::expr("IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('" . \Config::get('item_img_url')['nails'] . "', photo_cd, '" . ".jpg" . "'), nails.photo_url) as image_url")
            )
            ->from(self::$_table_name)
            ->join('nails')
            ->on(self::$_table_name . '.nail_id', '=', 'nails.id')
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where('nails.disable', '0');
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', '=', $param['nail_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if (!in_array(strtolower($sortExplode[1]), array('asc', 'desc'))) {
                $sortExplode[1] = 'desc';
            }
            switch ($sortExplode[0]) {
                case 'photo_cd':
                    $query->order_by('nails.'.$sortExplode[0], $sortExplode[1]);
                    break;    
                default:
                    $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
            } 
        } else {
            $query->order_by(self::$_table_name . '.updated', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        $nail_id = Lib\Arr::field($data, 'id');
        if (!empty($param['get_nail_colors'])) {            
            $nail_colors = Model_Nail_Color::get_all(array(
                'nail_id' => $nail_id
            ));            
        }
        if (!empty($param['get_nail_designs'])) {            
            $nail_designs = Model_Nail_Design::get_all(array(
                'nail_id' => $nail_id
            ));            
        }
        foreach ($data as &$row) {
            if (isset($nail_colors)) {
                $row['nail_colors'] = implode(', ', Lib\Arr::field(Lib\Arr::filter($nail_colors, 'nail_id', $row['id'], false, false), 'name'));
            }
            if (isset($nail_colors)) {
                $row['nail_designs'] = implode(', ', Lib\Arr::field(Lib\Arr::filter($nail_designs, 'nail_id', $row['id'], false, false), 'name'));            
            }
        }
        return array($total, $data);
    }

    /**
     * Disable a Nail Favorite
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'] = array(
                'nail_id' => $param['nail_id'],
                'user_id' => $param['user_id'],
                'disable' => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $param['nail_id'] = $data->_data['nail_id'];
                $param['user_id'] = $data->_data['user_id'];
                \Lib\Cache::delete('nails_detail_' . $data->_data['nail_id']);
                return true;
            }
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id');
            } else {
                static::errorNotExist('user_id_or_nail_id');
            }
        }
        return false;
    }

    /**
     * Get favorite nail id
     *
     * @author thailh
     * @param array $userId Input data
     * @return array List nail ID
     */
    public static function get_all_nail_id($userId)
    {
        if (empty($userId)) {
            return array();
        }
        $data = DB::select('nail_id')
            ->from(self::$_table_name)
            ->where('disable', '=', '0')
            ->where('user_id', '=', $userId)
            ->execute()
            ->as_array();
        return \Lib\Arr::field($data, 'nail_id');
    }

}