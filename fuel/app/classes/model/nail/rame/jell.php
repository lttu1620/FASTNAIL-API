<?php

class Model_Nail_Rame_Jell extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'rame_jell_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_rame_jells';

        
    /**
     * Get list of nail's rame_jells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Rame Jell Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['rame_jell_id'])) {
            $query->where('rame_jell_id', "{$param['rame_jell_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's rame_jells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Rame Jell Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['rame_jell_id'])) {
            $query->where('rame_jell_id', "{$param['rame_jell_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's rame_jell
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Rame Jell Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['rame_jell_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('rame_jell_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'rame_jell_id' => $param['rame_jell_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_rame_jells = self::find('first', $options);       
        if (empty($nail_rame_jells)) {
            $nail_rame_jells = new self;           
        }        
        $nail_rame_jells->set('rame_jell_id', $param['rame_jell_id']);
        $nail_rame_jells->set('nail_id', $param['nail_id']);       
        $nail_rame_jells->set('disable', $param['disable']);       
        if ($nail_rame_jells->save()) {
            if (empty($nail_rame_jells->id)) {
                $nail_rame_jells->id = self::cached_object($nail_rame_jells)->_original['id'];
            }
            return !empty($nail_rame_jells->id) ? $nail_rame_jells->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a rame_jell
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_rame_jell_id($param)
    {
        //Rame Jell id
        $rame_jell_id = $param['rame_jell_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: rame_jell's id
        if (empty(Model_Rame_Jell::find($rame_jell_id))) {
            self::errorNotExist('rame_jell_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current rame_jell's nails
        $options['where'] = array(
            'rame_jell_id' => $rame_jell_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_rame_jells = self::find('all', $options);  
       
        //Get current rame_jell's nails' id
        $array_nail_rame_jells_id = array ();
        
        foreach ($current_nail_rame_jells as $obj_nail_rame_jell) {
            $array_nail_rame_jells_id[] = $obj_nail_rame_jell->get('nail_id');
        }
        
        //New nails's id
        $new_nail_rame_jells = array_diff($nail_ids, $array_nail_rame_jells_id);
        
        //Nails's id to remove
        $remove_nail_rame_jells = array_diff($array_nail_rame_jells_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_rame_jells as $new_nail_id) {
            $data[] = array (
                'rame_jell_id' => $rame_jell_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_rame_jells as $remove_nail_id) {
            $data[] = array (
                'rame_jell_id' => $rame_jell_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update rame_jells for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of rame_jell's id
        $rame_jell_ids = !empty($param['rame_jell_id']) ? 
                array_filter(array_unique(explode(',', $param['rame_jell_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: rame_jell's ids
        if (!empty($rame_jell_ids)) {
            //Count rame_jell's ids from db
            $count_rame_jell = Model_Rame_Jell::query()
            ->where('id', 'in', $rame_jell_ids)
            ->count();        

            //Do compare
            if ($count_rame_jell != count($rame_jell_ids)) {
                self::errorNotExist('rame_jell_id');
                return false;
            }
        }
        
        //Get current nail's rame_jells
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_rame_jells_nail = self::find('all', $options);  
       
        //Get current nail's rame_jell' id
        $array_rame_jells_nail_id = array ();
        
        foreach ($current_rame_jells_nail as $obj_rame_jell_nail) {
            $array_rame_jells_nail_id[] = $obj_rame_jell_nail->get('rame_jell_id');
        }
        
        //New rame_jell's id
        $new_rame_jell_nails = array_diff($rame_jell_ids, $array_rame_jells_nail_id);
        
        //Rame Jells's id to remove
        $remove_rame_jell_nails = array_diff($array_rame_jells_nail_id, $rame_jell_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_rame_jell_nails as $new_rame_jell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'rame_jell_id' => $new_rame_jell_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_rame_jell_nails as $remove_rame_jell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'rame_jell_id' => $remove_rame_jell_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
