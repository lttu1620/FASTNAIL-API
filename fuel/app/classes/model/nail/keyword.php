<?php

class Model_Nail_Keyword extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'keyword_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_keywords';

    /**
     * Get list of nail's keywords
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Keyword Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['keyword_id'])) {
            $query->where('keyword_id', "{$param['keyword_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's keywords
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Keyword Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['keyword_id'])) {
            $query->where('keyword_id', "{$param['keyword_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's keyword
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Keyword Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['keyword_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('keyword_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'keyword_id' => $param['keyword_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_keywords = self::find('first', $options);       
        if (empty($nail_keywords)) {
            $nail_keywords = new self;           
        }        
        $nail_keywords->set('keyword_id', $param['keyword_id']);
        $nail_keywords->set('nail_id', $param['nail_id']);       
        $nail_keywords->set('disable', $param['disable']);       
        if ($nail_keywords->save()) {
            if (empty($nail_keywords->id)) {
                $nail_keywords->id = self::cached_object($nail_keywords)->_original['id'];
            }
            return !empty($nail_keywords->id) ? $nail_keywords->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a keyword
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_keyword_id($param)
    {
        //Keyword id
        $keyword_id = $param['keyword_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: keyword's id
        if (empty(Model_Keyword::find($keyword_id))) {
            self::errorNotExist('keyword_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current keyword's nails
        $options['where'] = array(
            'keyword_id' => $keyword_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_keywords = self::find('all', $options);  
       
        //Get current keyword's nails' id
        $array_nail_keywords_id = array ();
        
        foreach ($current_nail_keywords as $obj_nail_keyword) {
            $array_nail_keywords_id[] = $obj_nail_keyword->get('nail_id');
        }
        
        //New nails's id
        $new_nail_keywords = array_diff($nail_ids, $array_nail_keywords_id);
        
        //Nails's id to remove
        $remove_nail_keywords = array_diff($array_nail_keywords_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_keywords as $new_nail_id) {
            $data[] = array (
                'keyword_id' => $keyword_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_keywords as $remove_nail_id) {
            $data[] = array (
                'keyword_id' => $keyword_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update keywords for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of keyword's id
        $keyword_ids = !empty($param['keyword_id']) ? 
                array_filter(array_unique(explode(',', $param['keyword_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: keyword's ids
        if (!empty($keyword_ids)) {
            //Count keyword's ids from db
            $count_keyword = Model_Keyword::query()
            ->where('id', 'in', $keyword_ids)
            ->count();        

            //Do compare
            if ($count_keyword != count($keyword_ids)) {
                self::errorNotExist('keyword_id');
                return false;
            }
        }
        
        //Get current nail's keywords
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_keywords_nail = self::find('all', $options);  
       
        //Get current nail's keyword' id
        $array_keywords_nail_id = array ();
        
        foreach ($current_keywords_nail as $obj_keyword_nail) {
            $array_keywords_nail_id[] = $obj_keyword_nail->get('keyword_id');
        }
        
        //New keyword's id
        $new_keyword_nails = array_diff($keyword_ids, $array_keywords_nail_id);
        
        //Keywords's id to remove
        $remove_keyword_nails = array_diff($array_keywords_nail_id, $keyword_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_keyword_nails as $new_keyword_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'keyword_id' => $new_keyword_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_keyword_nails as $remove_keyword_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'keyword_id' => $remove_keyword_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
