<?php

class Model_Nail_Genre extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'genre_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_genres';

    /**
     * Get list of nail's genres
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Genre Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['genre_id'])) {
            $query->where('genre_id', "{$param['genre_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's genres
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Genre Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['genre_id'])) {
            $query->where('genre_id', "{$param['genre_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's genre
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Genre Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['genre_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('genre_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'genre_id' => $param['genre_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_genres = self::find('first', $options);       
        if (empty($nail_genres)) {
            $nail_genres = new self;           
        }        
        $nail_genres->set('genre_id', $param['genre_id']);
        $nail_genres->set('nail_id', $param['nail_id']);       
        $nail_genres->set('disable', $param['disable']);       
        if ($nail_genres->save()) {
            if (empty($nail_genres->id)) {
                $nail_genres->id = self::cached_object($nail_genres)->_original['id'];
            }
            return !empty($nail_genres->id) ? $nail_genres->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a genre
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_genre_id($param)
    {
        //Genre id
        $genre_id = $param['genre_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: genre's id
        if (empty(Model_Genre::find($genre_id))) {
            self::errorNotExist('genre_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current genre's nails
        $options['where'] = array(
            'genre_id' => $genre_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_genres = self::find('all', $options);  
       
        //Get current genre's nails' id
        $array_nail_genres_id = array ();
        
        foreach ($current_nail_genres as $obj_nail_genre) {
            $array_nail_genres_id[] = $obj_nail_genre->get('nail_id');
        }
        
        //New nails's id
        $new_nail_genres = array_diff($nail_ids, $array_nail_genres_id);
        
        //Nails's id to remove
        $remove_nail_genres = array_diff($array_nail_genres_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_genres as $new_nail_id) {
            $data[] = array (
                'genre_id' => $genre_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_genres as $remove_nail_id) {
            $data[] = array (
                'genre_id' => $genre_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update genres for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of genre's id
        $genre_ids = !empty($param['genre_id']) ? 
                array_filter(array_unique(explode(',', $param['genre_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: genre's ids
        if (!empty($genre_ids)) {
            //Count genre's ids from db
            $count_genre = Model_Genre::query()
            ->where('id', 'in', $genre_ids)
            ->count();        

            //Do compare
            if ($count_genre != count($genre_ids)) {
                self::errorNotExist('genre_id');
                return false;
            }
        }
        
        //Get current nail's genres
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_genres_nail = self::find('all', $options);  
       
        //Get current nail's genre' id
        $array_genres_nail_id = array ();
        
        foreach ($current_genres_nail as $obj_genre_nail) {
            $array_genres_nail_id[] = $obj_genre_nail->get('genre_id');
        }
        
        //New genre's id
        $new_genre_nails = array_diff($genre_ids, $array_genres_nail_id);
        
        //Genres's id to remove
        $remove_genre_nails = array_diff($array_genres_nail_id, $genre_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_genre_nails as $new_genre_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'genre_id' => $new_genre_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_genre_nails as $remove_genre_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'genre_id' => $remove_genre_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
}
