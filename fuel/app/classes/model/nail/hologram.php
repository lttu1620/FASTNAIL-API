<?php

class Model_Nail_Hologram extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'hologram_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_holograms';

        
    /**
     * Get list of nail's holograms
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Hologram Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['hologram_id'])) {
            $query->where('hologram_id', "{$param['hologram_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's holograms
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Hologram Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['hologram_id'])) {
            $query->where('hologram_id', "{$param['hologram_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's hologram
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Hologram Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['hologram_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('hologram_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'hologram_id' => $param['hologram_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_holograms = self::find('first', $options);       
        if (empty($nail_holograms)) {
            $nail_holograms = new self;           
        }        
        $nail_holograms->set('hologram_id', $param['hologram_id']);
        $nail_holograms->set('nail_id', $param['nail_id']);       
        $nail_holograms->set('disable', $param['disable']);       
        if ($nail_holograms->save()) {
            if (empty($nail_holograms->id)) {
                $nail_holograms->id = self::cached_object($nail_holograms)->_original['id'];
            }
            return !empty($nail_holograms->id) ? $nail_holograms->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a hologram
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_hologram_id($param)
    {
        //Hologram id
        $hologram_id = $param['hologram_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: hologram's id
        if (empty(Model_Hologram::find($hologram_id))) {
            self::errorNotExist('hologram_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current hologram's nails
        $options['where'] = array(
            'hologram_id' => $hologram_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_holograms = self::find('all', $options);  
       
        //Get current hologram's nails' id
        $array_nail_holograms_id = array ();
        
        foreach ($current_nail_holograms as $obj_nail_hologram) {
            $array_nail_holograms_id[] = $obj_nail_hologram->get('nail_id');
        }
        
        //New nails's id
        $new_nail_holograms = array_diff($nail_ids, $array_nail_holograms_id);
        
        //Nails's id to remove
        $remove_nail_holograms = array_diff($array_nail_holograms_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_holograms as $new_nail_id) {
            $data[] = array (
                'hologram_id' => $hologram_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_holograms as $remove_nail_id) {
            $data[] = array (
                'hologram_id' => $hologram_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update holograms for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of hologram's id
        $hologram_ids = !empty($param['hologram_id']) ? 
                array_filter(array_unique(explode(',', $param['hologram_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: hologram's ids
        if (!empty($hologram_ids)) {
            //Count hologram's ids from db
            $count_hologram = Model_Hologram::query()
            ->where('id', 'in', $hologram_ids)
            ->count();        

            //Do compare
            if ($count_hologram != count($hologram_ids)) {
                self::errorNotExist('hologram_id');
                return false;
            }
        }
        
        //Get current nail's holograms
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_holograms_nail = self::find('all', $options);  
       
        //Get current nail's hologram' id
        $array_holograms_nail_id = array ();
        
        foreach ($current_holograms_nail as $obj_hologram_nail) {
            $array_holograms_nail_id[] = $obj_hologram_nail->get('hologram_id');
        }
        
        //New hologram's id
        $new_hologram_nails = array_diff($hologram_ids, $array_holograms_nail_id);
        
        //Holograms's id to remove
        $remove_hologram_nails = array_diff($array_holograms_nail_id, $hologram_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_hologram_nails as $new_hologram_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'hologram_id' => $new_hologram_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_hologram_nails as $remove_hologram_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'hologram_id' => $remove_hologram_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }            
}
