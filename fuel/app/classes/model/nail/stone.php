<?php

class Model_Nail_Stone extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'stone_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_stones';
    
    /**
     * Get list of nail's stones
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['stone_id'])) {
            $query->where('stone_id', "{$param['stone_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's stones
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['stone_id'])) {
            $query->where('stone_id', "{$param['stone_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's stone
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['stone_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('stone_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'stone_id' => $param['stone_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_stones = self::find('first', $options);       
        if (empty($nail_stones)) {
            $nail_stones = new self;           
        }        
        $nail_stones->set('stone_id', $param['stone_id']);
        $nail_stones->set('nail_id', $param['nail_id']);       
        $nail_stones->set('disable', $param['disable']);       
        if ($nail_stones->save()) {
            if (empty($nail_stones->id)) {
                $nail_stones->id = self::cached_object($nail_stones)->_original['id'];
            }
            return !empty($nail_stones->id) ? $nail_stones->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a stone
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_stone_id($param)
    {
        //Design id
        $stone_id = $param['stone_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: stone's id
        if (empty(Model_Design::find($stone_id))) {
            self::errorNotExist('stone_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current stone's nails
        $options['where'] = array(
            'stone_id' => $stone_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_stones = self::find('all', $options);  
       
        //Get current stone's nails' id
        $array_nail_stones_id = array ();
        
        foreach ($current_nail_stones as $obj_nail_stone) {
            $array_nail_stones_id[] = $obj_nail_stone->get('nail_id');
        }
        
        //New nails's id
        $new_nail_stones = array_diff($nail_ids, $array_nail_stones_id);
        
        //Nails's id to remove
        $remove_nail_stones = array_diff($array_nail_stones_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_stones as $new_nail_id) {
            $data[] = array (
                'stone_id' => $stone_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_stones as $remove_nail_id) {
            $data[] = array (
                'stone_id' => $stone_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update stones for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of stone's id
        $stone_ids = !empty($param['stone_id']) ? 
                array_filter(array_unique(explode(',', $param['stone_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: stone's ids
        if (!empty($stone_ids)) {
            //Count stone's ids from db
            $count_stone = Model_Stone::query()
            ->where('id', 'in', $stone_ids)
            ->count();        

            //Do compare
            if ($count_stone != count($stone_ids)) {
                self::errorNotExist('stone_id');
                return false;
            }
        }
        
        //Get current nail's stones
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_stones_nail = self::find('all', $options);  
       
        //Get current nail's stone' id
        $array_stones_nail_id = array ();
        
        foreach ($current_stones_nail as $obj_stone_nail) {
            $array_stones_nail_id[] = $obj_stone_nail->get('stone_id');
        }
        
        //New stone's id
        $new_stone_nails = array_diff($stone_ids, $array_stones_nail_id);
        
        //Designs's id to remove
        $remove_stone_nails = array_diff($array_stones_nail_id, $stone_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_stone_nails as $new_stone_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'stone_id' => $new_stone_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_stone_nails as $remove_stone_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'stone_id' => $remove_stone_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
}
