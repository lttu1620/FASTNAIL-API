<?php

class Model_Nail_Bullion extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'bullion_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_bullions';
        
    /**
     * Get list of nail's bullions
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Bullion Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['bullion_id'])) {
            $query->where('bullion_id', "{$param['bullion_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's bullions
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Bullion Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['bullion_id'])) {
            $query->where('bullion_id', "{$param['bullion_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's bullion
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Bullion Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['bullion_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('bullion_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'bullion_id' => $param['bullion_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_bullions = self::find('first', $options);       
        if (empty($nail_bullions)) {
            $nail_bullions = new self;           
        }        
        $nail_bullions->set('bullion_id', $param['bullion_id']);
        $nail_bullions->set('nail_id', $param['nail_id']);       
        $nail_bullions->set('disable', $param['disable']);       
        if ($nail_bullions->save()) {
            if (empty($nail_bullions->id)) {
                $nail_bullions->id = self::cached_object($nail_bullions)->_original['id'];
            }
            return !empty($nail_bullions->id) ? $nail_bullions->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a bullion
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_bullion_id($param)
    {
        //Bullion id
        $bullion_id = $param['bullion_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: bullion's id
        if (empty(Model_Bullion::find($bullion_id))) {
            self::errorNotExist('bullion_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current bullion's nails
        $options['where'] = array(
            'bullion_id' => $bullion_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_bullions = self::find('all', $options);  
       
        //Get current bullion's nails' id
        $array_nail_bullions_id = array ();
        
        foreach ($current_nail_bullions as $obj_nail_bullion) {
            $array_nail_bullions_id[] = $obj_nail_bullion->get('nail_id');
        }
        
        //New nails's id
        $new_nail_bullions = array_diff($nail_ids, $array_nail_bullions_id);
        
        //Nails's id to remove
        $remove_nail_bullions = array_diff($array_nail_bullions_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_bullions as $new_nail_id) {
            $data[] = array (
                'bullion_id' => $bullion_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_bullions as $remove_nail_id) {
            $data[] = array (
                'bullion_id' => $bullion_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update bullions for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of bullion's id
        $bullion_ids = !empty($param['bullion_id']) ? 
                array_filter(array_unique(explode(',', $param['bullion_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: bullion's ids
        if (!empty($bullion_ids)) {
            //Count bullion's ids from db
            $count_bullion = Model_Bullion::query()
            ->where('id', 'in', $bullion_ids)
            ->count();        

            //Do compare
            if ($count_bullion != count($bullion_ids)) {
                self::errorNotExist('bullion_id');
                return false;
            }
        }
        
        //Get current nail's bullions
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_bullions_nail = self::find('all', $options);  
       
        //Get current nail's bullion' id
        $array_bullions_nail_id = array ();
        
        foreach ($current_bullions_nail as $obj_bullion_nail) {
            $array_bullions_nail_id[] = $obj_bullion_nail->get('bullion_id');
        }
        
        //New bullion's id
        $new_bullion_nails = array_diff($bullion_ids, $array_bullions_nail_id);
        
        //Bullions's id to remove
        $remove_bullion_nails = array_diff($array_bullions_nail_id, $bullion_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_bullion_nails as $new_bullion_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'bullion_id' => $new_bullion_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_bullion_nails as $remove_bullion_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'bullion_id' => $remove_bullion_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    

}
