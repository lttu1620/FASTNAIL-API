<?php

class Model_Nail_Tag extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'tag_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_tags';
    /**
     * Get list of nail's tags
     *
     * @author Hoang Gia Thong
     * @param array array $param Input data.
     * @return bool|int Nail Tag Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by tag
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['tag_id'])) {
            $query->where('tag_id', "{$param['tag_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's tags
     *
     * @author Hoang Gia Thong
     * @param array array $param Input data.
     * @return bool|int Nail Tag Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . ".id",
                self::$_table_name . ".nail_id",
                self::$_table_name . ".tag_id",
                "tags.name"
            )
            ->from(self::$_table_name)
            ->join('tags')
            ->on(self::$_table_name . '.tag_id', '=', 'tags.id');
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }        
        if (!empty($param['tag_id'])) {
            $query->where('tag_id', "{$param['tag_id']}");
        }
        $query->where(self::$_table_name . '.disable', '0');
        $query->where('tags.disable', '0');
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Add or update nail's tag
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Tag Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['tag_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('tag_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'tag_id' => $param['tag_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_tags = self::find('first', $options);       
        if (empty($nail_tags)) {
            $nail_tags = new self;           
        }        
        $nail_tags->set('tag_id', $param['tag_id']);
        $nail_tags->set('nail_id', $param['nail_id']);       
        $nail_tags->set('disable', $param['disable']);       
        if ($nail_tags->save()) {
            if (empty($nail_tags->id)) {
                $nail_tags->id = self::cached_object($nail_tags)->_original['id'];
            }
            return !empty($nail_tags->id) ? $nail_tags->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a tag
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_tag_id($param)
    {
        //Keyword id
        $tag_id = $param['tag_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: tag's id
        if (empty(Model_Tag::find($tag_id))) {
            self::errorNotExist('tag_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current tag's nails
        $options['where'] = array(
            'tag_id' => $tag_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_tags = self::find('all', $options);  
       
        //Get current tag's nails' id
        $array_nail_tags_id = array ();
        
        foreach ($current_nail_tags as $obj_nail_tag) {
            $array_nail_tags_id[] = $obj_nail_tag->get('nail_id');
        }
        
        //New nails's id
        $new_nail_tags = array_diff($nail_ids, $array_nail_tags_id);
        
        //Nails's id to remove
        $remove_nail_tags = array_diff($array_nail_tags_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_tags as $new_nail_id) {
            $data[] = array (
                'tag_id' => $tag_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_tags as $remove_nail_id) {
            $data[] = array (
                'tag_id' => $tag_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update tags for a nail
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        // attribute field name
        $attrFieldName = 'tag_id';
        
        // check exist nail
        $options['where'] = array(
            'id' => $param['nail_id'],
            'disable' => '0'
        );
        $nail = Model_Nail::find('first', $options);
        if (empty($nail)) {
            static::errorNotExist('nail_id');
            return false;
        }
      
        // disable all if attr is empty
        if (empty($param[$attrFieldName])) {
            return DB::update(self::$_table_name)
                ->value('disable', '1')
                ->where('nail_id', '=', $nail->get('id'))
                ->execute();            
        }
        
        // get existing attrs
        $attrs = \Lib\Arr::key_values(
                DB::select()
                    ->from(self::$_table_name)
                    ->where('nail_id', $nail->get('id'))
                    ->execute()
                    ->as_array(),
                $attrFieldName
            );        
        
        // prepare data for insert/update
        $attrIds = explode(',', $param[$attrFieldName]);        
        $dataUpdate = array();
        foreach ($attrIds as $attrId) {
            if (!empty($attrId)) {                 
                $dataUpdate[] = array(                   
                    'id' => !empty($attrs[$attrId]['id']) ? $attrs[$attrId]['id'] : 0,
                    'nail_id' => $nail->get('id'),
                    $attrFieldName => $attrId,
                    'disable' => 0,
                );               
            }
        }        
        foreach ($attrs as $attrId => $attr) {
            if (!in_array($attrId, $attrIds)) { 
                $dataUpdate[] = array(                   
                    'id' => $attr['id'],
                    'nail_id' => $nail->get('id'),
                    $attrFieldName => $attrId,
                    'disable' => 1,
                );
            }
        }
        
        // execute insert/update
        if (!empty($dataUpdate) && !parent::batchInsert(
            self::$_table_name,
            $dataUpdate,
            array('disable' => DB::expr('VALUES(disable)')),
            false
        )) {
            \LogLib::warning('Can not update ' . self::$_table_name, __METHOD__, $param);
            return false;
        }
        return true;
    }
    
}
