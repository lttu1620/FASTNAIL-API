<?php

class Model_Nail_Flower extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'flower_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_flowers';
        
    /**
     * Get list of nail's flowers
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Flower Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['flower_id'])) {
            $query->where('flower_id', "{$param['flower_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's flowers
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Flower Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['flower_id'])) {
            $query->where('flower_id', "{$param['flower_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's flower
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Flower Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['flower_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('flower_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'flower_id' => $param['flower_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_flowers = self::find('first', $options);       
        if (empty($nail_flowers)) {
            $nail_flowers = new self;           
        }        
        $nail_flowers->set('flower_id', $param['flower_id']);
        $nail_flowers->set('nail_id', $param['nail_id']);       
        $nail_flowers->set('disable', $param['disable']);       
        if ($nail_flowers->save()) {
            if (empty($nail_flowers->id)) {
                $nail_flowers->id = self::cached_object($nail_flowers)->_original['id'];
            }
            return !empty($nail_flowers->id) ? $nail_flowers->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a flower
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_flower_id($param)
    {
        //Flower id
        $flower_id = $param['flower_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: flower's id
        if (empty(Model_Flower::find($flower_id))) {
            self::errorNotExist('flower_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current flower's nails
        $options['where'] = array(
            'flower_id' => $flower_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_flowers = self::find('all', $options);  
       
        //Get current flower's nails' id
        $array_nail_flowers_id = array ();
        
        foreach ($current_nail_flowers as $obj_nail_flower) {
            $array_nail_flowers_id[] = $obj_nail_flower->get('nail_id');
        }
        
        //New nails's id
        $new_nail_flowers = array_diff($nail_ids, $array_nail_flowers_id);
        
        //Nails's id to remove
        $remove_nail_flowers = array_diff($array_nail_flowers_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_flowers as $new_nail_id) {
            $data[] = array (
                'flower_id' => $flower_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_flowers as $remove_nail_id) {
            $data[] = array (
                'flower_id' => $flower_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update flowers for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of flower's id
        $flower_ids = !empty($param['flower_id']) ? 
                array_filter(array_unique(explode(',', $param['flower_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: flower's ids
        if (!empty($flower_ids)) {
            //Count flower's ids from db
            $count_flower = Model_Flower::query()
            ->where('id', 'in', $flower_ids)
            ->count();        

            //Do compare
            if ($count_flower != count($flower_ids)) {
                self::errorNotExist('flower_id');
                return false;
            }
        }
        
        //Get current nail's flowers
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_flowers_nail = self::find('all', $options);  
       
        //Get current nail's flower' id
        $array_flowers_nail_id = array ();
        
        foreach ($current_flowers_nail as $obj_flower_nail) {
            $array_flowers_nail_id[] = $obj_flower_nail->get('flower_id');
        }
        
        //New flower's id
        $new_flower_nails = array_diff($flower_ids, $array_flowers_nail_id);
        
        //Flowers's id to remove
        $remove_flower_nails = array_diff($array_flowers_nail_id, $flower_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_flower_nails as $new_flower_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'flower_id' => $new_flower_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_flower_nails as $remove_flower_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'flower_id' => $remove_flower_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    

}
