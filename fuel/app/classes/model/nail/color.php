<?php

class Model_Nail_Color extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'color_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_colors';
    
    /**
     * Get list of nail's color
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array list of nail's colors
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['color_id'])) {
            $query->where('color_id', "{$param['color_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }    
    
    /**
     * Get all of nail's colors
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of nail's colors
     */
    public static function get_all($param)
    { 
        $query = DB::select(                
                self::$_table_name . '.id',              
                self::$_table_name . '.nail_id',              
                self::$_table_name . '.color_id',               
                'colors.name'
            )
            ->from(self::$_table_name)
            ->join('colors')
            ->on(self::$_table_name . '.color_id', '=', 'colors.id')
            ->where(self::$_table_name . '.disable', '0');
        // filter by keyword
        if (!empty($param['nail_id'])) {
            if (!is_array($param['nail_id'])) {
                $param['nail_id'] = array($param['nail_id']);
            }
            $query->where('nail_id', 'IN', $param['nail_id']);
        }        
        if (!empty($param['color_id'])) {   
            if (!is_array($param['color_id'])) {
                $param['color_id'] = array($param['color_id']);
            }
            $query->where('color_id', 'IN', $param['color_id']);          
        }
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Add or update nail's color
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Color Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['color_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('color_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'color_id' => $param['color_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_colors = self::find('first', $options);       
        if (empty($nail_colors)) {
            $nail_colors = new self;           
        }        
        $nail_colors->set('color_id', $param['color_id']);
        $nail_colors->set('nail_id', $param['nail_id']);       
        $nail_colors->set('disable', $param['disable']);       
        if ($nail_colors->save()) {
            if (empty($nail_colors->id)) {
                $nail_colors->id = self::cached_object($nail_colors)->_original['id'];
            }
            return !empty($nail_colors->id) ? $nail_colors->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a color
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_color_id($param)
    {
        //Color id
        $color_id = $param['color_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: color's id
        if (empty(Model_Color::find($color_id))) {
            self::errorNotExist('color_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current color's nails
        $options['where'] = array(
            'color_id' => $color_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_colors = self::find('all', $options);  
       
        //Get current color's nails' id
        $array_nail_colors_id = array ();
        
        foreach ($current_nail_colors as $obj_nail_color) {
            $array_nail_colors_id[] = $obj_nail_color->get('nail_id');
        }
        
        //New nails's id
        $new_nail_colors = array_diff($nail_ids, $array_nail_colors_id);
        
        //Nails's id to remove
        $remove_nail_colors = array_diff($array_nail_colors_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_colors as $new_nail_id) {
            $data[] = array (
                'color_id' => $color_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_colors as $remove_nail_id) {
            $data[] = array (
                'color_id' => $color_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update colors for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of color's id
        $color_ids = !empty($param['color_id']) ? 
                array_filter(array_unique(explode(',', $param['color_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: color's ids
        if (!empty($color_ids)) {
            //Count color's ids from db
            $count_color = Model_Color::query()
            ->where('id', 'in', $color_ids)
            ->count();        

            //Do compare
            if ($count_color != count($color_ids)) {
                self::errorNotExist('color_id');
                return false;
            }
        }
        
        //Get current nail's colors
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_colors_nail = self::find('all', $options);  
       
        //Get current nail's color' id
        $array_colors_nail_id = array ();
        
        foreach ($current_colors_nail as $obj_color_nail) {
            $array_colors_nail_id[] = $obj_color_nail->get('color_id');
        }
        
        //New color's id
        $new_color_nails = array_diff($color_ids, $array_colors_nail_id);
        
        //Colors's id to remove
        $remove_color_nails = array_diff($array_colors_nail_id, $color_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_color_nails as $new_color_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'color_id' => $new_color_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_color_nails as $remove_color_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'color_id' => $remove_color_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
}
