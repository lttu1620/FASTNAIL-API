<?php

class Model_Nail_Design extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'design_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_designs';
    
    /**
     * Get list of nail's designs
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['design_id'])) {
            $query->where('design_id', "{$param['design_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's designs
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select(                
                self::$_table_name . '.id',              
                self::$_table_name . '.nail_id',              
                self::$_table_name . '.design_id',              
                'designs.name'
            )
            ->from(self::$_table_name)
            ->join('designs')
            ->on(self::$_table_name . '.design_id', '=', 'designs.id')
            ->where(self::$_table_name . '.disable', '0');
        // filter by keyword
        if (!empty($param['nail_id'])) {
            if (!is_array($param['nail_id'])) {
                $param['nail_id'] = array($param['nail_id']);
            }
            $query->where('nail_id', 'IN', $param['nail_id']);
        }        
        if (!empty($param['design_id'])) {
             if (!is_array($param['design_id'])) {
                $param['design_id'] = array($param['design_id']);
            }
            $query->where('design_id', 'IN', $param['design_id']);
        }
        $data = $query->execute()->as_array();
        return $data;   
    }
    
    /**
     * Add or update nail's design
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Design Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['design_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('design_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'design_id' => $param['design_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_designs = self::find('first', $options);       
        if (empty($nail_designs)) {
            $nail_designs = new self;           
        }        
        $nail_designs->set('design_id', $param['design_id']);
        $nail_designs->set('nail_id', $param['nail_id']);       
        $nail_designs->set('disable', $param['disable']);       
        if ($nail_designs->save()) {
            if (empty($nail_designs->id)) {
                $nail_designs->id = self::cached_object($nail_designs)->_original['id'];
            }
            return !empty($nail_designs->id) ? $nail_designs->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a design
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_design_id($param)
    {
        //Design id
        $design_id = $param['design_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: design's id
        if (empty(Model_Design::find($design_id))) {
            self::errorNotExist('design_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current design's nails
        $options['where'] = array(
            'design_id' => $design_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_designs = self::find('all', $options);  
       
        //Get current design's nails' id
        $array_nail_designs_id = array ();
        
        foreach ($current_nail_designs as $obj_nail_design) {
            $array_nail_designs_id[] = $obj_nail_design->get('nail_id');
        }
        
        //New nails's id
        $new_nail_designs = array_diff($nail_ids, $array_nail_designs_id);
        
        //Nails's id to remove
        $remove_nail_designs = array_diff($array_nail_designs_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_designs as $new_nail_id) {
            $data[] = array (
                'design_id' => $design_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_designs as $remove_nail_id) {
            $data[] = array (
                'design_id' => $design_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update designs for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of design's id
        $design_ids = !empty($param['design_id']) ? 
                array_filter(array_unique(explode(',', $param['design_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: design's ids
        if (!empty($design_ids)) {
            //Count design's ids from db
            $count_design = Model_Design::query()
            ->where('id', 'in', $design_ids)
            ->count();        

            //Do compare
            if ($count_design != count($design_ids)) {
                self::errorNotExist('design_id');
                return false;
            }
        }
        
        //Get current nail's designs
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_designs_nail = self::find('all', $options);  
       
        //Get current nail's design' id
        $array_designs_nail_id = array ();
        
        foreach ($current_designs_nail as $obj_design_nail) {
            $array_designs_nail_id[] = $obj_design_nail->get('design_id');
        }
        
        //New design's id
        $new_design_nails = array_diff($design_ids, $array_designs_nail_id);
        
        //Designs's id to remove
        $remove_design_nails = array_diff($array_designs_nail_id, $design_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_design_nails as $new_design_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'design_id' => $new_design_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_design_nails as $remove_design_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'design_id' => $remove_design_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }

}
