<?php

class Model_Nail_Shell extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'shell_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_shells';

        
    /**
     * Get list of nail's shells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Shell Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['shell_id'])) {
            $query->where('shell_id', "{$param['shell_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's shells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Shell Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['shell_id'])) {
            $query->where('shell_id', "{$param['shell_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's shell
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Shell Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['shell_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('shell_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'shell_id' => $param['shell_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_shells = self::find('first', $options);       
        if (empty($nail_shells)) {
            $nail_shells = new self;           
        }        
        $nail_shells->set('shell_id', $param['shell_id']);
        $nail_shells->set('nail_id', $param['nail_id']);       
        $nail_shells->set('disable', $param['disable']);       
        if ($nail_shells->save()) {
            if (empty($nail_shells->id)) {
                $nail_shells->id = self::cached_object($nail_shells)->_original['id'];
            }
            return !empty($nail_shells->id) ? $nail_shells->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a shell
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_shell_id($param)
    {
        //Shell id
        $shell_id = $param['shell_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: shell's id
        if (empty(Model_Shell::find($shell_id))) {
            self::errorNotExist('shell_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current shell's nails
        $options['where'] = array(
            'shell_id' => $shell_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_shells = self::find('all', $options);  
       
        //Get current shell's nails' id
        $array_nail_shells_id = array ();
        
        foreach ($current_nail_shells as $obj_nail_shell) {
            $array_nail_shells_id[] = $obj_nail_shell->get('nail_id');
        }
        
        //New nails's id
        $new_nail_shells = array_diff($nail_ids, $array_nail_shells_id);
        
        //Nails's id to remove
        $remove_nail_shells = array_diff($array_nail_shells_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_shells as $new_nail_id) {
            $data[] = array (
                'shell_id' => $shell_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_shells as $remove_nail_id) {
            $data[] = array (
                'shell_id' => $shell_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update shells for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of shell's id
        $shell_ids = !empty($param['shell_id']) ? 
                array_filter(array_unique(explode(',', $param['shell_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: shell's ids
        if (!empty($shell_ids)) {
            //Count shell's ids from db
            $count_shell = Model_Shell::query()
            ->where('id', 'in', $shell_ids)
            ->count();        

            //Do compare
            if ($count_shell != count($shell_ids)) {
                self::errorNotExist('shell_id');
                return false;
            }
        }
        
        //Get current nail's shells
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_shells_nail = self::find('all', $options);  
       
        //Get current nail's shell' id
        $array_shells_nail_id = array ();
        
        foreach ($current_shells_nail as $obj_shell_nail) {
            $array_shells_nail_id[] = $obj_shell_nail->get('shell_id');
        }
        
        //New shell's id
        $new_shell_nails = array_diff($shell_ids, $array_shells_nail_id);
        
        //Shells's id to remove
        $remove_shell_nails = array_diff($array_shells_nail_id, $shell_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_shell_nails as $new_shell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'shell_id' => $new_shell_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_shell_nails as $remove_shell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'shell_id' => $remove_shell_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
