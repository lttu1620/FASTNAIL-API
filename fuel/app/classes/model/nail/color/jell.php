<?php

class Model_Nail_Color_Jell extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'color_jell_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_color_jells';

    /**
     * Get list of nail's color jells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail ColorJell Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['color_jell_id'])) {
            $query->where('color_jell_id', "{$param['color_jell_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's color jells
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail ColorJell Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select(            
                array(self::$_table_name . '.nail_id', 'nail_id'),
                array('color_jells.id', 'id'),
                array('color_jells.name', 'name'),
                array('color_jells.icon', 'icon')              
            )
            ->from(self::$_table_name)
            ->join('color_jells')
            ->on(self::$_table_name . '.color_jell_id', '=', 'color_jells.id')
            ->where(self::$_table_name . '.disable', '0')
            ->where('color_jells.disable', '0');
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }        
        if (!empty($param['color_jell_id'])) {
            $query->where('color_jell_id', "{$param['color_jell_id']}");
        }
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Add or update nail's color jell
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail ColorJell Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['color_jell_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('color_jell_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'color_jell_id' => $param['color_jell_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_color_jells = self::find('first', $options);       
        if (empty($nail_color_jells)) {
            $nail_color_jells = new self;           
        }        
        $nail_color_jells->set('color_jell_id', $param['color_jell_id']);
        $nail_color_jells->set('nail_id', $param['nail_id']);       
        $nail_color_jells->set('disable', $param['disable']);       
        if ($nail_color_jells->save()) {
            if (empty($nail_color_jells->id)) {
                $nail_color_jells->id = self::cached_object($nail_color_jells)->_original['id'];
            }
            return !empty($nail_color_jells->id) ? $nail_color_jells->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a color jell
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_color_jell_id($param)
    {
        //ColorJell id
        $color_jell_id = $param['color_jell_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: color jell's id
        if (empty(Model_Color_Jell::find($color_jell_id))) {
            self::errorNotExist('color_jell_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current color jell's nails
        $options['where'] = array(
            'color_jell_id' => $color_jell_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_color_jells = self::find('all', $options);  
       
        //Get current color jell's nails' id
        $array_nail_color_jells_id = array ();
        
        foreach ($current_nail_color_jells as $obj_nail_color_jell) {
            $array_nail_color_jells_id[] = $obj_nail_color_jell->get('nail_id');
        }
        
        //New nails's id
        $new_nail_color_jells = array_diff($nail_ids, $array_nail_color_jells_id);
        
        //Nails's id to remove
        $remove_nail_color_jells = array_diff($array_nail_color_jells_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_color_jells as $new_nail_id) {
            $data[] = array (
                'color_jell_id' => $color_jell_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_color_jells as $remove_nail_id) {
            $data[] = array (
                'color_jell_id' => $color_jell_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update color jells for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of color jell's id
        $color_jell_ids = !empty($param['color_jell_id']) ? 
                array_filter(array_unique(explode(',', $param['color_jell_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: color jell's ids
        if (!empty($color_jell_ids)) {
            //Count color jell's ids from db
            $count_color_jell = Model_Color_Jell::query()
            ->where('id', 'in', $color_jell_ids)
            ->count();        

            //Do compare
            if ($count_color_jell != count($color_jell_ids)) {
                self::errorNotExist('color_jell_id');
                return false;
            }
        }
        
        //Get current nail's color jells
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_color_jells_nail = self::find('all', $options);  
       
        //Get current nail's color jell' id
        $array_color_jells_nail_id = array ();
        
        foreach ($current_color_jells_nail as $obj_color_jell_nail) {
            $array_color_jells_nail_id[] = $obj_color_jell_nail->get('color_jell_id');
        }
        
        //New color jell's id
        $new_color_jell_nails = array_diff($color_jell_ids, $array_color_jells_nail_id);
        
        //Color_Jells's id to remove
        $remove_color_jell_nails = array_diff($array_color_jells_nail_id, $color_jell_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_color_jell_nails as $new_color_jell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'color_jell_id' => $new_color_jell_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_color_jell_nails as $remove_color_jell_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'color_jell_id' => $remove_color_jell_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
