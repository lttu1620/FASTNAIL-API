<?php

class Model_Nail_Powder extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'powder_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_powders';

        
    /**
     * Get list of nail's powders
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Powder Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['powder_id'])) {
            $query->where('powder_id', "{$param['powder_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's powders
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Powder Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['powder_id'])) {
            $query->where('powder_id', "{$param['powder_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's powder
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Powder Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['powder_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('powder_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'powder_id' => $param['powder_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_powders = self::find('first', $options);       
        if (empty($nail_powders)) {
            $nail_powders = new self;           
        }        
        $nail_powders->set('powder_id', $param['powder_id']);
        $nail_powders->set('nail_id', $param['nail_id']);       
        $nail_powders->set('disable', $param['disable']);       
        if ($nail_powders->save()) {
            if (empty($nail_powders->id)) {
                $nail_powders->id = self::cached_object($nail_powders)->_original['id'];
            }
            return !empty($nail_powders->id) ? $nail_powders->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a powder
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_powder_id($param)
    {
        //Powder id
        $powder_id = $param['powder_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: powder's id
        if (empty(Model_Powder::find($powder_id))) {
            self::errorNotExist('powder_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current powder's nails
        $options['where'] = array(
            'powder_id' => $powder_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_powders = self::find('all', $options);  
       
        //Get current powder's nails' id
        $array_nail_powders_id = array ();
        
        foreach ($current_nail_powders as $obj_nail_powder) {
            $array_nail_powders_id[] = $obj_nail_powder->get('nail_id');
        }
        
        //New nails's id
        $new_nail_powders = array_diff($nail_ids, $array_nail_powders_id);
        
        //Nails's id to remove
        $remove_nail_powders = array_diff($array_nail_powders_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_powders as $new_nail_id) {
            $data[] = array (
                'powder_id' => $powder_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_powders as $remove_nail_id) {
            $data[] = array (
                'powder_id' => $powder_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update powders for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of powder's id
        $powder_ids = !empty($param['powder_id']) ? 
                array_filter(array_unique(explode(',', $param['powder_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: powder's ids
        if (!empty($powder_ids)) {
            //Count powder's ids from db
            $count_powder = Model_Powder::query()
            ->where('id', 'in', $powder_ids)
            ->count();        

            //Do compare
            if ($count_powder != count($powder_ids)) {
                self::errorNotExist('powder_id');
                return false;
            }
        }
        
        //Get current nail's powders
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_powders_nail = self::find('all', $options);  
       
        //Get current nail's powder' id
        $array_powders_nail_id = array ();
        
        foreach ($current_powders_nail as $obj_powder_nail) {
            $array_powders_nail_id[] = $obj_powder_nail->get('powder_id');
        }
        
        //New powder's id
        $new_powder_nails = array_diff($powder_ids, $array_powders_nail_id);
        
        //Powders's id to remove
        $remove_powder_nails = array_diff($array_powders_nail_id, $powder_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_powder_nails as $new_powder_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'powder_id' => $new_powder_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_powder_nails as $remove_powder_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'powder_id' => $remove_powder_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
