<?php

class Model_Nail_Scene extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'nail_id',
		'scene_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_scenes';

    /**
     * Get list of nail's scenes
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Scene Id or False
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['scene_id'])) {
            $query->where('scene_id', "{$param['scene_id']}");
        }
        
        $query->where('disable', 0);//TODO define
        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }  
    
    /**
     * Get all of nail's scenes
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return bool|int Nail Scene Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', "{$param['nail_id']}");
        }
        
        if (!empty($param['scene_id'])) {
            $query->where('scene_id', "{$param['scene_id']}");
        }
        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where('disable', $param['disable']);
        
        // get data
        $data = $query->execute()->as_array();
        
        //return data
        return array($data);
    }
    
    /**
     * Add or update nail's scene
     *
     * @author Lai Hac Thai
     * @param array array $param Input data.
     * @return bool|int Nail Scene Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['scene_id']) || empty($param['nail_id'])) {
            self::errorParamInvalid('scene_id_or_nail_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'scene_id' => $param['scene_id'],
            'nail_id' => $param['nail_id'],           
        );       
        $nail_scenes = self::find('first', $options);       
        if (empty($nail_scenes)) {
            $nail_scenes = new self;           
        }        
        $nail_scenes->set('scene_id', $param['scene_id']);
        $nail_scenes->set('nail_id', $param['nail_id']);       
        $nail_scenes->set('disable', $param['disable']);       
        if ($nail_scenes->save()) {
            if (empty($nail_scenes->id)) {
                $nail_scenes->id = self::cached_object($nail_scenes)->_original['id'];
            }
            return !empty($nail_scenes->id) ? $nail_scenes->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update nails for a scene
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_scene_id($param)
    {
        //Scene id
        $scene_id = $param['scene_id'];
        
        //Array of nail's id
        $nail_ids = !empty($param['nail_id']) ? 
                array_filter(array_unique(explode(',', $param['nail_id'])))
                : array ();
        
        //Check exist: scene's id
        if (empty(Model_Scene::find($scene_id))) {
            self::errorNotExist('scene_id');
            return false;
        }
        
        //Check exist: nail's ids
        if (!empty($nail_ids)) {
            //Count nail's ids from db
            $count_nail = Model_Nail::query()
            ->where('id', 'in', $nail_ids)
            ->count();        

            //Do compare
            if ($count_nail != count($nail_ids)) {
                self::errorNotExist('nail_id');
                return false;
            }
        }
        
        //Get current scene's nails
        $options['where'] = array(
            'scene_id' => $scene_id,
            'disable' => 0// TODO define
        ); 
        $current_nail_scenes = self::find('all', $options);  
       
        //Get current scene's nails' id
        $array_nail_scenes_id = array ();
        
        foreach ($current_nail_scenes as $obj_nail_scene) {
            $array_nail_scenes_id[] = $obj_nail_scene->get('nail_id');
        }
        
        //New nails's id
        $new_nail_scenes = array_diff($nail_ids, $array_nail_scenes_id);
        
        //Nails's id to remove
        $remove_nail_scenes = array_diff($array_nail_scenes_id, $nail_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nail_scenes as $new_nail_id) {
            $data[] = array (
                'scene_id' => $scene_id,
                'nail_id' => $new_nail_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nail_scenes as $remove_nail_id) {
            $data[] = array (
                'scene_id' => $scene_id,
                'nail_id' => $remove_nail_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Add or update scenes for a nail
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_nail_id($param)
    {
        //Nail id
        $nail_id = $param['nail_id'];
        
        //Array of scene's id
        $scene_ids = !empty($param['scene_id']) ? 
                array_filter(array_unique(explode(',', $param['scene_id'])))
                : array ();
        
        //Check exist: nail's id
        if (empty(Model_Nail::find($nail_id))) {
            self::errorNotExist('nail_id');
            return false;
        }
        
        //Check exist: scene's ids
        if (!empty($scene_ids)) {
            //Count scene's ids from db
            $count_scene = Model_Scene::query()
            ->where('id', 'in', $scene_ids)
            ->count();        

            //Do compare
            if ($count_scene != count($scene_ids)) {
                self::errorNotExist('scene_id');
                return false;
            }
        }
        
        //Get current nail's scenes
        $options['where'] = array(
            'nail_id' => $nail_id,
            'disable' => 0// TODO define
        ); 
        $current_scenes_nail = self::find('all', $options);  
       
        //Get current nail's scene' id
        $array_scenes_nail_id = array ();
        
        foreach ($current_scenes_nail as $obj_scene_nail) {
            $array_scenes_nail_id[] = $obj_scene_nail->get('scene_id');
        }
        
        //New scene's id
        $new_scene_nails = array_diff($scene_ids, $array_scenes_nail_id);
        
        //Scenes's id to remove
        $remove_scene_nails = array_diff($array_scenes_nail_id, $scene_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_scene_nails as $new_scene_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'scene_id' => $new_scene_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_scene_nails as $remove_scene_id) {
            $data[] = array (
                'nail_id' => $nail_id,
                'scene_id' => $remove_scene_id,
                'disable' => 1//TODO define
            );
        }
        
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }    
}
