<?php

/**
 * Any query in Model Nail Rank Log
 *
 * @package Model
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Nail_Rank_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'admin_id',
        'nail_id',
        'old_rank',
        'rank',
        'created'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'nail_rank_logs';

    /**
     * Add info for Nail Rank Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Nail Rank Log id or false if error
     */
    public static function add($param)
    {
        $log = new self;
        $log->set('admin_id', $param['admin_id']);
        $log->set('nail_id', $param['nail_id']);
        $log->set('old_rank', DB::expr("(SELECT rank from nails where id = {$param['nail_id']})"));
        $log->set('rank', $param['rank']);
        if ($log->create()) {
            $log->id = self::cached_object($log)->_original['id'];
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list Nail Rank Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nail Rank Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            'nails.photo_cd',
            'nails.photo_url',
            'admins.name'
        )
            ->from(self::$_table_name)
            ->join('nails')
            ->on(self::$_table_name . '.nail_id', '=', 'nails.id')
            ->join('admins')
            ->on(self::$_table_name . '.admin_id', '=', 'admins.id');
        // filter by keyword
        if (!empty($param['nail_id'])) {
            $query->where('nail_id', '=', $param['nail_id']);
        }
        if (!empty($param['photo_cd'])) {
            $query->where('nails.photo_cd', '=', $param['photo_cd']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
