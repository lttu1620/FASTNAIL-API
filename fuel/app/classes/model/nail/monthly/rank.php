<?php

/**
 * Any query in Model Nail Monthly Rank
 *
 * @package Model
 * @created 2015-06-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Nail_Monthly_Rank extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'nail_id',
        'year',
        'month',
        'rank',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'nail_monthly_ranks';

    /**
     * Update rank nail monthly
     *
     * @author Le Tuan Tu
     * @return bool true if success, false if error
     */
    public static function update_rank_nail_monthly()
    {
        $nails = self::count_order_current_month();
        $rank = array();
        $currentRank = 0;
        foreach ($nails as $i => $nail) {
            if (!isset($rank[$nail['order_count']])) {
                $currentRank++;
                $rank[$nail['order_count']] = $currentRank;
            }
        }
        $dataUpdate = array();
        foreach ($nails as $i => $nail) {
            $dataUpdate[] = array(
                'nail_id' => $nail['nail_id'],
                'year'    => date('Y'),
                'month'   => date('n'),
                'rank'    => $rank[$nail['order_count']]
            );
        }

        if (!parent::batchInsert(
            'nail_monthly_ranks',
            $dataUpdate,
            array(
                'rank' => DB::expr('VALUES(`rank`)')
            ),
            false
        )
        ) {
            \LogLib::warning('Can not insert/update nail_monthly_ranks', __METHOD__);
            return false;
        }
        return true;
    }

    /**
     * Count order nail in current month
     *
     * @author Le Tuan Tu
     * @return array List nail with order count by this nail
     */
    public static function count_order_current_month()
    {
        $query = DB::select(
            'nail_id',
            DB::expr("
                COUNT(order_id) AS order_count
            ")
        )
            ->from('order_nails')
            ->join('orders')
            ->on('order_nails.order_id', '=', 'orders.id')
            ->where('order_nails.disable', '=', '0')
            ->where('orders.disable', '=', '0')
            ->where(DB::expr("
                IFNULL(orders.is_cancel, 0) = 0
            "))
            ->where(DB::expr("
                IFNULL(orders.is_autocancel, 0) = 0
            "))
            ->where(DB::expr("
                order_nails.created >= UNIX_TIMESTAMP(DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-01'))
            "))
            ->where(DB::expr("
                order_nails.created < UNIX_TIMESTAMP(CURRENT_DATE() + INTERVAL 1 DAY)
            "))
            ->group_by('nail_id')
            ->order_by('order_count', 'DESC');
        $data = $query->execute()->as_array();
        return empty($data) ? array() : $data;
    }

}