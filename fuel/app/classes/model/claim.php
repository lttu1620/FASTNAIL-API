<?php

/**
 * Any query in Model Claim
 *
 * @package Model
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Claim extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'nailist_id',
        'shop_id',
        'order_id',
        'content',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'claims';

    /**
     * Add or update info for Claim
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Claim id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $claim = new self;
        // check exist
        if (!empty($id)) {
            $claim = self::find($id);
            if (empty($claim)) {
                self::errorNotExist('claim_id');
                return false;
            }
        }
        // set value
        if (!empty($param['user_id'])) {
            $claim->set('user_id', $param['user_id']);
        }
        if (!empty($param['nailist_id'])) {
            $claim->set('nailist_id', $param['nailist_id']);
        }
        if (!empty($param['shop_id'])) {
            $claim->set('shop_id', $param['shop_id']);
        }
        if (!empty($param['order_id'])) {
            $claim->set('order_id', $param['order_id']);
        }
        if (!empty($param['content'])) {
            $claim->set('content', $param['content']);
        }
        // save to database
        if ($claim->save()) {
            if (empty($claim->id)) {
                $claim->id = self::cached_object($claim)->_original['id'];
            }
            return !empty($claim->id) ? $claim->id : 0;
        }
        return false;
    }

    /**
     * Get list Claim (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Claim
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('shops.name', 'shop_name'),
            array('users.first_name', 'first_name'),
            array('users.last_name', 'last_name'),
            array('nailists.name', 'nailist_name')
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('orders', 'LEFT')
            ->on(self::$_table_name . '.order_id', '=', 'orders.id')
            ->join('nailists')
            ->on(self::$_table_name . '.nailist_id', '=', 'nailists.id');

        // filter by keyword
        if (!empty($param['shop_id'])) {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name . '.order_id', '=', $param['order_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where('users.first_name', 'LIKE', "%{$param['user_name']}%")
                ->or_where('users.last_name', 'LIKE', "%{$param['user_name']}%");
        }

        if (!empty($param['nailist_name'])) {
            $query->where('nailists.name', 'LIKE', "%{$param['nailist_name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Claim (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Claim
     */
    public static function get_all()
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('shops.name', 'shop_name'),
            array('users.first_name', 'first_name'),
            array('users.last_name', 'last_name'),
            array('nailists.name', 'nailist_name')
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('orders', 'LEFT')
            ->on(self::$_table_name . '.order_id', '=', 'orders.id')
            ->join('nailists')
            ->on(self::$_table_name . '.nailist_id', '=', 'nailists.id');

        // filter by keyword
        if (!empty($param['shop_id'])) {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Claim
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $claim = self::find($id);
            if ($claim) {
                $claim->set('disable', $param['disable']);
                if (!$claim->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('claim_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Claim
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Claim or false if error
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('shops.name', 'shop_name'),
            array('users.first_name', 'first_name'),
            array('users.last_name', 'last_name'),
            array('nailists.name', 'nailist_name')
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('orders', 'LEFT')
            ->on(self::$_table_name . '.order_id', '=', 'orders.id')
            ->join('nailists')
            ->on(self::$_table_name . '.nailist_id', '=', 'nailists.id')
            ->where(self::$_table_name . '.id', '=', $param['id']);
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('claim_id');
            return false;
        }
        return $data;
    }
}
