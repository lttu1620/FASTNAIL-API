<?php

class Model_Item extends Model_Abstract {
    /**
     * Add or update info for Item
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_properties = array(
        'id',
        'item_cd',
        'item_section',
        'name',
        'price',
        'tax_price',
        'is_sale',
        'is_discount',
        'is_tax',
        'priority',
        'is_discount_combi',
        'receipt_coupon',
        'discount_price',
        'created',
        'updated',
        'disable',                
    );
    /**
     * Add or update info for Item
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'items';
    /**
     * Add or update info for Item
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    public static function add_update($param)
    {      
        $id = !empty($param['id']) ? $param['id'] : 0;
        $query = new self;
        // check exist
        if (!empty($id)) {
            $query = self::find($id);
            if (empty($query)) {
                self::errorNotExist('id');
                return false;
            }
        }
        // set value    
        if (!empty($param['item_cd'])) {
            $query->set('item_cd', $param['item_cd']);
        }
        if (!empty($param['item_section'])) {
            $query->set('item_section', $param['item_section']);
        }       
        if (!empty($param['name'])) {
            $query->set('name', $param['name']);
        }
        if (isset($param['price']) && $param['price'] != '') {
            $query->set('price', $param['price']);
        }
        if (isset($param['is_sale']) && $param['is_sale'] != '') {
            $query->set('is_sale', $param['is_sale']);
        }
        if (isset($param['price']) && $param['price'] != '') {
            $query->set('price', $param['price']);
        }
        if (isset($param['tax_price'])) {
            $query->set('tax_price', $param['tax_price']);
        }
        if (isset($param['is_discount']) && $param['is_discount'] != '') {
            $query->set('is_discount', $param['is_discount']);
        }
        if (isset($param['is_tax']) && $param['is_tax'] != '') {
            $query->set('is_tax', $param['is_tax']);
        }
        if (isset($param['priority']) && $param['priority'] != '') {
            $query->set('priority', $param['priority']);
        }
        if (isset($param['is_discount_combi']) && $param['is_discount_combi'] != '') {
            $query->set('is_discount_combi', $param['is_discount_combi']);
        }
        if (isset($param['receipt_coupon']) && $param['receipt_coupon'] != '') {
            $query->set('receipt_coupon', $param['receipt_coupon']);
        }
        if (isset($param['discount_price']) && $param['discount_price'] != '') {
            $query->set('discount_price', $param['discount_price']);
        }
        // save to database
        if ($query->save()) {
            if (empty($query->id)) {
                $query->id = self::cached_object($query)->_original['id'];
            }
            return !empty($query->id) ? $query->id : 0;
        }
        return false;
    }    
    /**
     * Get list Item (using array count)
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array List Item
     */
    public static function get_list($param) {        
        $query = DB::select(
            self::$_table_name . '.*',
            DB::expr("(price + tax_price) AS sell_price")
        )
        ->from(
            self::$_table_name          
        );
        // filter by keyword        
        if (!empty($param['price_from']) && !empty($param['price_to'])) {
            $query->where(self::$_table_name . '.price', 'between', array($param['price_from'],$param['price_to']));
        }
        else
        {
            if (!empty($param['price_from'])) {                
                $query->where(self::$_table_name . '.price', '=', $param['price_from']);
            }
            if (!empty($param['price_to'])) {                
                $query->where(self::$_table_name . '.price', '=', $param['price_to']);
            }
        }
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['item_cd'])) {
            $query->where('item_cd', '=', $param['item_cd']);
        }
        if (isset($param['is_sale']) && $param['is_sale'] != '') {
            $query->where(self::$_table_name . '.is_sale', '=', $param['is_sale']);
        }        
        if (isset($param['is_discount']) && $param['is_discount'] != '') {
            $query->where(self::$_table_name . '.is_discount', '=', $param['is_discount']);
        }        
        if (isset($param['is_tax']) && $param['is_tax'] != '') {
            $query->where(self::$_table_name . '.is_tax', '=', $param['is_tax']);
        }  
        if (isset($param['is_discount_combi']) && $param['is_discount_combi'] != '') {
            $query->where(self::$_table_name . '.is_discount_combi', '=', $param['is_discount_combi']);
        }        
        if (isset($param['receipt_coupon']) && $param['receipt_coupon'] != '') {
            $query->where(self::$_table_name . '.receipt_coupon', '=', $param['receipt_coupon']);
        } 
        if (!empty($param['item_section'])) {
            $query->where('item_section', $param['item_section']);
        } 
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Item (without array count)
     *
     * @author Hoang Gia Thong
     * @return array List Item
     */
    public static function get_all($param) {
        $query = DB::select(
                    self::$_table_name . '.*',
                    DB::expr("(price + tax_price) AS sell_price")
                )
                ->from(self::$_table_name)
                ->where('disable', '=', '0');
        if (!empty($param['limit'])) {
            $query->limit($param['limit'])->offset(0);
        }
        if (!empty($param['id'])) {
            if (is_array($param['id'])) {
                $param['id'] = implode(',', $param['id']);
            }
            $query->where('id', 'IN', DB::expr('(' . $param['id'] . ')'));
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Item
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param) {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $admin = self::find($id);
            if ($admin) {
                $admin->set('disable', $param['disable']);
                if (!$admin->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Item
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array|bool Detail Item or false if error
     */
    public static function get_detail($param) {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('id');
            return false;
        }
        return $data;
    }

}
