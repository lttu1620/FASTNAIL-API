<?php

/**
 * Any query in Model Order Item
 *
 * @package Model
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Order_Item extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'item_id',
        'item_name',
        'item_price',
        'item_tax_price',
        'item_quantity',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_items';

    /**
     * Add info for Order Item
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Item id or false if error
     */
    public static function add($param)
    {
        $options['where'] = array(
            'id'      => $param['item_id'],
            'disable' => '0'
        );
        $item = \Model_Item::find('first', $options);
        if (empty($item)) {
            static::errorNotExist('item_id');
            return false;
        }
        $query = DB::select(
            array('items.id', 'item_id'),
            array('order_items.id', 'order_item_id'),
            array('order_items.order_id', 'order_id'),
            array('order_items.disable', 'order_disable')
        )
            ->from('items')
            ->join(
                DB::expr(
                    "(SELECT * FROM order_items
                     WHERE order_id = {$param['order_id']}) AS order_items"
                ),
                'LEFT'
            )
            ->on('items.id', '=', 'order_items.item_id')
            ->where('items.id', '=', $param['item_id'])
            ->where('items.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);
        if (empty($data['item_id'])) {
            static::errorNotExist('item_id');
            return false;
        }
        if (!empty($data['order_id']) && $data['order_disable'] == 0) {
            static::errorDuplicate('order_id');
            return false;
        }
        $new = false;
        if (!empty($data['order_id']) && $data['order_disable'] == 1) {
            $dataUpdate = array(
                'id'         => $data['order_item_id'],
                'disable'    => '0',
                'item_name'  => $item->get('name'),
                'item_price' => $item->get('price') + $item->get('tax_price')
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'item_id'    => $data['item_id'],
                'order_id'   => $param['order_id'],
                'item_name'  => $item->get('name'),
                'item_price' => $item->get('price') + $item->get('tax_price')
            );
        }
        $order = new self($dataUpdate, $new);
        if ($order->save()) {
            if ($new == true) {
                $order->id = self::cached_object($order)->_original['id'];
            }
            return $order->id;
        }
        return false;
    }

    /**
     * Get all Order Item (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Item
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'order_id',
                'item_id',
                'item_name',
                'item_price',
                'item_tax_price',                
                'item_quantity',
                DB::expr("(item_price + item_tax_price) AS sell_price"),
                DB::expr("(item_price + item_tax_price) * item_quantity AS total_sell_price")
            )
            ->from(self::$_table_name)
            ->where('order_id', '=', $param['order_id'])
            ->where('disable', '=', '0');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Order Item
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'] = array(
                'order_id' => $param['order_id'],
                'item_id'  => $param['item_id'],
                'disable'  => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $param['order_id'] = $data->_data['order_id'];
                $param['item_id'] = $data->_data['item_id'];
                return true;
            }
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id');
            } else {
                static::errorNotExist('order_item_id');
            }
        }
        return false;
    }

    /**
     * Add or update order's items
     *
     * @author khoatx
     * @param array array $param Input data.
     * @return bool|int Order Log Id or False
     */
    public static function add_update($param)
    {
        //p($param, 1);
        if (empty($param['item_id']) || empty($param['order_id'])) {
            self::errorParamInvalid('item_id_or_order_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'item_id' => $param['item_id'],
            'order_id' => $param['order_id'],
        );
        $order_item = self::find('first', $options);
        if (empty($order_item)) {
            $order_item = new self;
        }
        $order_item->set('item_id', $param['item_id']);

        $itemDetail = Model_Item::get_detail(array('id' => $param['item_id']));

        if (isset($itemDetail['name'])) {
            $order_item->set('item_name', $itemDetail['name']);
        }
        if (isset($itemDetail['price'])) {
            $order_item->set('item_price', $itemDetail['price']);
        }

        if (!empty($param['item_quantity'])) {
            $order_item->set('item_quantity', $param['item_quantity']);
        } else {
            $order_item->set('item_quantity', 1);
        }

        $order_item->set('order_id', $param['order_id']);
        $order_item->set('disable', $param['disable']);
        if ($order_item->save()) {
            if (empty($order_item->id)) {
                $order_item->id = self::cached_object($order_item)->_original['id'];
            }
            return !empty($order_item->id) ? $order_item->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for Order Item by order id
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return int|bool Order Item id or false if error
     */
    public static function add_update_by_order_id($param)
    {
        $items = Model_Item::get_all(array(
            'id' => $param['item_id']
        ));       
        $items = \Lib\Arr::key_values($items, 'id');
        $quantityParts = !empty($param['item_quantity']) ? explode(',', $param['item_quantity']) : array();
        if (!empty($quantityParts) && !empty($items)) {
            $dataUpdate = array();
            foreach ($quantityParts as $row) {                
                list($itmId, $itmQlt) = explode('_', $row);
                if (isset($items[$itmId])) {
                    $dataUpdate[] = array(
                        'order_id' => $param['order_id'],
                        'item_id' => $itmId,
                        'item_name' => $items[$itmId]['name'],
                        'item_price' => $items[$itmId]['price'],
                        'item_tax_price' => $items[$itmId]['tax_price'],
                        'item_quantity' => $itmQlt
                    );
                }               
            }  
            if (!empty($dataUpdate)) {
                if (!parent::batchInsert(
                    self::$_table_name, 
                    $dataUpdate,
                    array(                     
                        'disable' => '0',         
                        'item_name' => DB::expr('VALUES(item_name)'),         
                        'item_quantity' => DB::expr('VALUES(item_quantity)'),         
                        'item_price' => DB::expr('VALUES(item_price)'),      
                        'item_tax_price' => DB::expr('VALUES(item_tax_price)'),         
                    ), 
                    false
                )) {
                    \LogLib::warning('Can not insert/update order_nails', __METHOD__, $param);
                    return false;
                }
            }
        }
        
        $query = DB::select(
                self::$_table_name . '.item_id'            
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(self::$_table_name . '.order_id', '=', $param['order_id']);
        if (!empty($param['item_id'])) {
            $query->where(self::$_table_name . '.item_id', 'NOT IN', DB::expr('(' . $param['item_id'] . ')'));
        }
        $existItems = $query->execute()->as_array();
        if (!empty($existItems)) {            
            foreach ($existItems as $row) {  
                self::disable(array(
                    'order_id' => $param['order_id'],
                    'item_id' => $row['item_id'],
                    'disable' => '1',
                ));          
            }
        }
        return true;  
    }
    
}