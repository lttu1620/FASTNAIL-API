<?php

/**
 * Any query in Model_Order_Update_Timebar_Log
 *
 * @package Model
 * @created 2015-08-24
 * @version 1.0
 * @author diennnvt
 * @copyright Oceanize INC
 */
class Model_Order_Update_Timebar_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'admin_id',
        'order_id',
        'before_seat_id',
        'before_order_start_time',
        'before_order_end_time',
        'after_seat_id',
        'after_order_start_time',
        'after_order_end_time',
        'created',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_update_timebar_logs';

    /**
     * Add info for Order_Update_Timebar_Log
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Order_Update_Timebar_Log id or false if error
     */
    public static function add($param)
    {
        $log = new self;
        // set value
        if (empty($param['admin_id'])) {
            $param['admin_id'] = 0;
        }

        $log->set('admin_id', $param['admin_id']);

        if (!empty($param['order_id'])) {
            $log->set('order_id', $param['order_id']);
        }
        if (!empty($param['before_seat_id'])) {
            $log->set('before_seat_id', $param['before_seat_id']);
        }
        if (!empty($param['before_order_start_time'])) {
            $log->set('before_order_start_time', $param['before_order_start_time']);
        }
        if (!empty($param['before_order_end_time'])) {
            $log->set('before_order_end_time', $param['before_order_end_time']);
        }
        if (!empty($param['after_seat_id'])) {
            $log->set('after_seat_id', $param['after_seat_id']);
        }
        if (!empty($param['after_order_start_time'])) {
            $log->set('after_order_start_time', $param['after_order_start_time']);
        }
        if (!empty($param['after_order_end_time'])) {
            $log->set('after_order_end_time', $param['after_order_end_time']);
        }
        // save to database
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get all Order Update Timebar Log (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Update Timebar Log
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            array('admins.name', 'admin_name')
        )
            ->from(self::$_table_name)
            ->join('admins')
            ->on(self::$_table_name.'.admin_id', '=', 'admins.id')
            ->join('orders')
            ->on(self::$_table_name.'.order_id', '=', 'orders.id');
        if (!empty($param['admin_id'])) {
            $query->where(self::$_table_name.'.admin_id', '=', $param['admin_id']);
        }
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name.'.order_id', '=', $param['order_id']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
