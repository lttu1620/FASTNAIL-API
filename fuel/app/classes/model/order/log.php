<?php

/**
 * Any query in Model_Order_Log
 *
 * @package Model
 * @created 2015-03-26
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Order_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'nailist_id',
        'created',
        'updated',
        'disable'
    );
    protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

    /** @var array $_table_name name of table */
	protected static $_table_name = 'order_logs';
    
    /**
     * Add or update order's nailist
     *
     * @author diennvt
     * @param array array $param Input data.
     * @return bool|int Order Log Id or False
     */
    public static function add_update($param)
    {        
        //p($param, 1);
        if (empty($param['nailist_id']) || empty($param['order_id'])) {
            self::errorParamInvalid('nailist_id_or_order_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'nailist_id' => $param['nailist_id'],
            'order_id' => $param['order_id'],           
        );       
        $order_nailist = self::find('first', $options);       
        if (empty($order_nailist)) {
            $order_nailist = new self;           
        }        
        $order_nailist->set('nailist_id', $param['nailist_id']);
        $order_nailist->set('order_id', $param['order_id']);       
        $order_nailist->set('disable', $param['disable']);       
        if ($order_nailist->save()) {
            if (empty($order_nailist->id)) {
                $order_nailist->id = self::cached_object($order_nailist)->_original['id'];
            }
            return !empty($order_nailist->id) ? $order_nailist->id : 0;
        }
        return false;
    }
    
    /**
     * Add or update orderist for a order
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool false if error
     */
    public static function add_update_by_order_id($param)
    {
        //Order id
        $order_id = $param['order_id'];
        
        //Array of nailist's id
        $nailist_ids = !empty($param['nailist_id']) ? 
                array_filter(array_unique(explode(',', $param['nailist_id'])))
                : array ();
        
        //Check exist: order's id
        if (empty(Model_Order::find($order_id))) {
            self::errorNotExist('order_id');
            return false;
        }
        
        //Check exist: nailist's ids
        if (!empty($nailist_ids)) {
            //Count nailist's ids from db
            $count_nailist = Model_Nailist::query()
            ->where('id', 'in', $nailist_ids)
            ->count();        

            //Do compare
            if ($count_nailist != count($nailist_ids)) {
                self::errorNotExist('nailist_id');
                return false;
            }
        }
        
        //Get current order's nailists
        $options['where'] = array(
            'order_id' => $order_id,
            'disable' => 0// TODO define
        ); 
        $current_nailists_order = self::find('all', $options);  
       
        //Get current order's nailist' id
        $array_nailists_order_id = array ();
        
        foreach ($current_nailists_order as $obj_nailist_order) {
            $array_nailists_order_id[] = $obj_nailist_order->get('nailist_id');
        }
        
        //New nailist's id
        $new_nailist_orders = array_diff($nailist_ids, $array_nailists_order_id);
        
        //Nailist's id to remove
        $remove_nailist_orders = array_diff($array_nailists_order_id, $nailist_ids);
        
        //Data to add or update
        $data = array ();
        foreach ($new_nailist_orders as $new_nailist_id) {
            $data[] = array (
                'order_id' => $order_id,
                'nailist_id' => $new_nailist_id,
                'disable' => 0//TODO define
            );
        }
        
        foreach ($remove_nailist_orders as $remove_nailist_id) {
            $data[] = array (
                'order_id' => $order_id,
                'nailist_id' => $remove_nailist_id,
                'disable' => 1//TODO define
            );
        }
        //p($data,1);
        //Add or update row by row
        foreach ($data as $row) {
            if (!self::add_update($row)) {
                return false;
            }
        }
        
        return true;
    }   
    
    /**
     * Get all of order's nailist
     *
     * @author diennvt
     * @param array array $param Input data.
     * @return bool|int order log Id or False
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::table() . '.nailist_id',                          
                self::table() . '.order_id',     
                self::table() . '.disable',     
                Model_Order::table() . '.reservation_date',
                Model_Order::table() . '.order_start_date',
                Model_Order::table() . '.order_end_date',
                Model_Nailist::table() . '.code',
                DB::expr("IFNULL(IF(".Model_Nailist::table().".image_url='',NULL,".Model_Nailist::table().".image_url), '" . \Config::get('no_image_nailist') . "') AS nailist_image_url"),
                Model_Nailist::table() . '.name'
            )
            ->from(self::table())
            ->join(Model_Order::table())
            ->on(Model_Order::table() . '.id', '=', self::table() . '.order_id')
            ->join(Model_Nailist::table())
            ->on(Model_Nailist::table() . '.id', '=', self::table() . '.nailist_id')
            ->where(self::table() . '.disable', '0')
            ->where(Model_Order::table() . '.disable', '0')
        	->where(DB::expr(" IFNULL(orders.is_cancel,0) = 0 "));
        if (!empty($param['shop_id'])) {
            $query->where(Model_Order::table() . '.shop_id', "{$param['shop_id']}");
        }
        if (!empty($param['order_id'])) {
            $query->where(self::table() . '.order_id', "{$param['order_id']}");
        }  
        if (!empty($param['not_in_order_id'])) {
            $query->where(self::table() . '.order_id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
        }
        if (!empty($param['nailist_id'])) {
            if (is_array($param['nailist_id'])) {
                $param['nailist_id'] = implode(',', $param['nailist_id']);
            }
            $query->where(self::table() . '.nailist_id', 'IN', DB::expr("({$param['nailist_id']})"));
        }        
        if (!empty($param['order_start_date']) && !empty($param['order_end_date'])) {
            if (!is_numeric($param['order_start_date'])) {
                $param['order_start_date'] = self::time_to_val($param['order_start_date']);
            }
            if (!is_numeric($param['order_end_date'])) {
                $param['order_end_date'] = self::time_to_val($param['order_end_date']);
            }
            $query->and_where_open()
                ->where_open()
                ->where(Model_Order::table() . '.order_start_date', '>=', $param['order_start_date'])
                ->where(Model_Order::table() . '.order_start_date', '<', $param['order_end_date'])
                ->where_close()
                ->or_where_open()
                ->where(Model_Order::table() . '.order_end_date', '>', $param['order_start_date'])
                ->where(Model_Order::table() . '.order_end_date', '<=', $param['order_end_date'])
                ->or_where_close() 
                ->and_where_close();
        }
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Get all nailists by order.
     * 
     * @param array $param Input data.
     * @author truongnn
     * @return array List nailists
     */
    public static function get_all_nailist_by_order($param){
        $query =  DB::select('nailists.id',
                            'nailists.code',
                            'nailists.name',
                            DB::expr("IFNULL(IF(nailists.image_url='',NULL,nailists.image_url), '" . \Config::get('no_image_nailist') . "') AS nailist_image_url"),
                            DB::expr("IF(IFNULL(order_log.nailist_id, 0) = 0, 0 , 1) AS checked")
                )
                ->from('nailists')
                ->join(DB::expr("(SELECT * FROM order_logs WHERE disable = 0 AND order_id = {$param['order_id']}) AS order_log"),'LEFT')
                ->on('order_log.nailist_id','=', 'nailists.id')
                ->where('nailists.shop_id', '=', $param['shop_id']);
         $data = $query->execute()->as_array();
         return empty($data) ? array() : $data;
    }
    /**
     * Get detail Order
     *
     * @author Vulth
     * @param array $param Input data
     * @return array|bool Detail Order or false if error
     */
    public static function get_nailist($param)
    {
         $query =  DB::select('order_logs.nailist_id')
         ->from('order_logs');
        if (!empty($param['order_id'])) {
            $query->where('order_logs.order_id','=', $param['order_id']);
        }
        $data = $query->execute()->as_array();
        return empty($data) ? array() : $data;
    }
}
