<?php

/**
 * Any query in Model Order Service
 *
 * @package Model
 * @created 2015-05-25
 * @version 1.0
 * @author KhoaTX
 * @copyright Oceanize INC
 */
class Model_Order_Service extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'service_id',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_services';

    /**
     * Add info for Order Service
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return int|bool Order Service id or false if error
     */
    public static function add($param)
    {
        // check exist orders
        $options['where'] = array(
            'id' => $param['order_id'],
            'disable' => '0'
        );
        $order = Model_Order::find('first', $options);
        if (empty($order)) {
            static::errorNotExist('order_id');
            return false;
        }

        // check exist service
        $options['where'] = array(
            'id' => $param['service_id'],
            'disable' => '0'
        );
        $service = Model_Service::find('first', $options);
        if (empty($service)) {
            static::errorNotExist('service_id');
            return false;
        }

        $dataUpdate = array(
            'order_id' => $param['order_id'],
            'service_id' => $param['service_id']
        );
        if (!parent::batchInsert(
            self::$_table_name,
            $dataUpdate,
            array(
                'service_id' => $param['service_id'],
                'disable' => '0',
            ),
            false
        )) {
            \LogLib::warning('Can not insert/update order_services', __METHOD__, $param);
            return false;
        }
        
        return true;
    }

    /**
     * Get all Order Service (without array count)
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return array List Order Service
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'order_id',
                'service_id',
                'services.name',
                DB::expr(' services.time*60 as time '),
                'services.device_type'
            )
            ->from(self::$_table_name)
            ->join('services')
            ->on('services.id', '=', 'order_services.service_id')
            ->where('order_id', '=', $param['order_id'])
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['service_id'])) {
            $query->where(self::$_table_name . '.service_id', 'IN', $param['service_id']);
        }
        $data = $query->execute()->as_array();        
        return $data;
    }

    /**
     * Disable/Enable list Order Service
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'] = array(
                'order_id' => $param['order_id'],
                'service_id'  => $param['service_id'],
                'disable'  => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $param['order_id'] = $data->_data['order_id'];
                $param['service_id'] = $data->_data['service_id'];
                return true;
            }
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id');
            } else {
                static::errorNotExist('order_service_id');
            }
        }
        return false;
    }

    /**
     * Add or update order's services
     *
     * @author khoatx
     * @param array array $param Input data.
     * @return bool|int Order Log Id or False
     */
    public static function add_update($param)
    {        
        if (empty($param['service_id']) || empty($param['order_id'])) {
            self::errorParamInvalid('service_id_or_order_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'service_id' => $param['service_id'],
            'order_id' => $param['order_id'],
        );
        $order_service = self::find('first', $options);
        if (empty($order_service)) {
            $order_service = new self;
        }

        $order_service->set('service_id', $param['service_id']);
        $order_service->set('order_id', $param['order_id']);
        $order_service->set('disable', $param['disable']);
        if ($order_service->save()) {
            if (empty($order_service->id)) {
                $order_service->id = self::cached_object($order_service)->_original['id'];
            }
            return !empty($order_service->id) ? $order_service->id : 0;
        }
        return false;
    }
    
    /**
     * Multi update order services
     *
     * @author thailh
     * @param array $param Input data
     * @return book True or False if error
     */
    public static function multi_update($param) {
        if (empty($param['service_id'])) {
            $query = DB::update("order_services");
            $query->value('disable', 1);
            $query->where('order_id', '=', $param['order_id']);
            $query->execute();
            return true;
        }
        $order = Model_Order::find($param['order_id']);
        if (empty($order->get('id'))) {
            static::errorNotExist('order_id');
            return false;
        }
        if (is_array($param['service_id'])) {
            $param['service_id'] = implode(',', $param['service_id']);
        }        
        $dataUpdate = array();
        $serviceIds = explode(',', $param['service_id']);
        foreach ($serviceIds as $serviceId) {
            if (!empty($serviceId)) {
                $dataUpdate[] = array(
                    'order_id' => $order->get('id'),
                    'service_id' => $serviceId
                );
            }
        } 
        if (!empty($dataUpdate) && !parent::batchInsert(
                    self::$_table_name, 
                    $dataUpdate, 
                    array(
                        'disable' => '0'
                    ), 
                    false
                )) {
            \LogLib::warning('Can not update ' . self::$_table_name, __METHOD__, $param);
            return false;
        }

        $query = DB::select(
                    self::$_table_name . '.service_id'
                )
                ->from(self::$_table_name)
                ->where(self::$_table_name . '.disable', '=', '0')
                ->where(self::$_table_name . '.order_id', '=', $param['order_id']);
        if (!empty($param['service_id'])) {
            $query->where(self::$_table_name . '.service_id', 'NOT IN', DB::expr('(' . $param['service_id'] . ')'));
        }
        $existServices = $query->execute()->as_array();        
        if (!empty($existServices)) {
            foreach ($existServices as $row) {
                self::disable(array(
                    'order_id' => $param['order_id'],
                    'service_id' => $row['service_id'],
                    'disable' => '1',
                ));
            }
        }
        return true;
    }

}