<?php

/**
 * Any query in Model Order Nail
 *
 * @package Model
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Order_Nail extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'nail_id',
        'price',
        'tax_price',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load', 
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_nails';

    /**
     * Add info for Order Nail
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Nail id or false if error
     */
    public static function add($param)
    {
        // check exist orders
        $options['where'] = array(
            'id' => $param['order_id'],
            'disable' => '0'
        );
        $order = Model_Order::find('first', $options);
        if (empty($order)) {
            static::errorNotExist('order_id');
            return false;
        }

        // check exist nails
        $options['where'] = array(
            'id' => $param['nail_id'],
            'disable' => '0'
        );
        $nail = Model_Nail::find('first', $options);
        if (empty($nail)) {
            static::errorNotExist('nail_id');
            return false;
        }

        $dataUpdate = array(
            'order_id' => $param['order_id'],
            'nail_id' => $param['nail_id'],
            'price' => $nail->get('price'),
            'tax_price' => $nail->get('tax_price'),
        );
        if (!parent::batchInsert(
            self::$_table_name,
            $dataUpdate,
            array(
                'nail_id' => $param['nail_id'],
                'price' => $nail->get('price'),
                'tax_price' => $nail->get('tax_price'),           
                'disable' => '0',         
            ), 
            false
        )) {
            \LogLib::warning('Can not insert/update order_nails', __METHOD__, $param);
            return false;
        }
        
        $changeOrderInfo = false;
        if ($order->get('photo_code') != $nail->get('photo_cd')) {
            $order->set('photo_code', $nail->get('photo_cd'));
            $changeOrderInfo = true;
            if (!$order->update()) {
                \LogLib::warning('Can not update orders.hf_section', __METHOD__, $param);
                return false;
            }
        }        
        if ($order->get('hf_section') != $nail->get('hf_section') 
            && in_array($nail->get('hf_section'), array(1, 2)) ) {
            $order->set('hf_section', $nail->get('hf_section'));
            $changeOrderInfo = true;
        }        
        if ($changeOrderInfo == true) {
            if (!$order->update()) {
                \LogLib::warning('Can not update orders.hf_section', __METHOD__, $param);
                return false;
            }
        }
        
        // re-update order's price
        if (isset($param['update_order_prices'])) {
            Model_Order::updatePrice(array(
                'order_id' => $param['order_id']
            ));  
            if (self::error()) {
                return false;
            }
        }
        // delete cache
        \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
        \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
        return true;
    }

    /**
     * Get all Order Nail (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Nail
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . '.id',
                self::$_table_name . '.order_id',
                self::$_table_name . '.nail_id',
                self::$_table_name . '.price',
                self::$_table_name . '.tax_price',
                array('nails.photo_cd', 'nail_photo_cd'),
                array('nails.price', 'nail_price'),
                array('nails.hf_section', 'hf_section'),
                array('nails.tax_price', 'nail_tax_price'),
                array(self::$_table_name . '.tax_price', 'nail_tax_price'),
                DB::expr("IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('" . \Config::get('item_img_url')['nails'] . "', photo_cd, '" . ".jpg" . "'), nails.photo_url) as nail_image_url")                
            )
            ->from(self::$_table_name)
            ->join('nails')
            ->on(self::$_table_name . '.nail_id', '=', 'nails.id')
            ->where('order_id', '=', $param['order_id'])
            ->where(self::$_table_name . '.disable', '=', '0');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Order Nail
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'][] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'][] = array(
                'order_id' => $param['order_id'],
                'nail_id'  => $param['nail_id'],
                'disable'  => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $param['order_id'] = $data->_data['order_id'];
                $param['nail_id'] = $data->_data['nail_id'];
                return true;
            }
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id');
            } else {
                static::errorNotExist('order_nail_id');
            }
        }
        return false;
    }
}