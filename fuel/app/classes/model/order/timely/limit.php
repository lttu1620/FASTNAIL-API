<?php

/**
 * Any query in Model Order Timely Limit
 *
 * @package Model
 * @created 2015-05-13
 * @version 1.0
 * @author Thai Lai
 * @copyright Oceanize INC
 */
class Model_Order_Timely_Limit extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'shop_id',
        'time',
        'limit',
        'hp_limit',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_timely_limits';

    /**
     * Add or update info for Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Timely Limit id or false if error
     */
    public static function add_update($param)
    {
        $param['time'] = strtotime(date('Y-m-d H:i', strtotime($param['time'])));
        $options['where'] = array(
            'shop_id' => $param['shop_id'],
            'time'    => $param['time'],
        );
        $limit = self::find('first', $options);
        if (empty($limit)) {
            $limit = new self;
        }     
        if (!empty($param['time'])) {
            $limit->set('time', $param['time']);
        }
        if (isset($param['limit']) && $param['limit'] != '') {
            $limit->set('limit', $param['limit']);
        }
        if (isset($param['hp_limit']) && $param['hp_limit'] != '') {
            $limit->set('hp_limit', $param['hp_limit']);
        }
        if ($limit->save()) {
            if (empty($limit->id)) {
                $limit->id = self::cached_object($limit)->_original['id'];
            }
            \Lib\Cache::delete('order_timely_limit_' . $param['shop_id'] . '_' . date('Y-m-d', strtotime($param['time'])));
            return !empty($limit->id) ? $limit->id : 0;
        }
        return false;
    }

    /**
     * Get list Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Timely Limit
     */
    public static function get_list($param, $force = false)
    {
        if (isset($param['get_list_from_db'])) {
            return self::get_list_from_db($param);
        }       
        $key = 'order_timely_limit_' . $param['shop_id'] . '_' . $param['date'];
        $result = \Lib\Cache::get($key);
        if ($result !== false && $force == false) {
            return $result;
        }     
        \Lib\Cache::set($key, $result = self::get_list_from_db($param), 60*60);
        return $result;
    }

    /**
     * Get list Order Timely Limit without cache
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Timely Limit
     */
    public static function get_list_from_db($param)
    {       
        if (empty($param['shop_id'])) {
            return array(0, array());
        }
        $shop = Model_Shop::find($param['shop_id']);
        if (empty($shop)) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (empty($shop->get('open_time')) || empty($shop->get('close_time'))) {
            return array(0, array());
        }
        $openTime = $shop->get('open_time');
        $closeTime = $shop->get('close_time');
        $param['time_from'] = $param['date'] . ' ' . gmdate('H:i:s', $openTime);
        $param['time_to'] = $param['date'] . ' ' . gmdate('H:i:s', $closeTime);
        $query = DB::select(
                DB::expr("
                    DATE_FORMAT(FROM_UNIXTIME(order_timely_limits.time), '%H:%i') AS time
                "),
                self::$_table_name . '.limit',
                self::$_table_name . '.hp_limit'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::$_table_name . '.time', '>=', self::time_to_val($param['time_from']))
            ->where(self::$_table_name . '.time', '<=', self::time_to_val($param['time_to']))
            ->where(self::$_table_name . '.disable', '=', '0')
            ->order_by(self::$_table_name . '.time', 'ASC');
        // get data
        $timelyLimit = $query->execute()->as_array();
        $total = !empty($timelyLimit) ? DB::count_last_query() : 0;
        $data = array();
        foreach ($timelyLimit as $item) {
            $data[$item['time']] = $item;
            unset($data[$item['time']]['time']);
        }
        return array($total, $data);               
    }
    
    /**
     * Disable/Enable list Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $shop = self::find($id);
            if ($shop) {
                $shop->set('disable', $param['disable']);
                if (!$shop->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('order_timely_limit_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Decrease field limit for Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Timely Limit id or false if error
     */
    public static function decrease_limit($param)
    {
        $shop = Model_Shop::find($param['shop_id']);
        if (!$shop) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (empty($param['decrease_fn'])) {
            $param['decrease_fn'] = 0;
            $param['to_row_fn'] = 0;
        }
        if (empty($param['decrease_hp'])) {
            $param['decrease_hp'] = 0;
            $param['to_row_hp'] = 0;
        }
        $updateLimitFN = $shop->get('max_seat') > $param['decrease_fn'] ? $shop->get('max_seat') - $param['decrease_fn'] : 0;
        $updateLimitHP = $shop->get('hp_max_seat') > $param['decrease_hp'] ? $shop->get('hp_max_seat') - $param['decrease_hp'] : 0;        
        if ($param['decrease_fn'] > 0 || $param['decrease_hp'] > 0) {
            $openTime = !empty($shop->get('open_time')) ? $shop->get('open_time') : 8 * 60 * 60;
            $closeTime = !empty($shop->get('close_time')) ? $shop->get('close_time') : 17 * 60 * 60;
            //$interval = !empty($shop->get('reservation_interval')) ? $shop->get('reservation_interval') : '15';
            $interval = '15';
            $block = ($closeTime - $openTime) / (60 * $interval) + 1; // every $interval minute have 1 block
            $limit = self::find('all', array(
                'where' => array(
                    array('shop_id', '=', $param['shop_id']),
                    array('time', '>=', self::time_to_val($param['time_from'])),
                    array('time', '<', self::time_to_val($param['time_to'])),
                    array('disable', '=', '0')
                ),
                'order_by' => array(
                    'time' => 'asc'
                )
            ));
            $paramUpdate['shop_id'] = $param['shop_id'];
            $paramUpdate['data_json'] = array();
            for ($i = 0; $i < $block; $i++) {
                $time = date('Y-m-d H:i', strtotime('+' . $interval * $i . 'minutes', strtotime($param['time_from'])));
                $exist = false;
                foreach ($limit as $key => $item) {
                    if (date('Y-m-d H:i', $item->get('time')) == $time) {   
                        $exist = true;                         
                        if ($param['decrease_fn'] > 0 && $param['to_row_fn'] - $param['decrease_fn'] < $item->get('limit')) {
                            $new_decrease_fn = self::reCaldecrease($param['decrease_fn'], $item->get('limit'), $param['to_row_fn']);
                        } else {
                            $new_decrease_fn = $item->get('limit');
                        }
                        if ($param['decrease_hp'] > 0 && $param['to_row_hp'] - $param['decrease_hp'] < $item->get('hp_limit')) {
                            $new_decrease_hp = self::reCaldecrease($param['decrease_hp'], $item->get('hp_limit'), $param['to_row_hp']);
                        } else {
                            $new_decrease_hp = $item->get('hp_limit');
                        }                      
                        $paramUpdate['data_json'][] = array(
                            'time' => $time,
                            'limit' => $new_decrease_fn,
                            'hp_limit' => $new_decrease_hp
                        );
                        break;
                        
//                        $new_decrease_fn = self::reCaldecrease($param['decrease_fn'], $item->get('limit'), $param['to_row_fn']);
//                        $new_decrease_hp = self::reCaldecrease($param['decrease_hp'], $item->get('hp_limit'), $param['to_row_hp']);
//                        $paramUpdate['data_json'][] = array(
//                            'time' => $time,
//                            'limit' => ($item->get('limit') > $new_decrease_fn) > 0 ? $item->get('limit') - $new_decrease_fn : 0,
//                            'hp_limit' => ($item->get('hp_limit') > $new_decrease_hp) > 0 ? $item->get('hp_limit') - $new_decrease_hp : 0
//                        );
//                        unset($limit[$key]);                        
                    }
                }   
                if ($exist === false 
                    && self::time_to_val($time) >= self::time_to_val($param['time_from']) 
                    && self::time_to_val($time) < self::time_to_val($param['time_to'])) {
                    $paramUpdate['data_json'][] = array(
                        'time' => $time,
                        'limit' => $updateLimitFN,
                        'hp_limit' => $updateLimitHP
                    );
                }
            }
            $paramUpdate['data_json'] = json_encode($paramUpdate['data_json']);
            if (self::multi_update($paramUpdate)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Increase field limit for Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Timely Limit id or false if error
     */
    public static function increase_limit($param)
    {
    	$shop = Model_Shop::find($param['shop_id']);
    	if (!$shop) {
    		static::errorNotExist('shop_id');
    		return false;
    	}
    	if (empty($param['increase_fn'])) {
    		$param['increase_fn'] = 0;
    		$param['to_row_fn'] = 0;
    	}
    	if (empty($param['increase_hp'])) {
    		$param['increase_hp'] = 0;
    		$param['to_row_hp'] = 0;
    	}    	
    	
    	if ($param['increase_fn'] > 0 || $param['increase_hp'] > 0) {
    		$openTime = !empty($shop->get('open_time')) ? $shop->get('open_time') : 8 * 60 * 60;
    		$closeTime = !empty($shop->get('close_time')) ? $shop->get('close_time') : 17 * 60 * 60;
    		//$interval = !empty($shop->get('reservation_interval')) ? $shop->get('reservation_interval') : '15';
    		$interval = '15';
    		$block = ($closeTime - $openTime) / (60 * $interval) + 1; // every $interval minute have 1 block
    		$limit = self::find('all', array(
    				'where' => array(
    						array('shop_id', '=', $param['shop_id']),
    						array('time', '>=', self::time_to_val($param['time_from'])),
    						array('time', '<', self::time_to_val($param['time_to'])),
    						array('disable', '=', '0')
    				),
    				'order_by' => array(
    						'time' => 'asc'
    				)
    		));
    		$paramUpdate['shop_id'] = $param['shop_id'];
    		$paramUpdate['data_json'] = array();
    		for ($i = 0; $i < $block; $i++) {
    			$time = date('Y-m-d H:i', strtotime('+' . $interval * $i . 'minutes', strtotime($param['time_from'])));
    			$exist = false;
    			foreach ($limit as $key => $item) {
    				if (date('Y-m-d H:i', $item->get('time')) == $time) {
    					$exist = true;
    					$increase_fn = self::calIncrease($param['increase_fn'], $item->get('limit'), $param['to_row_fn']); 
    					$increase_hp = self::calIncrease($param['increase_hp'], $item->get('hp_limit'), $param['to_row_hp']); 
    					$paramUpdate['data_json'][] = array(
    							'time' => $time,
    							'limit' => (($item->get('limit') + $increase_fn) < $shop->get('max_seat')) ? $item->get('limit') + $increase_fn : $shop->get('max_seat'),
    							'hp_limit' => ($item->get('hp_limit') + $increase_hp < $shop->get('hp_max_seat')) ? $item->get('hp_limit') + $increase_hp : $shop->get('hp_max_seat')
    					);
    					unset($limit[$key]);
    				}
    			}    			
    		}
    		$paramUpdate['data_json'] = json_encode($paramUpdate['data_json']);
    		if (self::multi_update($paramUpdate)) {
    			return true;
    		}
    	}
    	return false;
    }

    /**
     * Multi update info for Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Timely Limit id or false if error
     */
    public static function multi_update($param)
    {
        $shop = Model_Shop::find($param['shop_id']);
        if (!$shop) {
            static::errorNotExist('shop_id');
            return false;
        }
        $data = json_decode($param['data_json'], true); 
        $date = array();
        if ($data) {
            $dataUpdate = array();
            foreach ($data as $item) {
                if (!empty($item['time']) && isset($item['limit']) && isset($item['hp_limit'])) {
                    $item['shop_id'] = $param['shop_id'];
                    $item['time'] = self::time_to_val($item['time']);
                    $dataUpdate[] = $item;
                    $date[] = date('Y-m-d', $item['time']);
                }
            }
            if (!empty($dataUpdate) && !parent::batchInsert(
                self::$_table_name,
                $dataUpdate,
                array(
                    'shop_id' => $param['shop_id'],
                    'time' => DB::expr('VALUES(`time`)'),
                    'limit' => DB::expr('VALUES(`limit`)'),
                    'hp_limit' => DB::expr('VALUES(`hp_limit`)')
                ),
                false
            )) {
                \LogLib::warning('Can not update order_timely_limits', __METHOD__, $param);
                return false;
            }
        }
        // delete cache
        foreach ($date AS $d) {
            \Lib\Cache::delete('order_timely_limit_' . $param['shop_id'] . '_' . $d);
        }
        return true;
    }
    
    /**
     * Re-calculate decrease
     *
     * @author diennvt
     * @param int $cur_decrease Current decrease
     * @param int $limit Limit seat
     * @param int $max_row Seat ids
     * @param int $booked_seats Number of booked seats
     * @return int Decrease
     */
    public static function reCaldecrease($cur_decrease, $limit, $max_row, $booked_seats = 0){
    	if ($cur_decrease == 0) {
            return 0;
        }
        if ($max_row < $limit) {
            $new_decrease = $limit - $cur_decrease;
        } else {
            $new_decrease = ($limit - ($cur_decrease - ($max_row - $limit)));
        }
        return $new_decrease;
        
//        $value = $max_row - $limit;
//        $new_decrease = $cur_decrease;
//        if ($value > 0) {
//            $new_decrease = $new_decrease - $value;
//        }
//        return $new_decrease > 0 ? $new_decrease : $cur_decrease;
    }
    
    /**
     * Calculate increase
     *
     * @author diennvt
     * @param int $cur_increase Current increase
     * @param int $limit Limit seat
     * @param int $max_row Seat ids
     * @return int Increase
     */
    public static function calIncrease($cur_increase, $limit, $max_row){	
    	if($max_row == 0 || $max_row <= $limit) return 0;   	
    	if($cur_increase < ($max_row - $limit)){
    		return $cur_increase;
    	}
    	return $max_row - $limit;
    }
    
}
