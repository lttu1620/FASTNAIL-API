<?php

/**
 * Any query in Model Order Device
 *
 * @package Model
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Order_Device extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'device_id',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_devices';

    /**
     * Add or update info for Order Device
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Device id or false if error
     */
    public static function add_update($param)
    {
        if (empty($param['device_id']) || empty($param['order_id'])) {
            self::errorParamInvalid('device_id_or_order_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'device_id' => $param['device_id'],
            'order_id'    => $param['order_id'],
        );
        $orderDevice = self::find('first', $options);
        if (empty($orderDevice)) {
            $orderDevice = new self;
        }
        $orderDevice->set('device_id', $param['device_id']);
        $orderDevice->set('order_id', $param['order_id']);
        $orderDevice->set('disable', $param['disable']);
        if ($orderDevice->save()) {
            if (empty($orderDevice->id)) {
                $orderDevice->id = self::cached_object($orderDevice)->_original['id'];
            }
            return !empty($orderDevice->id) ? $orderDevice->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for Order Device by order id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Device id or false if error
     */
    public static function add_update_by_order_id($param)
    {
        // convert string param to array
        $arrayDevice = array_filter(array_unique(explode(',', $param['device_id'])));
        // check order exist
        $options['where'] = array(
            'id' => $param['order_id'],
            'disable' => 0
        );
        $order = Model_Order::find('first', $options);
        if (empty($order)) {
            self::errorNotExist('order_id');
            return false;
        }
        // check device exist
        if (!empty($arrayDevice)) {
            $countDevice = Model_Device::query()
                ->where('id', 'in', $arrayDevice)
                ->where('disable', '=', '0')
                ->count();
            if ($countDevice != count($arrayDevice)) {
                self::errorNotExist('device_id');
                return false;
            }
        }
        // get current record in database
        $options['where'] = array(
            'order_id' => $param['order_id'],
            'disable' => 0
        );
        $currentRecord = self::find('all', $options);
        // get device id from current record
        $currentDeviceInRecord = array ();
        foreach ($currentRecord as $orderDevice) {
            $currentDeviceInRecord[] = $orderDevice->get('device_id');
        }
        // distribute param to add and update
        $addUpdateDeviceInRecord = array_diff($arrayDevice, $currentDeviceInRecord);
        $disableDeviceInRecord = array_diff($currentDeviceInRecord, $arrayDevice);
        $data = array();
        foreach ($addUpdateDeviceInRecord as $DeviceId) {
            $data[] = array(
                'order_id'    => $param['order_id'],
                'device_id' => $DeviceId,
                'disable'    => '0'
            );
        }
        foreach ($disableDeviceInRecord as $DeviceId) {
            $data[] = array(
                'order_id'    => $param['order_id'],
                'device_id' => $DeviceId,
                'disable'    => '1'
            );
        }
        // add and update to database
        foreach ($data as $orderDevice) {
            if (!self::add_update($orderDevice)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get all Order Device (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Device
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::table() . '.device_id',                          
                self::table() . '.order_id',     
                self::table() . '.disable',     
                Model_Order::table() . '.reservation_date',
                Model_Order::table() . '.order_start_date',
                Model_Order::table() . '.order_end_date',
                Model_Device::table() . '.id',
                Model_Device::table() . '.name'
            )
            ->from(self::table())
            ->join(Model_Order::table())
            ->on(Model_Order::table() . '.id', '=', self::table() . '.order_id')
            ->join(Model_Device::table())
            ->on(Model_Device::table() . '.id', '=', self::table() . '.device_id')
            ->where(self::table() . '.disable', '0')
            ->where(Model_Order::table() . '.disable', '0')
            ->where(DB::expr(" IFNULL(orders.is_cancel,0) = 0 "));
        if (!empty($param['shop_id'])) {
            $query->where(Model_Order::table() . '.shop_id', "{$param['shop_id']}");
        }
        if (!empty($param['order_id'])) {
            $query->where(self::table() . '.order_id', "{$param['order_id']}");
        }  
        if (!empty($param['not_in_order_id'])) {
            $query->where(self::table() . '.order_id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
        }
        if (!empty($param['device_id'])) {
            if (is_array($param['device_id'])) {
                $param['device_id'] = implode(',', $param['device_id']);
            }
            $query->where(self::table() . '.device_id', 'IN', DB::expr("({$param['device_id']})"));
        }        
        if (!empty($param['order_start_date']) && !empty($param['order_end_date'])) {
            if (!is_numeric($param['order_start_date'])) {
                $param['order_start_date'] = self::time_to_val($param['order_start_date']);
            }
            if (!is_numeric($param['order_end_date'])) {
                $param['order_end_date'] = self::time_to_val($param['order_end_date']);
            }
            $query->and_where_open()
                ->where_open()
                ->where(Model_Order::table() . '.order_start_date', '>=', $param['order_start_date'])
                ->where(Model_Order::table() . '.order_start_date', '<', $param['order_end_date'])
                ->where_close()
                ->or_where_open()
                ->where(Model_Order::table() . '.order_end_date', '>', $param['order_start_date'])
                ->where(Model_Order::table() . '.order_end_date', '<=', $param['order_end_date'])
                ->or_where_close() 
                ->or_where_open()
                ->where(Model_Order::table() . '.order_start_date', '<', $param['order_start_date'])
                ->where(Model_Order::table() . '.order_end_date', '>', $param['order_end_date'])
                ->or_where_close()
                ->and_where_close();
        }
        
        $data = $query->execute()->as_array();
        return $data;
    }
    
    public static function get_all1($param)
    { 
        $query = DB::select(
            self::$_table_name . '.*',
            'devices.name'
        )
            ->from(self::$_table_name)
            ->join('devices')
            ->on(self::$_table_name . '.device_id', '=', 'devices.id')
            ->join('orders')
            ->on(self::$_table_name . '.order_id', '=', 'orders.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('devices.disable', '=', '0')
            ->where('orders.disable', '=', '0');
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name . '.order_id', '=', $param['order_id']);
        }
        if (!empty($param['device_id'])) {
            $query->where(self::$_table_name . '.device_id', '=', $param['device_id']);
        }
        // get data
        $data = $query->execute()->as_array();

        //return data
        return $data;
    }
}
