<?php

/**
 * Any query in Model Order Nailist Log
 *
 * @package Model
 * @created 2015-05-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Order_Nailist_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'admin_id',
        'nailist_id',
        'type',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_nailist_logs';

    /**
     * Add info for Order Nailist Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order Nailist Log id or false if error
     */
    public static function add($param)
    {        
        if (empty($param['order_id']) || empty($param['type']) || empty($param['admin_id'])) {
            self::errorParamInvalid('order_id_or_admin_id_or_type');
            return false;
        }
        // check duplicate
        $options['where'] = array(
            'order_id' => $param['order_id'],
            'admin_id' => $param['admin_id'],
            'nailist_id' => $param['nailist_id'],
            'type' => $param['type'],
        );
        $log = self::find('first', $options);
        if ($log) {
        	$log->set('disable', 0);
        	$log->save();
        	return !empty($log->id) ? $log->id : false;
        }
        // insert data
        $log = new self;       
        $log->set('order_id', $param['order_id']);
        $log->set('admin_id', $param['admin_id']);
        $log->set('nailist_id', !empty($param['nailist_id']) ? $param['nailist_id'] : 0 );
        $log->set('type', $param['type']);
        // save to database
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : false;
        }
        return false;
    }

    /**
     * Get all Order Nailist Log (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Order Nailist Log
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('admins.name', 'admin_name'),
            array('nailists.name', 'nailist_name'),
            DB::expr("IFNULL(IF(nailists.image_url='',NULL,nailists.image_url), '" . \Config::get('no_image_nailist') . "') AS nailist_image_url")
        )
            ->from(self::$_table_name)
            ->join('admins', 'LEFT')
            ->on(self::$_table_name . '.admin_id', '=', 'admins.id')
            ->join('nailists', 'LEFT')
            ->on(self::$_table_name . '.nailist_id', '=', 'nailists.id');
        // filter by keyword
        if (!empty($param['order_id'])) {
            $query->where('order_id', '=', $param['order_id']);
        }
        if (!empty($param['admin_id'])) {
            $query->where('admin_id', '=', $param['admin_id']);
        }
        if (!empty($param['nailist_id'])) {
            $query->where('nailist_id', '=', $param['nailist_id']);
        }
        if (!empty($param['type'])) {
            $query->where('type', '=', $param['type']);
        }
        if (!empty($param['sort'])) {
        	$sortExplode = explode('-', $param['sort']);
        	$query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
        	$query->order_by(self::$_table_name . '.created', 'DESC');
        }
        
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable log by type
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool True if success or false if error
     */
    public static function disable_by_type($param)
    {
        $options['where'] = array(
            'order_id' => $param['order_id'],
            'type'     => $param['type'],
            'disable'  => '0'
        );
        $log = self::find('first', $options);
        if (empty($log)) {
            self::errorNotExist('order_nailist_log__id');
            return false;
        }
        $log->set('disable', '1');
        // save to database
        if ($log->save()) {
            return true;
        }
        return false;
    }
}
