<?php

class Model_Order_Nail_Hologram extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'order_id',
		'nail_id',
		'hologram_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'order_nail_holograms';

}
