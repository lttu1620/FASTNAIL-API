<?php

/**
 * Any query in Model Order Daily Limit
 *
 * @package Model
 * @created 2015-04-01
 * @version 1.0
 * @author Thai Lai
 * @copyright Oceanize INC
 */
class Model_Order_Daily_Limit extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'shop_id',
        'date',
        'limit',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'order_daily_limits';

    /**
     * Add or update info for Order Daily Limit
     *
     * @author Thai Lai
     * @param array $param Input data
     * @return int|bool Order Daily Limit id or false if error
     */
    public static function add_update($param)
    {
        $param['date'] = strtotime($param['date']);
        $options['where'] = array(
            'shop_id' => $param['shop_id'],
            'date'    => $param['date'],
        );
        $limit = self::find('first', $options);
        if (empty($limit)) {
            $limit = new self;
        }
        if (!empty($param['shop_id'])) {
            $limit->set('shop_id', $param['shop_id']);
        }
        if (!empty($param['date'])) {
            $limit->set('date', $param['date']);
        }
        if (isset($param['limit']) && $param['limit'] != '') {
            $limit->set('limit', $param['limit']);
        }
        if ($limit->save()) {
            if (empty($limit->id)) {
                $limit->id = self::cached_object($limit)->_original['id'];
            }
            return !empty($limit->id) ? $limit->id : 0;
        }
        return false;
    }

    /**
     * Get list Order Daily Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order Daily Limit
     */
    public static function get_list($param)
    {
        $shop = Model_Shop::find($param['shop_id']);
        if (!$shop) {
            static::errorNotExist('shop_id');
            return false;
        }
        $param['date'] = strtotime($param['date']);
        $calendar = \Lib\Calendar::get_calendar($param['date']);
        $param['date_from'] = $calendar['firstDayOfCalendar'];
        $param['date_to'] = $calendar['lastDayOfCalendar'];
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.shop_id',
            DB::expr("
                DATE(FROM_UNIXTIME(order_daily_limits.date)) AS date
            "),
            self::$_table_name . '.limit',
            self::$_table_name . '.created',
            self::$_table_name . '.updated',
            self::$_table_name . '.disable'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::$_table_name . '.date', '>=', self::date_from_val($param['date_from']))
            ->where(self::$_table_name . '.date', '<=', self::date_to_val($param['date_to']))
            ->where(self::$_table_name . '.disable', '=', '0')
            ->order_by(self::$_table_name . '.date', 'ASC');
        $dailyLimit = $query->execute()->as_array();
        // distributing by day
        $firstDayOfLastMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) - 1, 1, date('Y')));
        $firstDayOfNextMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) + 1, 1, date('Y')));
        $data = array(
            'calendarInfo' => array(
                'thisMonth'           => date('n', $param['date']),
                'thisYear'            => date('Y', $param['date']),
                'firstDayOfLastMonth' => $firstDayOfLastMonth,
                'lastMonth'           => date('n', strtotime($firstDayOfLastMonth)),
                'yearOfLastMonth'     => date('Y', strtotime($firstDayOfLastMonth)),
                'firstDayOfNextMonth' => $firstDayOfNextMonth,
                'nextMonth'           => date('n', strtotime($firstDayOfNextMonth)),
                'yearOfNextMonth'     => date('Y', strtotime($firstDayOfNextMonth))
            )
        );
        for ($i = 0; $i < $calendar['daysInCalendar']; $i++) {
            $dayInCalendar = strtotime('+' . $i . 'days', strtotime($calendar['firstDayOfCalendar']));
            $temp = array(
                'dateInfo' => array(
                    'date'        => date('Y-m-d', $dayInCalendar),
                    'day'         => date('j', $dayInCalendar),
                    'isSunday'    => date('w', $dayInCalendar) == '0' ? true : false,
                    'isThisMonth' => date('Y-m', $dayInCalendar) == date('Y-m', $param['date']),
                    'isToday'     => $dayInCalendar == strtotime(date('Y-m-d', time()))
                )
            );
            foreach ($dailyLimit as $item) {
                $dayInOrder = strtotime($item['date']);
                if ($dayInCalendar == $dayInOrder) {
                    $temp['dateData'] = $item;
                }
            }
            if (!isset($temp['dateData'])) {
                $temp['dateData']['limit'] = $shop->get('order_daily_limit');
            }
            $data['calendarData'][] = $temp;
        }
        return $data;
    }

    /**
     * Disable/Enable list Order Daily Limit
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $shop = self::find($id);
            if ($shop) {
                $shop->set('disable', $param['disable']);
                if (!$shop->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('order_daily_limit_id');
                return false;
            }
        }
        return true;
    }
}
