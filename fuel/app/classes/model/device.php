<?php

class Model_Device extends Model_Abstract {
    /**
     * Add or update info for Device
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_properties = array(
        'id',
        'shop_id',
        'name',
        'device_type',
        'created',
        'updated',
        'disable',
    );
    /**
     * Add or update info for Device
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'devices';
    /**
     * Add or update info for Device
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $query = new self;
        // check exist
        if (!empty($id)) {
            $query = self::find($id);
            if (empty($query)) {
                self::errorNotExist('id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $query->set('name', $param['name']);
        }
        if (!empty($param['shop_id'])) {
            $query->set('shop_id', $param['shop_id']);
        }
        if (isset($param['device_type']) && $param['device_type'] != '') {
            $query->set('device_type', $param['device_type']);
        }
        // save to database
        if ($query->save()) {
            if (empty($query->id)) {
                $query->id = self::cached_object($query)->_original['id'];
            }
            return !empty($query->id) ? $query->id : 0;
        }
        return false;
    }
    /**
     * Get list Device (using array count)
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array List shells
     */
    public static function get_list($param) {
        $query = DB::select(
                self::$_table_name . '.*',
                array('shops.name', 'shop_name')
        )
        ->from(self::$_table_name)
        ->join('shops', 'LEFT')
        ->on(self::$_table_name . '.shop_id', '=', 'shops.id');
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['device_type'])) {
            $query->where(self::$_table_name . '.device_type', '=', $param['device_type']);
        }
        if (isset($param['shop_id']) && $param['shop_id'] !== '') {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.id', 'ASC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Device (without array count)
     *
     * @author Hoang Gia Thong
     * @return array List shells
     */
    public static function get_all($param) {
        $query = DB::select(self::$_table_name.'.*',
            array('shops.name', 'shop_name')
            )
            ->from(self::$_table_name)
            ->join('shops')
            ->on(self::$_table_name.'.shop_id','=','shops.id')
            ->where(self::$_table_name.'.disable', '=', '0');
        
        // filter by shop_id
        if (!empty($param['shop_id'])) {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        if (isset($param['device_type']) && $param['device_type'] != '') {
            $query->where(self::$_table_name . '.device_type', '=', $param['device_type']);
        }
        $query->order_by(self::$_table_name . '.name', 'ASC');
        
        // get data
        $data = $query->execute()->as_array();

        if (isset($param['check_avalable'])                  
            && !empty($param['order_start_date'])
            && !empty($param['order_end_date'])
            && !empty($param['shop_id'])
        ) {          
            $result = array();
            if (!empty($data)) {
                $devicesId = \Lib\Arr::field($data, 'id');
                $deviceList = Model_Order_Device::get_all(array(
                    'device_id' => $devicesId,
                    'not_in_order_id' => !empty($param['not_in_order_id']) ? $param['not_in_order_id'] : 0,
                    'order_start_date' => $param['order_start_date'],
                    'order_end_date' => $param['order_end_date'],
                    'shop_id' => $param['shop_id'],                
                ));
                if (!empty($deviceList)) {
                    $orderDeviceId = \Lib\Arr::field($deviceList, 'device_id');
                	foreach ($data as $item) {
                        if (!in_array($item['id'], $orderDeviceId)) {                			
                            $result[] =  $item;
                        }
                	}  
                } else{
                    return $data;    
                }
            }
            return $result;         
        }        
        return $data;
    }

    /**
     * Disable/Enable list Device
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param) {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $admin = self::find($id);
            if ($admin) {
                $admin->set('disable', $param['disable']);
                if (!$admin->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Device
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array|bool Detail shells or false if error
     */
    public static function get_detail($param) {
         $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('id');
            return false;
        }
        return $data;
    }

    /**
     * Get all available Device
     *
     * @author Caolp
     * @return array
     */
    public static function get_available_device_by_name($param) {
        if (empty($param['shop_id'])) {
            self::errorParamInvalid();
            return false;
        }
        $query = DB::select(
                self::$_table_name.'.*'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name.'.name', '=', $param['device_name']);
        // filter by shop_id
        if (!empty($param['shop_id'])) {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['device_type'])) {
            $query->where(self::$_table_name . '.device_type', '=', $param['device_type']);
        }
        $query->order_by(self::$_table_name . '.id', 'ASC');
        // get data
        $data = $query->execute()->as_array();
        if (isset($param['check_avalable'])
            && !empty($param['order_start_date'])
            && !empty($param['order_end_date'])
            && !empty($param['shop_id'])
        ) {
            $result = array();
            if (!empty($data)) {
                $devicesId = \Lib\Arr::field($data, 'id');
                $deviceList = Model_Order_Device::get_all(array(
                    'device_id' => $devicesId,
                    'not_in_order_id' => !empty($param['not_in_order_id']) ? $param['not_in_order_id'] : 0,
                    'order_start_date' => $param['order_start_date'],
                    'order_end_date' => $param['order_end_date'],
                    'shop_id' => $param['shop_id'],
                ));
                if (!empty($deviceList)) {
                    $orderDeviceId = \Lib\Arr::field($deviceList, 'device_id');
                    foreach ($data as $item) {
                        if (!in_array($item['id'], $orderDeviceId)) {
                            $result[] =  $item;
                        }
                    }
                } else {
                    return $data;
                }
            }
            return $result;
        }
        return $data;
    }
    
    /**
     * Get all ordered devices
     *
     * @author thailh
     * @return array
     */
    public static function get_order_device_by_name($param) {
        if (empty($param['shop_id'])) {
            self::errorParamInvalid();
            return false;
        }
        $data = DB::select(
                self::$_table_name.'.*'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name.'.name', '=', $param['device_name'])
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->order_by(self::$_table_name . '.id', 'ASC')
            ->execute()
            ->as_array();
        if (!empty($data)) {
            $devicesId = Lib\Arr::field($data, 'id');
            $orderDevices = Model_Order_Device::get_all(array(
                'device_id' => $devicesId,
                'not_in_order_id' => !empty($param['not_in_order_id']) ? $param['not_in_order_id'] : 0,
                'order_start_date' => $param['order_start_date'],
                'order_end_date' => $param['order_end_date'],
                'shop_id' => $param['shop_id'],
            ));
            $result = array();
            if (!empty($orderDevices)) {
                foreach ($orderDevices as $item) {
                    if (!isset($result[$item['order_id']])) {
                        $result[$item['order_id']] = array();
                    }
                    $result[$item['order_id']][] = $item['device_id'];
                }
            }
            return $result;
        }
        return array();
    }
    
    /**
     * Get all available Device
     *
     * @author Caolp
     * @return array
     */
    public static function get_available_device_by_type($param) {
        if (empty($param['shop_id'])) {
            self::errorParamInvalid('shop_id');
            return false;
        }
        if (empty($param['device_type'])) {
            self::errorParamInvalid('device_type');
            return false;
        }
        if (empty($param['order_start_date']) || empty($param['order_end_date'])) {
            self::errorParamInvalid('start_date_or_end_date');
            return false;
        }
        $data = DB::select(
                self::$_table_name.'.*'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::$_table_name . '.device_type', '=', $param['device_type'])
            ->order_by(self::$_table_name . '.id', 'ASC')
            ->execute()
            ->as_array();        
        if (!empty($data)) {
            $devicesId = \Lib\Arr::field($data, 'id');
            $deviceList = Model_Order_Device::get_all(array(
                'device_id' => $devicesId,
                'not_in_order_id' => !empty($param['not_in_order_id']) ? $param['not_in_order_id'] : 0,
                'order_start_date' => $param['order_start_date'],
                'order_end_date' => $param['order_end_date'],
                'shop_id' => $param['shop_id'],
            ));           
            if (!empty($deviceList)) {
                $orderDeviceId = \Lib\Arr::field($deviceList, 'device_id');
                foreach ($data as $item) {
                    if (!in_array($item['id'], $orderDeviceId)) {
                        return $item;
                    }
                }
            } else {
                return $data[0];
            }
        }
        return array();
    }
    
}
