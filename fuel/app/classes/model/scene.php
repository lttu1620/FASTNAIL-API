<?php

class Model_Scene extends Model_Abstract
{
	/** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'icon',
        'active_icon',
        'header_icon',
        'mobile_icon',
        'mobile_active_icon',
        'mobile_header_icon',
        'created',
        'updated',
        'disable',
    );
    /** @var array $_observers field of table */
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    /** @var array $_table_name name of table */

	protected static $_table_name = 'scenes';
	/**
     * Add and update info of scene.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $scene = new self;
        if (!empty($id)) {
            $scene = self::find($id);
            if (empty($scene)) {
                static::errorNotExist('scene_id', $id);
                return false;
            }
        }        
        if (isset($param['name'])) {
            $scene->set('name', $param['name']);
        }
        if (isset($param['icon'])) {
            $scene->set('icon', $param['icon']);
        }
        if (isset($param['active_icon'])) {
            $scene->set('active_icon', $param['active_icon']);
        }
        if (isset($param['header_icon'])) {
            $scene->set('header_icon', $param['header_icon']);
        }
        if (isset($param['mobile_icon'])) {
            $scene->set('mobile_icon', $param['mobile_icon']);
        }
        if (isset($param['mobile_active_icon'])) {
            $scene->set('mobile_active_icon', $param['mobile_active_icon']);
        }
        if (isset($param['mobile_header_icon'])) {
            $scene->set('mobile_header_icon', $param['mobile_header_icon']);
        }
        if ($scene->save()) {
            if (empty($scene->id)) {
                $scene->id = self::cached_object($scene)->_original['id'];
            }
            return !empty($scene->id) ? $scene->id : 0;
        }
        return false;
    }

    /**
     * Get detail scene.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array|bool Detail or false if error.
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('scene_id');
            return false;
        }
        return $data;
    }

    /**
     * Disable/enable scene.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $scene = self::find($id);
            if ($scene) {
                $scene->set('disable', $param['disable']);
                if (!$scene->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('tag_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all scene.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get list tags.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (empty($param['page']) ) 
        {
            $param['page'] = 1;
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

}
