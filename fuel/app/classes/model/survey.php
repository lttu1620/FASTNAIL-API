<?php

/**
 * Any query in Model Survey
 *
 * @package Model
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Survey extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'order_id',
        'title',
        'url',
        'started',
        'finished',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'surveys';

    /**
     * Add or update info for Survey
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Survey id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $survey = new self;
        // check exist
        if (!empty($id)) {
            $survey = self::find($id);
            if (empty($survey)) {
                self::errorNotExist('survey_id');
                return false;
            }
        }
        // set value
        if (!empty($param['order_id'])) {
            $survey->set('order_id', $param['order_id']);
        }
        if (!empty($param['title'])) {
            $survey->set('title', $param['title']);
        }
        if (!empty($param['url'])) {
            $survey->set('url', $param['url']);
        }
        if (!empty($param['started'])) {
            $survey->set('started', self::time_to_val($param['started']));
        }
        if (!empty($param['finished'])) {
            $survey->set('finished', self::time_to_val($param['finished']));
        }
        // save to database
        if ($survey->save()) {
            if (empty($survey->id)) {
                $survey->id = self::cached_object($survey)->_original['id'];
            }
            return !empty($survey->id) ? $survey->id : 0;
        }
        return false;
    }

    /**
     * Get list Survey (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Survey
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['order_id'])) {
            $query->where('order_id', '=', $param['order_id']);
        }
        if (!empty($param['title'])) {
            $query->where('title', 'LIKE', "%{$param['title']}%");
        }
        if (!empty($param['url'])) {
            $query->where('url', 'LIKE', "%{$param['url']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.finished', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.started', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Survey (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Survey
     */
    public static function get_all()
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        // filter by keyword
        if (!empty($param['order_id'])) {
            $query->where('order_id', '=', $param['order_id']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.started', '>=', self::date_from_val($param['date_from']));
            $query->or_where(self::$_table_name . '.finished', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.started', '<=', self::date_to_val($param['date_to']));
            $query->or_where(self::$_table_name . '.finished', '>=', self::date_from_val($param['date_to']));
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Survey
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $survey = self::find($id);
            if ($survey) {
                $survey->set('disable', $param['disable']);
                if (!$survey->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('survey_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Survey
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Survey or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('survey_id');
            return false;
        }
        return $data;
    }
}
