<?php

/**
 * Any query in Model Sub Order
 *
 * @package Model
 * @created 2015-09-15
 * @version 1.0
 * @author DienNVT
 * @copyright Oceanize INC
 */
class Model_Sub_Order extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'order_id',
        'order_start_date',
        'order_end_date',
        'hf_section',
        'off_nails',
        'nail_length',
        'nail_type',
        'nail_add_length',
        'problem',
        'request',
        'sr_start_date',
        'sr_end_date',
        'off_start_date',
        'off_end_date',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'sub_orders';

    
    /**
     * Get all sub order of a order
     *
     * @author thailh
     * @param array $param Input data
     * @return int|bool Order id or false if error
     */
    public static function get_all($param) {        
         return DB::select()
            ->from(self::$_table_name)               
            ->where(self::$_table_name . '.order_id', '=', $param['order_id'])
            ->where(self::$_table_name . '.disable', '=', '0')
            ->order_by('id')
            ->execute()
            ->as_array();         
    }
    
    /**
     * Add/Update info for Sub Order
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Order id or false if error
     */
    public static function add_update($param) {
        if (isset($param['order_start_date']) && !is_numeric($param['order_start_date'])) {
            $param['order_start_date'] = self::time_to_val($param['order_start_date']);
        }
        if (isset($param['order_end_date']) && !is_numeric($param['order_end_date'])) {
            $param['order_end_date'] = self::time_to_val($param['order_end_date']);
        }
        if (isset($param['sr_start_date']) && !is_numeric($param['sr_start_date'])) {
            $param['sr_start_date'] = self::time_to_val($param['sr_start_date']);
        }
        if (isset($param['sr_end_date']) && !is_numeric($param['sr_end_date'])) {
            $param['sr_end_date'] = self::time_to_val($param['sr_end_date']);
        }
        if (isset($param['off_start_date']) && !is_numeric($param['off_start_date'])) {
            $param['off_start_date'] = self::time_to_val($param['off_start_date']);
        }
        if (isset($param['off_end_date']) && !is_numeric($param['off_end_date'])) {
            $param['off_end_date'] = self::time_to_val($param['off_end_date']);
        }
        $sub_order = new self;
        if (!empty($param['id'])) {
            $sub_order = self::find($param['id']);
            if (empty($sub_order)) {
                self::errorNotExist('sub_order_id');
                return false;
            }
            $param['order_id'] = $sub_order->get('order_id');
        }
        $sub_order->set('order_id', $param['order_id']);
        $sub_order->set('disable', '0');
        if (isset($param['order_start_date']) && $param['order_start_date'] !== '') {
            $sub_order->set('order_start_date', $param['order_start_date']);
        }
        
        if (isset($param['order_end_date']) && $param['order_end_date'] !== '') {
            $sub_order->set('order_end_date', $param['order_end_date']);
        }

        if (isset($param['sr_start_date']) && $param['sr_start_date'] !== '') {
            $sub_order->set('sr_start_date', $param['sr_start_date']);
        }

        if (isset($param['sr_end_date']) && $param['sr_end_date'] !== '') {
            $sub_order->set('sr_end_date', $param['sr_end_date']);
        }

        if (isset($param['off_start_date']) && $param['off_start_date'] !== '') {
            $sub_order->set('off_start_date', $param['off_start_date']);
        }

        if (isset($param['off_end_date']) && $param['off_end_date'] !== '') {
            $sub_order->set('off_end_date', $param['off_end_date']);
        }

        if (isset($param['hf_section']) && $param['hf_section'] !== '') {
            $sub_order->set('hf_section', $param['hf_section']);
        }
        
        if (isset($param['off_nails']) && $param['off_nails'] !== '') {
            $sub_order->set('off_nails', $param['off_nails']);
        }
        
        if (isset($param['nail_length']) && $param['nail_length'] !== '') {
            $sub_order->set('nail_length', $param['nail_length']);
        }
        
        if (isset($param['nail_type']) && $param['nail_type'] !== '') {
            $sub_order->set('nail_type', $param['nail_type']);
        }
        
        if (isset($param['nail_add_length']) && $param['nail_add_length'] !== '') {
            $sub_order->set('nail_add_length', $param['nail_add_length']);
        }
        
        if (isset($param['problem'])) {
            $sub_order->set('problem', $param['problem']);
        }
        
        if (isset($param['request'])) {
            $sub_order->set('request', $param['request']);
        }
        
        if ($sub_order->save()) {
            if (empty($sub_order->id)) {
                $sub_order->id = self::cached_object($sub_order)->_original['id'];
            }
            return $sub_order->id;
        }
        return false;
    }
    
    /**
     * Add info for Sub Order
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Order id or false if error
     */
    public static function add($param) {
        
        if (!empty($param['order_id'])) {
            $order = Model_Order::find($param['order_id']);
            if (empty($order)) {
                static::errorNotExist('order_id');
                return false;
            }
        }

        if (isset($param['order_start_date']) && !is_numeric($param['order_start_date'])) {
            $param['order_start_date'] = self::time_to_val($param['order_start_date']);
        }
        if (isset($param['order_end_date']) && !is_numeric($param['order_end_date'])) {
            $param['order_end_date'] = self::time_to_val($param['order_end_date']);
        }
        if (isset($param['sr_start_date']) && !is_numeric($param['sr_start_date'])) {
            $param['sr_start_date'] = self::time_to_val($param['sr_start_date']);
        }
        if (isset($param['sr_end_date']) && !is_numeric($param['sr_end_date'])) {
            $param['sr_end_date'] = self::time_to_val($param['sr_end_date']);
        }
        if (isset($param['off_start_date']) && !is_numeric($param['off_start_date'])) {
            $param['off_start_date'] = self::time_to_val($param['off_start_date']);
        }
        if (isset($param['off_end_date']) && !is_numeric($param['off_end_date'])) {
            $param['off_end_date'] = self::time_to_val($param['off_end_date']);
        }

        $sub_order = new self;

        $sub_order->set('order_id', $param['order_id']);

        if (isset($param['order_start_date']) && $param['order_start_date'] !== '') {
            $sub_order->set('order_start_date', $param['order_start_date']);
        }

        if (isset($param['order_end_date']) && $param['order_end_date'] !== '') {
            $sub_order->set('order_end_date', $param['order_end_date']);
        }

        if (isset($param['sr_start_date']) && $param['sr_start_date'] !== '') {
            $sub_order->set('sr_start_date', $param['sr_start_date']);
        }

        if (isset($param['sr_end_date']) && $param['sr_end_date'] !== '') {
            $sub_order->set('sr_end_date', $param['sr_end_date']);
        }

        if (isset($param['off_start_date']) && $param['off_start_date'] !== '') {
            $sub_order->set('off_start_date', $param['off_start_date']);
        }

        if (isset($param['off_end_date']) && $param['off_end_date'] !== '') {
            $sub_order->set('off_end_date', $param['off_end_date']);
        }

        if (isset($param['hf_section']) && $param['hf_section'] !== '') {
            $sub_order->set('hf_section', $param['hf_section']);
        }
        
        if (isset($param['off_nails']) && $param['off_nails'] !== '') {
            $sub_order->set('off_nails', $param['off_nails']);
        }
        
        if (isset($param['nail_length']) && $param['nail_length'] !== '') {
            $sub_order->set('nail_length', $param['nail_length']);
        }
        
        if (isset($param['nail_type']) && $param['nail_type'] !== '') {
            $sub_order->set('nail_type', $param['nail_type']);
        }
        
        if (isset($param['nail_add_length']) && $param['nail_add_length'] !== '') {
            $sub_order->set('nail_add_length', $param['nail_add_length']);
        }
        
        if (isset($param['problem'])) {
            $sub_order->set('problem', $param['problem']);
        }
        
        if (isset($param['request'])) {
            $sub_order->set('request', $param['request']);
        }

        if ($sub_order->save()) {
            if (empty($sub_order->id)) {
                $sub_order->id = self::cached_object($sub_order)->_original['id'];
            }
            return $sub_order->id;
        }

        return false;
    }

    /**
     * Update info for Sub Order
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Order id or false if error 
     */
    public static function update_sub_order($param) {
        if (!empty($param['id'])) {
            $sub_order = self::find($param['id']);
            if (empty($sub_order)) {
                self::errorNotExist('sub_order_id');
                return false;
            }
        }

        if (!empty($param['order_id'])) {
            $order = Model_Order::find($param['order_id']);
            if (empty($order)) {
                static::errorNotExist('order_id');
                return false;
            }
        }

        if (isset($param['order_start_date']) && !is_numeric($param['order_start_date'])) {
            $param['order_start_date'] = self::time_to_val($param['order_start_date']);
        }
        if (isset($param['order_end_date']) && !is_numeric($param['order_end_date'])) {
            $param['order_end_date'] = self::time_to_val($param['order_end_date']);
        }
        if (isset($param['sr_start_date']) && !is_numeric($param['sr_start_date'])) {
            $param['sr_start_date'] = self::time_to_val($param['sr_start_date']);
        }
        if (isset($param['sr_end_date']) && !is_numeric($param['sr_end_date'])) {
            $param['sr_end_date'] = self::time_to_val($param['sr_end_date']);
        }
        if (isset($param['off_start_date']) && !is_numeric($param['off_start_date'])) {
            $param['off_start_date'] = self::time_to_val($param['off_start_date']);
        }
        if (isset($param['off_end_date']) && !is_numeric($param['off_end_date'])) {
            $param['off_end_date'] = self::time_to_val($param['off_end_date']);
        }
        
        $sub_order->set('order_id', $param['order_id']);

        if (isset($param['order_start_date']) && $param['order_start_date'] !== '') {
            $sub_order->set('order_start_date', $param['order_start_date']);
        }

        if (isset($param['order_end_date']) && $param['order_end_date'] !== '') {
            $sub_order->set('order_end_date', $param['order_end_date']);
        }

        if (isset($param['sr_start_date']) && $param['sr_start_date'] !== '') {
            $sub_order->set('sr_start_date', $param['sr_start_date']);
        }

        if (isset($param['sr_end_date']) && $param['sr_end_date'] !== '') {
            $sub_order->set('sr_end_date', $param['sr_end_date']);
        }

        if (isset($param['off_start_date']) && $param['off_start_date'] !== '') {
            $sub_order->set('off_start_date', $param['off_start_date']);
        }

        if (isset($param['off_end_date']) && $param['off_end_date'] !== '') {
            $sub_order->set('off_end_date', $param['off_end_date']);
        }

        if (isset($param['hf_section']) && $param['hf_section'] !== '') {
            $sub_order->set('hf_section', $param['hf_section']);
        }
        
        if (isset($param['off_nails']) && $param['off_nails'] !== '') {
            $sub_order->set('off_nails', $param['off_nails']);
        }
        
        if (isset($param['nail_length']) && $param['nail_length'] !== '') {
            $sub_order->set('nail_length', $param['nail_length']);
        }
        
        if (isset($param['nail_type']) && $param['nail_type'] !== '') {
            $sub_order->set('nail_type', $param['nail_type']);
        }
        
        if (isset($param['nail_add_length']) && $param['nail_add_length'] !== '') {
            $sub_order->set('nail_add_length', $param['nail_add_length']);
        }
        
        if (isset($param['problem'])) {
            $sub_order->set('problem', $param['problem']);
        }
        
        if (isset($param['request'])) {
            $sub_order->set('request', $param['request']);
        }

        if ($sub_order->save()) {
            if (empty($sub_order->id)) {
                $sub_order->id = self::cached_object($sub_order)->_original['id'];
            }
            return $sub_order->id;
        }

        return false;
    }
    
    /**
     * Add/Update sub orders from a order
     *
     * @author thailh
     * @param array $param Input data
     * @param bool $new New | Edit
     * @return int|bool Order id or false if error
     */
    public static function add_update_by_order($param, $new = true) {
        if (empty($param['order_id'])) {
            static::errorNotExist('order_id');
            return false;
        } 
        $order = Model_Order::find($param['order_id']);
        if (empty($order)) {
            static::errorNotExist('order_id');
            return false;
        }
            
        // check hand & foot services
        $param['service_id'] = array(
            1, // Hオフオン
            2, // Hオーダーオン
            3, // Fオフオン
            4, // Fオーダーオン
        );
        $order_service = Model_Order_Service::get_all($param); 
        $countH = 0;
        $countF = 0;
        foreach ($order_service as $service) { 
            preg_match('/H.+/', $service['name'], $name);
            if (!empty($name[0])) {
                $countH++;
            }
            preg_match('/F.+/', $service['name'], $name);
            if (!empty($name[0])) {
                $countF++;
            }
        }
        if ($countH >= 1 && $countF >= 1) { // exists hand and foot services
            if ($order->get('hf_section') != 3) {
                $order->set('hf_section', 3);
                $order->save();
            }
            
            $sub_orders = DB::select()
                ->from(self::$_table_name)               
                ->where(self::$_table_name . '.order_id', '=', $param['order_id'])
                ->execute()
                ->as_array();
            
            $sub_order_param = array();
            $time = $order->get('order_end_date') - $order->get('order_start_date');
            for ($t = 15*60; $t <= $time; $t += 15*60) {
                if ($t >= $t/2) {
                    $sub_order_param = array(
                        array(
                            'order_start_date' => $order->get('order_start_date'),
                            'order_end_date' => $order->get('order_start_date') + $t,
                        ),
                        array(
                            'order_start_date' => $order->get('order_start_date') + $t,
                            'order_end_date' => $order->get('order_end_date'),
                        ),
                    );
                    break;
                }
            }            
            foreach ($sub_order_param as $i => $sub) {  
                $sub['order_id'] = $order->get('id');
                $sub['hf_section'] = ($i == 0 ? 1 : 2);
                if (empty($sub_orders[$i]['id'])) {
                    $sub['id'] = 0;
                    $sub['off_nails'] = $order->get('off_nails');
                    $sub['nail_length'] = $order->get('nail_length');
                    $sub['nail_type'] = $order->get('nail_type');
                    $sub['nail_add_length'] = $order->get('nail_add_length');
                    $sub['problem'] = $order->get('problem');
                    $sub['request'] = $order->get('request');
                } else {
                    $sub['id'] = $sub_orders[$i]['id'];
                }
                if (!self::add_update($sub)) {
                    return false;
                }
            }
        } elseif ($new == false) {
            DB::update(self::$_table_name)
                ->value('disable', '1')
                ->where('disable', '0')                
                ->where('order_id', $param['order_id'])                
                ->execute();
            if ($order->get('hf_section') == 3) {
                $order->set('hf_section', 0);
                $order->save();
            }
        }
        return true;
    }
    
}
