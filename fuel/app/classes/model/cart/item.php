<?php

/**
 * Any query in Model Cart Item
 *
 * @package Model
 * @created 2015-03-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Cart_Item extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'cart_id',
        'item_id',
        'price',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'cart_items';

    /**
     * Add info for Cart Item
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Cart Item id or false if error
     */
    public static function add($param)
    {
        $options['where'] = array(
            'id'      => $param['item_id'],
            'disable' => '0'
        );
        $item = \Model_Item::find('first', $options);
        if (empty($item)) {
            static::errorNotExist('item_id');
            return false;
        }
        $query = DB::select(
            array('items.id', 'item_id'),
            array('cart_items.id', 'cart_item_id'),
            array('cart_items.cart_id', 'cart_id'),
            array('cart_items.disable', 'cart_disable')
        )
            ->from('items')
            ->join(
                DB::expr(
                    "(SELECT * FROM cart_items
                     WHERE cart_id = {$param['cart_id']}) AS cart_items"
                ),
                'LEFT'
            )
            ->on('items.id', '=', 'cart_items.item_id')
            ->where('items.id', '=', $param['item_id'])
            ->where('items.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);
        if (empty($data['item_id'])) {
            static::errorNotExist('item_id');
            return false;
        }
        if (!empty($data['cart_id']) && $data['cart_disable'] == 0) {
            static::errorDuplicate('cart_id');
            return false;
        }
        $new = false;
        if (!empty($data['cart_id']) && $data['cart_disable'] == 1) {
            $dataUpdate = array(
                'id'      => $data['cart_item_id'],
                'disable' => '0',
                'price'   => $item->get('price')
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'item_id' => $data['item_id'],
                'cart_id' => $param['cart_id'],
                'price'   => $item->get('price')
            );
        }
        $cart = new self($dataUpdate, $new);
        if ($cart->save()) {
            if ($new == true) {
                $cart->id = self::cached_object($cart)->_original['id'];
            }
            return $cart->id;
        }
        return false;
    }

    /**
     * Get all Cart Item (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Cart Item
     */
    public static function get_all($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('cart_id', '=', $param['cart_id'])
            ->where('disable', '=', '0');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Cart Item
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'][] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'][] = array(
                'cart_id' => $param['cart_id'],
                'item_id' => $param['item_id'],
                'disable' => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $param['cart_id'] = $data->_data['cart_id'];
                $param['item_id'] = $data->_data['item_id'];
                return true;
            }
        } else {
            if (!empty($param['id'])) {
                static::errorNotExist('id');
            } else {
                static::errorNotExist('cart_item_id');
            }
        }
        return false;
    }
}