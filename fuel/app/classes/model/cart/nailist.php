<?php

/**
 * Any query in Model Cart Nailist
 *
 * @package Model
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Cart_Nailist extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'cart_id',
        'nailist_id',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'cart_nailists';

    /**
     * Add or update info for Cart Nailist
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Cart Nailist id or false if error
     */
    public static function add_update($param)
    {
        if (empty($param['nailist_id']) || empty($param['cart_id'])) {
            self::errorParamInvalid('nailist_id_or_cart_id');
            return false;
        }
        if (!isset($param['disable'])) {
            $param['disable'] = '0';
        }
        $options['where'] = array(
            'nailist_id' => $param['nailist_id'],
            'cart_id'    => $param['cart_id'],
        );
        $log = self::find('first', $options);
        if (empty($log)) {
            $log = new self;
        }
        $log->set('nailist_id', $param['nailist_id']);
        $log->set('cart_id', $param['cart_id']);
        $log->set('disable', $param['disable']);
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for Cart Nailist by card id
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Cart Nailist id or false if error
     */
    public static function add_update_by_cart_id($param)
    {
        // convert string param to array
        $arrayNailist = array_filter(array_unique(explode(',', $param['nailist_id'])));
        // check cart exist
        $options['where'] = array(
            'id' => $param['cart_id'],
            'disable' => 0
        );
        $cart = Model_Cart::find('first', $options);
        if (empty($cart)) {
            self::errorNotExist('cart_id');
            return false;
        }
        // check nailist exist
        if (!empty($arrayNailist)) {
            $countNailist = Model_Nailist::query()
                ->where('id', 'in', $arrayNailist)
                ->where('disable', '=', '0')
                ->count();
            if ($countNailist != count($arrayNailist)) {
                self::errorNotExist('nailist_id');
                return false;
            }
        }
        // get current log in database
        $options['where'] = array(
            'cart_id' => $param['cart_id'],
            'disable' => 0
        );
        $currentLog = self::find('all', $options);
        // get nailist id from current log
        $currentNailistInLog = array ();
        foreach ($currentLog as $log) {
            $currentNailistInLog[] = $log->get('nailist_id');
        }
        // distribute param to add and update
        $addUpdateNailistInLog = array_diff($arrayNailist, $currentNailistInLog);
        $disableNailistInLog = array_diff($currentNailistInLog, $arrayNailist);
        $dataLog = array();
        foreach ($addUpdateNailistInLog as $nailistId) {
            $dataLog[] = array(
                'cart_id'    => $param['cart_id'],
                'nailist_id' => $nailistId,
                'disable'    => '0'
            );
        }
        foreach ($disableNailistInLog as $nailistId) {
            $dataLog[] = array(
                'cart_id'    => $param['cart_id'],
                'nailist_id' => $nailistId,
                'disable'    => '1'
            );
        }
        // add and update to database
        foreach ($dataLog as $log) {
            if (!self::add_update($log)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get all Cart Nailist (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Cart Nailist
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            'nailists.code',
            'nailists.name'
        )
            ->from(self::$_table_name)
            ->join('nailists')
            ->on(self::$_table_name . '.nailist_id', '=', 'nailists.id')
            ->join('carts')
            ->on(self::$_table_name . '.cart_id', '=', 'carts.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('nailists.disable', '=', '0')
            ->where('carts.disable', '=', '0');
        if (!empty($param['cart_id'])) {
            $query->where('cart_id', '=', $param['cart_id']);
        }
        if (!empty($param['nailist_id'])) {
            $query->where('nailist_id', '=', $param['nailist_id']);
        }
        // get data
        $data = $query->execute()->as_array();

        //return data
        return $data;
    }

}
