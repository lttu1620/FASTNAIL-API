<?php

class Model_Prefecture extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'area_id',
		'name',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'prefectures';

    /**
     * Get list of prefectures
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of prefectures
     */
    public static function get_list($param)
    {
         $query = DB::select(
                self::$_table_name . '.*',
                array('areas.name', 'area_name')
            )
            ->from(self::$_table_name)
            ->join('areas', 'LEFT')
            ->on(self::$_table_name . '.area_id', '=', 'areas.id');
        // filter by keyword
        if (!empty($param['area_id'])) {
            $query->where(self::$_table_name . '.area_id', "{$param['area_id']}");
        }        
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }        
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
    
    /**
     * Get all of prefectures
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of prefectures
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . '.*',
                array('areas.name', 'area_name')
            )
            ->from(self::$_table_name)
            ->join('areas', 'LEFT')
            ->on(self::$_table_name . '.area_id', '=', 'areas.id');
        // filter by keyword
        if (!empty($param['area_id'])) {
            $query->where(self::$_table_name .  '.area_id', "{$param['area_id']}");
        }        
        if (empty($param['disable'])) {
            $param['disable'] = 0;
        }
        $query->where(self::$_table_name . '.disable', $param['disable'])
            ->order_by(self::$_table_name . '.id', 'ASC');
        
        // Get data
        $data = $query->execute()->as_array();
        
        //Return data
        return $data;
    }
    
    /**
     * Add or update info for prefecture
     *
     * @author Tran Xuan Khoa
     * @param array $param Values of each field of prefecture
     * @return int|bool Prefecture id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $area = new self;
        // check exist
        if (!empty($id)) {
            $area = self::find($id);
            if (empty($area)) {
                self::errorNotExist('prefecture_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $area->set('name', $param['name']);
        }
        if (!empty($param['area_id'])) {
            $area->set('area_id', $param['area_id']);
        }
        // save to database
        if ($area->save()) {
            if (empty($area->id)) {
                $area->id = self::cached_object($area)->_original['id'];
            }
            return !empty($area->id) ? $area->id : 0;
        }
        return false;
    }
    
    /**
     * Disable/Enable list Prefecture
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        
        foreach ($ids as $id) {
            $area = self::find($id);
            
            if (!$area) {
                self::errorNotExist('area_id');
                return false;
            }
            
            $area->set('disable', 1);
            if (!$area->save()) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Get detail Prefecture
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return array|bool Detail Prefecture or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('area_id');
            return false;
        }
        return $data;
    }
}
