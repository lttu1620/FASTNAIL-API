<?php
/**
 * Any query in Model_User_Selected_Nail_Log
 *
 * @package Model
 * @created 2015-05-28
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_User_Selected_Nail_Log extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'user_id',
		'nail_id',
		'unselected_nail_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'user_selected_nail_logs';
        
     /**
     * Add info for Survey Log
     *
     * @author truongnn
     * @param array $param Input data
     * @return int|bool insertedID  or false if error
     */
    public static function add($param)
    {       // var_dump('abc');exit;
        $log = new self;
        // set value
        if (isset($param['user_id']) && $param['user_id'] != '') {
            $log->set('user_id', $param['user_id']);
        }
        if (isset($param['nail_id']) && $param['nail_id'] != '') {
            $log->set('nail_id', $param['nail_id']);
        }
        if (isset($param['unselected_nail_id']) && $param['unselected_nail_id'] != '') {
            $log->set('unselected_nail_id', $param['unselected_nail_id']);
        }
        // save to database
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list user_selected_nail Log
     *
     * @author truongnn
     * @param array $param Input data
     * @return array List user_selected_nail
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            'users.first_name',
            'users.last_name',
            'users.name',
            DB::expr("IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('" . \Config::get('item_img_url')['nails'] . "', photo_cd, '" . ".jpg" . "'), nails.photo_url) as image_url")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name .'.user_id', '=', 'users.id')
            ->join('nails', 'LEFT')
            ->on(self::$_table_name .'.nail_id', '=', 'nails.id');
        // filter by keyword
        if (isset($param['user_id']) && $param['user_id'] != '') {
            $query->where('user_id', '=', $param['user_id']);
        }
         // filter by keyword
        if (isset($param['nail_id']) && $param['nail_id'] != '') {
            $query->where(DB::expr("user_selected_nail_logs.nail_id = {$param['nail_id']} OR user_selected_nail_logs.unselected_nail_id = {$param['nail_id']}"));
        }
         // filter by keyword
        if (isset($param['unselected_nail_id']) && $param['unselected_nail_id'] != '') {
            $query->where('unselected_nail_id', '=', $param['unselected_nail_id']);
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'like', "%{$param['name']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name.'.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name.'.created', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(($sortExplode[0] == 'name' ? 'users' : self::$_table_name) . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
