<?php

/**
 * Any query in Model User Point Log
 *
 * @package Model
 * @created 2015-06-17
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Point_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'point_get_id',
        'point_item_id',
        'point',
        'type',
        'expire_date',
        'user_bought_item_id',
        'is_paid',
        'return_id',
        'order_id',
        'created'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load', 
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'user_point_logs';

    /**
     * Add info for User Point Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Point Log id or false if error
     */
    public static function add($param)
    {
        $user = Model_User::find($param['login_user_id']);
        if (empty($user)) {
            self::errorNotExist('login_user_id');
            return false;
        }
        if (empty($param['point']) || empty($param['type'])) {
            self::errorParamInvalid('point');
            return false;
        }
        // check over user\'s point
        if (isset($param['used_point']) && $param['point'] > $user->get('point')) {
            self::errorOther(self::ERROR_CODE_OTHER_1, 'point', 'Over user\'s point');
            return false;
        }
        if (!isset($param['point_get_id'])) {
            $param['point_get_id'] = 0;
        }
        if (!isset($param['point_item_id'])) {
            $param['point_item_id'] = 0;
        }
        $log = new self;
        $log->set('user_id', $param['login_user_id']);        
        $log->set('point', $param['point']);
        $log->set('type', $param['type']);        
        if (isset($param['point_get_id'])) {
            $log->set('point_get_id', $param['point_get_id']);
        }
        if (isset($param['point_item_id'])) {
            $log->set('point_item_id', $param['point_item_id']);
        }
        if (isset($param['user_bought_item_id'])) {
            $log->set('user_bought_item_id', $param['user_bought_item_id']);
        }
        if (isset($param['is_paid'])) {
            $log->set('is_paid', $param['is_paid']);
        }
        if (isset($param['return_id'])) {
            $log->set('return_id', $param['return_id']);
        }
        if (isset($param['order_id'])) {
            $log->set('order_id', $param['order_id']);
        }
        $usedType = array(
            \Config::get('user_point_logs.type.point_item'),
            \Config::get('user_point_logs.type.point_used_by_order'),
            \Config::get('user_point_logs.type.point_expired'),
        );
        if (!in_array($param['type'], $usedType)) {
            if (isset($param['expire_date'])) {
                $log->set('expire_date', $param['expire_date']);
            } else {
                $log->set('expire_date', self::time_to_val(date('Y-m-d')) + Config::get('point_expire_days') * 24*60*60);
            }      
        }
        $get_point = false;
        if(isset($param['get_point'])) $get_point = true;
        if ($log->create()) {
            $log->id = self::cached_object($log)->_original['id'];
            if (isset($param['used_point'])) {
                //use point
                $user->set('point', $user->get('point') - $param['point']);
                Model_User_Point_Used_Log::add_multiple(
                    array (
                        'user_point_log_id' => $log->id,
                        'point' => $param['point'],
                        'point_list' => Model_User_Point_Log::get_list_for_batch(
                            array(
                                'user_id' => $param['login_user_id'],
                                'not_expire' => time(),
                            )
                        ),
                    )
                );
                if (self::error()) {
                    return false;
                }
            } else {
                $user->set('point', $user->get('point') + $param['point']);
            }
            if ($get_point) {
                return $user->get('point');
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list User Point Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Point Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            DB::expr("IF (user_point_logs.point > 0, user_point_logs.point, 0) AS got_point"),
            DB::expr("IF (user_point_logs.point < 0, ABS(user_point_logs.point), 0) AS used_point"),
            'users.name',
            'users.email',
            'point_items.description',
            array('point_items.name', 'point_item_name'),
            DB::expr("
                CONCAT('".str_replace('{token}', '', \Config::get('user_bought_items.qr_link'))."', user_bought_items.token) AS link_token
            ")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('point_items', 'LEFT')
            ->on(self::$_table_name.'.point_item_id', '=', 'point_items.id')
            ->join('user_bought_items', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'user_bought_items.user_id')
            ->on(self::$_table_name.'.point_item_id', '=', 'user_bought_items.point_item_id');

        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['type'])) {
            if (!is_array($param['type']) 
                && $param['type'] == \Config::get('user_point_logs.type.point_item')) {                
                $query->where(self::$_table_name.'.point_item_id', '<>', 0);
            } else {
                $query->where(self::$_table_name.'.type', 'IN', (array) $param['type']);
            }
        }
        if (!empty($param['username'])) {
            $query->where('users.name', 'LIKE', "%{$param['username']}%");
        }
        if (!empty($param['email'])) {
            $query->where('users.email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array('total' => $total, 'data' => $data);
    }

    /**
     * Get list User Point Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Point Log
     */
    public static function get_list_for_mobile($param)
    {
        $query = DB::select(                
                self::$_table_name.'.created',
               'point_items.description',
                DB::expr("
                    CASE
                        WHEN user_point_logs.type = '".\Config::get('user_point_logs.type.point_used_by_order')."' THEN FROM_UNIXTIME(`orders`.`reservation_date`, '%m月%d日の予約で使用')
                        ELSE point_items.name
                    END AS point_item_name
                "),
                DB::expr("
                    CASE
                        WHEN user_point_logs.type = '".\Config::get('user_point_logs.type.point_return')."' THEN 'ポイント返還'
                        WHEN user_point_logs.type = '".\Config::get('user_point_logs.type.point_return_by_cancel_order')."' THEN 'ポイント返還'
                        ELSE point_gets.name
                      END AS point_get_name
                "),                
                DB::expr("IF(ISNULL(user_bought_items.read_date),0,1) AS qr_is_read"),
                DB::expr("IF(ISNULL(user_bought_items.completed),0,1) AS qr_is_completed"),
                DB::expr("IF(ISNULL(user_bought_items.canceled),0,1) AS qr_is_canceled"),
                //DB::expr("SUM(user_point_logs.point) AS point"),
                DB::expr("SUM(IF(user_point_logs.point > 0, user_point_logs.point, 0)) AS got_point"),
                DB::expr("SUM(IF(user_point_logs.point < 0, ABS(user_point_logs.point), 0)) AS used_point"),
                DB::expr("
                    CONCAT('".str_replace('{token}', '', \Config::get('user_bought_items.qr_link'))."', user_bought_items.token) AS link_token
                ")
            )
            ->from(self::$_table_name)            
            ->join('point_items', 'LEFT')
            ->on(self::$_table_name.'.point_item_id', '=', 'point_items.id')
            ->join('point_gets', 'LEFT')
            ->on(self::$_table_name.'.point_get_id', '=', 'point_gets.id')
            ->join('user_bought_items', 'LEFT')           
            ->on(self::$_table_name.'.user_bought_item_id', '=', 'user_bought_items.id')
            ->join('orders', 'LEFT')
            ->on(self::$_table_name.'.order_id', '=', 'orders.id');

        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['type'])) {           
            $query->where(self::$_table_name.'.type', 'IN', (array) $param['type']);            
        }        
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
            $query->order_by(self::$_table_name . '.id', 'DESC');
        }
        $query->group_by(DB::expr("
            user_point_logs.type, 
            user_point_logs.created, 
            user_point_logs.user_bought_item_id, 
            user_point_logs.order_id
        "));
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array('total' => $total, 'data' => $data);
    }
    
    /**
     * Get list User Point Log for Batch
     *
     * @author CaoLP
     * @param array $param Input data
     * @return array List User Point Log
     */
    public static function get_list_for_batch($param)
    {
        $query = DB::select(
                self::$_table_name.'.user_id',
                self::$_table_name.'.id',
                self::$_table_name.'.point',
                DB::expr('IF(sum(`user_point_used_logs`.`point`) IS NULL, 0 , sum(`user_point_used_logs`.`point`) ) as `minus`')
            )
            ->from(self::$_table_name)
            ->join('user_point_used_logs', 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'user_point_used_logs.user_point_log_used_id');
        if (isset($param['expired_yesterday'])) {
            $param['type'] = array(
                \Config::get('user_point_logs.type.access_link'),
                \Config::get('user_point_logs.type.answer_survey'),
                \Config::get('user_point_logs.type.capture_photo'),
                \Config::get('user_point_logs.type.register_from_other_site'),
                \Config::get('user_point_logs.type.register_beauty'),
                \Config::get('user_point_logs.type.point_return_by_cancel_order'),
            );
            $query->where(self::$_table_name.'.type', 'IN', $param['type']);
            $query->where(DB::expr("
                expire_date IS NOT NULL
                AND FROM_UNIXTIME(expire_date, '%Y-%m-%d') <= ADDDATE(CURDATE(),-1)"
            ));
        }
        if (isset($param['not_expire'])) {
            $query->where(DB::expr("
                FROM_UNIXTIME(expire_date, '%Y-%m-%d') >= CURDATE()"
            ));
        }
        if (isset($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        $query->where(self::$_table_name.'.point', '>=', 0);
        $query->group_by(self::$_table_name.'.id', self::$_table_name.'.point');
        if (isset($param['set_expire']) && $param['set_expire']) {
            $query->having(DB::expr('(`point` - `minus`)'), '<>', 0);
        }
        $query->order_by(self::$_table_name.'.expire_date', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }
    /**
 * Cancel User Point Log
 *
 * @author CaoLP
 * @param array $param Input data
 * @return true|false
 */
    public static function cancel_item($param){
        if (empty($param['user_bought_item_id'])) {
            self::errorParamInvalid('user_bought_item_id');
            return false;
        }
        $point_used_list = self::get_list_point_used(array('user_bought_item_id' => $param['user_bought_item_id']));
        $expired_point = 0; 
        foreach ($point_used_list as $item) {
            if ($item['is_expired']) {
                $expired_point += $item['point'];
            } else {
                $param = array(
                    'login_user_id' => $item['user_id'],
                    'type' => \Config::get('user_point_logs.type.point_return'),
                    'point_get_id' => 0,
                    'user_bought_item_id' => $param['user_bought_item_id'],
                    'point' => $item['point'],
                    'expire_date' => $item['expire_date'],
                    'is_paid' => 1,
                    'order_id' => 0,
                    'return_id' => $item['id'],
                );
                self::add($param);
            }
            if (self::error()) {
                return false;
            }
        }              
        return $expired_point;
    }
    
    /**
     * Confirm Cancel User Point Log [point item]
     *
     * @author CaoLP
     * @param array $param Input data
     * @return message/false
     */
    public static function confirm_cancel_item($param){
        if (empty($param['user_bought_item_id'])) {
            self::errorParamInvalid('user_bought_item_id');
            return false;
        }
        //1440453600
        $point_used_list = self::get_list_point_used(array('user_bought_item_id'=>$param['user_bought_item_id']));
        $expired_point = 0;
        foreach ($point_used_list as $item) {
            if ($item['is_expired']) {
                $expired_point += $item['point'];
            }
        }
        if ($expired_point > 0) {
            self::errorOther(self::ERROR_CODE_OTHER_2, 'point', 'キャンセルすると期限切れのポイントが消失します。消失ポイント: ' . $expired_point . 'ポイント');
            return false;
        }
        return true;
    }
    /**
     * confirm Cancel User Point Log [Order]
     *
     * @author CaoLP
     * @param array $param Input data
     * @return true|false
     */
    public static function confirm_cancel_order($param){
        if (empty($param['order_id'])) {
            self::errorParamInvalid('order_id');
            return false;
        }
        //get available order point to cal
        $query = DB::select(
            self::$_table_name. '.id',
            self::$_table_name. '.point',
            DB::expr(self::$_table_name. '.point + sum(b.point) as sum_point')
        )
            ->from(self::$_table_name)
            ->join(DB::expr(self::$_table_name . ' as b'),'left')
            ->on(self::$_table_name. '.id', '=', 'b.return_id')
            ->where(self::$_table_name. '.order_id' , '=' , $param['order_id'])
            ->where(self::$_table_name. '.return_id' , '=' , null)
            ->group_by(self::$_table_name.'.id')
            ->having('sum_point', '<>', '0')
            ->or_having('sum_point', '=', null);
        $data = $query->execute()->as_array();
        $ids = array();
        foreach($data as $d){
            $ids[] = $d['id'];
        }
        $point_used_list = self::get_list_point_used(array('order_id'=>$param['order_id'],'user_point_log_id' => $ids));
        $expired_point = 0;
        foreach($point_used_list as $item){
            if($item['is_expired']) $expired_point+=$item['point'];
        }
        if($expired_point > 0){
            self::errorOther(self::ERROR_CODE_OTHER_2, 'point', 'キャンセルすると期限切れのポイントが消失します。消失ポイント: '.$expired_point.'ポイント');
            return false;
        }
        return true;
    }
    /**
     * Cancel User Point Log
     *
     * @author CaoLP
     * @param array $param Input data
     * @return true|false
     */
    public static function cancel_order($param){
        if (empty($param['order_id'])) {
            self::errorParamInvalid('order_id');
            return false;
        }
        //get available order point to cal
        $query = DB::select(
            self::$_table_name. '.id',
            self::$_table_name. '.point',
            DB::expr(self::$_table_name. '.point + sum(b.point) as sum_point')
        )
            ->from(self::$_table_name)
            ->join(DB::expr(self::$_table_name . ' as b'),'left')
            ->on(self::$_table_name. '.id', '=', 'b.return_id')
            ->where(self::$_table_name. '.order_id', $param['order_id'])
            ->where(self::$_table_name. '.return_id', null)
            ->group_by(self::$_table_name.'.id')
            ->having('sum_point', '<>', '0')
            ->or_having('sum_point', '=', null);
        $data = $query->execute()->as_array();
        $ids = array();
        foreach($data as $d){
            $ids[] = $d['id'];
        }
        $point_used_list = self::get_list_point_used(array(
            'order_id' => $param['order_id'], 
            'user_point_log_id' => $ids
        ));
        foreach($point_used_list as $item){
            $param = array(
                'login_user_id' => $item['user_id'],
                'type' => \Config::get('user_point_logs.type.point_return_by_cancel_order'),
                'point_get_id' => 0,
                'order_id' => $param['order_id'],
                'point' => $item['point'],
                'expire_date' => $item['expire_date'],
                'is_paid' => 1,
                'return_id' => $item['user_point_log_id'],
            );
            self::add($param);
            if (self::error()) {
                return false;
            }
        }
        return true;
    }
    /**
     * Get list User Point Used Item
     *
     * @author Caolp
     * @param array $param Input data
     * @return array List User Point Used Item
     */
    public static function get_list_point_used($param)
    {
        $query = DB::select(
                self::$_table_name.'.user_id',
                'second.point_get_id',
                'second.expire_date',
                DB::expr("IF(FROM_UNIXTIME(second.expire_date, '%Y-%m-%d') <= ADDDATE(CURDATE(),-1),1,0) as `is_expired`"),
                'user_point_used_logs.*'
            )
            ->from(self::$_table_name)
            ->join('user_point_used_logs')
            ->on(self::$_table_name.'.id', '=', 'user_point_used_logs.user_point_log_id')
            ->join(DB::expr('user_point_logs as second'))
            ->on('user_point_used_logs.user_point_log_used_id', '=', 'second.id');
        // filter by keyword
        if (!empty($param['user_bought_item_id'])) {
            $query->where(self::$_table_name.'.user_bought_item_id', $param['user_bought_item_id']);
        }
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name.'.order_id', $param['order_id']);
            if (count($param['user_point_log_id']) > 0) {
                $query->where('user_point_used_logs.user_point_log_id', $param['user_point_log_id']);
            }
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }    
    
}
