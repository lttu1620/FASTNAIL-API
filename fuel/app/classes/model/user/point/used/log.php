<?php

/**
 * Any query in Model User Point Used Item
 *
 * @package Model
 * @created 2015-08-18
 * @version 1.0
 * @author Caolp
 * @copyright Oceanize INC
 */
class Model_User_Point_Used_Log extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'user_point_log_id',
		'user_point_log_used_id',
		'point',
		'created',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'user_point_used_logs';

	/**
	 * Add info for User Point Used Log
	 *
	 * @author Caolp
	 * @param array $param Input data
	 * @return int|bool User Point Used Log id or false if error
	 */
	public static function add($param)
	{
		if (!isset($param['user_point_log_id'])) {
			self::errorParamInvalid('user_point_log_id');
			return false;
		}
		if (!isset($param['user_point_log_used_id'])) {
			self::errorParamInvalid('user_point_log_used_id');
			return false;
		}
		if (!isset($param['point'])) {
			self::errorParamInvalid('point');
			return false;
		}
		$log = new self;
		$log->set('user_point_log_id', $param['user_point_log_id']);
		$log->set('user_point_log_used_id', $param['user_point_log_used_id']);
		$log->set('point', $param['point']);

		if ($log->create()) {
			$log->id = self::cached_object($log)->_original['id'];
			return !empty($log->id) ? $log->id : 0;
		}
		return false;
	}
	/**
	 * Add info for User Point Used Log
	 *
	 * @author Caolp
	 * @param array $param Input data
	 * @return int|bool User Point Used Log id or false if error
	 */
	public static function add_multiple($param)
	{
		if (!isset($param['user_point_log_id'])) {
			self::errorParamInvalid('user_point_log_id');
			return false;
		}
		if (!isset($param['point'])) {
			self::errorParamInvalid('point');
			return false;
		}
		if (!isset($param['point_list'])) {
			self::errorParamInvalid('point_list');
			return false;
		}
		$id_used = array ();
		$point_after = $param['point'];
		foreach ($param['point_list'] as $point_item) {
			$point = ($point_item['point'] - $point_item['minus']);
			$point_after = $point_after - $point;
			if ($point == 0) continue;
			$id_used[$point_item['id']] = $point;
			if ($point_after <= 0) {
				$id_used[$point_item['id']] = $point + $point_after;
				break;
			}
		}
		if(count($id_used) == 0){
			return false;
		}
		$query = \DB::insert(self::$_table_name)
			->columns(array('user_point_log_id', 'user_point_log_used_id', 'point'));
		foreach ($id_used as $key => $point) {
			$query->values(array($param['user_point_log_id'], $key , $point));
		}
		if ($query->execute()) {
			return true;
		}
		return false;
	}
	/**
	 * Get list User Point Used Item
	 *
	 * @author Caolp
	 * @param array $param Input data
	 * @return array List User Point Used Item
	 */
	public static function get_list($param)
	{
		$query = DB::select(
			self::$_table_name.'.*',
			'user_point_logs.expire_date'
		)
			->from(self::$_table_name)
			->join('user_point_logs')
			->on(self::$_table_name.'.user_point_log_used_id','=','user_point_logs.id');
		// filter by keyword
		if (!empty($param['user_point_log_used_id'])) {
			$query->where(self::$_table_name.'.user_point_log_used_id', '=', $param['user_point_log_used_id']);
		}
		// get data
		$data = $query->execute()->as_array();
		return $data;
	}
}
