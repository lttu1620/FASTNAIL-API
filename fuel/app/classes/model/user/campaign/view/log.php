<?php

class Model_User_Campaign_View_Log extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'campaign_id',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'user_campaign_view_logs';

    /**
     * Add info for User Campaign View Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Campaign View Log id or false if error
     */
    public static function add($param)
    {
        $self = new self;
        $self->set('user_id', $param['login_user_id']);
        $self->set('campaign_id', $param['campaign_id']);
        if ($self->create()) {
            $self->id = self::cached_object($self)->_original['id'];
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list User Campaign View Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Campaign View Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            array('users.name', 'user_name'),
            array('campaigns.name', 'campaign_name')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('campaigns')
            ->on(self::$_table_name.'.campaign_id', '=', 'campaigns.id');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['campaign_id'])) {
            $query->where(self::$_table_name.'.campaign_id', '=', $param['campaign_id']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
