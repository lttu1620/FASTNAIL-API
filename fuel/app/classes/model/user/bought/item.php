<?php

/**
 * Any query in Model User Bought Item
 *
 * @package Model
 * @created 2015-08-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Bought_Item extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'point_item_id',
        'point_item_name',
        'point',
        'quantity',
        'read_date',
        'completed',
        'canceled',
        'token',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Model\Observer_Log'     => array(
            'events'          => array(
                'after_load',
                'after_create',
                'after_update',
                'after_delete',
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'user_bought_items';

    /**
     * Add and update info for User Bought Item
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Bought Item id or false if error
     */
    public static function add_update($param)
    {               
        $user = Model_User::find($param['login_user_id']);
        if (empty($user)) {
            self::errorNotExist('login_user_id');
            return false;
        }    
        // check exist point item        
        $point_item = Model_Point_Item::find($param['point_item_id']);
        if (empty($point_item)) {
            self::errorNotExist('point_item_id');
            return false;
        }
        if (empty($point_item->get('point'))) {
            self::errorOther(self::ERROR_CODE_OTHER_1, 'point', 'Point is zero');
            return false;
        } 
       
        // check over quantity limit
        $user_bought_items = self::find('all', array(
            'where' => array(
                'user_id' => $user->get('id'),
                'point_item_id' => $point_item->get('id'),               
                'canceled' => null,               
            )
        ));        
       
        $bought_quantity = 0;
        foreach ($user_bought_items as $item) {
            $bought_quantity += $item['quantity'];
        }
        if (!empty($point_item->get('limit_quantity')) 
            && $bought_quantity + $param['quantity'] > $point_item->get('limit_quantity')) {
            self::errorOther(self::ERROR_CODE_OTHER_2, 'limit_quantity', 'Over quantity limit');
            return false;
        }
        
        // check over user\'s point
        if ($param['quantity'] * $point_item->get('point') > $user->get('point')) {
            self::errorOther(self::ERROR_CODE_OTHER_3, 'point', 'Over user\'s point');
            return false;
        }
       
        // set value for self
        $self = new self;
        $self->set('user_id', $user->get('id'));        
        $self->set('point_item_id', $point_item->get('id'));
        $self->set('point_item_name', $point_item->get('name')); 
        $self->set('point', $point_item->get('point'));
        $self->set('quantity', $param['quantity']);        
        $token = \Lib\Str::generate_token();
        $self->set('token', $token);
        if ($self->create()) {
            Model_User_Point_Log::add(
                array (
                    'login_user_id' => $self->get('user_id'),
                    'type' => \Config::get('user_point_logs.type.point_item'),
                    'point_item_id' => $self->get('point_item_id'),
                    'point' => $self->get('point') * $self->get('quantity'),
                    'user_bought_item_id' => $self->get('id'),
                    'order_id' => 0,
                    'is_paid' => 0,
                    'used_point' => true,
                )
            );
            if (self::error()) {
                $self->delete(); // delete inserted user_bought_items if error
                return false;
            }
            $result = array(
                'id' => $self->get('id'),
                'link' => str_replace('{token}', $token, \Config::get('user_bought_items.qr_link')),
                'point' => $user->get('point'),
                'remain_quantity' => $point_item->get('limit_quantity') - ($bought_quantity + $self->get('quantity'))
            );
            return $result;
        }
        return false;
    }

    /**
     * Get list User Bought Item (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Bought Item
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            array('users.name', 'user_name'),
            DB::expr(
                "CASE
                    WHEN completed IS NULL
                         AND read_date IS NOT NULL
                         AND canceled IS NULL THEN 0
                    ELSE 1
                END AS is_completed"
            ),
            DB::expr(
                "CASE
                    WHEN completed IS NULL
                         AND read_date IS NOT NULL
                         AND canceled IS NULL THEN 0
                    ELSE 1
                END AS set_completed"
            ),
            DB::expr(
                "CASE
                    WHEN completed IS NULL
                         AND canceled IS NULL THEN 0
                    ELSE 1
                END AS is_canceled"
            ),
            DB::expr(
                "CASE
                    WHEN completed IS NULL
                         AND canceled IS NULL THEN 0
                    ELSE 1
                END AS set_canceled"
            ),
            DB::expr("FORMAT(user_bought_items.point*user_bought_items.quantity,0) AS total_point"),
            DB::expr("CONCAT(user_bought_items.quantity, 'x', FORMAT(user_bought_items.point,0), '=', FORMAT(user_bought_items.point*user_bought_items.quantity,0)) AS total_point_text")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where('users.name', 'LIKE', "%{$param['user_id']}%");
        }
        if (!empty($param['point_item_id'])) {
            $query->where(self::$_table_name.'.point_item_id', '=', $param['point_item_id']);
        }
        if (!empty($param['point_item_name'])) {
            $query->where(self::$_table_name.'.point_item_name', 'LIKE', "%{$param['point_item_name']}%");
        }
        if (!empty($param['status'])) {
            switch ($param['status']) {
                case '1': // Bought items
                    $query->where(self::$_table_name.'.canceled', '=', null);
                    break;
                case '2': // Canceled items
                    $query->where(self::$_table_name.'.canceled', '!=', null);
                    break;
                case '3': // Completed
                    $query->where(self::$_table_name.'.completed', '!=', null);
                    break;
                case '4': // Scaned qr items
                    $query->where(self::$_table_name.'.read_date', '!=', null);
                    break;
                case '5': // Not scan qr items
                    $query->where(self::$_table_name.'.read_date', '=', null);
                    break;
            }
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Scan QR
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array detail User Bought Item
     */
    public static function scan_qr($param)
    {  
        if (empty($param['token'])) {
            self::errorParamInvalid('token', 'トークンの有効期限が切れてしまいました‚');
            return false;
        }
        $token = $param['token'];        
        $self = self::find('first', array(
            'where' => array(
                array('token', '=', $token),                                 
                array('read_date', '=', NULL),                                 
                array('completed', '=', NULL),                                 
            )
        ));
        if (empty($self)) {
            self::errorNotExist('token', 'トークンの有効期限が切れてしまいました‚');
            return false;
        }
        // get user info
        $user = \Model_User::find($self->get('user_id'));
        if (!empty($user) && $user->get('disable') == 1) {
            self::errorNotExist('user_id', 'ユーザが見つかりません');
            return false;
        }
        $self->set('read_date', time());
        if ($self->save()) {            
            return $self->get('id');
        }
        return false;
    }

    /**
     * Complete process
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array detail User Bought Item
     */
    public static function completed($param)
    {
        $options['where'] = array(
            'id' => $param['id'],
            array('read_date', '!=', null),
            array('completed', '=', null),
            array('canceled', '=', null)
        );
        $self = self::find('first', $options);
        if (empty($self)) {
            self::errorNotExist('user_bought_item_id');
            return false;
        }
        $self->set('completed', time());
//        $options['where'] = array(
//            'user_bought_item_id' => $self->get('id')
//        );
//        if (Model_User_Point_Log::find('first', $options)) {
//            $self->set('is_paid', '1');
//        }
        if ($self->save()) {
            return true;
        }
        return false;
    }

    /**
     * Cancel item
     *
     * @author Caolp
     * @param array $param Input data
     * @return array detail User Bought Item
     */
    public static function cancel($param)
    { 
        $self = self::find('first', array(
            'where' => array(
                'id' => $param['id'],
                array('canceled', '=', null),
                array('completed', '=', null)
            )
        ));
        if (empty($self)) {
            self::errorNotExist('user_bought_item_id');
            return false;
        }
        if (!empty($param['confirm'])) {
            return Model_User_Point_Log::confirm_cancel_item(array('user_bought_item_id' => $param['id']));
        }
        $self->set('canceled', time());
        $result = Model_User_Point_Log::cancel_item(array('user_bought_item_id' => $param['id']));
        $user = Model_User::find($self->get('user_id'));
        if ($result > 0) {
            $result = array('point' => $user->get('point'), 'message' => '');
        }
        if ($self->save()) {
//            if (!Model_Push_Message::add_update(array(
//                'receive_user_id' => $self->get('user_id'),
//                'message' => $self->get('point_item_name') . 'がキャンセルされました。',
//                'param' => json_encode(array(
//                    'id' => $self->get('id'),
//                    'point_item_id' => $self->get('point_item_id'),
//                    'point_item_name' => $self->get('point_item_name'),
//                )),
//                'type' => \Config::get('push_messages.type.cancel_bought_item'),
//                'is_sent' => 0
//            ))) {
//                \LogLib::error('Can not insert to push_message', __METHOD__, $param);
//                return false;
//            }
            return $result;
        }
        return false;
    }
    
}