<?php

class Model_User_Beauty_View_Log extends Model_Abstract
{

    protected static $_properties = array(
        'id',
        'user_id',
        'beauty_id',
        'image_url',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'user_beauty_view_logs';

    /**
     * Add and update info of user_log.
     *
     * @author thailh
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        if (empty($param['user_id']) || empty($param['beauty_id'])) {
            self::errorParamInvalid('user_id_or_beauty_id');
            return false;
        }
        $self = self::find(
            'first',
            array(
                'where' => array(
                    'user_id'   => $param['user_id'],
                    'beauty_id' => $param['beauty_id'],
                ),
            )
        );
        if (empty($self)) {
            $beauty = Model_Beauty::get_detail(
                array(
                    'beauty_id'   => $param['beauty_id'],
                    'for_mission' => 1,
                )
            );
            // check exist
            if (empty($beauty)) {
                self::errorNotExist('beauty_id');
                return false;
            }
            // check mission_type
            if ($beauty['mission_type'] != \Config::get('beauties.type.register')) {
                self::errorParamInvalid('beauty_id');
                return false;
            }
            $param['type'] = \Config::get('user_point_logs.type.register');
            $param['point'] = $beauty['point'];
            $param['point_get_id'] = $beauty['point_get_id'];
            $param['point_item_id'] = 0;
            if (!Model_User_Point_Log::add($param)) {
                \LogLib::warning("Can not write user_point_logs", __METHOD__, $param);
                return false;
            }
            $self = new self;
        }
        $self->set('disable', '0');
        $self->set('user_id', $param['user_id']);
        $self->set('beauty_id', $param['beauty_id']);
        if ($self->save()) {
            if (empty($self->id)) {
                $self->id = self::cached_object($self)->_original['id'];
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get detail user_beauties
     *
     * @author thailh
     * @param array $param Input data.
     * @return array|bool Detail  or false if error.
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            array('users.name', 'user_name'),
            array('beauties.title', 'beauty_title')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('beauties')
            ->on(self::$_table_name.'.beauty_id', '=', 'beauties.id')
            ->where(self::$_table_name.'.id', $param['id'])
            ->where(self::$_table_name.'.disable', '0');
        $data = $query->execute()->as_array();
        if (empty($data)) {
            self::errorNotExist('id');
            return false;
        }
        return $data;
    }

    /**
     * Approve image
     *
     * @author thailh
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $beauty = Model_Beauty::get_detail(
            array(
                'login_user_id' => $param['user_id'],
                'id'            => $param['beauty_id'],
            )
        );
        if (empty($beauty)) {
            self::errorNotExist('beauty_id');
            return false;
        }
        if ($beauty['is_used'] == 1) {
            self::errorDuplicate('beauty_id', 'Used');
            return false;
        }
        $items = self::find(
            'all',
            array(
                'where' => array(
                    'user_id'   => $param['user_id'],
                    'beauty_id' => $param['beauty_id'],
                    'disable'   => '1',
                ),
            )
        );
        if (empty($items)) {
            self::errorNotExist('user_id_or_beauty_id');
            return false;
        }
        foreach ($items as $item) {
            $item->set('disable', '0');
            if (!$item->save()) {
                self::errorOther(self::ERROR_CODE_OTHER_1, 'update');
                return false;
            }
        }
        if (!Model_User_Point_Log::add(
            array(
                'login_user_id' => $param['user_id'],
                'point_get_id'  => $beauty['point_get_id'],
                'point'         => $beauty['point'],
                'type'          => Config::get('user_point_logs.type.capture_photo'),
            )
        )
        ) {
            return false;
        }
        if (!Model_Push_Message::add_update(array(
            'receive_user_id' => $param['user_id'],
            'message' => $beauty['point'].'ポイントGetしました',
            'param' => json_encode(array(
                'beauty_id' => $beauty['id'],
                'title' => $beauty['title'],
                'point' => $beauty['point'],
            )),
            'type' => \Config::get('push_messages.type.approve_mission_take_photo'),
            'is_sent' => 0,
        ))) {
            \LogLib::error('Can not insert to push_message', __METHOD__, $param);
            return false;
        }
        return true;
    }

    /**
     * Get all user_log.
     *
     * @author thailh
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $data = DB::select(
            self::$_table_name.'.*',
            array('users.name', 'user_name'),
            array('beauties.title', 'beauty_title')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('beauties')
            ->on(self::$_table_name.'.beauty_id', '=', 'beauties.id')
            ->where(self::$_table_name.'.disable', '0')
            ->execute()
            ->as_array();
        return $data;
    }

    /**
     * Add info for User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Beauty View Log id or false if error
     */
    public static function add($param)
    {
        $beauty = Model_Beauty::get_detail(
            array(
                'login_user_id' => $param['login_user_id'],
                'id'            => $param['beauty_id'],
                'for_mission'   => 1,
            )
        );
        if (empty($beauty)) {
            self::errorNotExist('beauty_id');
            return false;
        }
        if ($beauty['is_used'] == 1) {
            self::errorDuplicate('id');
            return false;
        }
        $self = new self;
        $self->set('user_id', $param['login_user_id']);
        $self->set('beauty_id', $param['beauty_id']);
        if (!empty($_FILES)) {
            $uploadResult = \Lib\Util::uploadImage();
            if (isset($uploadResult['body']['image_url'])) {
                $self->set('image_url', $uploadResult['body']['image_url']);
            }
        }
        if ($beauty['mission_type'] == Config::get('beauties.type.capture_photo')
            || $beauty['mission_type'] == Config::get('beauties.type.register_from_other_site')
        ) {
            // waiting approve
            $self->disable = 1;
        }
        if ($self->create()) {
            $self->id = self::cached_object($self)->_original['id'];
            if ($self->get('disable') == 0) {
                switch ($beauty['mission_type']) {
                    case \Config::get('beauties.type.access_link'):
                        $param['type'] = \Config::get('user_point_logs.type.access_link');
                        break;
                    case \Config::get('beauties.type.answer_survey'):
                        $param['type'] = \Config::get('user_point_logs.type.answer_survey');
                        break;
                    case \Config::get('beauties.type.register_from_other_site'):
                        $param['type'] = \Config::get('user_point_logs.type.register_from_other_site');
                        break;
                }
                if (isset($param['type'])) {
                    Model_User_Point_Log::add(
                        array(
                            'login_user_id' => $param['login_user_id'],
                            'point'         => $beauty['point'],
                            'point_get_id'  => $beauty['point_get_id'],
                            'type'          => $param['type'],
                        )
                    );
                }
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Add info for User Beauty View Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User Beauty View Log id or false if error
     */
    public static function addLog($param)
    {
        $self = new self;
        $self->set('user_id', $param['login_user_id']);
        $self->set('beauty_id', $param['beauty_id']);
        if ($self->create()) {
            $self->id = self::cached_object($self)->_original['id'];
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list User Beauty View Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List User Beauty View Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
                self::$_table_name.'.*',
                'users.name',
                'point_gets.point',
                'beauties.title',
                'beauties.description',
                'beauties.point_get_id',
                'beauties.mission_type'
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('beauties')
            ->on(self::$_table_name.'.beauty_id', '=', 'beauties.id')
            ->join(
                DB::expr(
                    "(SELECT * FROM point_gets
                    WHERE disable = 0) point_gets"
                ),
                'LEFT'
            )
            ->on('beauties.point_get_id', '=', 'point_gets.id');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', $param['user_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where('users.name', 'like', "%{$param['user_name']}%");
        }
        if (!empty($param['beauty_id'])) {
            $query->where(self::$_table_name.'.beauty_id', $param['beauty_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', $param['disable']);
        } else {
            $query->where(self::$_table_name.'.disable', '1');
        }
        $query->where(self::$_table_name.'.image_url', '!=', null);
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get page view
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_pv($param)
    {
        $query = DB::select(
            "beauty_id",
            DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),
            DB::expr("COUNT(beauty_id) AS pv")
        )
            ->from(self::$_table_name);
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name.'.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name.'.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['beauty_id'])) {
            $query->where('beauty_id', '=', $param['beauty_id']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $query->group_by('beauty_id')
            ->group_by('date')
            ->order_by('created');
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get unique data.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_uu($param)
    {
        $date_from = "";
        $date_to = "";
        if (!empty($param['date_from'])) {
            $date_from = " AND UNIX_TIMESTAMP(date(FROM_UNIXTIME(created ))) >= ".self::date_from_val(
                    $param['date_from']
                )." ";
        }
        if (!empty($param['date_to'])) {
            $date_to = " AND UNIX_TIMESTAMP(date(FROM_UNIXTIME(created ))) <= ".self::date_from_val(
                    $param['date_to']
                )." ";
        }
        $query = DB::select(
            "beauty_id",
            "date",
            DB::expr("COUNT(user_id) AS uu")
        )
            ->from(
                DB::expr(
                    "(SELECT DISTINCT user_id, beauty_id, DATE(FROM_UNIXTIME(created)) AS date
                                FROM user_beauty_view_logs
                                WHERE 1=1 {$date_from} {$date_to}
                                ) AS user_beauty_view_logs"
                )
            );

        if (!empty($param['beauty_id'])) {
            $query->where('beauty_id', '=', $param['beauty_id']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $query->group_by('beauty_id')
            ->group_by('date')
            ->order_by('date');
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get cvr.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_cvr($param)
    {
        list($cnt_pv, $pvs) = self::get_pv($param);
        list($cnt_uu, $uus) = self::get_uu($param);
        $crv = array();
        $report = array();
        foreach ($pvs as $pv) {
            foreach ($uus as $uu) {
                if (($pv['beauty_id'] == $uu['beauty_id']) && ($pv['date'] == $uu['date'])) {
                    $crv[] = array(
                        'beauty_id' => $pv['beauty_id'],
                        'date'      => strtotime($pv['date']),
                        'cvr'       => round($pv['pv'] / ($uu['uu'] * 100), 2),
                    );

                    $report[] = array(
                        'beauty_id' => $pv['beauty_id'],
                        'date'      => strtotime($pv['date']),
                        'pv'        => $pv['pv'],
                        'uu'        => $uu['uu'],
                        'cvr'       => round($pv['pv'] / ($uu['uu'] * 100), 2),
                    );
                    break;
                }
            }
        }
        if (!empty($param['get_beauty_report_all'])) {
            return array($cnt_pv, $report);
        }
        return array($cnt_pv, $crv);
    }

}
