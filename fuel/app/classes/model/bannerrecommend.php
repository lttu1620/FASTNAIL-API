<?php

class Model_BannerRecommend extends Model_Abstract {
    /**
     * Add or update info for Banner Recommend
     *
     * @author Caolp
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_properties = array(
        'id',
        'name',
        'tag_name',
        'image_url',
        'start_date',
        'end_date',
        'created',
        'updated',
        'disable',
    );
    /**
     * Add or update info for Banner Recommend
     *
     * @author Caolp
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'banner_recommends';
    /**
     * Add or update info for Banner Recommend
     *
     * @author Caolp
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $query = new self;
        // check exist
        if (!empty($id)) {
            $query = self::find($id);
            if (empty($query)) {
                self::errorNotExist('id');
                return false;
            }
        }
        // set value    
        if (!empty($param['name'])) {
            $query->set('name', $param['name']);
        }  
        if (isset($param['tag_name']) && $param['tag_name'] !='') {
            $query->set('tag_name', $param['tag_name']);
        }
        if (isset($param['image_url'])) {
            $query->set('image_url', $param['image_url']);
        }
        if (!empty($param['start_date'])) {
            $query->set('start_date', self::time_to_val( $param['start_date']));
        }
        if (!empty($param['end_date'])) {
            $query->set('end_date', self::time_to_val($param['end_date']));
        }
        if (!empty($param['disable'])) {
            $query->set('disable', $param['disable']);
        }
        // save to database
        if ($query->save()) {
            if (empty($query->id)) {
                $query->id = self::cached_object($query)->_original['id'];
            }
            return !empty($query->id) ? $query->id : 0;
        }
        return false;
    }
    /**
     * Get list Banner Recommend (using array count)
     *
     * @author Caolp
     * @param array $param Input data
     * @return array List Banner Recommend
     */
    public static function get_list($param) {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
     
        if (!empty($param['id'])) {
        	$query->where('id', '=', $param['id']);
        }
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }      
        if (!empty($param['tag_name'])) {
        	$query->where('tag_name', 'LIKE', "%{$param['tag_name']}%");
        }      
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['start_date'])) {
        	$query->where(self::$_table_name . '.created', '>=', self::time_to_val($param['start_date']));
        }
        if (!empty($param['end_date'])) {
        	$query->where(self::$_table_name . '.created', '<=', self::time_to_val($param['end_date']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.id', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Banner Recommend (without array count)
     *
     * @author Caolp
     * @return array List Banner Recommend
     */
    public static function get_all($param) {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '=', '0')
            ->where(DB::expr(
                "(start_date  <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP())"
            ));
        if (!empty($param['nail_id'])) {
            $tags = Lib\Arr::field(Model_Nail_Tag::get_all($param), 'name');
            if (empty($tags)) {
                return array();
            }
            $query->where('tag_name', 'IN', $tags);
        }
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Banner Recommend
     *
     * @author Caolp
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param) {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $bannerrecommend = self::find($id);
            if ($bannerrecommend) {
                $bannerrecommend->set('disable', $param['disable']);
                if (!$bannerrecommend->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Banner Recommend
     *
     * @author Caolp
     * @param array $param Input data
     * @return array|bool Detail Banner Recommend or false if error
     */
    public static function get_detail($param) {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('id');
            return false;
        }
        return $data;
    }

    /**
     * Get random Banner Recommend
     *
     * @author Caolp
     * @param
     * @return array|bool Detail Banner Recommend or false if error
     */
    public static function get_random() {
        $query = DB::select(
                self::$_table_name.'.id',
                self::$_table_name.'.name',
                self::$_table_name.'.image_url'
            )
            ->from(self::$_table_name)
            ->where('disable', '=', '0')
            ->where(DB::expr(
                "(start_date  <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP())"
            ))
            ->order_by(
                DB::expr("RAND()")
            )
        ->limit(1);
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }


}
