<?php

class Model_Cart extends Model_Abstract
{
	protected static $_properties = array(

		'id',
		'shop_id',
		'user_id',
		'total_price',
		'total_tax_price',
		'reservation_date',
		'user_name',
		'visit_section',
		'visit_element',
		'reservation_type',
		'hf_section',
		'off_nails',
		'nail_length',
		'nail_type',
		'nail_add_length',
		'problem',
		'pay_type',
		'photo_code',
		'is_mail',
		'is_designate',
		'welfare_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'carts';
	/**
     * Add or update info for Cart
     *
     * @author VuLTH
     * @param array $param Input data
     * @return int|bool Cart id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $cart = new self;
        // check exist
        if (!empty($id)) {
            $cart = self::find($id);
            if (empty($cart)) {
                self::errorNotExist('cart_id');
                return false;
            }
        }
        // set value
        if (!empty($param['shop_id'])) {
            $cart->set('shop_id', $param['shop_id']);
        }
        if (!empty($param['user_id'])) {
            $cart->set('user_id', $param['user_id']);
        }
        if (isset($param['total_price']) && $param['total_price'] != '') {
            $cart->set('total_price', $param['total_price']);
        }
        if (isset($param['total_tax_price']) && $param['total_tax_price'] != '') {
            $cart->set('total_tax_price', $param['total_tax_price']);
        }
        if (!empty($param['reservation_date'])) {
            $cart->set('reservation_date', self::time_to_val($param['reservation_date']));
        }
        if (!empty($param['user_name'])) {
            $cart->set('user_name', $param['user_name']);
        }
        if (!empty($param['visit_section'])) {
            $cart->set('visit_section', $param['visit_section']);
        }
        if (!empty($param['visit_element'])) {
            $cart->set('visit_element', $param['visit_element']);
        }
        if (isset($param['reservation_type']) && $param['reservation_type'] != '') {
            $cart->set('reservation_type', $param['reservation_type']);
        }
        if (!empty($param['hf_section'])) {
            $cart->set('hf_section', $param['hf_section']);
        }
        if (isset($param['off_nails']) && $param['off_nails'] != '') {
            $cart->set('off_nails', $param['off_nails']);
        }
        if (!empty($param['nail_length'])) {
            $cart->set('nail_length', $param['nail_length']);
        }
        if (isset($param['nail_type']) && $param['nail_type'] != '') {
            $cart->set('nail_type', $param['nail_type']);
        }
        if (!empty($param['nail_add_length'])) {
            $cart->set('nail_add_length', $param['nail_add_length']);
        }
        if (!empty($param['problem'])) {
            $cart->set('problem', $param['problem']);
        }
        if (isset($param['pay_type']) && $param['pay_type'] != '') {
            $cart->set('pay_type', $param['pay_type']);
        }
        if (!empty($param['photo_code'])) {
            $cart->set('photo_code', $param['photo_code']);
        }
        if (isset($param['is_mail']) && $param['is_mail'] != '') {
            $cart->set('is_mail', $param['is_mail']);
        }
        if (isset($param['is_designate']) && $param['is_designate'] != '') {
            $cart->set('is_designate', $param['is_designate']);
        }
        if (!empty($param['welfare_id'])) {
            $cart->set('welfare_id', $param['welfare_id']);
        }
        // save to database
        if ($cart->save()) {
            if (empty($cart->id)) {
                $cart->id = self::cached_object($cart)->_original['id'];
            }

            // save nailist
            if (isset($param['nailist_id'])) {
                if (!Model_Cart_Nailist::add_update_by_cart_id(array(
                        'cart_id'   => $cart->id,
                        'nailist_id' => $param['nailist_id']
                    ))) {
                    return false;
                }
            }

            return !empty($cart->id) ? $cart->id : 0;
        }
        return false;
    }

    /**
     * Get list Cart (using array count)
     *
     * @author VuLTH
     * @param array $param Input data
     * @return array List Cart
     */
    public static function get_list($param)
    {
        $query = DB::select(
                DB::expr('DISTINCT ' . self::$_table_name . '.*, shops.name as shop_name')
                //self::$_table_name . '.*',
                //array('shops.name', 'shop_name')
            )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')

            ->join('cart_nailists', 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'cart_nailists.cart_id');
        // filter by keyword
        if (!empty($param['shop_id'])) {
            $query->where('shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['nailist_id'])) {
            $query->where('cart_nailists.nailist_id', '=', $param['nailist_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (empty($param['page']) ) {
            $param['page'] = 1;
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        //array of cart
        $arr_cart_id = Lib\Arr::field($data, 'id');

        // get data of nailist
        $dataNailist = array ();
        if (!empty($arr_cart_id)) {
            $nailistQuery = DB::select(
                    array('cart_nailists.cart_id', 'cart_id'),
                    'nailists.id', 'nailists.name'
                )
                ->from('cart_nailists')
                ->join('nailists', 'INNER')
                ->on('cart_nailists.nailist_id', '=', 'nailists.id')
                ->where('cart_nailists.cart_id', 'in', $arr_cart_id)
                ->where('cart_nailists.disable', 0);

            $dataNailist = $nailistQuery->execute()->as_array();
        }

        foreach ($data as $index => $row)
        {
            $data[$index]['nailists'] = Lib\Arr::filter($dataNailist, 'cart_id', $row['id']);
        }

        return array($total, $data);
    }

    /**
     * Disable/Enable list Cart
     *
     * @author VuLTH
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $cart = self::find($id);
            if ($cart) {
                $cart->set('disable', $param['disable']);
                if (!$cart->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('cart_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Cart
     *
     * @author VuLTH
     * @param array $param Input data
     * @return array|bool Detail Cart or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('cart_id');
            return false;
        }
        return $data;
    }
    /**
     * Get all cart.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'id',
				'shop_id',
				'user_id',
				'total_price',
				'total_tax_price',
				'reservation_date',
				'user_name',
				'visit_section',
				'visit_element',
				'reservation_type',
				'hf_section',
				'off_nails',
				'nail_length',
				'nail_type',
				'nail_add_length',
				'problem',
				'pay_type',
				'photo_code',
				'is_mail',
				'is_designate',
				'welfare_id',
				'created',
				'updated',
				'disable'
            )
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }
}
