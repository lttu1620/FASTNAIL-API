<?php

namespace Model;

/**
 * Observer_Log
 *
 * @author thailh
 * @copyright Oceanize INC
 */
class Observer_Order extends \Orm\Observer
{
    /**
     * 
     * Action after execute method \Orm\Model::save
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_save(\Orm\Model $model)
    {
        
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::create
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_insert(\Orm\Model $model)
    {       
        if (empty($model->get('hp_order_code'))) {
            $this->_reupdate_seat_id($model);
        }
    }
    
    /**
     * 
     * Check, find and update new seat_id
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    private function _reupdate_seat_id(\Orm\Model $model)
    {
        return true;
        
        if (!empty($model->get('id'))
			&& empty($model->get('shop_id'))
			&& !empty($model->get('seat_id'))
			&& !empty($model->get('order_start_date'))
			&& !empty($model->get('order_end_date'))
		) {
			$vacancy = \Model_Order::get_available_vacancy(array(
					'shop_id' => $model->get('shop_id'),
					'not_in_order_id' => $model->get('id'),
					'order_start_date' => $model->get('order_start_date'),
					'order_end_date' => $model->get('order_end_date')
				)
			);
			if (empty($vacancy)) {
                $model->delete();
				throw new \Exception("Unavailable vacancy", 400);             
			}
			$new_seat = min($vacancy); 
			if ($new_seat != $model->get('seat_id')) {				
				$model->set('seat_id', $new_seat);
				$model->save();
			}
		}
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::update
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_update(\Orm\Model $model) {
        
        $order_change_data = !empty($model->get_diff()) ? $model->get_diff() : array();
        \LogLib::info('Order change data', __METHOD__, $order_change_data);
        
        // find and update new seat_id if duplicate
        if (!empty($order_change_data[1]) && (
            (!empty($order_change_data[1]['order_start_date']) && ($order_change_data[0]['order_start_date'] != $order_change_data[1]['order_start_date'])) ||
            (!empty($order_change_data[1]['order_end_date']) && ($order_change_data[0]['order_end_date'] != $order_change_data[1]['order_end_date']))
        )) {           
            $this->_reupdate_seat_id($model);          
        }
        
        // write log when change seat_id | order_start_date | order_end_date
        if (!empty($order_change_data[1]) && (
            (!empty($order_change_data[1]['seat_id']) && ($order_change_data[0]['seat_id'] != $order_change_data[1]['seat_id'])) ||
            (!empty($order_change_data[1]['order_start_date']) && ($order_change_data[0]['order_start_date'] != $order_change_data[1]['order_start_date'])) ||
            (!empty($order_change_data[1]['order_end_date']) && ($order_change_data[0]['order_end_date'] != $order_change_data[1]['order_end_date']))
            )) {
            $update_log['admin_id'] = !empty($model->get('last_update_admin_id')) ? $model->get('last_update_admin_id') : 0;
            $update_log['order_id'] = $model->get('id');
            $update_log['before_seat_id'] = $model->get('seat_id');
            $update_log['after_seat_id'] = $model->get('seat_id');
            $update_log['before_order_start_time'] = $model->get('order_start_date');
            $update_log['after_order_start_time'] = $model->get('order_start_date');
            $update_log['before_order_end_time'] = $model->get('order_end_date');
            $update_log['after_order_end_time'] = $model->get('order_end_date');

            if (!empty($order_change_data[1]['seat_id'])) {
                $update_log['before_seat_id'] = $order_change_data[0]['seat_id'];
                $update_log['after_seat_id'] = $order_change_data[1]['seat_id'];
            }

            if (!empty($order_change_data[1]['order_start_date'])) {
                $update_log['before_order_start_time'] = $order_change_data[0]['order_start_date'];
                $update_log['after_order_start_time'] = $order_change_data[1]['order_start_date'];
            }

            if (!empty($order_change_data[1]['order_end_date'])) {
                $update_log['before_order_end_time'] = $order_change_data[0]['order_end_date'];
                $update_log['after_order_end_time'] = $order_change_data[1]['order_end_date'];
            }

            if (!\Model_Order_Update_Timebar_Log::add($update_log)) {
                \LogLib::warning('Add order update timebar log failed !!!', __METHOD__, $update_log);
            }
        }
    }

    /**
     * 
     * Action after execute method \Orm\Model::find
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_load(\Orm\Model $model)
    {       
       
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::delete
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_delete(\Orm\Model $model)
    {
        
    }  
    
     /**
     * 
     * Action after execute method \Orm\Model::clone
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_clone(\Orm\Model $model)
    {
        
    }
    
    /**
     * TODO
     */
    public function before_insert()
    {
        
    }  
    
     /**
     * TODO
     */
    public function before_update()
    {
        
    }  
    
     /**
     * TODO
     */
    public function before_delete()
    {
        
    } 
    
}
