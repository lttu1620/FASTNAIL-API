<?php

namespace Model;

/**
 * Observer_Log
 *
 * @author thailh
 * @copyright Oceanize INC
 */
class Observer_Log extends \Orm\Observer
{
    /**
     * 
     * Action after execute method \Orm\Model::save
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_save(\Orm\Model $model)
    {
        \LogLib::db(\DB::last_query());    
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::create
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_create(\Orm\Model $model)
    {
        \LogLib::db(\DB::last_query());    
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::update
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_update(\Orm\Model $model)
    {
        \LogLib::db(\DB::last_query());    
    }    

    /**
     * 
     * Action after execute method \Orm\Model::find
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_load(\Orm\Model $model)
    {       
        \LogLib::db(\DB::last_query());    
    }
    
    /**
     * 
     * Action after execute method \Orm\Model::delete
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_delete(\Orm\Model $model)
    {
        \LogLib::db(\DB::last_query());    
    }  
    
     /**
     * 
     * Action after execute method \Orm\Model::clone
     * @author thailh
     * @param object $model \Orm\Model
     * @return void
     */
    public function after_clone(\Orm\Model $model)
    {
        
    }
    
    /**
     * TODO
     */
    public function before_insert()
    {
        
    }  
    
     /**
     * TODO
     */
    public function before_update()
    {
        
    }  
    
     /**
     * TODO
     */
    public function before_delete()
    {
        
    } 
    
}
