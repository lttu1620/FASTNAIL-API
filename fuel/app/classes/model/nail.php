<?php

class Model_Nail extends Model_Abstract
{

    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'photo_url',
        'photo_cd',
        'show_section',
        'hf_section',
        'menu_section',
        'price',
        'tax_price',
        'time',
        'print',
        'limited',
        'rank',
        'hp_coupon',
        'coupon_memo',
        'is_change',
        'favorite_count',
        'coupon_type',
        'attention',
        'created',
        'updated',
        'disable',
    );

    /**
     * Add or update info for Nail
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_observers  = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load', 
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    
    protected static $_table_name = 'nails';

    /**
     * Add or update info for Nail
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $query = new self;
        // check exist
        if (!empty($id)) {
            $query = self::find($id);
            if (empty($query)) {
                self::errorNotExist('id');
                return false;
            }
        }
        if (empty($param['menu_section'])) {
            $param['menu_section'] = 0;
        }
        if (empty($param['is_change'])) {
            $param['is_change'] = 0;
        }
        if (empty($param['coupon_type'])) {
            $param['coupon_type'] = 0;
        }
        // set value
        if (isset($param['photo_url']) && is_string($param['photo_url'])) {
            $query->set('photo_url', $param['photo_url']);
        }
        if (isset($param['photo_cd'])) {
            $query->set('photo_cd', $param['photo_cd']);
        }
        if (isset($param['show_section'])) {
            $query->set('show_section', $param['show_section']);
        }
        if (isset($param['hf_section'])) {
            $query->set('hf_section', $param['hf_section']);
        }
        if (isset($param['menu_section'])) {
            $query->set('menu_section', $param['menu_section']);
        }
        if (isset($param['price'])) {
            $query->set('price', $param['price']);
        }
        if (isset($param['tax_price'])) {
            $query->set('tax_price', $param['tax_price']);
        }
        if (isset($param['time'])) {
            $query->set('time', $param['time']);
        }
        if (isset($param['print'])) {
            $query->set('print', $param['print']);
        }
        if (isset($param['limited'])) {
            $query->set('limited', $param['limited']);
        }
        if (isset($param['rank'])) {
            $query->set('rank', $param['rank']);
        }
        if (isset($param['hp_coupon'])) {
            $query->set('hp_coupon', $param['hp_coupon']);
        }
        if (isset($param['coupon_memo'])) {
            $query->set('coupon_memo', $param['coupon_memo']);
        }
        if (isset($param['is_change'])) {
            $query->set('is_change', $param['is_change']);
        }
        if (isset($param['coupon_type'])) {
            $query->set('coupon_type', $param['coupon_type']);
        }
        if (isset($param['attention'])) {
            $query->set('attention', $param['attention']);
        }

        // save to database
        if ($query->save()) {
            if (empty($query->id)) {
                $query->id = self::cached_object($query)->_original['id'];
            }
            //save attributes
            $attribute = array(
                'bullion',
                'color',
                'color_jell',
                'design',
                'flower',
                'genre',
                'hologram',
                'keyword',
                'paint',
                'powder',
                'rame_jell',
                'scene',
                'shell',
                'stone',
                'tag'
            );
            foreach ($attribute as $attr) {
                if (isset($param[$attr.'_id'])) {
                    self::update_attributes(
                        array(
                            'nail_id'   => $query->id,
                            $attr.'_id' => $param[$attr.'_id'],
                        )
                    );
                }
            }
            \Lib\Cache::delete('nails_detail_'.$query->id);
            return !empty($query->id) ? $query->id : 0;
        }
        return false;
    }

    /**
     * Get list Nail (using array count)
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array List Nail
     */
    public static function get_list($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name.'.*',
            DB::expr("(price + tax_price) AS sell_price"),
            DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            )
        )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM nail_favorites
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'nail_favorites.nail_id');
        // filter by keyword
        if (!empty($param['id'])) {
            $query->where(self::$_table_name.'.id', '=', $param['id']);
        }
        if (!empty($param['photo_cd'])) {
            $query->where(self::$_table_name.'.photo_cd', '=', $param['photo_cd']);
        }
        if (!empty($param['price'])) {
            $query->where(self::$_table_name.'.price', '=', $param['price']);
        }
        if (!empty($param['time'])) {
            $query->where(self::$_table_name.'.time', '=', $param['time']);
        }
        if (isset($param['rank']) && $param['rank'] != '') {
            $query->where(self::$_table_name.'.rank', '=', $param['rank']);
        }
        if (isset($param['print']) && $param['print'] != '') {
            $query->where(self::$_table_name.'.print', '=', $param['print']);
        }
        if (isset($param['limited']) && $param['limited'] != '') {
            $query->where(self::$_table_name.'.limited', '=', $param['limited']);
        }
        if (!empty($param['hp_coupon'])) {
            $query->where(self::$_table_name.'.hp_coupon', '=', $param['hp_coupon']);
        }
        if (isset($param['show_section']) && $param['show_section'] != '') {
            $query->where(self::$_table_name.'.show_section', '=', $param['show_section']);
        }
        if (isset($param['hf_section']) && $param['hf_section'] != '') {
            $query->where(self::$_table_name.'.hf_section', '=', $param['hf_section']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (isset($param['coupon_type']) && $param['coupon_type'] != '') {
            $query->where(self::$_table_name.'.coupon_type', '=', $param['coupon_type']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Nail (without array count)
     *
     * @author Hoang Gia Thong
     * @return array List Nail
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            DB::expr("(price + tax_price) AS sell_price"),
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            )
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        if (!empty($param['hf_section'])) {
            $query->where('hf_section', '=', $param['hf_section']);
        }
        if (!empty($param['id'])) {
            if (is_array($param['id'])) {
                $param['id'] = implode(',', $param['id']);
            }
            $query->where('id', 'IN', DB::expr('('.$param['id'].')'));
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Nail
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $nail = self::find($id);
            if ($nail) {
                $nail->set('disable', $param['disable']);
                if (!$nail->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
            \Lib\Cache::delete('nails_detail_'.$id);
        }
        return true;
    }

    /**
     * Get detail Nail
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return array|bool Detail Nail or false if error
     */
    public static function get_detail($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
                self::$_table_name.'.*',
                DB::expr("(price + tax_price) AS sell_price"),
                DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
                DB::expr(
                    "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                        'item_img_url'
                    )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
                )
            )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "
                        (SELECT *
                        FROM nail_favorites
                        WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
                    "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'nail_favorites.nail_id')
            ->where(self::$_table_name.'.id', '=', $param['id']);
        // get data
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id');
            return false;
        }
        if (!empty($param['get_relation'])) {
            $data['relations'] = self::get_relation(
                array(
                    'nail_id' => $param['id'],
                    'login_user_id' => $param['login_user_id'],
                    'limit' => \Config::get('limit_nail_relation')
                )
            );
        }
        if (!empty($param['get_attribute_for_view'])) {
            $data['attributes'] = self::get_attribute(
                array(
                    'nail_id'  => $param['id'],
                    'order_id' => !empty($param['order_id']) ? $param['order_id'] : 0,
                    'checked'  => true
                )
            );
        } elseif (!empty($param['get_attribute_for_edit'])) {
            $data['attributes'] = self::get_attribute(
                array(
                    'nail_id' => $param['id'],
                )
            );
        }
        if (!empty($param['get_tag'])) {
            $data['tags'] = Model_Nail_Tag::get_all(
                array(
                    'nail_id' => $param['id'],
                )
            );
        }
        if (!empty($param['get_color_jell'])) {
            $data['color_jells'] = Model_Nail_Color_Jell::get_all(
                array(
                    'nail_id' => $param['id'],
                )
            );
        }
        if (!empty($param['get_color'])) {
            $data['colors'] = Model_Nail_Color::get_all(
                array(
                    'nail_id' => $param['id'],
                )
            );
        }
        if (!empty($param['get_design'])) {
            $data['designs'] = Model_Nail_Design::get_all(
                array(
                    'nail_id' => $param['id'],
                )
            );
        }
        if (!empty($param['get_items'])) {
            $data['items'] = \Model_Item::get_all(
                array(
                    'limit' => 10,
                )
            );
        }
        return $data;
    }

    /**
     * Search Nails
     *
     * @author thailh
     * @param array $param Input data
     * @return array|bool Search Nail or false if error
     */
    public static function search($param)
    {  
        $select = array(
            self::$_table_name.'.id',
            self::$_table_name.'.photo_url',
            self::$_table_name.'.photo_cd',
            self::$_table_name.'.price',
            self::$_table_name.'.tax_price',
            self::$_table_name.'.rank',
            self::$_table_name.'.hp_coupon',
            self::$_table_name.'.coupon_type',
            self::$_table_name.'.favorite_count',
            self::$_table_name.'.coupon_type',
            DB::expr("IF(IFNULL(rank,0)=0,10000,rank) AS rank_sort"),
            DB::expr("IF(IFNULL(coupon_type,0)=0,10000,coupon_type) AS coupon_type_sort"),
            DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
            DB::expr("IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get('item_img_url')['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url")
        );
        $sortByRecommend = false;
        $subQuery = array();
        if (!empty($param['survey_nail_id'])) {
            if (is_array($param['survey_nail_id'])) {
                $param['survey_nail_id'] = implode(',', $param['survey_nail_id']);
            }
            $data = DB::select(
                    'type',
                    'attr_id',
                    DB::expr("COUNT(*) AS cnt")
                )
                ->from(
                    DB::expr(
                        "(
                            SELECT 'nail_designs' AS type, design_id AS attr_id, id
                            FROM nail_designs
                            WHERE disable=0
                                AND nail_id IN ({$param['survey_nail_id']})
                            UNION
                            SELECT 'nail_genres' AS type, genre_id AS attr_id, id
                            FROM nail_genres
                            WHERE disable=0
                                AND nail_id IN ({$param['survey_nail_id']})
                            UNION
                            SELECT 'nail_scenes' AS type, scene_id AS attr_id, id
                            FROM nail_scenes WHERE disable=0
                                AND nail_id IN ({$param['survey_nail_id']})
                            UNION
                            SELECT 'nail_colors' AS type, color_id AS attr_id, id
                            FROM nail_colors
                            WHERE disable=0
                                AND nail_id IN ({$param['survey_nail_id']})
                            UNION
                            SELECT 'nail_tags' AS type, tag_id AS attr_id, id
                            FROM nail_tags
                            WHERE disable=0
                                AND nail_id IN ({$param['survey_nail_id']})
                        ) AS nail_attrs"
                    )
                )
                ->group_by('type', 'attr_id')
                ->execute()
                ->as_array();
            $pk = array(
                'nail_designs' => 'design_id',
                'nail_genres'  => 'genre_id',
                'nail_scenes'  => 'scene_id',
                'nail_colors'  => 'color_id',
                'nail_tags'    => 'tag_id',
            );
            foreach ($data as $attr) {
                $table = $attr['type'];
                if (empty($subQuery[$table])) {
                    $subQuery[$table] = array();
                }
                $subQuery[$table][] = $attr;
            }
            foreach ($subQuery as $table => $attrs) {
                $attrSql = array();
                foreach ($attrs as $attr) {
                    $attrSql[] = "WHEN {$attr['attr_id']} THEN {$attr['cnt']}";
                }
                $attrSql[] = 'ELSE 0';
                $attrSql = implode(' ', $attrSql);
                $subQuery[$table] = "
                    SELECT nail_id, SUM(CASE {$pk[$table]} {$attrSql} END) AS point
                    FROM {$table}
                    WHERE disable = 0
                    GROUP BY nail_id
                ";
            }
            if (!empty($subQuery)) {
                $subQuery = "
                    SELECT DISTINCT nail_id, SUM(point) AS point 
                    FROM (".implode(' UNION ', $subQuery).") nail_attributes
                    GROUP BY nail_id
                ";
            }
        }
        if (!empty($subQuery)) {
            $sortByRecommend = true;
            $select[] = DB::expr("IF(ISNULL(nail_recommends.point),0,nail_recommends.point) AS point");
        }
        // prepare a select statement
        if (isset($param['price']) && $param['price'] == '0000') {
            unset($param['price']);
            $param['hp_coupon'] = 1;
        }
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        if (empty($param['page'])) {
            $param['page'] = 1;
        }
        $query = DB::select_array($select)
            ->distinct(true)
            ->from(self::$_table_name);
        if ($sortByRecommend) {
            $query->join(DB::expr("({$subQuery}) nail_recommends"), "LEFT");
            $query->on(self::$_table_name.'.id', '=', 'nail_recommends.nail_id');
        }
        // Join a table
        $query->join(
            DB::expr(
                "(
                    SELECT *
                    FROM nail_favorites
                    WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
                "
            ),
            'LEFT'
        );
        $query->on('nail_favorites.nail_id', '=', self::$_table_name.'.id');
        if (isset($param['design_id']) && $param['design_id'] !== '') {
            $query->join('nail_designs');
            $query->on('nail_designs.nail_id', '=', self::$_table_name.'.id');
            $query->where('nail_designs.design_id', '=', $param['design_id']);
        }
        if (isset($param['scene_id']) && $param['scene_id'] !== '') {
            $query->join('nail_scenes');
            $query->on('nail_scenes.nail_id', '=', self::$_table_name.'.id');
            $query->where('nail_scenes.scene_id', '=', $param['scene_id']);
        }
        if (isset($param['genre_id']) && $param['genre_id'] !== '') {
            $query->join('nail_genres');
            $query->on('nail_genres.nail_id', '=', self::$_table_name.'.id');
            $query->where('nail_genres.genre_id', '=', $param['genre_id']);
        }
        if (isset($param['color_id']) && $param['color_id'] !== '') {
            $query->join('nail_colors');
            $query->on('nail_colors.nail_id', '=', self::$_table_name.'.id');
            $query->where('nail_colors.color_id', '=', $param['color_id']);
        }
        if (isset($param['keyword_id']) && $param['keyword_id'] !== '') {
            $query->join('nail_keywords');
            $query->on('nail_keywords.nail_id', '=', self::$_table_name.'.id');
            $query->where('nail_keywords.keyword_id', '=', $param['keyword_id']);
        }
        if (isset($param['campaign_id']) && $param['campaign_id'] !== '') {
            $tags = DB::select('id')
                ->from('tags')
                ->where('disable', '0')
                ->where(DB::expr("name IN (
                        SELECT tag_name
                        FROM campaigns
                        WHERE id = {$param['campaign_id']}
                            AND start_date <= UNIX_TIMESTAMP() 
                            AND end_date >= UNIX_TIMESTAMP()
                            AND disable = 0
                    )")
                )
                ->execute()
                ->as_array();
            $param['tag_id'] = \Lib\Arr::arrayValues($tags, 'id');            
        }
        if (isset($param['tag_id']) && $param['tag_id'] !== '') {
            if (!is_array($param['tag_id']) && is_string($param['tag_id'])) {
                $param['tag_id'] = explode(',', $param['tag_id']);
            }
            $param['tag_id'] = array_filter($param['tag_id']);
            if (count($param['tag_id']) > 0) {
                $query->where('nails.id', 'IN', DB::expr('(
                    SELECT `nail_id` FROM `nail_tags`
                    WHERE `nail_tags`.`tag_id` IN (' . implode(',', $param['tag_id']) . ')
                    GROUP BY `nail_id`
                    HAVING COUNT(`id`) = ' . count($param['tag_id']) . ')')
                );
            }
        }
        if (isset($param['update_tag']) && $param['update_tag'] !== '') {
            \Model_Tag::update_tags_count(array('id' => $param['update_tag']));
        }
        // where condition
        if (isset($param['hf_section']) && $param['hf_section'] !== '') {
            $query->where(self::$_table_name.'.hf_section', '=', $param['hf_section']);
        }
        if (isset($param['price']) && $param['price'] !== '') {
            $query->where(self::$_table_name.'.price', '=', $param['price']);
        }
        if (isset($param['keyword']) && !empty(trim($param['keyword']))) {
            if (!is_numeric($param['keyword'])) { // fix auto convert data type in mysql
                $param['keyword'] = -1111111111;
            }
            $query->where('photo_cd', '=', $param['keyword']);
        }
        if (!empty($param['hp_coupon']) && $param['hp_coupon'] == 1) {
            //$query->where('hp_coupon', '=', $param['hp_coupon']);
            $query->where(self::$_table_name . '.coupon_type', 'IN', array(1, 2)); 
        } else {
            if (!isset($param['get_coupon'])) {
                //$query->where('hp_coupon', '<>', 1);
            }
        }
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        } else {
            $query->where(self::$_table_name.'.disable', '=', 0);
        }
        if (isset($param['hf_section']) && $param['hf_section'] !== '') {
            $query->where(self::$_table_name.'.hf_section', '=', $param['hf_section']);
        }  
        
//        
//        if ($sortByRecommend && empty($param['sort'])) {            
//            $param['sort'] = 'point-DESC';             
//        }
        
        $user_id = 0;
        if (!empty($param['login_user_id'])) {
            $user_id = $param['login_user_id'];
        } elseif (!empty($param['order_id'])) {
            $order = Model_Order::find($param['order_id']);
            if (!empty($order) && !empty($order->get('user_id'))) {
                $user_id = $order->get('user_id');
            }
        }
        // check for the first order without nail or there is no order
        if (!empty($user_id)) {
            $orders = DB::select(
                    'orders.id', 
                    'orders.reservation_date', 
                    'orders.hf_section',
                    DB::expr('IF(ISNULL(order_nails.id) OR order_nails.disable = 1, 0, 1) AS has_nail')
                )
                ->from('orders')               
                ->join('order_nails', 'LEFT')               
                ->on('orders.id', '=', 'order_nails.order_id')               
                ->where('orders.is_cancel', '0') 
                ->where('orders.is_autocancel', '0')               
                ->where('orders.disable', '0') 
                ->where('orders.user_id', $user_id)               
                ->execute()
                ->as_array();
            // priority search nails.coupon_type
            if (Lib\Arr::count($orders, 'has_nail', 1) == 0 && Lib\Arr::count($orders, 'has_nail', 0) <= 1) {                   
                $query->order_by('coupon_type_sort', 'ASC');
            } else {
                $query->where(self::$_table_name . '.coupon_type', 'IN', array(0, 2));  //0: no coupon; 2: for old customer                 
                $query->order_by('coupon_type_sort', 'ASC');
            }
        } else {
            $query->order_by('coupon_type_sort', 'ASC');
        }
        /*
        if ($sortByRecommend) {
            if (preg_match('/^point\-(asc|desc)$/i', $param['sort'])) {
                $sortExplode = explode('-', $param['sort']);
                $query->order_by('nail_recommends.'.$sortExplode[0], $sortExplode[1]);
                unset($param['sort']);
            }
        } 
        */    
        if (!empty($param['sort']))
        {
            $sortExplode = explode('-', $param['sort']);
            if (!in_array(strtolower($sortExplode[1]), array('asc', 'desc'))) {
                $sortExplode[1] = 'desc';
            }
            switch ($sortExplode[0]) {
                case 'point':
                    $query->order_by('nail_recommends.'.$sortExplode[0], $sortExplode[1]);
                    break;
                case 'rank':
                    $query->where(self::$_table_name . '.price', '>=', '3990');
                    $query->order_by('rank_sort', $sortExplode[1]);
                    break;
                case 'price':                    
                    $query->order_by(self::$_table_name . '.price', $sortExplode[1]);
                    break;
                case 'created':                   
                    $query->order_by(self::$_table_name . '.created', $sortExplode[1]);
                    break;
                default:
                    $query->order_by(self::$_table_name.'.created', 'DESC');
            }
            /*
            if (strtolower($sortExplode[0]) == 'rank') {
                $sortExplode[0] = 'rank_sort';
                $query->where(self::$_table_name.'.price', '>=', '3990');
                $query->order_by($sortExplode[0], $sortExplode[1]);
            } else {                
                $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
            }
            * 
            */
        } else {
            //$query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        //p($query->execute(), 1);
        
        $data = $query->execute()->as_array();        
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get nail's attribute
     *
     * @author diennvt
     * @param array $param Input data
     * @return array|bool Nail's attribute or false if error
     */
    public static function get_attribute($param)
    {
        $data = array();
        $attributes = array(
            'bullions'    => 'bullion_id',
            'colors'      => 'color_id',
            'color_jells' => 'color_jell_id',
            'designs'     => 'design_id',
            'flowers'     => 'flower_id',
            'genres'      => 'genre_id',
            'holograms'   => 'hologram_id',
            'keywords'    => 'keyword_id',
            'paints'      => 'paint_id',
            'powders'     => 'powder_id',
            'rame_jells'  => 'rame_jell_id',
            'scenes'      => 'scene_id',
            'shells'      => 'shell_id',
            'stones'      => 'stone_id',
            'tags'        => 'tag_id',
        );
        if (!empty($param['order_id'])) {
            foreach ($attributes as $attr => $attrId) {
                $data[$attr] = Model_Order::get_nail_attributes(
                    array(
                        'order_id'     => $param['order_id'],
                        'attribute_id' => $attrId,
                        'checked'      => isset($param['checked']) ? 1 : 0,
                    )
                );
            }
            return $data;
        }

        foreach ($attributes as $attr => $attrId) {
            $nailAttrTable = "nail_{$attr}";
            $dataAttr = DB::select(
                $attr.'.id',
                $attr.'.name',
                DB::expr("IF(ISNULL(nail_".$attr.".id),0,1) AS checked")
            )
                ->from($attr)
                ->join(
                    DB::expr(
                        "(
                        SELECT id, {$attrId} 
                        FROM {$nailAttrTable}
                        WHERE nail_id = {$param['nail_id']} 
                            AND disable = 0                        
                        ) {$nailAttrTable}"
                    ),
                    'LEFT'
                )
                ->on($attr.'.id', '=', "{$nailAttrTable}.{$attrId}")
                ->where($attr.'.disable', '0')
                ->order_by($attr.'.name', 'ASC')
                ->execute()
                ->as_array();
            if (isset($param['checked'])) {
                $dataAttr = \Lib\Arr::filter($dataAttr, 'checked', 1);
            }
            $data[$attr] = $dataAttr;
        }
        return !empty($data) ? $data : array();
    }

    /**
     * Get nail's attribute
     *
     * @author diennvt
     * @param array $param Input data
     * @return array|bool Nail's attribute or false if error
     */
    public static function get_attribute_for_frontend($param)
    {
        $data = array();
        $attributes = array(
            'colors',
            'designs',
            'genres',
            'scenes'
        );
        foreach ($attributes as $attr) {
            $table_nail_attr = "nail_".$attr;
            $query = DB::select()
                ->from($attr)
                ->join($table_nail_attr)
                ->on($attr.'.id', '=', $table_nail_attr.'.'.substr($attr, 0, -1).'_id')
                ->where($table_nail_attr.'.nail_id', '=', $param['nail_id'])
                ->order_by($attr.'.id', 'ASC');
            $data[$attr] = $query->execute()->as_array();
        }
        return !empty($data) ? $data : array();
    }

    /**
     * Get nails's relation
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nail
     */
    public static function get_relation($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $relationTable = array(
            //'bullion_id'    => 'nail_bullions',
            //'color_jell_id' => 'nail_color_jells',
            //'flower_id'     => 'nail_flowers',
            //'hologram_id'   => 'nail_holograms',
            //'keyword_id'    => 'nail_keywords',
            //'paint_id'      => 'nail_paints',
            //'powder_id'     => 'nail_powders',
            //'rame_jell_id'  => 'nail_rame_jells',
            //'shell_id'      => 'nail_shells',
            //'stone_id'      => 'nail_stones',
            'scene_id'      => 'nail_scenes',
            'design_id'     => 'nail_designs',
            'genre_id'      => 'nail_genres',
            'color_id'      => 'nail_colors',
            'tag_id'        => 'nail_tags'
        );
        $relationSql = '';
        foreach ($relationTable as $attrField => $table) {
            if ($relationSql != '') {
                $relationSql .= " UNION ";
            }
            $relationSql .= "
                SELECT nail_id
                FROM {$table}
                WHERE disable = 0
                    AND nail_id <> {$param['nail_id']}
                    AND {$attrField} IN (
                        SELECT {$attrField}
                        FROM {$table}
                        WHERE disable = 0
                            AND nail_id = {$param['nail_id']}
                )
            ";
        }
        $relationSql = "
            SELECT  DISTINCT 
                    nails.id, 
                    nails.photo_cd, 
                    nails.hf_section, 
                    nails.hp_coupon, 
                    nails.rank, 
                    nails.price, 
                    nails.tax_price,
                    nails.price + nails.tax_price AS sell_price,
                    IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get('item_img_url')['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url
            FROM nails JOIN ({$relationSql}) A ON nails.id = A.nail_id
            WHERE nails.disable = 0
            AND hf_section IN (1,2)
            ORDER BY rand()
        ";
        $nails = DB::query($relationSql)->execute()->as_array(); //p($nails, 1);
        $hand = \Lib\Arr::filter($nails, 'hf_section', 1);        
        $data = count($hand) > 3 ? \Lib\Arr::rand($hand, 3) : $hand;        
        function merge_nails($nails, $data = array(), $hf_section = false) {
            foreach ($nails as $nail) {              
                $exists = Lib\Arr::search($data, 'id', $nail['id']);            
                if ($exists == false
                    && ($hf_section == false || $nail['hf_section'] == $hf_section)
                ) { 
                    $data[] = $nail;
                    if (count($data) >= 6) {
                        break;
                    }
                }
            }
            if ($hf_section && count($data) < 6) {
                $data = merge_nails($nails, $data, false);
            }
            return $data;
        }
        $data = merge_nails($nails, $data, 2);
        $data = array_merge(
                    \Lib\Arr::filter($data, 'hf_section', 1), 
                    \Lib\Arr::filter($data, 'hf_section', 2)
                );
        return $data;
    }

    /**
     * Get top nail best-selling (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nail
     */
    public static function top_order($param)
    {
        $offset = ($param['page'] - 1) * $param['limit'];
        $earlyWeek = new DateTime();
        $day_of_week = $earlyWeek->format("w");
        $earlyWeek->modify("-".$day_of_week." day");
        $earlyWeek = strtotime($earlyWeek->format('Y-m-d'));
        $sql = "
            SELECT nails.*,
                   total_order
            FROM   nails
                   JOIN (SELECT nail_id,
                                COUNT(orders.id) AS total_order
                         FROM   orders
                                JOIN order_nails
                                  ON orders.id = order_nails.order_id
                         WHERE  orders.disable = 0
                                AND order_nails.disable = 0
                                AND DATE(FROM_UNIXTIME(orders.reservation_date)) > DATE(FROM_UNIXTIME({$earlyWeek}))) order_nails
                     ON nails.id = order_nails.nail_id
            WHERE  nails.disable = 0
            ORDER  BY total_order DESC
            LIMIT {$offset},{$param['limit']}
        ";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }

    /**
     * Get top Nail (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Nail
     */
    public static function get_top($param)
    {
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $query = DB::select(
            self::$_table_name.'.*',
            DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            )
        )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "
                (   SELECT *
                    FROM nail_favorites
                    WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
                "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'nail_favorites.nail_id')
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name.'.hp_coupon', '=', '0');
        // filter by keyword
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if (strtolower($sortExplode[0]) == 'rank') {
                $query->where(self::$_table_name.'.price', '>=', '3990');
            }
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $total_all = 0;
        if (!empty($param['page']) && $param['page'] == 1) {
            $total_all = self::count(
                array(
                    'where' => array(
                        array('disable', '0'),
                        /*array('hp_coupon', '<>', 1)*/
                    )
                )
            );
        }

        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data, $total_all);
    }

    /**
     * Get nail survey
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return array List Nail
     */
    /*
    public static function get_survey()
    {
        $nailImage = "CONCAT('" . \Config::get('item_img_url')['nails'] . "', nails.photo_cd, '" . ".jpg" . "') AS image_url";
        $sql = "
            SELECT * FROM
            (
                SELECT nails.*, {$nailImage}
                FROM   nails
                JOIN nail_genres
                    ON nails.id = nail_genres.nail_id
                WHERE  nails.disable = 0
                    AND nail_genres.disable = 0
                    AND genre_id IN (1, 2)
                    AND hf_section = 1
                ORDER BY RAND()
                LIMIT 1
            ) as sub_genre_1_or_2

            UNION

            SELECT * FROM
            (
                SELECT nails.*, {$nailImage}
                FROM   nails
                JOIN nail_genres
                    ON nails.id = nail_genres.nail_id
                WHERE  nails.disable = 0
                    AND nail_genres.disable = 0
                    AND genre_id IN (3, 4)
                    AND hf_section = 1
                ORDER BY RAND()
                LIMIT 1
            ) as sub_genre_3_or_4
        ";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }*/

    /**
     * Get nail survey
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return array List Nail
     */
    public static function get_survey()
    {
        /*
        $nailImage = "CONCAT('" . \Config::get('item_img_url')['nails'] . "', nails.photo_cd, '" . ".jpg" . "') AS image_url";
        $sql = "
            SELECT nails.*, {$nailImage}
            FROM   nails
            WHERE  nails.disable = 0
                AND hf_section = 1
            ORDER BY RAND()
            LIMIT 6
        ";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        */
        $query = DB::select(
            'id',
            'photo_url',
            'photo_cd',
            'price',
            'tax_price',
            'rank',
            'hp_coupon',
            'favorite_count',
            DB::expr(
                "IF(ISNULL(IF(photo_url = '', NULL, photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), photo_url) as image_url"
            )
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0')
            ->where('hf_section', '=', '1')
            ->where(DB::expr("(photo_cd IS NOT NULL OR photo_url IS NOT NULL)"))
            ->where(
                DB::expr(
                    "(
                EXISTS (SELECT nail_id FROM nail_colors WHERE nail_id = nails.id AND disable = 0)
                OR
                EXISTS (SELECT nail_id FROM nail_scenes WHERE nail_id = nails.id AND disable = 0)
                OR
                EXISTS (SELECT nail_id FROM nail_designs WHERE nail_id = nails.id AND disable = 0)
                OR
                EXISTS (SELECT nail_id FROM nail_genres WHERE nail_id = nails.id AND disable = 0)
                OR
                EXISTS (SELECT nail_id FROM nail_tags WHERE nail_id = nails.id AND disable = 0)
            )"
                )
            )
            ->order_by(DB::expr('RAND()'))
            ->limit(6);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }

    /**
     * Get nail of order
     *
     * @author diennvt
     * @param array $param Input data
     * @return array List nail
     */
    public static function get_nail_order($param)
    {
        $query = DB::select(
            array('orders.id', 'order_id'),
            array('nails.id', 'nail_id'),
            'nails.photo_cd',
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            ),
            'order_nails.price',
            'order_nails.tax_price',
            DB::expr("order_nails.price + order_nails.tax_price AS totalPrice")
        )
            ->from(self::$_table_name)
            ->join('order_nails', 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'order_nails.nail_id')
            ->join('orders', 'LEFT')
            ->on('order_nails.order_id', '=', 'orders.id')
            ->where(self::$_table_name.'.disable', '=', 0)
            ->where('order_nails.disable', '=', 0)
            ->where('orders.disable', '=', 0)
            ->where("orders.id", 'IN', $param['order_ids']);
        // get data
        $data = $query->execute()->as_array();
        // get name of design and color
        $param['nail_ids'] = \Lib\Arr::field($data, 'nail_id');
        if ($param['nail_ids']) {
            if (empty($param['implode'])) {
                $param['implode'] = ',';
            }
            $name_designs = self::get_nail_design($param);
            $name_colors = self::get_nail_color($param);
            foreach ($data as $key => $info) {
                $designs = \Lib\Arr::filter($name_designs, 'nail_id', $info['nail_id'], false, false);
                $colors = \Lib\Arr::filter($name_colors, 'nail_id', $info['nail_id'], false, false);
                $data[$key]['design'] = !empty($designs[0]['name']) ? $designs[0]['name'] : '';
                $data[$key]['color'] = !empty($colors[0]['name']) ? $colors[0]['name'] : '';
            }
        }
        return $data;
    }

    /**
     * Get design name of nail
     *
     * @author diennvt
     * @param array $param Input data
     * @return array List nail
     */
    public static function get_nail_design($param)
    {
        if (empty($param['implode'])) {
            $param['implode'] = ',';
        }
        $query = DB::select(
            array('nails.id', 'nail_id'),
            DB::expr("GROUP_CONCAT(designs.name SEPARATOR  '".$param['implode']."') as name")
        )
            ->from(self::$_table_name)
            ->join('nail_designs', 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'nail_designs.nail_id')
            ->join('designs', 'LEFT')
            ->on('nail_designs.design_id', '=', 'designs.id')
            ->where(self::$_table_name.'.disable', '=', 0)
            ->where('nail_designs.disable', '=', 0)
            ->where('designs.disable', '=', 0)
            ->where(self::$_table_name.'.id', 'IN', $param['nail_ids'])
            ->group_by(self::$_table_name.'.id');
        // get data
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get color name of nail
     *
     * @author diennvt
     * @param array $param Input data
     * @return array List nail
     */
    public static function get_nail_color($param)
    {
        if (empty($param['implode'])) {
            $param['implode'] = ',';
        }
        $query = DB::select(
            array('nails.id', 'nail_id'),
            DB::expr("GROUP_CONCAT(colors.name SEPARATOR  '".$param['implode']."') as name")
        )
            ->from(self::$_table_name)
            ->join('nail_colors', 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'nail_colors.nail_id')
            ->join('colors', 'LEFT')
            ->on('nail_colors.color_id', '=', 'colors.id')
            ->where(self::$_table_name.'.disable', '=', 0)
            ->where('nail_colors.disable', '=', 0)
            ->where('colors.disable', '=', 0)
            ->where(self::$_table_name.'.id', 'IN', $param['nail_ids'])
            ->group_by(self::$_table_name.'.id');
        // get data
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Check is_favorite a nail
     *
     * @author thailh
     * @param array $param Input data
     * @return int 1|0
     */
    public static function is_favorite($param)
    {
        if (empty($param['login_user_id']) || empty($param['nail_id'])) {
            return 0;
        }
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $data = DB::select()
            ->from(Model_Nail_Favorite::table())
            ->where(Model_Nail_Favorite::table().'.disable', '=', 0)
            ->where(Model_Nail_Favorite::table().'.user_id', '=', $param['login_user_id'])
            ->where(Model_Nail_Favorite::table().'.nail_id', 'IN', $param['nail_id'])
            ->execute()
            ->as_array();              
        return Lib\Arr::field($data, 'nail_id');
    }

    /**
     * Update nail's attributes
     *
     * @author thailh
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_attributes($param)
    {
        $attributeTable = array(
            'tag_id'        => Model_Nail_Tag::table(),
            'design_id'     => Model_Nail_Design::table(),
            'genre_id'      => Model_Nail_Genre::table(),
            'scene_id'      => Model_Nail_Scene::table(),
            'color_id'      => Model_Nail_Color::table(),
            'bullion_id'    => Model_Nail_Bullion::table(),
            'color_jell_id' => Model_Nail_Color_Jell::table(),
            'flower_id'     => Model_Nail_Flower::table(),
            'shell_id'      => Model_Nail_Shell::table(),
            'stone_id'      => Model_Nail_Stone::table(),
            'hologram_id'   => Model_Nail_Hologram::table(),
            'paint_id'      => Model_Nail_Paint::table(),
            'powder_id'     => Model_Nail_Powder::table(),
            'keyword_id'    => Model_Nail_Keyword::table(),
            'rame_jell_id'  => Model_Nail_Rame_Jell::table(),
        );

        // attribute field name
        foreach ($param as $field => $value) {
            if (isset($attributeTable[$field])) {
                $attrFieldName = $field;
                $attrTableName = $attributeTable[$field];
            }
        }

        // check exist nail
        $options['where'] = array(
            'id'      => $param['nail_id'],
            'disable' => '0'
        );
        $nail = self::find('first', $options);
        if (empty($nail)) {
            static::errorNotExist('nail_id');
            return false;
        }

        // disable all if attr is empty
        if (empty($param[$attrFieldName])) {
            return DB::update($attrTableName)
                ->value('disable', '1')
                ->where('nail_id', '=', $nail->get('id'))
                ->execute();
        }

        // get existing attrs
        $attrs = \Lib\Arr::key_values(
            DB::select()
                ->from($attrTableName)
                ->where('nail_id', $nail->get('id'))
                ->execute()
                ->as_array(),
            $attrFieldName
        );

        // prepare data for insert/update
        $attrIds = explode(',', $param[$attrFieldName]);
        $dataUpdate = array();
        foreach ($attrIds as $attrId) {
            if (!empty($attrId)) {
                $dataUpdate[] = array(
                    'id'           => !empty($attrs[$attrId]['id']) ? $attrs[$attrId]['id'] : 0,
                    'nail_id'      => $nail->get('id'),
                    $attrFieldName => $attrId,
                    'disable'      => 0,
                );
            }
        }
        foreach ($attrs as $attrId => $attr) {
            if (!in_array($attrId, $attrIds)) {
                $dataUpdate[] = array(
                    'id'           => $attr['id'],
                    'nail_id'      => $nail->get('id'),
                    $attrFieldName => $attrId,
                    'disable'      => 1,
                );
            }
        }

        // execute insert/update
        if (!empty($dataUpdate) && !parent::batchInsert(
                $attrTableName,
                $dataUpdate,
                array('disable' => DB::expr('VALUES(disable)')),
                false
            )
        ) {
            \LogLib::warning('Can not update '.$attrTableName, __METHOD__, $param);
            return false;
        }
        return true;
    }

    //MOBILE
    /**
     * get recommend nail for mobile
     *
     * @author CaoLP
     * @param array $param Input data
     * @return array
     */
    public static function get_mobile_recommend($param)
    {
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        if (empty($param['limit'])) {
            $param['limit'] = 4;
        }
        $year = date('Y');
        $month = date('n');
        if (date('j') >= 1 AND date('j') <= 7) {
            $month = $month > 1 ? $month - 1 : 12;
            $year = $month > 1 ? $year : $year - 1;
        }
        $query = DB::select(
            self::$_table_name.'.id',
            'nail_monthly_ranks.rank',
            self::$_table_name.'.hp_coupon',
            self::$_table_name.'.photo_cd',
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            ),
            self::$_table_name.'.price',
            self::$_table_name.'.tax_price',
            DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
            DB::expr("(price + tax_price) AS sell_price")
        )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "(
                    SELECT nail_id, rank 
                    FROM nail_monthly_ranks
                    WHERE year = {$year}
                    AND month = {$month}
                ) nail_monthly_ranks"
                )
            )
            ->on(self::$_table_name.'.id', '=', 'nail_monthly_ranks.nail_id')
            ->join(
                DB::expr(
                    "
                (   SELECT *
                    FROM nail_favorites
                    WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
                "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'nail_favorites.nail_id')
            ->where(self::$_table_name.'.disable', '=', '0')
            ->where(self::$_table_name.'.hp_coupon', '=', '0')
            ->where('nail_monthly_ranks.rank', '>', '0')
            ->limit($param['limit'])
            ->order_by('rank', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * get recommend nail for mobile
     *
     * @author CaoLP
     * @param array $param Input data
     * @return array
     */
    public static function get_mobile_searchpopup($param)
    {
        $data = array(
            'designs' => \Model_Design::get_all($param),
            'genres' => \Model_Genre::get_all($param),
            'scenes' => \Model_Scene::get_all($param),
            'colors' => \Model_Color::get_all($param),
            'tags' => \Model_Tag::get_all(
                array(
                    'sort' => 'search_count-DESC',
                )
            ),
            'prices' => \Config::get('nail_price_icon'),
            'campaigns' => \Model_Campaign::get_random(),
            'banner_recommends' => array(),
        );
        return $data;
    }

    /**
     * get nail details for mobile
     *
     * @author CaoLP
     * @param array $param Input data
     * @return array
     */
    public static function get_mobile_detail($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.rank',
            self::$_table_name.'.hp_coupon',
            self::$_table_name.'.photo_cd',
            self::$_table_name.'.price',
            self::$_table_name.'.tax_price',
            DB::expr("(".self::$_table_name.".price + ".self::$_table_name.".tax_price) AS sell_price"),
            DB::expr("IF(ISNULL(nail_favorites.nail_id),0,1) AS is_favorite"),
            DB::expr(
                "IF(ISNULL(IF(nails.photo_url = '', NULL, nails.photo_url)), CONCAT('".\Config::get(
                    'item_img_url'
                )['nails']."', photo_cd, '".".jpg"."'), nails.photo_url) as image_url"
            )
        )
            ->from(self::$_table_name)
            ->join(
                DB::expr(
                    "
                (SELECT *
                FROM nail_favorites
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) nail_favorites
            "
                ),
                'LEFT'
            )
            ->on(self::$_table_name.'.id', '=', 'nail_favorites.nail_id');

        if (isset($param['order_id']) && $param['order_id'] !== '') {
            $query->join('order_nails')
                ->on(self::$_table_name.'.id', '=', 'order_nails.nail_id')
                ->where('order_nails.disable', '=', 0);
        }

        $query->where(self::$_table_name.'.id', '=', $param['id'])
            ->where(self::$_table_name.'.disable', '=', 0);
        // get data
        $data = $query->execute()->offsetGet(0);
        return $data;
    }
}
