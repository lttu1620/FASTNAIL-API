<?php

/**
 * Any query in Model Contact
 *
 * @package Model
 * @created 2015-08-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Contact extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'name',
        'email',
        'tel',
        'address',
        'website',
        'subject',
        'content',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Model\Observer_Log'     => array(
            'events'          => array(
                'after_load',
                'after_create',
                'after_update',
                'after_delete',
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'contacts';

    /**
     * Add and update info for Contact
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Contact id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $self = new self;
        if (!empty($id)) {
            $self = self::find($id);
            if (empty($self)) {
                static::errorNotExist('contact_id', $id);
                return false;
            }
        }

        if (isset($param['user_id'])) {
            $self->set('user_id', $param['user_id']);
        }
        if (isset($param['name'])) {
            $self->set('name', $param['name']);
        }
        if (isset($param['email'])) {
            $self->set('email', $param['email']);
        }
        if (isset($param['tel'])) {
            $self->set('tel', $param['tel']);
        }
        if (isset($param['address'])) {
            $self->set('address', $param['address']);
        }
        if (isset($param['website'])) {
            $self->set('website', $param['website']);
        }
        if (isset($param['subject'])) {
            $self->set('subject', $param['subject']);
        }
        if (isset($param['content'])) {
            $self->set('content', $param['content']);
        }
        if ($self->save()) {
            if (empty($param['id']) && !empty($param['is_send_email']) && $param['is_send_email'] == '1') {
                if (!empty($param['email'])) {
                    $send = \Lib\Email::sendContactEmail($param);
                    if (!$send) {
                        return false;
                    }
                }
            }
            if (empty($self->id)) {
                $self->id = self::cached_object($self)->_original['id'];
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list Contact (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Contact
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['email'])) {
            $query->where('email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['tel'])) {
            $query->where('tel', 'LIKE', "%{$param['tel']}%");
        }
        if (!empty($param['address'])) {
            $query->where('address', 'LIKE', "%{$param['address']}%");
        }
        if (!empty($param['website'])) {
            $query->where('website', 'LIKE', "%{$param['website']}%");
        }
        if (!empty($param['subject'])) {
            $query->where('subject', 'LIKE', "%{$param['subject']}%");
        }
        if (!empty($param['content'])) {
            $query->where('content', 'LIKE', "%{$param['content']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get detail Contact
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Contact or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('contact_id');
            return false;
        }
        return $data;
    }
}