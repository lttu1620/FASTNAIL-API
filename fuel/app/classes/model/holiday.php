<?php

/**
 * Any query in Model Holiday
 *
 * @package Model
 * @created 2015-09-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Holiday extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'date',
        'name',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'holidays';

    /**
     * Add or update info for Holiday
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Holiday id or false if error
     */
    public static function add_update($param)
    {
        $self = DB::select()
            ->from(self::$_table_name)
            ->where(DB::expr("
                DATE(FROM_UNIXTIME(date)) = '{$param['date']}'
            "))
            ->execute()
            ->offsetget(0);
        if ($self) {
            $self = self::find($self['id']);
            if (empty($self)) {
                self::errorNotExist('holiday_id');
                return false;
            }
            if ($self->get('disable') == '1') $self->set('disable', '0');
        } else {
            $self = new self;
        }
        // set value
        if (isset($param['name'])) {
            $self->set('name', $param['name']);
        }
        if (isset($param['date']) && $param['date'] != '') {
            $self->set('date', self::time_to_val($param['date']));
        }
        // save to database
        if ($self->save()) {
            if (empty($self->id)) {
                $self->id = self::cached_object($self)->_original['id'];
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list Holiday (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Holiday
     */
    public static function get_list($param)
    {
        $param['date'] = strtotime($param['date']);
        $calendar = \Lib\Calendar::get_calendar($param['date']);
        $param['date_from'] = $calendar['firstDayOfCalendar'];
        $param['date_to'] = $calendar['lastDayOfCalendar'];
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.name',
            array('holidays.date', 'holiday_date'),
            DB::expr("
                FROM_UNIXTIME(holidays.date) AS date
            ")
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.date', '>=', self::date_from_val($param['date_from']))
            ->where(self::$_table_name.'.date', '<=', self::date_to_val($param['date_to']))
            ->where(self::$_table_name.'.disable', '=', '0');
        $query->order_by(self::$_table_name.'.date', 'ASC');
        $booking = $query->execute()->as_array();
        // distributing by day
        $firstDayOfLastMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) - 1, 1, date('Y')));
        $firstDayOfNextMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) + 1, 1, date('Y')));
        $data = array(
            'calendarInfo' => array(
                'thisMonth'           => date('n', $param['date']),
                'thisYear'            => date('Y', $param['date']),
                'firstDayOfLastMonth' => $firstDayOfLastMonth,
                'lastMonth'           => date('n', strtotime($firstDayOfLastMonth)),
                'yearOfLastMonth'     => date('Y', strtotime($firstDayOfLastMonth)),
                'firstDayOfNextMonth' => $firstDayOfNextMonth,
                'nextMonth'           => date('n', strtotime($firstDayOfNextMonth)),
                'yearOfNextMonth'     => date('Y', strtotime($firstDayOfNextMonth)),
            ),
        );
        for ($i = 0; $i < $calendar['daysInCalendar']; $i++) {
            $dayInCalendar = strtotime('+'.$i.'days', strtotime($calendar['firstDayOfCalendar']));
            $temp = array(
                'dateInfo' => array(
                    'date'        => date('Y-m-d', $dayInCalendar),
                    'day'         => date('j', $dayInCalendar),
                    'isSunday'    => date('w', $dayInCalendar) == '0' ? true : false,
                    'isThisMonth' => date('Y-m', $dayInCalendar) == date('Y-m', $param['date']),
                    'isToday'     => $dayInCalendar == strtotime(date('Y-m-d', time())),
                    'isHoliday'   => false,
                ),
            );
            foreach ($booking as $item) {
                $dayInOrder = strtotime(date('Y-m-d', $item['holiday_date']));
                if ($dayInCalendar == $dayInOrder) {
                    $temp['dateData'] = $item;
                    $temp['dateInfo']['isHoliday'] = true;
                }
            }
            $data['calendarData'][] = $temp;
        }
        return $data;
    }

    /**
     * Get all Holiday (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Holiday
     */
    public static function get_all($param)
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name.'.date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name.'.date', '<=', self::date_to_val($param['date_to']));
        }
        $query->order_by(self::$_table_name.'.date', 'ASC');
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Holiday
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $self = DB::select()
            ->from(self::$_table_name)
            ->where(DB::expr("
                DATE(FROM_UNIXTIME(date)) = '{$param['date']}'
            "))
            ->execute()
            ->offsetget(0);
        if ($self) {
            $self = self::find($self['id']);
            if ($self) {
                $self->set('disable', $param['disable']);
                if (!$self->save()) {
                    return false;
                }
                return true;
            }
        }
        self::errorNotExist('holiday_id');
        return false;
    }

    /**
     * Get detail Holiday
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Holiday or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('holiday_id');
            return false;
        }
        return $data;
    }
}
