<?php

class Model_Area extends Model_Abstract
{
	protected static $_properties = array(		
		'id',
		'name',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'areas';
        
    /**
     * Get list of areas
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of areas
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }        
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }        
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], 
               !empty($sortExplode[1]) ? $sortExplode[1] : 'DESC');
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }      
        
    /**
     * Get all of areas
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of areas
     */
    public static function get_all($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '0');
        $data = $query->execute()->as_array();        
        if (!empty($data) && isset($param['get_shop'])) {
            $shops = \Model_Shop::get_all(array(
                    'region' => !empty($param['region']) ? $param['region'] : '', 
                    'area_id' => \Lib\Arr::field($data, 'id')
                )
            );
            foreach ($data as &$row) {
                $row['shops'] = \Lib\Arr::filter($shops, 'area_id', $row['id'], false, false);                
            }
        }
        return $data;
    }
    
    /**
     * Add or update info for area
     *
     * @author Tran Xuan Khoa
     * @param array $param Values of each field of area
     * @return int|bool Area id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $area = new self;
        // check exist
        if (!empty($id)) {
            $area = self::find($id);
            if (empty($area)) {
                self::errorNotExist('area_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $area->set('name', $param['name']);
        }
        // save to database
        if ($area->save()) {
            if (empty($area->id)) {
                $area->id = self::cached_object($area)->_original['id'];
            }
            return !empty($area->id) ? $area->id : 0;
        }
        return false;
    }
    
    /**
     * Disable/Enable list Area
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $area = self::find($id);
            if ($area) {
                $area->set('disable', $param['disable']);
                if (!$area->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
        }
        return true;        
    }
    
    /**
     * Get detail Area
     *
     * @author Tran Xuan Khoa
     * @param array $param Input data
     * @return array|bool Detail Area or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('area_id');
            return false;
        }
        return $data;
    }
}
