<?php

class Model_Batch extends Model_Abstract {
    
    /**
     * Add or update info for Hologram
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_properties = array(
        
    );
    /**
     * Add or update info for Hologram
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Id or false if error
     */
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'orders';
    
    
    public static function send_thanks_email($param) {
        $data = DB::select(
                self::$_table_name . '.*'              
            )
            ->from(self::$_table_name)           
            ->where('disable', '=', '0')
            ->where('is_paid', '=', '1')
            ->where(DB::expr("
                reservation_date < UNIX_TIMESTAMP()
                updated >= UNIX_TIMESTAMP() + 60*60
                AND id NOT IN (SELECT order_id FROM mail_send_logs)
            "))
            ->order_by('reservation_date ASC')
            ->limit($param['limit'])
            ->offset(0)
            ->execute()
            ->as_array();
        return $data;
    }
}
