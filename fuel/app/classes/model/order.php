<?php

/**
 * Any query in Model Order
 *
 * @package Model
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Order extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'shop_id',
        'shop_counter_code',
        'hp_order_code',
        'cart_id',
        'admin_id',
        'last_update_admin_id',
        'user_id',
        'seat_id',
        'total_price',
        'total_tax_price',
        'reservation_date',
        'user_code',
        'user_name',
        'visit_section',
        'visit_element',
        'reservation_type',
        'hf_section',
        'off_nails',
        'nail_length',
        'nail_type',
        'nail_add_length',
        'problem',
        'request',
        'service',
    	'purpose',
        'pay_type',
        'photo_code',
        'start_date',
        'order_start_date',
        'order_end_date',
        'sr_start_date',
        'sr_end_date',
    	'consult_start_date',
    	'consult_end_date',
        'off_start_date',
        'off_end_date',
        'sales_code',
        'rating_1',
        'rating_2',
        'rating_3',
        'rating_4',
        'rating_5',
        'reservation_type_old',
        'is_mail',
        'is_designate',
        'is_next_reservation',
        'is_next_designate',
        'is_paid',
        'is_cancel',
        'is_autocancel',
        'welfare_id',
        'first_name',
        'last_name',
        'kana',
        'prefecture_id',
        'address1',
        'address2',
        'sex',
        'birthday',
        'email',
        'phone',
        'r_point',
        'created',
        'updated',
        'disable',
    	'estimate_time',
    	'fn_point',
    	'point_price',
    );

    protected static $_observers = array(
//        'Model\Observer_Log' => array(
//            'events' => array(
//                'after_load',               
//                'after_create', 
//                'after_update',
//                'after_delete'
//            ),
//            'mysql_timestamp' => false,
//        ),
        'Model\Observer_Order' => array(
            'events' => array(
                'after_update',
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'orders';

    
    /**
     * Add or update info for Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order id or false if error, -1 if reservation_date is duplicated
     */
    public static function add($param)
    {
        $new = true;
        if (!empty($param['order_id'])) {
            $new = false;
            $order = self::find($param['order_id']);
            if (empty($order)) {
                static::errorNotExist('order_id');
                return false;
            }
            $param['old_service'] = $order->get('service');
            $param['old_off_nails'] = $order->get('off_nails');
            if (empty($param['shop_id'])) {
                $param['shop_id'] = $order->get('shop_id');
            }  
            if (empty($param['reservation_date'])) {
                $param['reservation_date'] = date('Y-m-d H:i', $order->get('reservation_date'));
            }
        } else {
            $order = new self;
        }
        if (empty($param['shop_id'])) {
            self::errorParamInvalid('shop_id');
            return false;
        }
        // shop info
        $shop = Model_Shop::find($param['shop_id']);
        if (empty($shop)) {
            static::errorNotExist('shop_id');
            return false;
        }
        
        // user info 
        $user = false;
        if (!empty($param['login_user_id'])) {
            $user = Model_User::find($param['login_user_id']);        
            if (empty($user) || $user->get('disable') == 1) {
                \LogLib::warning('Can not register user', __METHOD__, $param);
                self::errorOther(self::ERROR_CODE_OTHER_2, 'user_id', 'Can not register user');
                return false;
            }
        }
        $is_book_foot = false;
        $is_placeholder = false;
        if (!empty($param['nail_id'])) {
            $nail = Model_Nail::find($param['nail_id']);
            if (!empty($nail)) {
                if (!empty($nail->get('hf_section'))) {
                    $param['service'] = $nail->get('hf_section') == 1 ? 1 : 2;   // ハンドジェル | フットジェル             
                }     
                $order->set('total_price', $nail->get('price'));
                $order->set('total_tax_price', $nail->get('tax_price'));
                $order->set('photo_code', $nail->get('photo_cd'));
                $order->set('hf_section', $nail->get('hf_section'));
                if ($nail->get('hf_section') == 2) {
                    $is_book_foot = true;
                }
                // only can book a Hand order and a Foot order on mobile
                if ($param['os'] != \Config::get('os')['webos']                    
                    && !empty($user)
                ) {
                    $query = DB::select(
                            self::$_table_name . '.id', 
                            self::$_table_name . '.reservation_date', 
                            self::$_table_name . '.hf_section'
                        )
                        ->from(self::$_table_name)
                        ->where(self::$_table_name . '.created', '<=', time())
                        ->where(self::$_table_name . '.order_end_date', '>', time())
                        ->where(self::$_table_name . '.hf_section', '>', 0)
                        ->where(self::$_table_name . '.is_cancel', '=', 0) 
                        ->where(self::$_table_name . '.is_autocancel', '=', 0) 
                        ->where(self::$_table_name . '.is_paid', '=', 0) 
                        ->where(self::$_table_name . '.disable', '=', 0) 
                        ->where(self::$_table_name . '.user_id', '=', $user->get('id'));
                    if (!empty($param['order_id'])) {
                        $query->where(self::$_table_name . '.id', '<>', $param['order_id']);
                    }                     
                    $lastOrders = $query->order_by(self::$_table_name . '.created', 'DESC')
                                        ->execute()
                                        ->as_array(); 
                    $hf_section = array(
                        1 => 0, // hand
                        2 => 0, // foot
                    );
                    foreach ($lastOrders as $lastOrder) {
                        $hf_section[$lastOrder['hf_section']]++;                    
                    }
                    if ($hf_section[1] > 0 && $hf_section[2] > 0) {
                        \LogLib::warning('Ordered to allowed quota', __METHOD__, $param);
                        self::errorOther(self::ERROR_CODE_OTHER_1, 'hf_section', 'You have ordered to allowed quota');
                        return false;
                    } elseif ($hf_section[$nail->get('hf_section')] > 0) {
                        \LogLib::warning('Can not order same hf_section', __METHOD__, $param);
                        self::errorDuplicate('hf_section', 'Can not order same hf_section');
                        return false;
                    }
                }
            }
        } else {
            $is_placeholder = true;
            $order->set('hf_section', 0);
            $order->set('total_price', 0);
            $order->set('total_tax_price', 0);
        }
        
        if (isset($param['reservation_date'])) {
            $plug = $shop->get('is_plus') ? 45 : 30;            
            if (isset($nail) && !empty($nail->get('hf_section'))) {
                $param['service'] = $nail->get('hf_section') == 1 ? 1 : 2;   // ハンドジェル | フットジェル             
            }
            if (!empty($param['service'])) {
                switch ($param['service']) {  
                    case 1: // ハンドジェル                     
                        $plug = 30;
                        break;
                    case 2: // フットジェル                     
                        $plug = !empty($param['off_nails']) && $param['off_nails'] > 1 ? 45 + 15 : 15 + 15;  // 1: なし +15 for Footbath
                        break;
                    case 3: // ハンド&フットジェル                       
                        $plug = !empty($param['off_nails']) && $param['off_nails'] > 1 ? 90 : 60;  // 1: なし
                        break;      
                    case 4: // ハンドオフのみ                     
                        $plug = 30;
                        break;      
                    case 5: // フットオフのみ                    
                        $plug = 30 + 15;
                        break;   
                    case 6: // ハンドお直し                    
                        $plug = 15;
                        break;   
                    case 7: // フットお直し                     
                        $plug = 15 + 15;
                        break; 
                }
            }
           
            $param['order_start_date'] = $param['reservation_date'];
            $param['order_end_date'] = date("Y-m-d H:i", self::time_to_val($param['reservation_date'] . ' +'.$plug.' minutes'));
            
            // re-update new shop_counter_code when duplicate with new reservation_date's shop_counter_code
            // re-update new seat_id when duplicate with new reservation_date's seat_id
            $dateParam = date('Y-m-d', self::time_to_val($param['reservation_date']));
            $dateOld = date('Y-m-d', $order->get('reservation_date'));
            if ($new == false && $dateParam != $dateOld) { 
                if (!empty($order->get('shop_counter_code')) && $dateParam == date('Y-m-d')) {                  
                    $param['update_shop_counter_code'] = true;
                }
                if (!empty($order->get('reservation_date'))) {
                    $vacancy = self::get_available_vacancy(array(
                            'shop_id' => $param['shop_id'],
                            'not_in_order_id' => $param['order_id'],
                            'order_start_date' => $param['order_start_date'],
                            'order_end_date' => $param['order_end_date']
                        )
                    );                        
                    $param['seat_id'] = min($vacancy);
                }
            }
            $order->set('reservation_date', self::time_to_val($param['reservation_date']));
            $order->set('order_start_date', self::time_to_val($param['order_start_date']));                
            $order->set('order_end_date', self::time_to_val($param['order_end_date']));
            $order->set('is_designate', !empty($shop->get('is_designate')) ? $shop->get('is_designate') : 0);
        }        
            
        //check available Booking time
        $date = date('Y-m-d', self::time_to_val($param['reservation_date']));
        $time = date('H:i', self::time_to_val($param['reservation_date']));
        $year = date('Y', self::time_to_val($param['reservation_date'] . ' -1day'));
        $month = date('m', self::time_to_val($param['reservation_date'] . ' -1day'));
        $day = date('d', self::time_to_val($param['reservation_date'] . ' -1day'));
        $param['year'] = $year;
        $param['month'] = $month;
        $param['date_from'] = $day;       
        if (!isset($param['no_check_reservation_date'])) {
            $calendar = self::calendar($param);
            if (!empty($calendar[$date][$time])) {
                \LogLib::info('Booking time unavailable', __METHOD__, $param);
                self::errorOther(self::ERROR_CODE_OTHER_1, 'reservation_date', 'Booking time unavailable');
                return false;
            }
        }
        $fields = array(            
            'cart_id' => 0,
            'admin_id' => 0,
            'visit_element' => 0,
            'is_mail' => 0,
            'sales_code' => 0,
            'is_autocancel' => 0,
            'is_cancel' => 0,
            'reservation_type_old' => 0,
            'is_next_reservation' => 0,
            'is_next_designate' => 0,
            'is_paid' => 0,
            'pay_type' => 0,
            'visit_section' => 0,
            'welfare_id' => 0,
            'reservation_type' => 5,
            'r_point' => 0,
            'fn_point' => 0,
            'point_price' => 0,
            'off_nails' => 0,
            'service' => 0,
            'rating_1' => 0,
            'rating_2' => 0,
            'rating_3' => 0,
            'rating_4' => 0,
            'rating_5' => 0,
            'problem' => '',
            'request' => '',
            'nail_length' => 0,
            'nail_type' => 0,
            'nail_add_length' => 0,
            'estimate_time' => 0,
        );
        if ($new) {
            foreach ($fields as $field => $value) {
                if (empty($param[$field])) {
                    $param[$field] = $value;
                }
            }
            if (!empty($user) 
                && !empty($param['fn_point']) 
                && $user->point < $param['fn_point']) {
                self::errorOther(self::ERROR_CODE_OTHER_2, 'fn_point', 'The maximum point must be less than or equal to ' . $user->point);
                return false;
            }
            if ($param['fn_point'] > 0) {
                $param['point_price'] = $param['fn_point'] * \Config::get('change_rate');                
            }   
            if ($user) {
                $cnt = self::count(array(
                    'where' => array(
                        'user_id' => $user->get('id'),                    
                        'disable' => '0',                    
                    )
                ));
                if ($cnt == 0) {
                    $param['visit_section'] = '1'; // The first order 
                }
            }
        } elseif (isset($param['reservation_type'])) {
            unset($param['reservation_type']);
        }
        
        $order->set('shop_id', $param['shop_id']);       
        foreach ($fields as $field => $value) {
            if (isset($param[$field])) {
                $order->set($field, $param[$field]);
            }
        }

        /*    
        Add order_services       
        service mapping
        1. ハンドジェル = 1 or 2 (has_off: 1, no off: 2)
        2. フットジェル = 3 or 4 (has_off: 3, no off: 4)
        3. ハンド＆フットジェル = 1,3 or 2,4 (has_off: 1,3 no off: 2,4)
        4. ハンドオフのみ = 6 
        5. フットオフのみ = 7
        6. ハンドお直し= 8
        7. フットお直し= 9
        */        
        if (!empty($param['service']) && $param['service'] > 0) { 
            $has_off_maping = array(
                1 => array(1), // Hオフオン
                2 => array(3), // Fオフオン
                3 => array(1,3), // Hオフオン, Fオフオン                
                4 => array(6), // Hオフのみ                   
                5 => array(7), // Fオフのみ                   
                6 => array(8), // Hお直し                   
                7 => array(9), // Fお直し                   
            );
            $no_off_maping = array(
                1 => array(2), // Hオーダーオン 
                2 => array(4), // Fオーダーオン
                3 => array(2,4), // Hオーダーオン, Fオーダーオン                    
                4 => array(6), // Hオフのみ                    
                5 => array(7), // Fオフのみ                  
                6 => array(8), // Hお直し                   
                7 => array(9), // Fお直し                   
            );
            if (isset($param['off_nails']) 
                && $param['off_nails'] > 1 
                && isset($has_off_maping[$param['service']])) {
                $param['order_service'] = $has_off_maping[$param['service']];
            } elseif (isset($no_off_maping[$param['service']])) {
                $param['order_service'] = $no_off_maping[$param['service']];
            }
            if ($param['service'] == 4) { // ハンドオフのみ - Handoff only
                $param['seat_id'] = 0;
            }
            $services = Model_Service::get_all();
            $param['estimate_time'] = 0;            
            foreach ($services as $service) { 
                if (in_array($service['id'], $param['order_service']) && !empty($service['time'])) {
                    $param['estimate_time'] += $service['time'];
                }
            }
            $order->set('estimate_time', $param['estimate_time']);
        } 
        
        //Caolp Set Seat ID auto
        if (!isset($param['seat_id'])) {
            if (isset($order) && $order->get('seat_id') > 0) {
                $param['seat_id'] = $order->get('seat_id');
            } else {
                $vacancy = self::get_available_vacancy(array(
                        'shop_id' => $param['shop_id'],
                        'not_in_order_id' => !empty($param['order_id']) ? $param['order_id'] : 0,
                        'order_start_date' => $param['order_start_date'],
                        'order_end_date' => $param['order_end_date']
                    )
                );
                $param['seat_id'] = min($vacancy);
            }
        }
//        print_r($vacancy);
//        print_r($param);die;
        if (isset($param['seat_id'])) {
            $order->set('seat_id', $param['seat_id']);
        }
        //End set Seat ID
        
        if (empty($user)) {
            if (empty($param['User'])) {
                self::errorParamInvalid('User');
                return false;
            }
            $param['User'] = json_decode($param['User'], true);            
            $user = Model_User::register($param['User']);
            if (self::error()) {
                return false;
            }
        }
        $order->set('user_id', $user->get('id'));
        $order->set('user_code', $user->get('code'));
        $order->set('visit_element', $user->get('visit_element'));
        $order->set('email', $user->get('email'));
        $order->set('user_name', $user->get('name'));
        $order->set('kana', $user->get('kana'));
        $order->set('phone', $user->get('phone'));
        $order->set('sex', $user->get('sex'));
        $order->set('birthday', $user->get('birthday'));
        $order->set('prefecture_id', $user->get('prefecture_id'));
        $order->set('address1', $user->get('address1'));
        $order->set('address2', $user->get('address2'));
        
        if (isset($param['update_shop_counter_code']) && !empty($order) && !empty($order->get('reservation_date'))) {            
    		$order->set('shop_counter_code', Model_Shop_Counter_Code::get_code(true));
    	}
        $return_point = false;
        $point = 0;        
        if ($order->save()) {
            if (empty($order->id)) {                
                $order->id = self::cached_object($order)->_original['id'];
                $order->total_price = self::cached_object($order)->_original['total_price'];
                $order->total_tax_price = self::cached_object($order)->_original['total_tax_price'];
                $order->reservation_date = self::cached_object($order)->_original['reservation_date'];
                $order->phone = self::cached_object($order)->_original['phone'];                
            }
            //re-check 1 more time to avoid duplicating seat_id,
            if ($new && !empty($order->seat_id)) {
                \LogLib::info('Re-check 1 more time', __METHOD__, $order);
                $recheck_param = $param;
                $recheck_param['order_id'] = $order->id;             
                usleep(rand(10000,100000));                
                $vacancy = self::get_available_vacancy(array(
                        'shop_id' => $param['shop_id'],
                        'not_in_order_id' => $order->id,
                        'order_start_date' => $param['order_start_date'],
                        'order_end_date' => $param['order_end_date']
                    )
                );
                if (empty($vacancy)) {
                    \LogLib::info('Booking time unavailable', __METHOD__, $param);
                    self::errorOther(self::ERROR_CODE_OTHER_5, 'reservation_date_recheck', 'Booking time unavailable');
                    $order->delete();
                    return false;
                }
                $new_seat = min($vacancy);
                if($new_seat != $param['seat_id']){
                    $order->set('seat_id',$new_seat);
                    $order->save();
                }
            }
//            if ($new &&
//                !empty($user) 
//                && !empty($param['fn_point']) 
//                && $user->point >= $param['fn_point']) {
//                $return_point = true;
//                $point = Model_User_Point_Log::add(array(
//                    'login_user_id' => $order->get('user_id'),
//                    'point' => $param['fn_point'],
//                    'get_point' => true,
//                    'order_id' => $order->id,
//                    'used_point' => true,
//                    'type' => \Config::get('user_point_logs.type.point_used_by_order')
//               ));
//            }
//            
            // save order_nailist = order_logs
            if (!empty($param['nailist_id'])) {
                if (!Model_Order_Log::add_update_by_order_id(array(
                        'order_id' => $order->id,
                        'nailist_id' => $param['nailist_id']
                    ))) {
                    \LogLib::warning('Can not create order_log', __METHOD__, $param);
                    return false;
                }
            } else {
                // delete cache
                \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
                \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            }

            // save order_nails
            if (!empty($param['nail_id'])) {
                $nailId = explode(',', $param['nail_id']);
                foreach ($nailId as $id) {
                    if (!Model_Order_Nail::add(array(
                        'order_id' => $order->id,
                        'nail_id' => $id
                    ))) {
                        \LogLib::warning('Can not create order_nail', __METHOD__, $param);
                        return false;
                    }
                    // write log for position of nail that booked from recommend
                    if (isset($param['recommend_nail_position']) 
                        && $param['recommend_nail_position'] >= 0) {
                        Model_Recommend_Reaction_Log::add(array(
                            'user_id' => $order->get('user_id'),
                            'recommend_order' => $param['recommend_nail_position'],
                            'order_id' => $order->get('id'),
                            'nail_id' => $id,
                            'action_type' => 2,
                        ));
                    }
                }
            }
            
            if (!empty($param['order_service'])) {
                // unchange order service if changed by admin 
                $update_order_service = true;
                if ($new == false) {
                    if ($param['old_off_nails'] > 1 
                        && isset($has_off_maping[$param['old_service']])) {
                        $param['old_order_service'] = $has_off_maping[$param['old_service']];
                    } elseif (isset($no_off_maping[$param['old_service']])) {
                        $param['old_order_service'] = $no_off_maping[$param['old_service']];
                    }
                    $current_order_service = Lib\Arr::field(
                        Model_Order_Service::get_all(array(
                            'order_id' => $order->get('id')
                        )),
                        'service_id'
                    );                    
                    $diff = array_diff($current_order_service, $param['old_order_service']);
                    if (!empty($diff)) {
                        $update_order_service = false;
                    }
                }
                if ($update_order_service == true && 
                    !empty($param['order_service']) 
                    && !Model_Order_Service::multi_update(array(
                        'order_id' => $order->get('id'),
                        'service_id' => $param['order_service'],
                    ))
                ) {
                    \LogLib::warning('Can not create order_service', __METHOD__, $param);
                    return false;
                }
            }          
                
            // action only for addnew
            if (empty($param['order_id'])) { 
                // check and add device when book foot | hand + foot services
                // case book hand + foot services                           
                switch ($param['service']) {
                    case 4: // ハンドオフのみ
                        $param['device_type'] = 1; // hand device
                        break;
                    case 2: // フットジェル
                    case 3: // ハンド＆フォットジェル
                    case 5: // フットオフのみ
                    case 7: // フットお直し
                        $param['device_type'] = 2; // foot device
                        break;
                }
                // case book foot
                if (!isset($param['device_type']) && $is_book_foot) {
                    $param['device_type'] = 2; // foot device
                }
                // get a available device and add to the order
                if (isset($param['device_type'])) {                    
                    $available_device = Model_Device::get_available_device_by_type($param);                    
                    if (!empty($available_device)) {
                        Model_Order_Device::add_update(array(
                            'order_id' => $order->get('id'),
                            'device_id' => $available_device['id']
                        ));                       
                    } 
                }
                
                // send email if add new order
                if (!\Lib\Email::sendCreateOrder(array(
                    'email' => $order->get('email'),
                    'order_id' => $order->get('id'),
                    'user_id' => $order->get('user_id'),
                    'user_name' => $order->get('user_name'),
                    'shop_id' => $order->get('shop_id'),
                    'service' => $order->get('service'),
                    'nailist_id' => empty($param['nailist_id']) ? 0 : $param['nailist_id'],
                	'total_price' => $order->get('total_price'),
                	'total_tax_price' => $order->get('total_tax_price'),
                	'reservation_date' => $order->get('reservation_date'),
                ))) {
                    \LogLib::warning('Can not send create order email', __METHOD__, $param);
                } else {
                	\LogLib::warning('Send create order email OK', __METHOD__, $param);
                }
            }
            
            // return data
            if (isset($param['get_login_user_info'])) {
                return array(
                    'id' => !empty($order->id) ? $order->id : 0,
                    'login_user_info' => $user
                );
            }
            // delete cache
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            return !empty($order->id) ? $order->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order id or false if error, -1 if reservation_date is duplicated
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $order = new self;        

        $dateParam = date('Y-m-d', self::time_to_val($param['reservation_date']));
        // check exist
        if (!empty($id)) {
            $param['not_in_order_id'] = $id;
            $order = self::find($id);
            if (empty($order)) {
                self::errorNotExist('order_id');
                return false;
            }
            // re-update new shop_counter_code when duplicate with new reservation_date's shop_counter_code            
            $dateOld = date('Y-m-d', $order->get('reservation_date'));
            if (($dateParam != $dateOld && $dateParam == date('Y-m-d'))
                || ($dateParam == date('Y-m-d') && empty($order->get('shop_counter_code')))) {
                $param['update_shop_counter_code'] = true;
            }          
        }
       
        // check nailist
        if (!empty($param['nailist_id'])) {
            if (self::check_duplicate_order_date_nailist($param, $id)) {
                self::errorDuplicate('nailist_id');
                return false;
            }
        }
        
        // check device
        if (!empty($param['devices_id'])) {
            if (self::check_duplicate_order_date_device($param, $id)) {
                self::errorDuplicate('devices_id');
                return false;
            }
        }
        
        // set default params
        if (empty($id)) {
            if (empty($param['cart_id'])) {
                $param['cart_id'] = 0;
            }
            if (empty($param['user_name'])) {
                $param['user_name'] = '';
            }
            if (empty($param['is_mail'])) {
                $param['is_mail'] = 0;
            }
            if (empty($param['is_paid'])) {
                $param['is_paid'] = 0;
            }
            if (empty($param['is_cancel'])) {
                $param['is_cancel'] = 0;
            }
            if (empty($param['is_autocancel'])) {
                $param['is_autocancel'] = 0;
            }
            if (empty($param['photo_code'])) {
                $param['photo_code'] = '';
            } 
            if (empty($param['reservation_type'])) {
                $param['reservation_type'] = 0;
            }
            if (empty($param['rating_1'])) {
                $param['rating_1'] = '0';
            } 
            if (empty($param['rating_2'])) {
                $param['rating_2'] = '0';
            } 
            if (empty($param['rating_3'])) {
                $param['rating_3'] = '0';
            } 
            if (empty($param['rating_4'])) {
                $param['rating_4'] = '0';
            } 
            if (empty($param['rating_5'])) {
                $param['rating_5'] = '0';
            }             
            if (empty($param['visit_section'])) {
                $param['visit_section'] = '0';
            }             
            if (empty($param['visit_element'])) {
                $param['visit_element'] = '0';
            } 
            if ($dateParam == date('Y-m-d')) {
                $param['update_shop_counter_code'] = true;
            }
        }
       
        // set value
        if (isset($param['shop_id'])) {
            $order->set('shop_id', $param['shop_id']);
        }
        if (isset($param['cart_id'])) {
            $order->set('cart_id', $param['cart_id']);
        }
        if (isset($param['admin_id'])) {
            $order->set('admin_id', $param['admin_id']);
        }
        if (isset($param['user_id']) && !empty($param['email'])) {
            $option['where'] = array(
                'email' => $param['email']
            );
            $user = Model_User::find('first', $option);
            if (!empty($user->id)) {
                $order->set('user_id', $user->get('id'));
                $order->set('user_code', $user->get('code'));
                $order->set('visit_element', $user->get('visit_element'));
                $order->set('email', $user->get('email'));
                $order->set('user_name', $user->get('name'));
                $order->set('kana', $user->get('kana'));
                $order->set('phone', $user->get('phone'));
                $order->set('sex', $user->get('sex'));
                $order->set('birthday', $user->get('birthday'));
                $order->set('prefecture_id', $user->get('prefecture_id'));
                $order->set('address1', $user->get('address1'));
                $order->set('address2', $user->get('address2'));
            } else {
            	$order->set('user_id', 0);
            }
        } else {
            if (empty($param['user_id'])) {
                $param['user_id'] = 0;
            }
            $order->set('user_id', $param['user_id']);
        }
        if (isset($param['total_price']) && $param['total_price'] != '') {
            $order->set('total_price', $param['total_price']);
        }
        if (isset($param['total_tax_price']) && $param['total_tax_price'] != '') {
            $order->set('total_tax_price', $param['total_tax_price']);
        }
        if (isset($param['reservation_date'])) {
            $order->set('reservation_date', self::time_to_val($param['reservation_date']));
        }
        if (isset($param['user_name'])) {
            $order->set('user_name', $param['user_name']);
        }        
        if (isset($param['visit_section'])) {
            $order->set('visit_section', $param['visit_section']);
        }
        if (isset($param['visit_element'])) {
            $order->set('visit_element', $param['visit_element']);
        }
        if (isset($param['reservation_type'])) {
            $order->set('reservation_type', $param['reservation_type']);
        }
        if (isset($param['hf_section']) && $param['off_nails'] !== '') {
            $order->set('hf_section', $param['hf_section']);
        }
        if (isset($param['off_nails']) && $param['off_nails'] !== '') {
            $order->set('off_nails', $param['off_nails']);
        }
        if (isset($param['nail_type']) && $param['nail_type'] !== '') {
            $order->set('nail_type', $param['nail_type']);
        }
        if (isset($param['nail_length']) && $param['nail_length'] !== '') {
            $order->set('nail_length', $param['nail_length']);
        }
        if (isset($param['nail_add_length']) && $param['nail_add_length'] !== '') {
            $order->set('nail_add_length', $param['nail_add_length']);
        } 
        if (isset($param['pay_type']) && $param['pay_type'] !== '') {
            $order->set('pay_type', $param['pay_type']);
        }
        if (isset($param['service']) && $param['service'] !== '') {
            $order->set('service', $param['service']);
        }          
        if (isset($param['problem'])) {
            $order->set('problem', $param['problem']);
        }
        if (isset($param['request']) && $param['request'] !== '') {
            $order->set('request', $param['request']);
        }        
        if (isset($param['photo_code']) && $param['photo_code'] !== '') {
            $order->set('photo_code', $param['photo_code']);
        }
        if (isset($param['start_date']) && $param['start_date'] !== '') {       
            $order->set('start_date', self::time_to_val($param['start_date']));
        }
        if (isset($param['order_start_date']) && $param['order_start_date'] !== '') {       
            $order->set('order_start_date', self::time_to_val($param['order_start_date']));
        }
        if (isset($param['order_end_date']) && $param['order_end_date'] !== '') {
            $order->set('order_end_date', self::time_to_val($param['order_end_date']));
        }
        if (isset($param['sr_start_date']) && $param['sr_start_date'] !== '') {      
            $order->set('sr_start_date', self::time_to_val($param['sr_start_date']));
        }
        if (isset($param['sr_end_date']) && $param['sr_end_date'] !== '') {       
            $order->set('sr_end_date', self::time_to_val($param['sr_end_date']));
        }
        if (isset($param['sales_code']) && $param['sales_code'] !== '') {
            $order->set('sales_code', $param['sales_code']);
        }
        if (isset($param['rating_1']) && $param['rating_1'] !== '') {
            $order->set('rating_1', $param['rating_1']);
        }
        if (isset($param['rating_2']) && $param['rating_2'] !== '') {
            $order->set('rating_2', $param['rating_2']);
        }
        if (isset($param['rating_3']) && $param['rating_3'] !== '') {
            $order->set('rating_3', $param['rating_3']);
        }
        if (isset($param['rating_4']) && $param['rating_4'] !== '') {
            $order->set('rating_4', $param['rating_4']);
        }
        if (isset($param['rating_5']) && $param['rating_5'] !== '') {
            $order->set('rating_5', $param['rating_5']);
        }
        if (isset($param['reservation_type_old']) && $param['reservation_type_old'] != '') {
            $order->set('reservation_type_old', $param['reservation_type_old']);
        }
        if (isset($param['is_mail']) && $param['is_mail'] !== '') {
            $order->set('is_mail', $param['is_mail']);
        }
        if (isset($param['is_designate']) && $param['is_designate'] !== '') {
            $order->set('is_designate', $param['is_designate']);
        }
        if (isset($param['is_next_reservation']) && $param['is_next_reservation'] != '') {
            $order->set('is_next_reservation', $param['is_next_reservation']);
        }
        if (isset($param['is_next_designate']) && $param['is_next_designate'] != '') {
            $order->set('is_next_designate', $param['is_next_designate']);
        }
        if (isset($param['is_paid']) && $param['is_paid'] != '') {
            $order->set('is_paid', $param['is_paid']);
        }
        if (isset($param['welfare_id']) && $param['welfare_id'] != '') {
            $order->set('welfare_id', $param['welfare_id']);
        }
        if (isset($param['kana']) && $param['kana'] != '') {
            $order->set('kana', $param['kana']);
        }
        if (isset($param['prefecture_id']) && $param['prefecture_id'] != '') {
            $order->set('prefecture_id', $param['prefecture_id']);
        }
        if (isset($param['address1'])) {
            $order->set('address1', $param['address1']);
        }
        if (isset($param['address2'])) {
            $order->set('address2', $param['address2']);
        }
        if (isset($param['sex'])) {
            $order->set('sex', $param['sex']);
        }
        if (isset($param['birthday'])) {
            $order->set('birthday', self::time_to_val($param['birthday']));
        }    
        if (isset($param['email'])) {
            $order->set('email', $param['email']);
        }
        if (isset($param['phone'])) { 
            $order->set('phone', $param['phone']);
        }
        if (isset($param['estimate_time'])) {
        	$order->set('estimate_time', $param['estimate_time']);
        }
        if (isset($param['fn_point'])) {
            $order->set('fn_point', $param['fn_point']);
        }
        if (isset($param['point_price'])) {
            $order->set('point_price', $param['point_price']);
        }
        if (empty($param['id']) && empty($param['seat_id'])) {
            $vacancy = self::get_available_vacancy(array(
                    'shop_id' => $param['shop_id'],
                    'not_in_order_id' => !empty($param['id']) ? $param['id'] : 0,
                    'order_start_date' => $param['order_start_date'],
                    'order_end_date' => $param['order_end_date']
                )
            );
            $param['seat_id'] = !empty($vacancy) ? min($vacancy) : 0;
            $order->set('seat_id', !empty($param['seat_id']) ? $param['seat_id'] : 0);
        }
        if (isset($param['update_shop_counter_code']) && !empty($order->get('reservation_date'))) {            
    		$order->set('shop_counter_code', Model_Shop_Counter_Code::get_code(true));
    	}
        // save to database
        if ($order->save()) { 
            if (empty($order->id)) {
                $order->id = self::cached_object($order)->_original['id'];
            }
            // save nailist
            if (isset($param['nailist_id'])) {
                if (!Model_Order_Log::add_update_by_order_id(array(
                    'order_id'   => $order->id,
                    'nailist_id' => $param['nailist_id'],
                ))) {
                    return false;
                }
            }

            // save device
            if (isset($param['devices_id'])) {
                if (!Model_Order_Device::add_update_by_order_id(array(
                    'order_id'   => $order->id,
                    'device_id' => $param['devices_id']
                ))) {
                    return false;
                }
            }

            // save service
            if (isset($param['service_id'])) {
            	if (!Model_Order_Service::multi_update(array(
            			'order_id'   => $order->id,
            			'service_id' => $param['service_id']
            	))) {
            		return false;
            	}
            }

            // save order's items
            if (isset($param['items_id'])) {
                if (!Model_Order_Item::add_update_by_order_id(array(
                    'order_id' => $order->id,
                    'item_id' => $param['items_id'],
                    'item_quantity' => isset($param['items_quantity']) ? $param['items_quantity'] : ''
                ))) {
                    return false;
                }
                // re-update order's price
                Model_Order::updatePrice(array(
                    'order_id' => $order->id
                ));
                if (self::error()) {
                    return false;
                }
            }

            // delete cache
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));

            return $order->id;
        }
        return false;
    }
    
    /**
     * Add or update info for Order for calendar
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Order id or false if error, -1 if reservation_date is duplicated
     */
    public static function add_update_calendar($param)
    {
        $is_blocked_seat = false;
        $is_hp = false;
    	$id = !empty($param['id']) ? $param['id'] : 0;        
    	// check exist
        $dateParam = date('Y-m-d', self::time_to_val($param['reservation_date']));
    	if (!empty($id)) {
    		$param['not_in_order_id'] = $id;
    		$order = self::find($id);
            $is_hp = !empty($order->get('hp_order_code')) ? true : false;
    		if (empty($order)) {
    			self::errorNotExist('order_id');
    			return false;
    		}
            // re-update new shop_counter_code when duplicate with new reservation_date's shop_counter_code
            $dateOld = date('Y-m-d', $order->get('reservation_date'));
            if (($dateParam != $dateOld && $dateParam == date('Y-m-d'))
                || ($dateParam == date('Y-m-d') && empty($order->get('shop_counter_code')))) {
                $param['update_shop_counter_code'] = true;
            }
    	} else {
            $order = new self;
            if ($dateParam == date('Y-m-d')) {
                $param['update_shop_counter_code'] = true;
            }
        }
        
    	//check duplicate
    	if (!empty($id) && !empty($param['is_seat']) && $param['service_id'] !== '6' && $dateParam == $dateOld) {// service_id = 6: Hオフのみ
            $param['seat_id'] = $order->get('seat_id');
            $param['hp_order_code'] = $order->get('hp_order_code');
            if (self::check_duplicate_order_date_seatlist($param, $id)) {
                \LogLib::info('Duplicate seat', __METHOD__, $param);
                return -1;
            }
        }

        if (!empty($param['nailist_id'])) {
    		if (self::check_duplicate_order_date_nailist($param, $id)) {
    			\LogLib::info('Duplicate nailist', __METHOD__, $param);
    			return -1;
    		}
    	}
    	
    	if (!empty($param['devices_id'])) {
    		if (self::check_duplicate_order_date_device($param, $id)) {
    			\LogLib::info('Duplicate device', __METHOD__, $param);
    			return -1;
    		}
    	}
    
    	//check out of time range
    	if (!empty($param['shop_open_time']) && !empty($param['shop_close_time'])){
    		//prepare info for checking time range
    		$shop_open_time = $param['shop_open_time'];
    		$shop_close_time = $param['shop_close_time'];
    		$minTimeOrder = strtotime(date("Y-m-d",self::time_to_val($param['order_start_date']))) + $shop_open_time;
    		$maxTimeOrder = strtotime(date("Y-m-d",self::time_to_val($param['order_end_date']))) + $shop_close_time;
    		$new_order_start_date = self::time_to_val($param['order_start_date']);
    		$new_order_end_date = self::time_to_val($param['order_end_date']);
    		if (($new_order_start_date < $minTimeOrder) || ( $maxTimeOrder < $new_order_end_date)){
    			return -2;
    		}
    	}
    
    	if (empty($id)) {
    		$order->set('is_mail', 0);
    		$order->set('is_cancel', 0);
    		$order->set('is_autocancel', 0);
    		$order->set('cart_id', 0);
    		$order->set('pay_type', 0);
    		$order->set('hf_section', 0);
            if (empty($param['service_id'])) {
    			$param['service_id'] = '';
    		}
    	}

    	// set value
    	if (!empty($param['shop_id'])) {
    		$order->set('shop_id', $param['shop_id']);
    	}
    	
    	if (!empty($param['admin_id'])) {
    		$order->set('admin_id', $param['admin_id']);
    	}
        
        if (!empty($param['last_update_admin_id'])) {
    		$order->set('last_update_admin_id', $param['last_update_admin_id']);
    	}
    	
    	if (isset($param['user_id']) && !empty($param['email'])) {
            $option['where'] = array(
                'email' => $param['email']
            );
            $user = Model_User::find('first', $option);
            if (!empty($user->id)) {
                $order->set('user_id', $user->get('id'));
                $order->set('user_code', $user->get('code'));
                $order->set('visit_element', $user->get('visit_element'));
                $order->set('email', $user->get('email'));
                $order->set('user_name', $user->get('name'));
                $order->set('kana', $user->get('kana'));
                $order->set('phone', $user->get('phone'));
                $order->set('sex', $user->get('sex'));
                $order->set('birthday', $user->get('birthday'));
                $order->set('prefecture_id', $user->get('prefecture_id'));
                $order->set('address1', $user->get('address1'));
                $order->set('address2', $user->get('address2'));
            } else {
                $order->set('user_id', 0);
                $order->set('user_name', $param['user_name']);
                $order->set('sex', $param['sex']);
                $order->set('email', $param['email']);
                $order->set('phone', $param['phone']);
            }
        } else {
            if (empty($param['user_id'])) {
                $param['user_id'] = 0;
            }
            $order->set('user_id', $param['user_id']);

            if (isset($param['user_name'])) {
                $order->set('user_name', $param['user_name']);
            }

            if (isset($param['sex']) && $param['sex'] != '') {
                $order->set('sex', $param['sex']);
            }

            if (isset($param['email'])) {
                $order->set('email', $param['email']);
            }

            if (isset($param['phone'])) {
                $order->set('phone', $param['phone']);
            }
        }

        if (isset($param['reservation_date']) && $param['reservation_date'] !== '') {
    		$order->set('reservation_date', self::time_to_val($param['reservation_date']));
    	}
    		
    	if (isset($param['visit_section']) && $param['visit_section'] !=='') {
    		$order->set('visit_section', $param['visit_section']);
    	}
    	
    	if (isset($param['visit_element']) && $param['visit_element'] !=='') {
    		$order->set('visit_element', $param['visit_element']);
    	}
    	
    	if (isset($param['reservation_type']) && $param['reservation_type'] !=='') {
    		$order->set('reservation_type', $param['reservation_type']);
    	}
    	
    	if (isset($param['request']) && $param['request'] !== '') {
    		$order->set('request', $param['request']);
    	}
        
        if (isset($param['start_date']) && $param['start_date'] !== '') {
    		$order->set('start_date', self::time_to_val($param['start_date']));
    	}
    	
    	if (isset($param['order_start_date']) && $param['order_start_date'] !== '') {
    		$order->set('order_start_date', self::time_to_val($param['order_start_date']));
    	}
    	
    	if (($is_hp == false && $param['service_id'] !== '6') 
            && (empty($id) || (!empty($id) && ($dateParam != $dateOld || $param['service_id'] !== '6')))) { // service_id = 6: Hオフのみ
    		$vacancy = self::check_duplicated_seat_ignore_block(array(
    				'shop_id' => $param['shop_id'],
    				'not_in_order_id' => !empty($param['id']) ? $param['id'] : 0,
    				'order_start_date' => $param['order_start_date'],
    				'order_end_date' => $param['order_end_date']
                )
    		);
            if (empty($vacancy)) {
                return -1;
            }
            $param['seat_id'] = min($vacancy);
    		$order->set('seat_id', !empty($param['seat_id']) ? $param['seat_id'] : 0);
        }elseif ($param['service_id'] === '6') {
            $order->set('seat_id', 0);
        }
        
        if ($is_hp == false && !empty($param['is_seat'])) {
            $input_param = array('shop_id' => $order->get('shop_id'),
                'order_start_date' => $order->get('order_start_date'),
                'order_end_date' => self::time_to_val($param['order_end_date']),
                'seat_id' => $order->get('seat_id'));
            $is_blocked_seat = self::check_one_order_is_blocked_seat($input_param);
        }

        if (isset($param['order_end_date']) && $param['order_end_date'] !== '') {
            $order->set('order_end_date', self::time_to_val($param['order_end_date']));
    	}
        
    	if (isset($param['estimate_time']) && $param['estimate_time'] != '') {
            $order->set('estimate_time', intval($param['estimate_time']));
    	}
        
        if (isset($param['update_shop_counter_code']) && !empty($order->get('reservation_date'))) {            
    		$order->set('shop_counter_code', Model_Shop_Counter_Code::get_code(true));
    	}
        
    	// save to database
    	if ($order->save()) {
    		if (empty($order->id)) {
    			$order->id = self::cached_object($order)->_original['id'];
                $order->shop_id = self::cached_object($order)->_original['shop_id'];
                $order->order_start_date = self::cached_object($order)->_original['order_start_date'];
                $order->order_end_date = self::cached_object($order)->_original['order_end_date'];
                $order->seat_id = self::cached_object($order)->_original['seat_id'];
    		} 
            
            //re-check 1 more time to avoid duplicating seat_id,
            if ($is_hp == false && empty($id) && !empty($order->seat_id)) {
                \LogLib::info('Re-check 1 more time', __METHOD__, $order->id);
                usleep(rand(10000,100000));
                $vacancy = self::check_duplicated_seat_ignore_block(array(
                        'shop_id' => $order->shop_id,
                        'not_in_order_id' => $order->id,
                        'order_start_date' => $order->order_start_date,
                        'order_end_date' => $order->order_end_date
                        )
                );
                //check overlap
                if (empty($vacancy)) {
                    \LogLib::info('Overlapped or no seat -> deleted!!!', __METHOD__, $order->id);
                    $entry = self::find($order->id);
                    $entry->delete();
                    return -1;
                }    
                $reCalSeatId = min($vacancy);
                if (($reCalSeatId > 0) && ($reCalSeatId != $order->seat_id)) {
                    \LogLib::info('Re-check update new seat', __METHOD__, $order->id);
                    $order->set('seat_id', $reCalSeatId);
                    $order->save();
                }
            }

            // save service
    		if (isset($param['service_id'])) {
    			if (!Model_Order_Service::multi_update(array(
    					'order_id'   => $order->id,
    					'service_id' => $param['service_id']
    			))) {
    				return false;
    			}
    		}
            
    		// save nailist
    		if (empty($param['is_seat']) && isset($param['nailist_id']) && $param['service_id'] !== '6') { // service_id = 6: Hオフのみ
    			if (!Model_Order_Log::add_update_by_order_id(array(
    					'order_id'   => $order->id,
    					'nailist_id' => $param['nailist_id'],
    			))) {
    				return false;
    			}
    		}
    
    		// save device
    		if (isset($param['devices_id']) && $is_blocked_seat == false) {
    			if (!Model_Order_Device::add_update_by_order_id(array(
                    'order_id' => $order->id,
                    'device_id' => $param['devices_id']
    			))) {
    				return false;
    			}
    		}
    
    		// delete cache
    		\Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
    		\Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
    
    		return $order->id;
    	}
    	return false;
    }

    /**
     * Get list Order (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order
     */
    public static function get_list($param)
    {
        $exprString = "
            DISTINCT orders.*,
                     (total_price + total_tax_price) AS total_sell_price,
                     shops.name AS shop_name
        ";
        if (!empty($param['nailist_customid'])) {
            $exprString .= ', nailists.name AS nailist_name';
        }
        if (!empty($param['device_customid'])) {
            $exprString .= ', devices.name AS device_name';
        }
        $query = DB::select(
            DB::expr($exprString)
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->join('order_items', 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'order_items.order_id');
        // filter by keyword
        if (!empty($param['item_id'])) {
            $query->where('item_id', '=', $param['item_id']);
        }
        if (!empty($param['shop_id'])) {
            $shop = Model_Shop::find($param['shop_id']);
            if ($shop->get('is_franchise')) {
                $query->where(
                    self::$_table_name.'.shop_id',
                    'IN',
                    DB::expr("(SELECT id from shops WHERE shop_group_id = ".$shop->get("shop_group_id").")")
                );
            } else {
                $query->where(self::$_table_name.'.shop_id', '=', $param['shop_id']);
            }
        }
        if (!empty($param['nailist_customid'])) {
            $query->join('order_logs', 'LEFT')
                ->on(self::$_table_name . '.id', '=', 'order_logs.order_id')
                ->join('nailists', 'LEFT')
                ->on('nailists.id', '=', 'order_logs.nailist_id')
                ->where('nailist_id', '=', $param['nailist_customid'])
                ->where('order_logs.disable', '=', 0);

        }
        if (!empty($param['device_customid'])) {
            $query->join('order_devices', 'LEFT')
                ->on(self::$_table_name . '.id', '=', 'order_devices.order_id')
                ->join('devices', 'LEFT')
                ->on('devices.id', '=', 'order_devices.device_id')
                ->where('order_devices.device_id', '=', $param['device_customid'])
                ->where('order_devices.disable', '=', 0);
        }
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['order_customid'])) {
            $query->where(self::$_table_name . '.id', '=', $param['order_customid']);
        }
        if (!empty($param['hp_order_code'])) {
            $query->where(self::$_table_name . '.hp_order_code', '=', $param['hp_order_code']);
        }
        if (!empty($param['sex'])) {
            $query->where('sex', '=', $param['sex']);
        }
        if (!empty($param['user_name'])) {
            $query->where('user_name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['email'])) {
            $query->where('email', '=', $param['email']);
        }
        if (!empty($param['phone'])) {
            $query->where(self::$_table_name . '.phone', '=', $param['phone']);
        }
        if (!empty($param['address'])) {
            $query->where(self::$_table_name . '.address', 'LIKE', "%{$param['address']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (isset($param['is_cancel']) && $param['is_cancel'] != '') {
        	$query->where(self::$_table_name . '.is_cancel', '=', $param['is_cancel']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Disable/Enable list Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $order = self::find($id);
            if ($order) {
                $order->set('disable', $param['disable']);
                if (!$order->save()) {
                    return false;
                }
                // delete cache
                 \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
                \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            } else {
                self::errorNotExist('order_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get reservation calendar of a shop
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    /**
     * Get reservation calendar of a shop
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function calendar($param)
    { 
        $shop = Model_Shop::find(array('shop_id' => $param['shop_id']));
        if (empty($shop) || empty($param['shop_id'])) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (!empty($param['nail_id'])) {
            $nail = Model_Nail::find($param['nail_id']);
            if (empty($nail)) {
                static::errorNotExist('nail_id');
                return false;
            }
            if (!empty($nail->get('hf_section'))) {
                $param['service'] = $nail->get('hf_section') == 1 ? 1 : 2;   // ハンドジェル | フットジェル             
            }
        }
        
        if (empty($param['off_nails'])) {
            $param['off_nails'] = 0;
        }
        if (empty($param['service'])) {
            $param['service'] = 0;
        }
        
        $user_id = false;
        if(isset($param['login_user_id']) && !empty($param['login_user_id'])){
            $user_id = $param['login_user_id'];
        }
        $year = date('Y');
        if (isset($param['year']) && $param['year'] != '') {
            $year = $param['year'];
        }
        $month = date('m');
        if (isset($param['month']) && $param['month'] != '') {
            $month = $param['month'];
        }
        $day = date('d');
        if (isset($param['date_from']) && $param['date_from'] != '') {
            $day = $param['date_from'];
        }
        
        $time = strtotime("{$year}-{$month}-{$day}");       
        $date_from = date('d', strtotime("monday this week", $time));
        $date_to = date('d', strtotime("sunday this week", $time));

        $month = date('m', strtotime("monday this week", $time));
        $month_next = date('m', strtotime("sunday this week", $time));

        $year = date('Y', strtotime("monday this week", $time));
        $next_year = date('Y', strtotime("sunday this week", $time));

        $date_from_format = strtotime("{$year}-{$month}-{$date_from} 00:00:00");
        $date_to_format = strtotime("{$next_year}-{$month_next}-{$date_to} 23:59:59");
        $datediff = $date_to_format - $date_from_format;
        $total_day = floor($datediff / (60 * 60 * 24));
         
        $plug = !empty($shop->get('is_plus')) ? 45 : 30; // number of minutes of a order        
        if (!empty($param['service'])) {
            switch ($param['service']) {  
                case 1: // ハンドジェル                     
                    $plug = 30;
                    break;
                case 2: // フットジェル                       
                    $plug = !empty($param['off_nails']) && $param['off_nails'] > 1 ? 45 + 15 : 15 + 15;  // 1: なし +15 for Footbath
                    break; 
                case 3: // ハンド&フットジェル                       
                    $plug = !empty($param['off_nails']) && $param['off_nails'] > 1 ? 90 : 60;  // 1: なし
                    break;      
                case 4: // ハンドオフのみ                     
                    $plug = 30;
                    break;      
                case 5: // フットオフのみ                    
                    $plug = 30 + 15;
                    break;   
                case 6: // ハンドお直し                    
                    $plug = 15;
                    break;   
                case 7: // フットお直し                     
                    $plug = 15 + 15;
                    break; 
            }                
        }                
       
        // for test API
        if (isset($param['plug'])) {
            $plug = $param['plug'];
        }
        
        $nextCheck = ceil($plug/$shop->get('reservation_interval'));  
        $config_hour_calendar = array();
        $open_time = !empty($shop->get('open_time')) ? $shop->get('open_time') : 0;
        $close_time = !empty($shop->get('close_time')) ? $shop->get('close_time') : 0;
        $t = $open_time;
        while ($t <= $close_time) {
            $h = floor($t/(60*60));
            $m = floor(($t-$h*(60*60))/(60));
            $config_hour_calendar[] = str_pad($h, 2, '0', STR_PAD_LEFT) . ':' . str_pad($m, 2, '0', STR_PAD_LEFT);
            $t += ($shop->get('reservation_interval')*60);
        }
        $arr_calendar = array();

        // get order data
        $query = DB::select(
                self::$_table_name . '.id',
                self::$_table_name . '.reservation_date',
                self::$_table_name . '.order_start_date',
                self::$_table_name . '.order_end_date',
                self::$_table_name . '.user_id',
                self::$_table_name . '.hf_section',
                self::$_table_name . '.service',
                DB::expr("FROM_UNIXTIME(order_start_date) AS order_start_date_ymd"),
                DB::expr("FROM_UNIXTIME(order_end_date) AS order_end_date_ymd"),
                DB::expr("(
                    SELECT GROUP_CONCAT(device_id SEPARATOR ',') 
                    FROM order_devices 
                    WHERE disable = 0 
                    AND order_id = orders.id
                ) AS device_id")
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(self::$_table_name . '.hp_order_code', '=', null)
            ->where(DB::expr("IFNULL(" . self::$_table_name . ".is_cancel,0) = 0"))
            ->where(self::$_table_name . '.reservation_date', 'BETWEEN', array($date_from_format, $date_to_format));
        if (!empty($param['order_id'])) {
            $query->where(self::$_table_name . '.id', 'NOT IN', DB::expr("({$param['order_id']})"));
        }
        $data = $query->execute()->as_array();
       
        // get timely limit
        $order_timely_limits = Model_shop::get_order_timely_limits(array(
            'shop_id' => $param['shop_id'],
            'date_from' => $date_from_format,
            'date_to' => $date_to_format,
        ));
       
        // get orders of nailist
        $orderNalist = array();
        if (!empty($param['nailist_id'])) {
            $orderLogs = Model_Order_Log::get_all(array(
                'nailist_id' => $param['nailist_id'],
                'order_start_date' => $date_from_format,
                'order_end_date' => $date_to_format,
            ));
            if (!empty($orderLogs)) {
                foreach ($orderLogs as $item) {
                    if (!isset($orderNalist[$item['order_id']])) {
                        $orderNalist[$item['order_id']] = array();
                    }
                    $orderNalist[$item['order_id']][] = $item['nailist_id'];
                }
            }
        }
        
        // get deviceId of hand | foot
        $availableDeviceId = null;          
        if (!empty($param['service']) && in_array($param['service'], array(2,3,4,5,7))) {            
            switch ($param['service']) {
                case 4: // ハンドオフのみ
                    $options['where']['device_type'] = 1; // hand device
                    break;
                case 2: // フットジェル
                case 3: // ハンド＆フォットジェル
                case 5: // フットオフのみ
                case 7: // フットお直し
                    $options['where']['device_type'] = 2; // foot device
                    break;
            }
            $options['where']['shop_id'] = $param['shop_id'];
            $availableDeviceId = Lib\Arr::field(Model_Device::find('all', $options), 'id');            
        } elseif (!empty($nail) && $nail->get('hf_section') == 2) {
            $options['where']['shop_id'] = $param['shop_id'];
            $options['where']['device_type'] = 2;
            $availableDeviceId = Lib\Arr::field(Model_Device::find('all', $options), 'id');   
        }
     
        $date = 0;
        $statusNext = 0;
        for ($i = 0; $i <= $total_day; $i++) {
            if ($statusNext == 1)
                $date = $date + 1;
            else
                $date = $date_from + $i;
            $true_day = checkdate($month, $date, $year);
            if ($true_day == false) {
                $month = $month + 1;
                $statusNext = 1;
                $date = 1;
            }
            if ($month > 12) {
                $year = $year + 1;
                $month = 1;
            }
            for ($j = 0; $j < count($config_hour_calendar) - 1; $j++) {
                $start_hour = $config_hour_calendar[$j];
                $piece_hour = explode(":", $config_hour_calendar[$j + 1]);
                if ($piece_hour[1] == '00') {
                    $piece_hour[0] = intval($piece_hour[0]) - 1;
                    $piece_hour[1] = '59';
                } else {
                    $piece_hour[1] = intval($piece_hour[1]) - 1;
                }
                $end_hour = $piece_hour[0] . ':' . $piece_hour[1];
                $start_time = strtotime("{$year}-{$month}-{$date} {$start_hour}:00");
                $end_time = strtotime("{$year}-{$month}-{$date} {$end_hour}:59");
                if ($start_time <= time() + 10*60) { // must order before 10'                  
                    $arr_hour[$start_hour] = 1;
                    continue;
                }
                
                $cnt = 0;
                $cntDevice = 0;               
                $disable = 0;
                
                // check order date
                foreach ($data as $value) {
                    if (   ($start_time <= $value['order_start_date'] && $value['order_start_date'] < $end_time)
                        || ($start_time < $value['order_end_date'] && $value['order_end_date'] < $end_time)
                        || ($start_time > $value['order_start_date'] && $start_time < $value['order_end_date'])
                    ) {
                        // has choose nailist
                        if (!empty($param['nailist_id'])
                            && !empty($orderNalist[$value['id']])
                            && in_array($param['nailist_id'], $orderNalist[$value['id']])) {
                            $disable = 1;
                        }
                        if (!empty($value['device_id']) && is_array($availableDeviceId)) {
                            $deviceIds = explode(',', $value['device_id']);
                            foreach ($deviceIds as $deviceId) {
                                if (in_array($deviceId, $availableDeviceId)) {
                                    $cntDevice++;
                                }
                            }                 
                        }
                        $cnt++;
                    }
                }
                
                // check timely limit and available device
                if ($disable == 0) {
                    $limit = isset($order_timely_limits[$start_time]['limit'])
                        ? $order_timely_limits[$start_time]['limit']
                        : $shop->get('max_seat');
                    
                    // check available seat
                    if ((empty($limit) || $cnt >= $limit)
                        && !(!empty($param['service']) && $param['service'] == 4) // not use seat, only use device
                    ) {                       
                        $disable = 1;
                    }
                    
                    // check available device
                    if (is_array($availableDeviceId) && $cntDevice >= count($availableDeviceId)) {
                        $disable = 1;
                    }
                }         
                $arr_hour[$start_hour] = $disable;              
                if ($arr_hour[$start_hour] == 0 && $j >= count($config_hour_calendar) - $nextCheck) {                  
                    $arr_hour[$start_hour] = 2;
                }
            }
            $date = str_pad($date, 2, 0, STR_PAD_LEFT) ;
            $month = str_pad($month, 2, 0, STR_PAD_LEFT) ;
            $arr_calendar["{$year}-{$month}-{$date}"] = $arr_hour;
        }

        // hand & food
        foreach ($arr_calendar as $date => $arr_hour) {
            foreach ($arr_hour as $hour => $disable) {
                foreach ($data as $key => $value) {
                    if ($user_id == $value['user_id']
                        && !empty($value['user_id'])
                        && $value['order_start_date'] <= strtotime($date . ' ' . $hour)
                        && strtotime($date . ' ' . $hour) <= $value['order_end_date']) {
                        $arr_calendar[$date][$hour] = $value['hf_section'] == 0 ? 1 : ($value['hf_section'] == 1 ? 3 : 4); // 1:X; 3:H; 4:F
                    }
                }
            }
        }
        // disable hour have been conflicted               
        foreach ($arr_calendar as $date => $arr_hour) {
            $arr_hour_checked = array();           
            foreach ($arr_hour as $hour => $disable) {   
                $arr_hour_checked[] = $hour;                              
                if ($disable != 0) {                     
                    // find prev items of disabled item and set value = 2 (warning icon △)
                    $f = count($arr_hour_checked) - 1;
                    if ($shop->get('reservation_interval') == 15) {                        
                        if ($f >= count($arr_hour) - $nextCheck && $disable != 1) {
                            $t = $f - 1;
                        } else {
                            $t = $f - $nextCheck;
                        }
                    } else { // = 30
                        if ($f >= count($arr_hour) - $nextCheck + 1 && $disable != 1) {                       
                            $t = $f;
                        } else {
                            $t = $f - $nextCheck + 1;
                        }
                    }
                    for ($i = $f; $i >= 0 && $i >= $t; $i--) {
                        if (isset($arr_calendar[$date][$arr_hour_checked[$i]]) 
                            && $arr_calendar[$date][$arr_hour_checked[$i]] == 0) {                           
                            $arr_calendar[$date][$arr_hour_checked[$i]] = 2;
                        }
                    }
                    
//                    for ($i = count($arr_hour_checked) - 1; $i >= 0 && $i >= count($arr_hour_checked) - $nextCheck; $i--) {
//                        if (isset($arr_calendar[$date][$arr_hour_checked[$i]]) 
//                            && $arr_calendar[$date][$arr_hour_checked[$i]] == 0) {                           
//                            $arr_calendar[$date][$arr_hour_checked[$i]] = 2;
//                        }
//                    }
                }              
            }
        }
        return $arr_calendar;
    }

    /**
     * Get detail Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Order or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('order_id');
            return false;
        }
        if (!empty($param['get_nail'])) {
            $data->nails =  \Model_Order_Nail::get_all(array(
                'order_id' => $param['id'],
            ));
        }
        if (!empty($param['get_nailist'])) {
            $data->nailist  =  \Model_Order_Log::get_all(array(
                'order_id' => $param['id'],
            ));
        }
        if (!empty($param['get_shop'])) {
            $data->shop =  \Model_Shop::get_detail(array(
                'id' => $data->shop_id,
            ));
        }
        if (!empty($param['get_service'])) {
            $data->services = \Model_Order_Service::get_all(array(
                'order_id' => $param['id']
            ));
            /*    
            Add order_services       
            service mapping
            1. ハンドジェル = 1 or 2 (has_off: 1, no off: 2)
            2. フットジェル = 3 or 4 (has_off: 3, no off: 4)
            3. ハンド＆フットジェル = 1,3 or 2,4 (has_off: 1,3 no off: 2,4)
            4. ハンドオフのみ = 6
            5. フットオフのみ = 7
            6. ハンドお直し= 8
            7. フットお直し= 9
            */
            $data->change_service_by_admin = 0;
            if (!empty($data->get('service'))) { 
                $has_off_maping = array(
                    1 => array(1),
                    2 => array(3),
                    3 => array(1,3),                  
                    4 => array(6),                   
                    5 => array(7),                   
                    6 => array(8),                   
                    7 => array(9),                    
                );
                $no_off_maping = array(
                    1 => array(2),
                    2 => array(4),
                    3 => array(2,4),                   
                    4 => array(6),                   
                    5 => array(7),                   
                    6 => array(8),                   
                    7 => array(9),                   
                );
                if ($data->get('off_nails') > 1) {
                    $current_order_service = $has_off_maping[$data->get('service')];
                } else {
                    $current_order_service = $no_off_maping[$data->get('service')];
                }    
                $diff = array_diff(Lib\Arr::field($data->services, 'service_id'), $current_order_service);                
                $data->change_service_by_admin = !empty($diff) ? 1 : 0;
            }        
        }
        if (!empty($param['get_order_history']) && !empty($data['user_id'])) {
            $order_history = self::get_all_by_user_id(array(
                'user_id' => $data['user_id']
            ));
            $data->order_history = \Lib\Arr::field($order_history, 'id');
        }

        //Get other items
        if (!empty($param['get_item'])) {
            //Get top 10 items
            $query = DB::select(
                'items.id', 'items.name',
                DB::expr('IF(order_items.id is NULL, 0, 1) as checked')
            )
                ->from('items')
                ->join('order_items', 'LEFT')
                ->on('items.id', '=', 'order_items.item_id')
                ->and_on('order_items.order_id', '=',  DB::expr("{$param['id']}"))
                ->and_on('order_items.disable', '=',  DB::expr("0"))
                ->where('items.disable', '=',  DB::expr("0"))
                ->order_by('items.id', 'ASC')
                ->limit(10)->offset(0);
            $data->fingers = $query->execute()->as_array();

            $query = DB::select()
                ->from('order_items')
                ->where('order_id', '=', $param['id'])
                ->where('disable', '=', '0');
            // get data
            $data->items = $query->execute()->as_array();
        }

        if (!empty($param['get_latest_order']) && !empty($data['user_id'])) {         
            $query = DB::select('orders.id', 'orders.reservation_date')
                ->from('orders')
                ->where('orders.disable', '=',  '0')
                ->where('orders.user_id', '=',  $data['user_id'])
                //->where('orders.shop_id', '=',  $data['shop_id'])
                ->where('orders.id', '<',  $param['id'])
                ->order_by('orders.id', 'DESC')
                ->limit(1)
                ->offset(0);
            $data->latest_order = $query->execute()->offsetGet(0);            
        }
        
        // convert point to price
        $data['r_point'] = $data['r_point'] ? $data['r_point'] : "0";
        $data['fn_point'] = $data['fn_point'] ? $data['fn_point'] : "0";
        $data['r_point_convert'] = $data['r_point'] * Config::get('change_rate', 1);
        $data['fn_point_convert'] = $data['fn_point'] * Config::get('change_rate', 1);
        return $data;
    }

    /**
     * Get detail Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Order or false if error
     */
    public static function get_detail_for_view($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('order_id');
            return false;
        }
        if (!empty($param['get_nail'])) {
            $data->nails =  \Model_Order_Nail::get_all(array(
                'order_id' => $param['id'],
            ));
        }

        //Get other items
        if (!empty($param['get_item'])) {
            //Get top 10 items
            $query = DB::select(
                'items.id', 'items.name',
                DB::expr('IF(order_items.id is NULL, 0, 1) as checked')
            )
                ->from('items')
                ->join('order_items', 'LEFT')
                ->on('items.id', '=', 'order_items.item_id')
                ->and_on('order_items.order_id', '=',  DB::expr("{$param['id']}"))
                ->and_on('order_items.disable', '=',  DB::expr("0"))
                ->where('items.disable', '=',  DB::expr("0"))
                ->order_by('items.id', 'ASC')
                ->limit(10)->offset(0);
            $data->fingers = $query->execute()->as_array();

            $query = DB::select()
                ->from('order_items')
                ->where('order_id', '=', $param['id'])
                ->where('disable', '=', '0')
                ->where('item_id', '>', 10);
            // get data
            $data->items = $query->execute()->as_array();
        }

        return $data;
    }

    /**
     * Get Revenue Report (using array count)
     *
     * @author VuLTH
     * @param array $param Input data
     * @return array Revenue Report
     */
    public static function get_revenue($param)
    {
        $query = DB::select(
            self::$_table_name . '.shop_id',
            'order_logs.nailist_id',
            DB::expr('SUM(orders.total_price) + SUM(orders.total_tax_price) AS total')
        )
            ->from(self::$_table_name)
            ->join('order_logs', 'INNER')
            ->on(self::$_table_name . '.id', '=', 'order_logs.order_id');
        //->group_by(self::$_table_name . '.shop_id','order_logs.nailist_id');
        // filter by keyword
        if (!empty($param['shop_id']) && $param['shop_id'] != '') {
            $query->where('shop_id', '=', $param['shop_id']);
        }
        if (!empty($param['nailist_id']) && $param['nailist_id'] != '') {
            $query->where('order_logs.nailist_id', '=', $param['nailist_id']);
        }
        if (!empty($param['date_from']) && $param['date_from'] != '') {
            $query->where(self::$_table_name . '.reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to']) && $param['date_to'] != '') {
            $query->where(self::$_table_name . '.reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort']) && $param['sort'] != '') {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        //echo DB::last_query();
        if (empty($param['limit']) || intval($param['limit'])==0) {
            return array($data);
        } else {
            $total = !empty($data) ? DB::count_last_query() : 0;
            return array($total, $data);

        }
    }
    /**
     * Get General Report (using array count)
     *
     * @author VuLTH
     * @param array $param Input data
     * @return array General Report
     */
    public static function get_general($param)
    {
        $query = DB::select(
            DB::expr('DATE(FROM_UNIXTIME(reservation_date)) AS day, SUM(total_price) + SUM(total_tax_price) as sum')
        )
            ->from(self::$_table_name)
            ->order_by('day', 'ASC')
            ->group_by('day');
        $int_last_day_prev_month = strtotime("last day of previous month");
        $int_now = strtotime("tomorrow")-1;
        $query->where(self::$_table_name . '.reservation_date', 'between', array($int_last_day_prev_month,$int_now));
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        $data = $query->execute()->as_array();
        //echo DB::last_query();
        $last_day_prev_month = date("Y-m-d", $int_last_day_prev_month);
        $yesterday = date('Y-m-d',strtotime("-1 day"));
        $today =  date('Y-m-d',$int_now);
        $day_of_week = date('w', $int_now);
        $arr_day_of_week = array();
        for ($i=0 ;$i<=$day_of_week;$i++) {
            $arr_day_of_week[]= date('Y-m-d',strtotime("-{$i} day"));
        }
        $revenue_yesterday= 0;
        $revenue_today= 0;
        $revenue_this_week= 0;
        $revenue_this_month= 0;
        foreach ($data as $key=>$value) {
            if ($value['day']== $yesterday) {
                $revenue_yesterday = $value['sum'];
            }
            if ($value['day']== $today) {
                $revenue_today = $value['sum'];
            }
            if (in_array($value['day'],$arr_day_of_week)) {
                $revenue_this_week += $value['sum'];
            }
            if ($value['day']!= $last_day_prev_month) {
                $revenue_this_month += $value['sum'];
            }
        }
        return array(
            'revenue_yesterday' => $revenue_yesterday,
            'revenue_today' => $revenue_today,
            'revenue_this_week' => $revenue_this_week,
            'revenue_this_month' => $revenue_this_month,
            'visit_today' => 0,
            'visit_this_week' => 0,
            'visit_this_month' => 0,
            'lt_this_week' => 0,
            'lt_this_month' => 0,
        );

    }

    /**
     * Order confirm
     *
     * @author VuLTH
     * @param array $param Input data
     * @return array Confirm
     */
    public static function confirm($param)
    {
        // Shop
        $query = DB::select(
            array('shops.id', 'id'),
            array('shops.name', 'name')
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->order_by('shops.name','ASC')
            ->where('shops.disable', '=', 0)
            ->where(self::$_table_name.'.disable', '=', 0);
        if (!empty($param['cart_id']) && $param['cart_id'] != '') {
            $query->where('cart_id', '=', $param['cart_id']);
        }

        // Nailists
        $shops = $query->execute()->as_array();
        $nailists = array();
        if (count($shops) >0  ) {
            $in_shop = array();
            foreach ($shops as $key => $shop) {
                $in_shop[] = $shop['id'];
            }
            //$in_shop=array(1,2);
            $query = DB::select(
                'nailists.*',
                DB::expr('IF(ISNULL(cart_logs.cart_id),0,1) AS checked')
            )
                ->from('nailists')
                ->join('cart_logs', 'LEFT')
                ->on( 'nailists.id', '=', 'cart_logs.nailist_id')
                ->where('nailists.shop_id', 'IN', $in_shop)
                ->where('cart_logs.disable', '=', 0)
                ->where('nailists.disable', '=', 0);
            $nailists = $query->execute()->as_array();
        }
        //nails
        $query = DB::select(
            'nails.*'
        )
            ->from('nails')
            ->join('cart_nails', 'LEFT')
            ->on( 'nails.id', '=', 'cart_nails.nail_id')
            ->where('cart_nails.cart_id', '=', $param['cart_id'])
            ->where('cart_nails.disable', '=', 0)
            ->where('nails.disable', '=', 0);
        $nails = $query->execute()->as_array();
        //items
        $query = DB::select(
            'items.*'
        )
            ->from('items')
            ->join('cart_items', 'LEFT')
            ->on( 'items.id', '=', 'cart_items.item_id')
            ->where('cart_items.cart_id', '=', $param['cart_id'])
            ->where('cart_items.disable', '=', 0)
            ->where('items.disable', '=', 0);
        $items = $query->execute()->as_array();
        $genres=$stones=$genres=$colors=$designs = array();
        if (count($nails) >0) {
            $in_nail = array();
            foreach ($nails as $key => $nail) {
                $in_nail[] = $nail['id'];
            }
            //design
            $query = DB::select(
                'designs.id',
                'designs.name'
            )

                ->from('designs')
                ->join('nail_designs', 'LEFT')
                ->on( 'designs.id', '=', 'nail_designs.design_id')
                ->where('nail_designs.nail_id', 'IN', $in_nail)
                ->where('designs.disable', '=', 0);
            $designs = $query->execute()->as_array();
            //colors
            $query = DB::select(
                'colors.id',
                'colors.name'
            )
                ->from('colors')
                ->join('nail_colors', 'LEFT')
                ->on( 'colors.id', '=', 'nail_colors.color_id')
                ->where('nail_colors.nail_id', 'IN', $in_nail)
                ->where('colors.disable', '=', 0);
            $colors = $query->execute()->as_array();
            //stones
            $query = DB::select(
                'stones.id',
                'stones.name'
            )
                ->from('stones')
                ->join('nail_stones', 'LEFT')
                ->on( 'stones.id', '=', 'nail_stones.stone_id')
                ->where('nail_stones.nail_id', 'IN', $in_nail)
                ->where('stones.disable', '=', 0);
            $stones = $query->execute()->as_array();
            //genres
            $query = DB::select(
                'genres.id',
                'genres.name'
            )
                ->from('genres')
                ->join('nail_genres', 'LEFT')
                ->on( 'genres.id', '=', 'nail_genres.genre_id')
                ->where('nail_genres.nail_id', 'IN', $in_nail)
                ->where('genres.disable', '=', 0);
            $genres = $query->execute()->as_array();
            //scenes
            $query = DB::select(
                'scenes.id',
                'scenes.name'
            )
                ->from('scenes')
                ->join('nail_scenes', 'LEFT')
                ->on( 'scenes.id', '=', 'nail_scenes.scene_id')
                ->where('nail_scenes.nail_id', 'IN', $in_nail)
                ->where('scenes.disable', '=', 0);
            $scenes = $query->execute()->as_array();
        }
        //echo DB::last_query();
        //user
        $user = array();
        if (!empty($param['user_id']) && $param['user_id'] != '') {
            $query = DB::select(
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.sex',
                'users.phone',
                'users.email'
            )
                ->from('users')
                ->where('users.id', '=', $param['user_id'])
                ->where('users.disable', '=', 0);
            $user = $query->execute()->as_array();
        }
        return array(
            'shop' => $shops,
            'nailists' => $nailists,
            'nails' => $nails,
            'items' => $items,
            'designs' => $designs,
            'colors' => $colors,
            'stones' => $stones,
            'genres' => $genres,
            'scenes' => $scenes,
            'user' => $user,
        );



    }

    /**
     * Get list booking of month
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order
     */
    public static function get_month_booking($param)
    {
        $param['date'] = strtotime($param['date']);
        $calendar = \Lib\Calendar::get_calendar($param['date']);
        $param['date_from'] = $calendar['firstDayOfCalendar'];
        $param['date_to'] = $calendar['lastDayOfCalendar'];
        $query = DB::select(
            DB::expr("
                FROM_UNIXTIME(orders.reservation_date) AS date
            "),
            self::$_table_name . '.id',
            self::$_table_name . '.reservation_date',
            self::$_table_name . '.total_price',
            self::$_table_name . '.total_tax_price',
            self::$_table_name . '.user_id',
            self::$_table_name . '.user_name',
            self::$_table_name . '.sex',
            self::$_table_name . '.birthday',
            self::$_table_name . '.email',
            self::$_table_name . '.phone',
            array('shops.name', 'shop_name'),
            array('shops.phone', 'shop_phone'),
            array('shops.address', 'shop_address')
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->where(self::$_table_name . '.reservation_date', '>=', self::date_from_val($param['date_from']))
            ->where(self::$_table_name . '.reservation_date', '<=', self::date_to_val($param['date_to']))
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['shop_id'])) {
            $query->where(self::$_table_name . '.shop_id', '=', $param['shop_id']);
        }
        $query->order_by(self::$_table_name . '.reservation_date', 'ASC');
        $booking = $query->execute()->as_array();
        // distributing by day
        $firstDayOfLastMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) - 1, 1, date('Y')));
        $firstDayOfNextMonth = date('Y-m-d', mktime(1, 1, 1, date('n', $param['date']) + 1, 1, date('Y')));
        $data = array(
            'calendarInfo' => array(
                'thisMonth'           => date('n', $param['date']),
                'thisYear'            => date('Y', $param['date']),
                'firstDayOfLastMonth' => $firstDayOfLastMonth,
                'lastMonth'           => date('n', strtotime($firstDayOfLastMonth)),
                'yearOfLastMonth'     => date('Y', strtotime($firstDayOfLastMonth)),
                'firstDayOfNextMonth' => $firstDayOfNextMonth,
                'nextMonth'           => date('n', strtotime($firstDayOfNextMonth)),
                'yearOfNextMonth'     => date('Y', strtotime($firstDayOfNextMonth))
            )
        );
        for ($i = 0; $i < $calendar['daysInCalendar']; $i++) {
            $dayInCalendar = strtotime('+' . $i . 'days', strtotime($calendar['firstDayOfCalendar']));
            $temp = array(
                'dateInfo' => array(
                    'date'        => date('Y-m-d', $dayInCalendar),
                    'day'         => date('j', $dayInCalendar),
                    'isSunday'    => date('w', $dayInCalendar) == '0' ? true : false,
                    'isThisMonth' => date('Y-m', $dayInCalendar) == date('Y-m', $param['date']),
                    'isToday'     => $dayInCalendar == strtotime(date('Y-m-d', time()))
                )
            );
            foreach ($booking as $item) {
                $dayInOrder = strtotime(date('Y-m-d', $item['reservation_date']));
                if ($dayInCalendar == $dayInOrder) {
                    $temp['dateData'][] = $item;
                }
            }
            $data['calendarData'][] = $temp;
        }
        return $data;
    }

    /**
     * Get booking calendar for staff
     *
     * @author Hoang Gia Thong
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function date_calendar($param)
    {
        $shop = Model_Shop::find($param['shop_id']);
        if (empty($shop) || empty($param['shop_id'])) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (empty($param['date'])) {
            $param['date'] = date('Y-m-d');
        } else {
            $param['date'] = date('Y-m-d', strtotime($param['date']));
        }
        if (!isset($param['is_seat'])) {
            $subQuery = "
                SELECT  orders.id,
                        orders.shop_id,
                        orders.user_name,
                        orders.kana,
                        GROUP_CONCAT(CONCAT('n_',order_logs.nailist_id) SEPARATOR  ',') as stylist_seat_keys,
                        orders.reservation_date,
                        orders.start_date,
                        orders.order_start_date,
                        orders.order_end_date,
                        orders.is_paid,
                        orders.is_cancel,
                        orders.is_autocancel,
                        orders.seat_id,
                        orders.visit_section,
                        orders.service,
                        IFNULL(orders.hp_order_code,0) as hp_order_code
                FROM orders
                    LEFT JOIN order_logs ON (order_logs.order_id = orders.id)
                WHERE order_logs.disable = 0
                    AND orders.disable = 0
                    AND IFNULL(orders.is_cancel,0) = 0
                    AND orders.shop_id = {$param['shop_id']}
                    AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
                GROUP BY orders.id
            ";
        } else {
            $subQuery = "
                SELECT  orders.id,
                        orders.shop_id,
                        orders.user_name,
                        orders.kana,
                        CONCAT('s_', seat_id) as stylist_seat_keys,
                        orders.reservation_date,
                        orders.start_date,
                        orders.order_start_date,
                        orders.order_end_date,
                        orders.is_paid,
                        orders.is_cancel,
                        orders.is_autocancel,
                        orders.seat_id,
                        orders.visit_section,
                        orders.service,
                        IFNULL(orders.hp_order_code,0) as hp_order_code
                FROM orders
                WHERE orders.disable = 0
                    AND IFNULL(orders.is_cancel,0) = 0
                    AND orders.shop_id = {$param['shop_id']}
                    AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
            ";
        }
        $query = DB::select(DB::expr("
                A.id,
                shop_id,
                user_name,
                kana,
                GROUP_CONCAT(stylist_seat_keys SEPARATOR  ',') as stylist_seat_keys,
        		reservation_date,
                start_date,
                order_start_date,
                order_end_date,
        		IFNULL(is_paid,0) AS is_paid,
                IFNULL(is_cancel,0) AS is_cancel,
                IFNULL(is_autocancel,0) AS is_autocancel,
                seat_id,
        		visit_section,
        		service,
        		hp_order_code,
                shops.max_seat,
                shops.hp_max_seat
            "),
            DB::expr("CASE WHEN seat_id > (shops.max_seat + shops.hp_max_seat) THEN 'extra-seat' ELSE ''  END AS status "),
        	DB::expr("IF(EXISTS(SELECT order_nails.id FROM order_nails WHERE order_nails.order_id = A.id), 1,0) AS has_nail "))
            ->from(DB::expr("(
                (
                    {$subQuery}
                )
                UNION
                (
                    SELECT  orders.id,
                            orders.shop_id,
                            orders.user_name,
                            orders.kana,
                            GROUP_CONCAT(CONCAT('d_',order_devices.device_id) SEPARATOR  ',') as stylist_seat_keys,
            				orders.reservation_date,
                            orders.start_date,
                            orders.order_start_date,
                            orders.order_end_date,
            				orders.is_paid,
                            orders.is_cancel,
                            orders.is_autocancel,
                            orders.seat_id,
                            orders.visit_section,
                            orders.service,
                            IFNULL(orders.hp_order_code,0) as hp_order_code
                    FROM orders
                        LEFT JOIN order_devices ON (order_devices.order_id = orders.id)
                    WHERE order_devices.disable = 0
                        AND orders.disable = 0
                        AND IFNULL(orders.is_cancel,0) = 0
                        AND orders.shop_id = {$param['shop_id']}
                        AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
                    GROUP BY orders.id
                )) AS A
            "
        ))
        ->join('shops','LEFT')
        ->on('shop_id','=','shops.id')
        ->where('shop_id', '=', $param['shop_id'])
        ->where(DB::expr("DATE(FROM_UNIXTIME(reservation_date)) = " . "'" . $param['date'] . "'"));
        if (!empty($param['order_id'])) {
        	$query->where('A.id', '=', $param['order_id']);
        }
        if (isset($param['is_seat'])) {
            $query->where(DB::expr("(
                    IFNULL(seat_id,0) > 0
                    OR IFNULL(stylist_seat_keys,'') <> ''
                )")
            );
        } else {
            $query->where(DB::expr("IFNULL(stylist_seat_keys,'') <> ''"));
        }
        $query->group_by('id');
        $data = $query->execute()->as_array();
        self::check_many_orders_is_blocked_seat($data, $param['shop_id'], $param['date']);
        $arr_calendar = array();
        if (!empty($data)) {
            $i = 0;
            $curtime = time();
            foreach ($data as $value) {
                if (isset($value['id'])){
                    $arr_calendar[$i]['id'] = $value['id'];
                }
                if (isset($value['id'])){
                    $arr_calendar[$i]['serial_num'] = $value['id'];
                }
                if (isset($value['order_start_date'])){
                	$arr_calendar[$i]['start_at'] = $value['order_start_date'];
                    $arr_calendar[$i]['start_at_epoch'] = intval($value['order_start_date']);
                }
                if(!empty($value['is_blocked_seat'])){
                	$arr_calendar[$i]['is_blocked'] = 1;
                }else{
                	$arr_calendar[$i]['is_blocked'] = 0;
                }
                if(!empty($value['hp_order_code'])){
                	$arr_calendar[$i]['is_hp'] = 1;
                }else{
                	$arr_calendar[$i]['is_hp'] = 0;
                }
                if (!empty($value['is_paid'])) {
                    $arr_calendar[$i]['status'] = 'is_paid';
                } elseif (!empty($value['start_date'])) {
                    $arr_calendar[$i]['status'] = 'start_date';
                } elseif (!empty($value['is_autocancel'])) { //$value['order_end_date'] < $curtime
                    $arr_calendar[$i]['status'] = 'not_come';
                } else {
                    $arr_calendar[$i]['status'] = !empty($value['status'])? $value['status']:'accepted';
                }
                if (isset($value['order_start_date']) && isset($value['order_end_date'])){
                    $arr_calendar[$i]['duration'] = $value['order_end_date'] - $value['order_start_date'];
                }
                if (!empty($value['user_name'])) {
//                     if (\Lib\Str::is_japanese($value['user_name'])) {
//                         $arr_calendar[$i]['customer_name'] = mb_substr($value['user_name'], 0, 2, "UTF-8");
//                     } else {
//                         $arr_calendar[$i]['customer_name'] = \Lib\Str::truncate($value['user_name'], 10);
//                     }
                	$arr_calendar[$i]['customer_name'] = $value['user_name'];
                }
                if (!empty($value['kana'])) {
                    $arr_calendar[$i]['customer_kana'] = $value['kana'];
                }
                if(isset($value['visit_section'])){
                	$arr_calendar[$i]['visit_section'] = !empty($value['visit_section']) ? 1: 0;
                }
                if(!empty($value['id'])){
                    $services = \Model_Order_Service::get_all(array('order_id' => $value['id']));
                    $arr_calendar[$i]['service_name']  = \Lib\Arr::field($services, 'name', true);
                }
                if(isset($value['has_nail'])){
                	$arr_calendar[$i]['has_nail'] = $value['has_nail'];
                }
                if (isset($value['stylist_seat_keys'])) {
                    $stylist_seat_keys = explode(',', $value['stylist_seat_keys']);
                    $arr = array();
                    foreach ($stylist_seat_keys as $value) {
                        $arr[] = $value ;
                    }
                    $arr_calendar[$i]['stylist_seat_keys'] = $arr;
                }
                $arr_calendar[$i]['points'] = '';
                $arr_calendar[$i]['customer_slug'] = '';
                $arr_calendar[$i]['orders'] = array();
                $arr_calendar[$i]['has_memo'] = false;
                $arr_calendar[$i]['has_requested_stylist'] = false;
                $arr_calendar[$i]['is_online'] = false;
                $arr_calendar[$i]['is_online_or_provider'] = false;
                $arr_calendar[$i]['error'] = false;
                $i++;
            }
        }
        return  json_encode($arr_calendar);
    }

    /**
     * Get all by date
     *
     * @author Tran Xuan Khoa
     * @param array array $param Input data.
     * @return array List of orders
     */
    public static function get_all_by_date($param)
    {
        $query = DB::select(
                self::$_table_name . '.*',
                array('admins.name', 'admin_name'),
                array(self::$_table_name . '.user_name', 'user_name'),
                array('nailists', 'nailists'),
                DB::expr("IF(IFNULL(user_id,0)>0,users.name,admins.name) AS author_name")
            )
            ->from(self::$_table_name)
            ->join('admins', 'LEFT')
            ->on(self::$_table_name . '.admin_id', '=', 'admins.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join(DB::expr("(
                        SELECT  order_logs.order_id,
                                GROUP_CONCAT(nailists.name SEPARATOR  ', ') AS nailists
                        FROM (  SELECT DISTINCT order_id, nailist_id
                                FROM order_logs
                                WHERE order_logs.disable = 0
                                ORDER BY id DESC
                            ) order_logs
                        JOIN nailists ON order_logs.nailist_id = nailists.id
                        WHERE nailists.disable = 0
                        GROUP BY order_logs.order_id
                    ) AS order_nailists"), 'LEFT')
                ->on('order_nailists.order_id', '=', self::$_table_name . ".id")
            ->where(self::$_table_name . '.shop_id', $param['shop_id'])
            ->where(self::$_table_name . '.disable', "0");

        if (!empty($param['date']))
        {
            $param['date'] = date('Y-m-d', strtotime($param['date']));
            $query->where(DB::expr("DATE(FROM_UNIXTIME(orders.reservation_date)) = "."'".$param['date']."'"));
        }
        if (!empty($param['user_name'])) {
            $query->where('user_name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['nailist_id']))
        {
            if ($param['nailist_id'] == -1) {
                $query->where( DB::expr("NOT EXISTS (SELECT order_id FROM order_logs WHERE order_id = orders.id AND disable = 0)"));
            } else {
                $query->where( DB::expr("EXISTS (SELECT order_id FROM order_logs WHERE order_id = orders.id AND nailist_id = {$param['nailist_id']}  AND disable = 0)"));
            }
        }
        if (!empty($param['keyword'])) {
            $query->where(DB::expr("(
                    orders.user_name LIKE '%{$param['keyword']}%' OR
                    orders.kana LIKE '%{$param['keyword']}%' OR
                    orders.phone LIKE '%{$param['keyword']}%' OR
                    orders.email LIKE '%{$param['keyword']}%'
                )"
            ));
        }
        $isSortStatus = false;
        $statusSort = '';
        if (!empty($param['sort']) && $param['sort'] != '') {
            $sortExplode = explode('-', $param['sort']);
            if($sortExplode[0] != 'status') {
                $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
            }else {
                $isSortStatus = true;
                $statusSort = $sortExplode[1];
            }
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $dataOrder = $query->execute()->as_array();
        if(!empty($dataOrder)){
            foreach ($dataOrder as &$item) {
                $status = 2 ;
                /*
                        status                    màu icon   
                ①   đang ở tiệm 来店中            yellow +
                ②   sắp tới tiệm 来店前             blue +
                ③   đã thanh toán 会計終了          grey +
                ④   auto cancel 無断キャンセル       red +
                ⑤   cancel キャンセル               white + 
                */
                if (!empty($item['is_paid'])) {
                    $status = 3;//'is_paid';//đã thanh toán 会計終了          grey
                } elseif (!empty($item['start_date'])) {
                    $status = 1;//'start_date';//đang ở tiệm 来店中            yellow
                } elseif (!empty($item['is_autocancel'])) { 
                    $status = 4;//'not_come_auto'; //auto cancel 無断キャンセル       red
                } elseif (!empty($item['is_cancel'])) { 
                    $status = 5;//'not_come'; //cancel キャンセル               white 
                }else {
                    $status = 2;// !empty($item['status'])? $item['status']:'accepted'; //sắp tới tiệm 来店前             blue
                }
                $item['status']=  $status;
            }
            if($isSortStatus) {
                $statusSort = ($statusSort == 'ASC') ? SORT_ASC : SORT_DESC;
                $dataOrder = \Lib\Arr::array_sort($dataOrder,'status',$statusSort);
            }
        }
        return $dataOrder;
    }

    /**
     * Get detail order for calendar
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Order or false if error
     */
    public static function get_detail_for_calendar($param)
    {
        $query = DB::select(
            DB::expr("
                FROM_UNIXTIME(orders.reservation_date) AS date
            "),
            self::$_table_name . '.*',
            array('shops.name', 'shop_name'),
            array('shops.phone', 'shop_phone'),
            array('shops.address', 'shop_address'),
            self::$_table_name . '.created'
        )
            ->from(self::$_table_name)
            ->join('shops', 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(self::$_table_name . '.id', '=', $param['order_id']);
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('order_id');
            return false;
        } else {
            $param['order_start_date'] = $data['order_start_date'];
            $param['order_end_date'] = $data['order_end_date'];
            $param['shop_id'] = $data['shop_id'];
            if(empty($param['is_seat'])) {
                $data['nailists'] = \Model_Order_Log::get_all($param);
            }else{
                $data['nailists'] = array();
            }
            $data['devices'] = \Model_Order_Device::get_all($param);
            $data['services'] = \Model_Order_Service::get_all($param);
        }
        return $data;
    }

    /**
     * Update info for Timebar
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Timebar id or false if error
     */
    /**
     * Update info for Timebar
     *
     * @author Hoang Gia Thong
     * @param array $param Input data
     * @return int|bool Timebar id or false if error
     */
    public static function update_timebar($param)
    {
        $id = !empty($param['order_id']) ? $param['order_id'] : '';
        $order = new self;
        $is_hp = false;
        $allSeats = array();
        // check exist
        if (!empty($id)) {
            $order = self::find($id);
            $param['date'] = date('Y-m-d',$order->get('reservation_date'));
            $is_hp = !empty($order->get('hp_order_code')) ? true : false;
            if (empty($order)) {
                self::errorNotExist('order_id');
                return false;
            }
            $order->set('last_update_admin_id', !empty($param['last_update_admin_id']) ? $param['last_update_admin_id'] : 0);
        }else{
            return false;
        }
        // Process for each case
        if(isset($param['start_at_epoch']) && isset($param['duration'])){
            // prepare info for checking duplicate
            $nailist_ids = Model_Order_Log::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $device_ids = Model_Order_Device::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $param['nailist_id'] = Lib\Arr::field($nailist_ids, 'nailist_id', true);
            $param['devices_id'] = Lib\Arr::field($device_ids, 'device_id', true);
            $param['order_start_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            $param['order_end_date'] = date("Y-m-d H:i",$param['start_at_epoch'] + $param['duration']);
            $param['reservation_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            $param['seat_id'] = !empty($order->seat_id) ? $order->seat_id : 0;

            // check duration = 0
            if($param['duration'] == 0){
                return self::returnErrorData($param, $id, 2);
            }

            // check dupplicate
            if($is_hp == false && self::check_duplicate_order_date_seatlist($param, $id)){
                return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_nailist($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_device($param, $id)){
            	return self::returnErrorData($param, $id);
            }

            // update data
            $order->set('reservation_date', $param['start_at_epoch']);
            $order->set('order_start_date', $param['start_at_epoch']);
            $order->set('order_end_date', $param['start_at_epoch']+ $param['duration']);
        }else if(isset($param['start_at_epoch']) && isset($param['stylist_ids']) && !empty($param['start_at_epoch']) && !empty($param['stylist_ids'])){
        	// prepare info for checking duplicate
            $st_date = $order->order_start_date;
            $en_date = $order->order_end_date;
            $duration = $en_date - $st_date;
            list($nailist_ids, $device_ids) = self::createListIds($param);
            $param['nailist_id'] = implode(',', $nailist_ids);
            $param['devices_id'] = implode(',', $device_ids);
            $param['order_start_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            $param['order_end_date'] = date("Y-m-d H:i",$param['start_at_epoch'] + $duration);
            $param['reservation_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            if(isset($param['is_seat']) && (!empty($param['nailist_id']) && strpos($param['nailist_id'], ',') === false)){
            	$param['seat_id'] = $param['nailist_id'];
            }else{
            	$param['seat_id'] = 0;
            }
            
            // check dupplicate
            if($is_hp == false && self::check_duplicate_order_date_seatlist($param, $id)){
                return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_nailist($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_device($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            
            //prepare info for checking time range
            $shop_open_time = !empty($param['shop_open_time']) ? $param['shop_open_time'] : 0;
            $shop_close_time = !empty($param['shop_close_time']) ? $param['shop_close_time'] : 0;
            $minTimeOrder = strtotime(date("Y-m-d",$param['start_at_epoch'])) + $shop_open_time;
            $maxTimeOrder = strtotime(date("Y-m-d",$param['start_at_epoch'])) + $shop_close_time;
            $new_order_start_date = $param['start_at_epoch'];
            $new_order_end_date = $param['start_at_epoch'] + $duration;
            //check out of time range
            if(($new_order_start_date < $minTimeOrder) || ( $maxTimeOrder < $new_order_end_date)){
            	return self::returnErrorData($param, $id, 1);
            }

            // update data
            self::saveList($order->id, $param);
            $order->set('reservation_date', $param['start_at_epoch']);
            $order->set('order_start_date', $new_order_start_date);
            $order->set('order_end_date', $new_order_end_date);
            if(isset($param['is_seat']) && (!empty($param['nailist_id']) && strpos($param['nailist_id'], ',') === false)){
                $order->set('seat_id', $param['nailist_id'] );
            }

        }
        else if (isset($param['duration']) && !empty($param['duration'])){
        	// prepare info for checking duplicate
            $st_date = $order->order_start_date;
            $nailist_ids = Model_Order_Log::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $device_ids = Model_Order_Device::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $param['nailist_id'] = Lib\Arr::field($nailist_ids, 'nailist_id', true);
            $param['devices_id'] = Lib\Arr::field($device_ids, 'device_id', true);
            $param['order_start_date'] = date("Y-m-d H:i",$st_date);
            $param['order_end_date'] = date("Y-m-d H:i",$st_date + $param['duration']);
            $param['reservation_date'] = date("Y-m-d H:i",$order->reservation_date);
            $param['seat_id'] = !empty($order->seat_id) ? $order->seat_id : 0;
            
            // check dupplicate
            if($is_hp == false && self::check_duplicate_order_date_seatlist($param, $id)){
                return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_nailist($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_device($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            //prepare info for checking time range
            $shop_open_time = !empty($param['shop_open_time']) ? $param['shop_open_time'] : 0;
            $shop_close_time = !empty($param['shop_close_time']) ? $param['shop_close_time'] : 0;
            $minTimeOrder = strtotime(date("Y-m-d",$st_date)) + $shop_open_time;
            $maxTimeOrder = strtotime(date("Y-m-d",$st_date)) + $shop_close_time;
            $new_order_start_date = $st_date;
            $new_order_end_date = $st_date + $param['duration'];
            //check out of time range
            if(($new_order_start_date < $minTimeOrder) || ( $maxTimeOrder < $new_order_end_date)){
                return self::returnErrorData($param, $id, 1);
            }

            // update data
            $order->set('order_end_date',$new_order_end_date);
        }
        else if(isset($param['start_at_epoch']) && !empty($param['start_at_epoch'])){
            // prepare info for checking duplicate
            $st_date = $order->order_start_date;
            $en_date = $order->order_end_date;
            $duration = $en_date - $st_date;
            $nailist_ids = Model_Order_Log::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $device_ids = Model_Order_Device::find('all', array('where' => array('order_id' => $id, 'disable'=> 0)));
            $param['nailist_id'] = Lib\Arr::field($nailist_ids, 'nailist_id', true);
            $param['devices_id'] = Lib\Arr::field($device_ids, 'device_id', true);
            $param['order_start_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            $param['order_end_date'] = date("Y-m-d H:i",$param['start_at_epoch'] + $duration);
            $param['reservation_date'] = date("Y-m-d H:i",$param['start_at_epoch']);
            $param['seat_id'] = !empty($order->seat_id) ? $order->seat_id : 0;
            
            // check dupplicate
            if($is_hp == false && self::check_duplicate_order_date_seatlist($param, $id)){
                return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_nailist($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_device($param, $id)){
            	return self::returnErrorData($param, $id);
            }

            //prepare info for checking time range
            $shop_open_time = !empty($param['shop_open_time']) ? $param['shop_open_time'] : 0;
            $shop_close_time = !empty($param['shop_close_time']) ? $param['shop_close_time'] : 0;
            $minTimeOrder = strtotime(date("Y-m-d",$param['start_at_epoch'])) + $shop_open_time;
            $maxTimeOrder = strtotime(date("Y-m-d",$param['start_at_epoch'])) + $shop_close_time;
            $new_order_start_date = $param['start_at_epoch'];
            $new_order_end_date = $new_order_start_date + $duration;
            //check out of time range
            if(($new_order_start_date < $minTimeOrder) || ( $maxTimeOrder < $new_order_end_date)){
                return self::returnErrorData($param, $id, 1);
            }

            // update data
            $order->set('reservation_date', $param['start_at_epoch']);
            $order->set('order_start_date', $new_order_start_date);
            $order->set('order_end_date', $new_order_end_date);
        }
        else if(isset($param['stylist_ids']) && !empty($param['stylist_ids'])){
            // prepare info for checking duplicate
            list($nailist_ids, $device_ids) = self::createListIds($param);
            $param['nailist_id'] = implode(',', $nailist_ids);
            $param['devices_id'] = implode(',', $device_ids);
            $param['order_start_date'] = date("Y-m-d H:i", $order->order_start_date);
            $param['order_end_date'] = date("Y-m-d H:i", $order->order_end_date);
            $param['reservation_date'] = date("Y-m-d H:i",$order->reservation_date);
            if(isset($param['is_seat']) && (!empty($nailist_ids) && count($nailist_ids) > 0)){
            	$param['seat_id'] = min($nailist_ids);
            }else{
            	$param['seat_id'] = 0;
            }

            // check dupplicate
            if($is_hp == false && self::check_duplicate_order_date_seatlist($param, $id)){
                return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_nailist($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            if(self::check_duplicate_order_date_device($param, $id)){
            	return self::returnErrorData($param, $id);
            }
            
            // update data
            self::saveList($order->id, $param);
            if (isset($param['is_seat']) && (!empty($param['nailist_id']) && strpos($param['nailist_id'], ',') === false)){
                $order->set('seat_id', $param['nailist_id'] );
            }
        }
        // save to database
        if ($order->save()) {
            if (!empty($order->id)) {
                $order->id = self::cached_object($order)->_original['id'];
            }
            $data = self::date_calendar($param);
            if ($data) {
                $data = json_decode($data);
                $arr['reservation'] = !empty($data[0]) ? $data[0] : array();
                return json_encode($arr);
            }
            return false;
        }
        return false;
    }

    /**
     * Check duplicate order date
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool True if duplicate otherwise False
     */
    public static function check_duplicate_order_date($param, $order_id = '')
    {
        $date = new DateTime($param['reservation_date']);
        $where_order_id = "";
        if(!empty($order_id)){//update
            $where_order_id .= " AND orders.id <> {$order_id} ";
        }

        $order_start_date_new = self::time_to_val($param['order_start_date']);
        $order_end_date_new = self::time_to_val($param['order_end_date']);

        $query = DB::select(DB::expr("DISTINCT order_logs.nailist_id"))
            ->from("order_logs")
            ->where(DB::expr(" order_logs.disable = 0 AND order_logs.order_id IN
                (SELECT orders.id
                FROM orders
                WHERE orders.disable = 0 AND IFNULL(orders.is_cancel,0) = 0 AND orders.shop_id = {$param['shop_id']}
                AND DATE(FROM_UNIXTIME(orders.reservation_date)) = '".$date->format('Y-m-d')."' {$where_order_id}
                AND (
                (       orders.order_start_date < ".$order_start_date_new." AND ((orders.order_end_date < ".$order_end_date_new." AND orders.order_end_date > ".$order_start_date_new.") OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
                OR ( orders.order_start_date > ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR ( orders.order_end_date > ".$order_end_date_new." AND orders.order_start_date < ".$order_end_date_new.") OR orders.order_end_date = ".$order_end_date_new."))
                OR ( orders.order_start_date = ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
                ))"));
        $data = $query->execute()->as_array();
        if (!empty($data))
        {
            $current_nailist_ids = \Lib\Arr::arrayValues($data, 'nailist_id');
            $new_nailist_ids = explode(',', $param['nailist_id']);
            $count = count(array_intersect($new_nailist_ids, $current_nailist_ids));
            if($count > 0){
                return true;
            }
        }
        return false;
    }

    /**
     * Check duplicate order date for nailist
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool True if duplicate otherwise False
     */
    public static function check_duplicate_order_date_nailist($param, $order_id = '')
    {
    	if(!empty($param['is_seat']) || empty($param['nailist_id'])){
    		return false;
    	}

        $orderNailist = Model_Order_Log::get_all(array(
            'nailist_id' => $param['nailist_id'],
            'not_in_order_id' => $order_id,
            'order_start_date' => $param['order_start_date'],
            'order_end_date' => $param['order_end_date'],
        	'shop_id' => $param['shop_id']
        ));
        return !empty($orderNailist) ? true : false;

//         $date = new DateTime($param['reservation_date']);
//         $where_order_id = "";
//         if(!empty($order_id)){//update
//             $where_order_id .= " AND orders.id <> {$order_id} ";
//         }

//         $order_start_date_new = self::time_to_val($param['order_start_date']);
//         $order_end_date_new = self::time_to_val($param['order_end_date']);

//         $query = DB::select(DB::expr("DISTINCT order_logs.nailist_id"))
//             ->from("order_logs")
//             ->where(DB::expr(" order_logs.disable = 0 AND order_logs.order_id IN
//                 (SELECT orders.id
//                 FROM orders
//                 WHERE orders.disable = 0 AND IFNULL(orders.is_cancel,0) = 0 AND orders.shop_id = {$param['shop_id']}
//                 AND DATE(FROM_UNIXTIME(orders.reservation_date)) = '".$date->format('Y-m-d')."' {$where_order_id}
//                 AND (
//                 (       orders.order_start_date < ".$order_start_date_new." AND ((orders.order_end_date < ".$order_end_date_new." AND orders.order_end_date > ".$order_start_date_new.") OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
//                 OR ( orders.order_start_date > ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR ( orders.order_end_date > ".$order_end_date_new." AND orders.order_start_date < ".$order_end_date_new.") OR orders.order_end_date = ".$order_end_date_new."))
//                 OR ( orders.order_start_date = ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
//                 ))"));
//         $data = $query->execute()->as_array();
//         if (!empty($data))
//         {
//             $current_nailist_ids = \Lib\Arr::arrayValues($data, 'nailist_id');
//             $new_nailist_ids = explode(',', $param['nailist_id']);
//             $count = count(array_intersect($new_nailist_ids, $current_nailist_ids));
//             if($count > 0){
//                 return true;
//             }
//         }
//         return false;
    }

    /**
     * Check duplicate order date for device
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool True if duplicate otherwise False
     */
    public static function check_duplicate_order_date_device($param, $order_id = '')
    {
    	if(empty($param['devices_id'])){
    		return false;
    	}
        $orderDevice = Model_Order_Device::get_all(array(
            'device_id' => $param['devices_id'],
            'not_in_order_id' => $order_id,
            'order_start_date' => $param['order_start_date'],
            'order_end_date' => $param['order_end_date'],
            'shop_id' => $param['shop_id'],
            'is_seat' => !empty($param['is_seat']) ? $param['is_seat'] : 0,
        ));
        return !empty($orderDevice) ? true : false;

//         $date = new DateTime($param['reservation_date']);
//         $where_order_id = "";
//         if(!empty($order_id)){//update
//             $where_order_id .= " AND orders.id <> {$order_id} ";
//         }

//         $order_start_date_new = self::time_to_val($param['order_start_date']);
//         $order_end_date_new = self::time_to_val($param['order_end_date']);

//         $query = DB::select(DB::expr("DISTINCT order_devices.device_id"))
//             ->from("order_devices")
//             ->where(DB::expr(" order_devices.disable = 0 AND order_devices.order_id IN
//                 (SELECT orders.id
//                 FROM orders
//                 WHERE orders.disable = 0 AND IFNULL(orders.is_cancel,0) = 0 AND orders.shop_id = {$param['shop_id']}
//                 AND DATE(FROM_UNIXTIME(orders.reservation_date)) = '".$date->format('Y-m-d')."' {$where_order_id}
//                 AND (
//                 (       orders.order_start_date < ".$order_start_date_new." AND ((orders.order_end_date < ".$order_end_date_new." AND orders.order_end_date > ".$order_start_date_new.") OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
//                 OR ( orders.order_start_date > ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR ( orders.order_end_date > ".$order_end_date_new." AND orders.order_start_date < ".$order_end_date_new.") OR orders.order_end_date = ".$order_end_date_new."))
//                 OR ( orders.order_start_date = ".$order_start_date_new." AND (orders.order_end_date < ".$order_end_date_new." OR  orders.order_end_date > ".$order_end_date_new." OR orders.order_end_date = ".$order_end_date_new."))
//                 ))"));
//         $data = $query->execute()->as_array();
//         if (!empty($data))
//         {
//             $current_device_ids = \Lib\Arr::arrayValues($data, 'device_id');
//             $new_device_ids = explode(',', $param['devices_id']);
//             $count = count(array_intersect($new_device_ids, $current_device_ids));
//             if($count > 0){
//                 return true;
//             }
//         }
//         return false;
    }

    /**
     * Check duplicate order booking
     *
     * @author thong
     * @param array $param Input data
     * @return bool True if duplicate otherwise False
     */
    public static function check_duplicate_order_booking_nailist($param,$nailist_id='',$order_id='')
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name);
        $query->join('order_logs');
        $query->on('order_logs.order_id', '=', self::$_table_name . '.id');
        if(!empty($order_id)){
            $query->where(self::$_table_name.'.order_id', '!=',$order_id);
        }
        $query->where('order_logs.nailist_id', '=',$nailist_id);
        $query->where(self::$_table_name . '.disable', '=',0);
        $query->where(DB::expr("(".self::$_table_name.".order_start_date <=".$param['start_at_epoch']." AND ".self::$_table_name.".order_end_date <= ".$param['end_at_epoch'].") OR (".self::$_table_name.".order_start_date >=".$param['end_at_epoch']." AND ".self::$_table_name.".order_end_date >= ".$param['start_at_epoch'].") "));
        //$query->or_where(array(array(self::$_table_name . '.order_start_date', '<=',$param['end_at_epoch']), array(self::$_table_name . '.order_end_date', '>=', $param['end_at_epoch'])));
        $data = $query->execute()->as_array();
        //echo DB::last_query();
        return $data;
    }

    /**
     * check uplicate order booking device
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool True if duplicate otherwise False
     */
    public static function check_duplicate_order_booking_device($param,$device_id='',$order_id='')
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name);
        $query->join('order_devices');
        $query->on('order_devices.order_id', '=', self::$_table_name . '.id');
        if(!empty($order_id)){
            $query->where(self::$_table_name.'.order_id', '!=',$order_id);
        }
        $query->where('order_devices.device_id', '=',$device_id);
        $query->where(self::$_table_name . '.disable', '=',0);
        $query->where(DB::expr("(".self::$_table_name.".order_start_date <=".$param['start_at_epoch']." AND ".self::$_table_name.".order_end_date <= ".$param['end_at_epoch'].") OR (".self::$_table_name.".order_start_date >=".$param['end_at_epoch']." AND ".self::$_table_name.".order_end_date >= ".$param['start_at_epoch'].") "));
        //$query->or_where(array(array(self::$_table_name . '.order_start_date', '<=',$param['end_at_epoch']), array(self::$_table_name . '.order_end_date', '>=', $param['end_at_epoch'])));
        $data = $query->execute()->as_array();
        //echo DB::last_query();
        return $data;
    }

    /**
     * Check new orders
     *
     * @author diennvt
     * @param array $param Input data
     * @return array Array(order without nailist, new orders array)
     */
    public static function check_neworder($param)
    {
        $count_without_nailist = $count_new_order = 0;
        $count_add_nail = array();

        if (!empty($param['count_add_nail']) || !empty($param['count_without_nailist'])) {
            $count_add_nail = self::count_add_nail($param);
            $count_without_nailist = $count_add_nail;
        }

        if (!empty($param['check_neworder'])) {
            $query = DB::select(
                    'id',
                    'reservation_date',
                    'user_name'
                )
                ->from(self::$_table_name)
                ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
                ->where(DB::expr("FROM_UNIXTIME(" . self::$_table_name . ".reservation_date, '%Y-%m-%d') = CURDATE()"))
                ->where(self::$_table_name . '.disable', '=', 0)
                ->where(self::$_table_name . '.is_cancel', '=', 0)
                ->where(self::$_table_name . '.start_date', 'IS', NULL);
            
            if(isset($param['drawedOrderIds'])) {
                $param['drawedOrderIds'] = json_decode($param['drawedOrderIds']);
                if(!empty($param['drawedOrderIds'])) {
                    $query->where(self::$_table_name . '.id',"NOT IN", $param['drawedOrderIds']);  
                }
            }
            $data = $query->execute();
            $count_new_order = count($data);
        }
        return array($count_without_nailist, $count_add_nail, $count_new_order);
    }

    /**
     * Check orders without nailist in today
     *
     * @author diennvt
     * @param array $param Input data
     * @return int Order without nailist
     */
    public static function count_without_nailist($param)
    {
        if (empty($param['shop_id'])) {
            return 0;
        }
        $key = 'orders_count_without_nailist_' . $param['shop_id'];
        $count = \Lib\Cache::get($key);
        if ($count !== false) {
            return $count;
        }
        $count = DB::select(
                DB::expr("orders.id as order_id"),
                DB::expr('DATE(FROM_UNIXTIME(orders.reservation_date)) as reservation_date'),
                 'user_name'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(DB::expr("DATE(FROM_UNIXTIME(orders.reservation_date)) = " . "'" . date('Y-m-d') . "'"))
            ->where(self::$_table_name . '.disable', '=', 0)
            ->where(DB::expr("orders.id NOT IN
                    (
                        SELECT DISTINCT order_id
                        FROM order_logs
                        WHERE disable = 0 AND order_id = orders.id
                    )"
                )
            )
            ->execute()
            ->count();
        \Lib\Cache::set($key, $count);
        return $count;
    }


    /**
     * Check orders that just add nail from current to end day
     *
     * @author thailh
     * @param array $param Input data
     * @return int Orders just add nail
     */
    public static function count_add_nail($param)
    {
        if (empty($param['shop_id'])) {
            return array();
        }
        $key = 'orders_count_add_nail_' . $param['shop_id'];
        $data = \Lib\Cache::get($key);
        if ($data !== false) {
            return $data;
        }
        $data = DB::select(
                self::$_table_name . '.id',
                'reservation_date',
                'user_name'
            )
            ->from(self::$_table_name)
            ->join('order_nails')
            ->on(self::$_table_name . '.id', '=', 'order_nails.order_id')
            ->where('shop_id', $param['shop_id'])
            ->where('is_cancel', '0')
            ->where('is_autocancel', '0')
            ->where(self::$_table_name . '.disable', '0')
            ->where('order_nails.disable', '0')
            ->where(DB::expr("
                    start_date IS NOT NULL
                    AND consult_start_date IS NULL
                    AND reservation_date >= UNIX_TIMESTAMP(CURRENT_DATE())
                    AND reservation_date < UNIX_TIMESTAMP(CURRENT_DATE() + INTERVAL 1 DAY)                   
                ")
            )
            ->order_by(self::$_table_name . '.created', 'DESC')
            ->execute()
            ->as_array();    
        \Lib\Cache::set($key, $data);
        return $data;
    }

    /**
     * Get list info for Order confirm
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Info
     */
    public static function get_confirm_for_frontend($param) {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        // get nail detail
        $nailDetail = array();
        if (!empty($param['nail_id'])) {
            $nailDetail = Model_Nail::get_detail(array(
                'id' => $param['nail_id'],
                'login_user_id' => $param['login_user_id']
            ));
            if (!$nailDetail) {
                static::errorNotExist('nail_id');
                return false;
            }
        }
        
        // get shop detail
        $shopDetail = array();
        if (!empty($param['shop_id'])) {
            $shopDetail = Model_Shop::get_detail(array(
                'id' => $param['shop_id']
            ));
            if (!$shopDetail) {
                static::errorNotExist('shop_id');
                return false;
            }
        }  
        
        // get list nailist
        $nailistDetail = array();
        if (!empty($param['nailist_id'])) {
            $nailistDetail = Model_Nailist::get_detail(array(
                'id' => $param['nailist_id']
            ));
            if (!$nailistDetail) {
                static::errorNotExist('nailist_id');
                return false;
            }
        }
        
        // get last order
        $lastOrder = array();
        if (!empty($param['login_user_id'])) {
            $query = DB::select(
                    self::$_table_name . '.id', 
                    self::$_table_name . '.reservation_date', 
                    self::$_table_name . '.hf_section'
                )
                ->from(self::$_table_name)
                ->where(self::$_table_name . '.created', '<=', time())               
                ->where(self::$_table_name . '.user_id', '=', $param['login_user_id'])
                ->where(self::$_table_name . '.hf_section', '>', 0)
                ->where(self::$_table_name . '.is_cancel', '=', 0) 
                ->where(self::$_table_name . '.is_autocancel', '=', 0) 
                ->where(self::$_table_name . '.is_paid', '=', 0)  
                ->order_by(self::$_table_name . '.created', 'DESC')
                ->limit(1)
                ->offset(0);           
            $lastOrder = $query->execute()->offsetGet(0);
        }
        return array(
            'nail' => $nailDetail,
            'shop' => $shopDetail,
            'nailist' => $nailistDetail,
            'last_order' => $lastOrder
        );
    }

    /**
     * Get list info for Order confirm on mobile APP
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Info
     */
    public static function get_confirm_for_mobile($param) {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        // get nail detail
        $nailDetail = null;
        if (!empty($param['nail_id'])) {
            $nailDetail = Model_Nail::get_detail(array(
                'id' => $param['nail_id'],
                'login_user_id' => $param['login_user_id']
            ));
            if (!$nailDetail) {
                static::errorNotExist('nail_id');
                return false;
            }
        }
        
        // get shop detail
        $shopDetail = null;
        if (!empty($param['shop_id'])) {
            $shopDetail = Model_Shop::get_detail(array(
                'id' => $param['shop_id']
            ));
            if (!$shopDetail) {
                static::errorNotExist('shop_id');
                return false;
            }
        }   
        
        // get list nailist
        $nailistDetail = null;
        if (!empty($param['nailist_id'])) {
            $nailistDetail = Model_Nailist::get_detail(array(
                'id' => $param['nailist_id']
            ));
            if (!$nailistDetail) {
                static::errorNotExist('nailist_id');
                return false;
            }
        }
        
        // get last order
        $lastOrders = null;
        if (!empty($param['nail_id']) 
            && !empty($param['login_user_id']) 
            && !isset($param['uncheck_order'])) {
            $lastOrders = DB::select(
                    self::$_table_name . '.id', 
                    self::$_table_name . '.reservation_date', 
                    self::$_table_name . '.hf_section'
                )
                ->from(self::$_table_name)
                ->where(self::$_table_name . '.created', '<=', time())
                ->where(self::$_table_name . '.order_end_date', '>', time())
                ->where(self::$_table_name . '.hf_section', '>', 0)
                ->where(self::$_table_name . '.is_cancel', '=', 0) 
                ->where(self::$_table_name . '.is_autocancel', '=', 0) 
                ->where(self::$_table_name . '.is_paid', '=', 0) 
                ->where(self::$_table_name . '.disable', '=', 0) 
                ->where(self::$_table_name . '.user_id', '=', $param['login_user_id'])
                ->order_by(self::$_table_name . '.created', 'DESC') 
                ->execute()
                ->as_array();
            if (!empty($lastOrders)) {                
                $hf_section = array(
                    1 => 0, // hand
                    2 => 0, // foot
                );
                foreach ($lastOrders as $lastOrder) {
                    $hf_section[$lastOrder['hf_section']]++;                    
                }
                if ($hf_section[1] > 0 && $hf_section[2] > 0) {
                    \LogLib::warning('Ordered to allowed quota', __METHOD__, $param);
                    self::errorOther(self::ERROR_CODE_OTHER_1, 'hf_section', 'You have ordered to allowed quota');
                    return false;
                } elseif ($hf_section[$nailDetail['hf_section']] > 0) {
                    \LogLib::warning('Can not order same hf_section', __METHOD__, $param);
                    self::errorDuplicate('hf_section', 'Can not order same hf_section');
                    return false;
                }
                $lastOrders = $lastOrders[0];
            }
        }
        return array(
            'nail' => !empty($nailDetail) ? $nailDetail : null,
            'shop' => !empty($shopDetail) ? $shopDetail : null,
            'nailist' => !empty($nailistDetail) ? $nailistDetail : null,
            'last_order' => !empty($lastOrders) ? $lastOrders : null,
        );
    }
    
    /**
     * Return unchange Data with warning
     *
     * @author diennvt
     * @param array $param Input data
     * @param int $id Order id
     * @param int $typeErr Error type (0: duplicate, 1: out of time range, 2: duration is 0)
     * @return array
     */
    public static function returnErrorData($param, $id, $typeErr = 0)
    {
        $request = array(
            'shop_id' => $param['shop_id'],
            'date' => $param['reservation_date'],
        );
        if(isset($param['is_seat'])){
        	$request['is_seat'] = 1;
        }
        $data = self::date_calendar($request);
        if ($data)
        {
            $data = json_decode($data);
            $arr['reservation_all'] = $data;
            $arr['order_id'] = $id;
            $arr['type_err'] = $typeErr;
            return $arr;
        }
        return array();
    }

    /**
     * Save list (nailist and device)
     *
     * @author diennvt
     * @param array $shop_id Input data
     * @return array
     */
    public static function saveList($order_id, $param)
    {
    	if(!isset($param['is_seat']))
    	{
    		// save nailist
    		if (!Model_Order_Log::add_update_by_order_id(array(
    				'order_id' => $order_id,
    				'nailist_id' => $param['nailist_id']
    		)))
    		{
    			return false;
    		}
    	}else{
    		//TODO
    	}
        // save device
        if (!Model_Order_Device::add_update_by_order_id(array(
            'order_id' => $order_id,
            'device_id' => $param['devices_id']
        )))
        {
            return false;
        }
    }

    /**
     * Create 2 list id of nailist and device
     *
     * @author diennvt
     * @param array $param Input data
     * @return array
     */
    public static function createListIds($param)
    {
        $nailist_ids = array();
        $device_ids = array();
        $seat_ids = array();
        $stylist_ids = explode(',', $param['stylist_ids']);
        foreach ($stylist_ids as $val)
        {
            if (substr($val, 0, 2) == 'n_')
            {
                $val = str_replace('n_', '', $val);
                $nailist_ids[] = $val;
            }
            elseif (substr($val, 0, 2) == 'd_')
            {
                $val = str_replace('d_', '', $val);
                $device_ids[] = $val;
            }else{
            	$val = str_replace('s_', '', $val);
            	$seat_ids[] = $val;
            }
        }

        if(isset($param['is_seat']))
        {
        	return array($seat_ids, $device_ids);
        }
        else
        {
        	return array($nailist_ids, $device_ids);
        }

    }

    /**
     * Stopwatch Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order id or false i
     * f error
     */
    public static function stopwatch($param)
    {
    	$curtime = time();
        $order = self::find($param['order_id']);
       
        if (empty($order)) {
            static::errorNotExist('order_id');
            return false;
        }
        
        $order_before_update = array(
        		'order_id' => $order['id'],
        		'start_date' => !empty($order['start_date']) ? $order['start_date'] : '',
        		'consult_start_date' => !empty($order['consult_start_date']) ? $order['consult_start_date'] : '',
        		'consult_end_date' => !empty($order['consult_end_date']) ? $order['consult_end_date'] : '',
        		'off_start_date' => !empty($order['off_start_date']) ? $order['off_start_date'] : '',
        		'off_end_date' => !empty($order['off_end_date']) ? $order['off_end_date'] : '',
        		'sr_start_date' => !empty($order['sr_start_date']) ? $order['sr_start_date'] : '',
        		'sr_end_date' => !empty($order['sr_end_date']) ? $order['sr_end_date'] : '',
        		'is_paid' => !empty($order['is_paid']) ? $order['is_paid'] : '' 
        );

        if (!empty($param['is_paid'])) {
            $param['type'] = 6;
            $order->set('is_paid', '1');
            $order_before_update['undo'] = 'is_paid';   
        } elseif (!empty($param['sr_end_date'])) {
            $param['type'] = 5;
            $order->set('sr_end_date', $param['sr_end_date']);
            $order->set('is_paid', '1');
            $order_before_update['undo'] = 'sr_end_date';
        } elseif (!empty($param['sr_start_date'])) {
            $param['type'] = 4;
            $order->set('sr_start_date', $param['sr_start_date']);
            $order_before_update['undo'] = 'sr_start_date';
        } elseif (!empty($param['off_end_date'])) {
            $param['type'] = 3;
            $order->set('off_end_date', $param['off_end_date']);
            $order_before_update['undo'] = 'off_end_date';
        } elseif (!empty($param['off_start_date'])) {
            $param['type'] = 2;
            $order->set('off_start_date', $param['off_start_date']);
            $order_before_update['undo'] = 'off_start_date';
        } elseif (!empty($param['consult_end_date'])) {
            $param['type'] = 8;
            $order->set('consult_end_date', $param['consult_end_date']);
            $order_before_update['undo'] = 'consult_end_date';
        } elseif (!empty($param['consult_start_date'])) {
            $param['type'] = 7;
            $order->set('consult_start_date', $param['consult_start_date']);
            $order_before_update['undo'] = 'consult_start_date';
        } elseif (!empty($param['start_date'])) {
            $param['type'] = 1;
            $order->set('start_date', $param['start_date']);
            $order_before_update['undo'] = 'start_date';
        }

        if (!empty($order->get('is_paid')) && empty($order->get('sr_end_date'))) {
        	$order->set('sr_end_date', $curtime);
        }
        if (!empty($order->get('sr_end_date')) && empty($order->get('sr_start_date'))) {
        	$order->set('sr_start_date', $order->get('sr_end_date'));
        }
        if (!empty($order->get('sr_start_date')) && empty($order->get('off_end_date'))) {
        	$order->set('off_end_date', $order->get('sr_start_date'));
        }
        if (!empty($order->get('off_end_date')) && empty($order->get('off_start_date'))) {
        	$order->set('off_start_date', $order->get('off_end_date'));
        }
        if (!empty($order->get('off_start_date')) && empty($order->get('consult_end_date'))) {
        	$order->set('consult_end_date', $order->get('off_start_date'));
        }
        if (!empty($order->get('consult_end_date')) && empty($order->get('consult_start_date'))) {
        	$order->set('consult_start_date', $order->get('consult_end_date'));
        }
        if (!empty($order->get('consult_start_date')) && empty($order->get('start_date'))) {
        	$order->set('start_date', $order->get('consult_start_date'));
        }
        if ($order->update()) {  
            // delete cache
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
        	if (!Model_Order_Nailist_Log::add($param)) {
                if (empty(self::error())) {
                    \LogLib::info('Add Order_Nailist_Log failed ', __METHOD__, $param);
                }
            }
            // Update point
            if(!empty($param['is_paid'])){
            	$r_point = $order->get('r_point');
            	if(!empty($r_point) && $r_point > 0){
            		 $option['where'] = array(
            		 		'id' => $order->get('user_id'),
				            'disable' => '0'
				     );
				    $user = Model_User::find('first', $option);
				    if (empty($user)) {
				    	\LogLib::info('User not exists ', __METHOD__, $option);
				    	return false;
				    }
				    if(!empty($user['point']) && $user['point'] > 0 && $user['point'] > $r_point){
				    	$order_before_update['is_update_point'] = 1;
				    	$update_point = $user['point'] - $r_point;
				    	if(!Model_User::update_point(array('user_id'=>$order->get('user_id') ,'point' => $update_point))){
				    		\LogLib::info('Update_point failed ', __METHOD__, $update_point);
				    		return false;
				    	}
				    }
            	}	
            }
            // in case of undo
            if(isset($param['get_undo'])){
            	return  $order_before_update; 
            }
            return $order->id;
        }
        return false;
    }

    /**
     * Get list order of user (with array count)
     *
     * @author diennvt
     * @param array $param Input data
     * @return array List Order
     */
    public static function get_user_order_list($param)
    {       
        if(empty($param['page'])){
            $param['page'] = 1;
        }
    	$query = DB::select(
            array('orders.id', 'order_id'),
            'reservation_date',
            'total_price',
            'total_tax_price',
            'is_cancel',
            'start_date',
            'hf_section',
            array('shops.name', 'shop_name'),
            array('shops.phone', 'shop_phone'),
            array('shops.address', 'shop_address'),
            array('shops.map_url', 'shop_map_url'),
            array('shops.open_time', 'shop_open_time'),
            array('shops.close_time', 'shop_close_time'),
            array('shops.is_plus', 'shop_is_plus')
    	)
    	->from(self::$_table_name)
    	->join('shops', 'LEFT')
    	->on(self::$_table_name . '.shop_id', '=', 'shops.id')
    	->where(self::$_table_name . '.disable', '=', 0);        
        if (!empty($param['login_user_id'])) {
            $query->where('user_id', '=', $param['login_user_id']);
        }
        if (isset($param['upcoming'])) {
            $query->where(DB::expr('reservation_date >= UNIX_TIMESTAMP()'));
        } elseif (isset($param['past'])) {
            $query->where(DB::expr('reservation_date < UNIX_TIMESTAMP()'));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }     
    	if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
    	}
    	// get order list of user
    	$data = $query->execute()->as_array();
    	$total = !empty($data) ? DB::count_last_query() : 0;
    	// get nail of order
        if ($data) {
            $param['order_ids'] = \Lib\Arr::field($data, 'order_id');
            $param['implode'] = '/';
            $nails_of_orders_arr = Model_Nail::get_nail_order($param);
            foreach ($data as $key => $info) {
                $nails = \Lib\Arr::filter($nails_of_orders_arr, 'order_id', $info['order_id']);
                $data[$key]['nails'] = array_values($nails);
            }
        }
    	return array($total, $data);
    }

    /**
     * Get detail to view all informations of order.
     *
     * @author truongnn
     * @param array $param Input data
     * @return array|bool Detail Order or false if error
     */
    public static function get_detailforview($param)
    {
        $query = DB::select(
                      self::$_table_name . '.user_id'
                    , self::$_table_name . '.user_code'
                    , self::$_table_name . '.user_name'
                    , self::$_table_name . '.kana'
                    , self::$_table_name . '.sex'
                    , self::$_table_name . '.birthday'
                    , self::$_table_name . '.email'
                    , self::$_table_name . '.phone'
                    , 'address'
                    , self::$_table_name . '.id'
                    , self::$_table_name . '.total_price'
                    , self::$_table_name . '.total_tax_price'
                    , DB::expr("(orders.total_price + orders.total_tax_price) AS total_sell_price")
                    , self::$_table_name . '.pay_type'
                    , self::$_table_name . '.seat_id'
                    , self::$_table_name . '.reservation_type'
                    , self::$_table_name . '.reservation_date'
                    , self::$_table_name . '.off_nails'
                    , self::$_table_name . '.hf_section'
                    , self::$_table_name . '.nail_length'
                    , self::$_table_name . '.nail_add_length'
                    , self::$_table_name . '.nail_type'
                    , self::$_table_name . '.problem'
                    , self::$_table_name . '.request'
                    , self::$_table_name . '.service'
                    , self::$_table_name . '.is_mail'
                    , self::$_table_name . '.is_designate'
                    , self::$_table_name . '.is_next_reservation'
                    , self::$_table_name . '.welfare_id'
                    , self::$_table_name . '.visit_section'
                    , self::$_table_name . '.visit_element'
                    , array('shops.name', 'shop_name')
                    , self::$_table_name . '.seat_id'
                    , self::$_table_name . '.is_cancel'
                    , self::$_table_name . '.is_autocancel'
                    , self::$_table_name . '.shop_counter_code'
                    , 'nail_orders.nailist'
                    , 'device_orders.device',
                    DB::expr("CONCAT_WS('',
                                prefectures.name, orders.address1, orders.address2
                                ) AS address"),
                     DB::expr("CONCAT_WS(' - ',
                                FROM_UNIXTIME(orders.order_start_date, '%Y年%m月%d日 %H:%i'), FROM_UNIXTIME(orders.order_end_date, '%Y年%m月%d日 %H:%i')
                                ) AS order_time"),
                     DB::expr("CONCAT_WS(' - ',
                                FROM_UNIXTIME(orders.off_start_date, '%Y年%m月%d日 %H:%i'), FROM_UNIXTIME(orders.off_end_date, '%Y年%m月%d日 %H:%i')
                                ) AS nail_off_time"),
                     DB::expr("CONCAT_WS(' - ',
                                FROM_UNIXTIME(orders.sr_start_date, '%Y年%m月%d日 %H:%i'), FROM_UNIXTIME(orders.sr_end_date, '%Y年%m月%d日 %H:%i')
                                ) AS servce_time"),
                    DB::expr("CONCAT(
                                'rating_1(', IFNULL(IF(rating_1='',NULL,rating_1), 0), '), ',
                                'rating_2(', IFNULL(IF(rating_2='',NULL,rating_2), 0), '), ',
                                'rating_3(', IFNULL(IF(rating_3='',NULL,rating_3), 0), '), ',
                                'rating_4(', IFNULL(IF(rating_4='',NULL,rating_4), 0), '), ',
                                'rating_5(', IFNULL(IF(rating_5='',NULL,rating_5), 0), ')') AS rating")
                )
    	->from(self::$_table_name)
    	->join('shops', 'LEFT')
    	->on(self::$_table_name . '.shop_id', '=', 'shops.id')
        ->join('prefectures', 'LEFT')
    	->on(self::$_table_name . '.prefecture_id', '=', 'prefectures.id')
        ->join(DB::expr("(
                SELECT order_logs.order_id, GROUP_CONCAT(DISTINCT nailists.name SEPARATOR  ', ') AS nailist
                FROM order_logs
                JOIN nailists ON order_logs.nailist_id = nailists.id
                WHERE order_logs.disable = 0 AND order_logs.order_id = {$param['id']}
                GROUP BY order_logs.order_id
            ) AS nail_orders"), 'LEFT')
        ->on(self::$_table_name . '.id', '=', 'nail_orders.order_id')
        ->join(DB::expr("(
                SELECT order_devices.order_id, GROUP_CONCAT(DISTINCT devices.name SEPARATOR  ', ') AS device
                FROM order_devices
                JOIN devices ON order_devices.device_id = devices.id
                WHERE order_devices.disable = 0 AND order_devices.order_id = {$param['id']}
                GROUP BY order_devices.order_id
            ) AS device_orders"), 'LEFT')
        ->on(self::$_table_name . '.id', '=', 'device_orders.order_id')
    	->limit(1)
        ->offset(0);
    	if (isset($param['id'])) {
            $query->where(self::$_table_name . '.id', $param['id']);
        }
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        } else {
            // Get list order_nails
            $order_nails = DB::select(
                    'nails.id',
                    DB::expr(" CONCAT('" . \Config::get('item_img_url')['nails'] . "', nails.photo_cd, '" . ".jpg" . "') AS image_url"),
                    'nails.photo_cd',
                    'order_nails.price',
                    'order_nails.tax_price',
                    'nails.time',
                    'nails.rank',
                    'nails.hf_section',
                    'nails.limited',
                    'nails.hp_coupon',
                    DB::expr("(order_nails.price + order_nails.tax_price) AS sell_price")
                )
                ->from(self::$_table_name)
                    ->join('order_nails')
                    ->on(self::$_table_name . '.id', '=', 'order_nails.order_id')
                    ->join('nails')
                    ->on('order_nails.nail_id', '=', 'nails.id')
                    ->where('order_nails.disable', 0)
                    ->where('order_nails.order_id', $param['id']);
          $data['order_nails'] = $order_nails->execute()->as_array();

          // Get list item_nails
            $order_items = DB::select(
                    'items.id',
                    'items.item_cd',
                    'order_items.item_name',
                    'order_items.item_price',
                    'order_items.item_tax_price',
                    'order_items.item_quantity',
                    DB::expr("(order_items.item_price+ order_items.item_tax_price) * order_items.item_quantity AS sell_price")
                )
                ->from(self::$_table_name)
                    ->join('order_items')
                    ->on(self::$_table_name . '.id', '=', 'order_items.order_id')
                    ->join('items')
                    ->on('order_items.item_id', '=', 'items.id')
                    ->where('order_items.disable', 0)
                    ->where('order_items.order_id', $param['id']);

          $data['order_items'] = $order_items->execute()->as_array();

        // Get list order_nailist_logs
        $order_nailist_logs = DB::select(
                 DB::expr("IFNULL(IF(nailists.image_url='',NULL,nailists.image_url), '" . \Config::get('no_image_nailist') . "') AS nailist_image_url"),
                array('nailists.name', 'nailist_name'),
                'order_logs.created'
            )
            ->from(self::$_table_name)
                ->join('order_logs')
                ->on(self::$_table_name . '.id', '=', 'order_logs.order_id')
                ->join('nailists')
                ->on('order_logs.nailist_id', '=', 'nailists.id')
                ->where('order_logs.disable', 0)
                ->where('order_logs.order_id', $param['id']);

          $data['order_nailist_logs'] = $order_nailist_logs->execute()->as_array();
        }

        return !empty($data) ? $data : array();
    }

    /**
     * Cancel Order
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function cancel($param)
    {
        if (!empty($param['user_id']) && $param['cancel'] == '1') {
            $query = DB::select()
                ->from(self::$_table_name)
                ->where('id', '=', $param['id'])
                ->where('user_id', '=', $param['user_id'])
                ->where(DB::expr("
                    DATE(FROM_UNIXTIME(orders.reservation_date)) > " . date('Y-m-d', time()) . "
                "))
                ->where(DB::expr("(
                    start_date = '' OR start_date IS NULL
                )"));
            $data = $query->execute()->offsetGet(0);
            if (!$data) {
                self::errorNotExist('order_id');
                return false;
            }
        }
        $order = self::find($param['id']);
        if ($order) {
            $order->set('is_cancel', $param['cancel']);
            if (!$order->save()) {
                return false;
            }
             //Delete cache
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            return true;
        } else {
            self::errorNotExist('order_id');
            return false;
        }
    }


    /**
     * Cancel Order
     *
     * @author Cao Dinh Tuan
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function autocancel($param)
    {
        if ($param['autocancel'] == '1') {
            $query = DB::select()
                ->from(self::$_table_name)
                ->where('id', '=', $param['id'])
                //->where('user_id', '=', $param['order_id'])
                ->where(DB::expr("
                    DATE(FROM_UNIXTIME(orders.reservation_date)) > " . date('Y-m-d', time()) . "
                "))
                ->where(DB::expr("(
                    start_date = '' OR start_date IS NULL
                )"));
            $data = $query->execute()->offsetGet(0);
            if (!$data) {
                self::errorNotExist('order_id');
                return false;
            }
        }
        $order = self::find($param['id']);
        if ($order) {
            $order->set('is_autocancel', $param['autocancel']);
            if (!$order->save()) {
                return false;
            }
            //Delete cache
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            return true;
        } else {
            self::errorNotExist('order_id');
            return false;
        }
    }

    /**
     * Update total_price, total_tax_price
     *
     * @author thailh
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function updatePrice($param)
    {
        $self = self::find($param['order_id']);
        if (empty($self)) {
            static::errorNotExist('order_id');
            return false;
        }
        // get list order nails
        $orderNails = Model_Order_Nail::get_all(array(
                'order_id' => $param['order_id'],
            )
        );
        // get list order items
        $orderItems = Model_Order_Item::get_all(array(
                'order_id' => $param['order_id'],
            )
        );
        // cal total price
        $totalPrice = $totalTaxPrice = 0;
        if (!empty($orderNails)) {
            foreach ($orderNails as $nail) {
                $totalPrice += $nail['price'];
                $totalTaxPrice += $nail['tax_price'];
            }
        }
        if (!empty($orderItems)) {
            foreach ($orderItems as $item) {
                if (!empty($item['item_quantity']) && $item['item_quantity'] > 0) {
                    $totalPrice += $item['item_quantity'] * $item['item_price'];
                    $totalTaxPrice += $item['item_quantity'] * $item['item_tax_price'];
                }
            }
        }
        // update total price
        $self->set('total_price', $totalPrice);
        $self->set('total_tax_price', $totalTaxPrice);
        if (!$self->update()) {
            \LogLib::warning('Can not update orders', __METHOD__, $param);
            return false;
        }
        return true;
    }

    /**
     * Import orders from email of HP
     *
     * @author thailvn
     * @param array $hpOrders Input data
     * @return array Import status of each hpcode
     */
    public static function importHpOrders($hpOrders)
    {
        if (empty($hpOrders)) {
            return false;
        }
        \LogLib::info('Import hporders', __METHOD__, $hpOrders);
        $hpOrderCodes = array_keys($hpOrders);
        $query = DB::select(
                self::$_table_name .'.hp_order_code'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name .'.hp_order_code', 'IN', $hpOrderCodes);
        $importedOrders = $query->execute()->as_array();
        if (!empty($importedOrders)) {
            $hpOrderCodes = array_diff($hpOrderCodes, \Lib\Arr::field($importedOrders, 'hp_order_code'));
        }
        // find canceled orders
        $hpOrderCanceled = \Lib\Arr::filter($hpOrders, 'is_cancel', 1);
        if (!empty($hpOrderCanceled)) {
            $hpOrderCodes = array_merge($hpOrderCodes, array_keys($hpOrderCanceled));
        }
        // process import
        $result = array();
        if (!empty($hpOrderCodes)) {
            foreach ($hpOrderCodes as $hpCode) {
                $order = $hpOrders[$hpCode];
                if (self::import($order)) {
                    $status = !empty($order['is_cancel']) ? 'Imported [cancel]' : 'Imported';
                    if (self::error()) {
                        foreach (self::error() as $error) {
                            if ($error['code'] == self::ERROR_CODE_FIELD_DUPLICATE
                                && $error['field'] == 'hp_order_code_and_is_cancel') {
                                $status = 'Ignore update is_cancel';
                            }
                        }
                    } else {
                        $result[$hpCode] = $status . ' [' . date('Y-m-d H:i:s') . ']';
                    }
                } else {
                    $result[$hpCode] = 'Fail';
                    \LogLib::warning("Can not import hp order {$hpCode}", __METHOD__, $order);
                }
            }
        }
        return $result;
    }

    /**
     * Import a order from email of HP
     *
     * @author truongnn
     * @param array $param Input data
     * @return int|bool Order id or false
     * @return int 1 If hp_order is updated 2 otherwise
     * @return int 3 If hp_order is empty and is_cancel = 1
     */
    public static function import($param)
    {
        \LogLib::info('START [import hporder]', __METHOD__, $param);
        if (empty($param['shop_name'])) {
            \LogLib::warning("Shop does not exists", __METHOD__, $param);
            return false;
        }
        // process for update is_cancel
        if (isset($param['is_cancel']) && $param['is_cancel'] == 1) {
            $option['where'] = array(
                'hp_order_code' => $param['hp_order_code'],
                'disable' => 0
            );
            $order = self::find('first', $option);
            if (!empty($order)) {
                if ($order->get('is_cancel') == 0) {
                    $order->set('is_cancel', '1');
                    if (!$order->update()) {
                        \LogLib::warning('Can not update orders.is_cancel', __METHOD__, $param);
                        return false;
                    }
                } else {
                    // ignore update is_cancel
                    self::errorDuplicate('hp_order_code_and_is_cancel', $param['hp_order_code']);
                }
                return true;
            }
        }
        if (empty($order)) {
            $order = new self;
            if (!isset($param['r_point'])) {
                $param['r_point'] = 0;                
            }
            $order->set('last_update_admin_id', '0');
            $order->set('rating_1', '0');
            $order->set('rating_2', '0');
            $order->set('rating_3', '0');
            $order->set('rating_4', '0');
            $order->set('rating_5', '0');
            $order->set('cart_id', '0');
            $order->set('admin_id', '0');              
            $order->set('sales_code', '0');              
            $order->set('welfare_id', '0');                      
            $order->set('fn_point', '0');                      
            $order->set('point_price', '0');             
            $order->set('service', '0');   
            $order->set('reservation_type', 5); // order from web
            $order->set('hf_section', 1);
            $order->set('pay_type', 1);
            $order->set('visit_element', 9);
            $order->set('is_mail', 1);
            $order->set('is_paid', 0);
            $order->set('is_autocancel', 0);
            $order->set('off_nails', '1');
            $order->set('nail_length', '0');
            $order->set('nail_type', '0');
            $order->set('nail_add_length', '0');
        }
        
        // set value        
        $option['where'] = array(
            'hpb_name' => $param['shop_name']           
        );
        $shop = Model_Shop::find('first', $option); 
        if (empty($shop)) {
            \LogLib::warning("Shop {$param['shop_name']} does not exists", __METHOD__, $param);
            return false;
        }
        $param['shop_id'] = $shop->get('id');
        $order->set('shop_id', $param['shop_id']);
               
        if (empty($param['total_price'])) {
            $param['total_price'] = 0;
        }
        
        //Set value user_id
        $user_id = 0;
        if (isset($param['user_name'])) {
            $order->set('user_name', $param['user_name']);
            /*
            $option['where'] = array(
                'name' => $param['user_name']              
            );
            $user = \Model_User::find('first', $option);
            if (!empty($user)) {
                $user_id = $user->get('id');
                $order->set('user_code', $user->get('code'));
                $order->set('visit_element', $user->get('visit_element'));
                $order->set('email', $user->get('email'));
                $order->set('user_name', $user->get('name'));
                $order->set('kana', $user->get('kana'));
                $order->set('phone', $user->get('phone'));
                $order->set('sex', $user->get('sex'));
                $order->set('birthday', $user->get('birthday'));
                $order->set('prefecture_id', $user->get('prefecture_id'));
                $order->set('address1', $user->get('address1'));
                $order->set('address2', $user->get('address2'));
            }
            * 
            */
        }
        $order->set('user_id', $user_id);
        $order->set('visit_section', $user_id > 0 ? 1 : 0);
        $order->set('total_price', $param['total_price']);
        $order->set('total_tax_price', $param['total_price'] * Config::get('taxNail'));
        if (isset($param['reservation_date'])) {            
            $plug = ($shop->get('is_plus') ? 45 : 30);
            if (isset($param['minute']) && $param['minute'] > 0) {
                if ($param['minute'] <= 15) {
                    $plug = 15;
                } elseif ($param['minute'] <= 30) {
                    $plug = 30;
                } elseif ($param['minute'] <= 45) {
                    $plug = 45;
                } elseif ($param['minute'] <= 60) {
                    $plug = 60;
                }
            }
            // round reservation_date to 15, 30, 45, 60 minutes
            $param['reservation_date'] = self::time_to_val($param['reservation_date']);
            $minute = date('i', $param['reservation_date']);
            if ($minute > 0) {
                if ($minute <= 15) {
                    $minuteRound = 15;
                } elseif ($minute <= 30) {
                    $minuteRound = 30;
                } elseif ($minute <= 45) {
                    $minuteRound = 45;
                } elseif ($minute <= 60) {
                    $minuteRound = 60;
                }
            }
            if (isset($minuteRound)) {
                $param['reservation_date'] += ($minuteRound - $minute) * 60;
            }            
            $param['order_start_date'] = $param['reservation_date'];
            $param['order_end_date'] = $param['order_start_date'] + $plug*60;
            $order->set('reservation_date', $param['reservation_date']);
            $order->set('order_start_date', $param['order_start_date']);
            $order->set('order_end_date', $param['order_end_date']);
            $order->set('is_designate', $shop->get('is_plus') ? 1 : 0);
        }
        if (isset($param['kana'])) {
            $order->set('kana', $param['kana']);
        }
        $order->set('hp_order_code', $param['hp_order_code']);
        $order->set('is_cancel', !empty($param['is_cancel']) ? 1 : 0);
        $order->set('request', !empty($param['request']) && $param['request'] != '-' ? $param['request'] : '');
        if (!empty($param['created'])) {
            $order->set('created', self::time_to_val($param['created']));
            $order->set('updated', self::time_to_val($param['created']));
        }
        $vacancy = self::get_available_vacancy_hp($shop, $param);
        if (!empty($vacancy)) {
            $order->set('seat_id', $vacancy[0]);
        } else {
            $max_seat = $shop->get('max_seat');
            $hp_max_seat = $shop->get('hp_max_seat');
            $over_seat = $max_seat + $hp_max_seat;
            $order->set('seat_id', $over_seat);
        }
        if (isset($param['r_point'])) {           
            $order->set('r_point', $param['r_point']);          
        }
        if (isset($param['reservation_date']) && date('Y-m-d', $param['reservation_date']) == date('Y-m-d')) {
            $order->set('shop_counter_code', Model_Shop_Counter_Code::get_code(true));
        }  
        // save to database
        if ($order->save()) {
            if (empty($order->id)) {
                $order->id = self::cached_object($order)->_original['id'];
            }
            // save nailist
            if (isset($param['nailist_name'])) {
                $option['where'] = array(
                    'name' => $param['nailist_name'],
                    'disable' => 0
                );
                $nailist = \Model_Nailist::find('first', $option);
                if (!empty($nailist)) {
                    if (!Model_Order_Log::add_update_by_order_id(array(
                        'order_id' => $order->id,
                        'nailist_id' => $nailist->get('id'),
                    ))) {
                        \LogLib::warning('Can not add order_log', __METHOD__, $param);
                        return false;
                    }
                } else {
                   \LogLib::warning("Nailist {$param['nailist_name']} does not exists", __METHOD__, $param);
                }
            }
            // delete cache
            if (!empty($order->get('shop_id'))) {
                \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
                \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            }
            \LogLib::info('END [import hporder]', __METHOD__, $order->id);
            return true;
        }
        return false;
    }

    /**
     * get available vacancy
     *
     * @author diennvt
     * @param array $param Input data
     * @return int|bool Seat Number or false
     */
    public static function get_min_available_vacancy($param){
        $vacancy = self::get_available_vacancy($param);
        if (self::error()) {
            return false;
        }
        $seatIds = explode(',', $param['seat_ids']);
        foreach ($seatIds as $seatId) {
            if (in_array($seatId, $vacancy)) {
                return $seatId;
            }
        }
        return !empty($vacancy) ? min($vacancy) : 0;
    }

    public static function get_available_vacancy($param){
    	$shop = Model_Shop::find($param['shop_id']);
        if (empty($shop) || empty($param['shop_id'])) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (!is_numeric($param['order_start_date'])) {
            $param['order_start_date'] = self::time_to_val($param['order_start_date']);
        }
        if (!is_numeric($param['order_end_date'])) {
            $param['order_end_date'] = self::time_to_val($param['order_end_date']);
        }
        if ($param['order_end_date'] <= $param['order_start_date']) {
            $param['order_end_date'] = $param['order_start_date'] + 1;
        }
        $table = self::table();
        $query = DB::select(
                self::$_table_name . '.shop_id',
                self::$_table_name . '.order_start_date',
                self::$_table_name . '.order_end_date',
                self::$_table_name . '.seat_id'
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::table() . '.disable', '0')
            ->where(
                DB::expr("
                    IFNULL({$table}.is_cancel, 0) = 0
                    AND IFNULL({$table}.is_autocancel, 0) = 0
                ")
        	)
            ->and_where_open()
            ->where_open()
            ->where(self::$_table_name . '.order_start_date', '>=', $param['order_start_date'])
            ->where(self::$_table_name . '.order_start_date', '<', $param['order_end_date'])
            ->where_close()
            ->or_where_open()
            ->where(self::$_table_name . '.order_end_date', '>', $param['order_start_date'])
            ->where(self::$_table_name . '.order_end_date', '<=', $param['order_end_date'])
            ->or_where_close()
            ->and_where_close()
            ->order_by(self::$_table_name . '.seat_id');
        if (!empty($param['not_in_order_id'])) {
            $query->where(self::table() . '.id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
        }
        
        if (!empty($param['is_seat'])) {
        	$query->where(DB::expr("(
                    IFNULL(orders.seat_id,0) > 0
                )")
        	);
        }
        
        $orders = $query->execute()->as_array();

        $vacancy = array();
        for ($i = 1; $i <= $shop->get('max_seat'); $i ++) {
            $find = array_search($i, array_column($orders, 'seat_id'));         
            if ($find === false) {
                $vacancy[$i] = $i;
            }
        }
        // check by time
        list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list(array(
                'shop_id' => $param['shop_id'],
                'date' => date('Y-m-d', $param['order_start_date'])
            )
        );

        $minLimit = count($vacancy);
        for ($i = $param['order_start_date']; $i < $param['order_end_date']; $i+=15*60) { 
            if (isset($order_timely_limit[date('H:i', $i)]['limit'])) {                
                if ($minLimit > $order_timely_limit[date('H:i', $i)]['limit']) {
                    $minLimit = $order_timely_limit[date('H:i', $i)]['limit'];
                }
            }
        }

        return array_slice($vacancy, 0, $minLimit);
    }
    
    public static function get_available_vacancy_calendar($param, &$allSeats=array()){
    	$shop = Model_Shop::find($param['shop_id']);
    	if (empty($shop) || empty($param['shop_id'])) {
    		static::errorNotExist('shop_id');
    		return false;
    	}
    	if (!is_numeric($param['order_start_date'])) {
    		$param['order_start_date'] = self::time_to_val($param['order_start_date']);
    	}
    	if (!is_numeric($param['order_end_date'])) {
    		$param['order_end_date'] = self::time_to_val($param['order_end_date']);
    	}
    	if ($param['order_end_date'] <= $param['order_start_date']) {
    		$param['order_end_date'] = $param['order_start_date'] + 1;
    	}
    	$table = self::table();
    	$query = DB::select(
    			self::$_table_name . '.shop_id',
    			self::$_table_name . '.order_start_date',
    			self::$_table_name . '.order_end_date',
    			self::$_table_name . '.seat_id'
    	)
    	->from(self::$_table_name)
    	->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
    	->where(self::table() . '.disable', '0')
    	->where(
    			DB::expr("
    					IFNULL({$table}.is_cancel, 0) = 0
    					AND IFNULL({$table}.is_autocancel, 0) = 0
    					")
    	)
    	->and_where_open()
    	->where_open()
    	->where(self::$_table_name . '.order_start_date', '>=', $param['order_start_date'])
    	->where(self::$_table_name . '.order_start_date', '<', $param['order_end_date'])
    	->where_close()
    	->or_where_open()
    	->where(self::$_table_name . '.order_end_date', '>', $param['order_start_date'])
    	->where(self::$_table_name . '.order_end_date', '<=', $param['order_end_date'])
    	->or_where_close()
        ->or_where_open()
    	->where(self::$_table_name . '.order_start_date', '<', $param['order_start_date'])
        ->where(self::$_table_name . '.order_end_date', '>', $param['order_end_date'])
    	->or_where_close()
    	->and_where_close()
    	->order_by(self::$_table_name . '.seat_id');
    	if (!empty($param['not_in_order_id'])) {
    		$query->where(self::table() . '.id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
    	}
    
    	if (!empty($param['is_seat'])) {
    		$query->where(DB::expr("(
                    IFNULL(orders.seat_id,0) > 0
                )")
    		);
    	}
    	$orders = $query->execute()->as_array();
    	$vacancy = array();
    	for ($i = 1; $i <= $shop->get('max_seat'); $i ++) {
    		$find = array_search($i, array_column($orders, 'seat_id'));
    		if ($find === false) {
    			$vacancy[$i] = $i;
    		}
    	}
        
        $allSeats = $vacancy;
        
        // check by time
    	list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list(array(
            'shop_id' => $param['shop_id'],
            'date' => date('Y-m-d', $param['order_start_date']),
    	));

    	$minLimit = count($vacancy);
    	
    	for ($i = $param['order_start_date']; $i < $param['order_end_date']; $i+=15*60) {
    		if (isset($order_timely_limit[date('H:i', $i)]['limit'])) {
    			$limit = $order_timely_limit[date('H:i', $i)]['limit'];
    			//start check if seat is disabled
    			$min_seat = !empty($vacancy) ? min($vacancy) : 0;
    			if(!empty($param['new_seat'])){
    				$min_seat = $param['new_seat'];
    			}
    			if($limit < $min_seat){
    				return array();
    			}
    			//end
    			if ($minLimit > $limit) {
    				$minLimit = $limit;
    			}
    		}
    	}
    	return array_slice($vacancy, 0, $minLimit);
    }

    public static function get_available_vacancy_hp($shop, $param){

        if (!is_numeric($param['order_start_date'])) {
            $param['order_start_date'] = self::time_to_val($param['order_start_date']);
        }
        if (!is_numeric($param['order_end_date'])) {
            $param['order_end_date'] = self::time_to_val($param['order_end_date']);
        }
        if ($param['order_end_date'] <= $param['order_start_date']) {
            $param['order_end_date'] = $param['order_start_date'] + 1;
        }
        $table = self::table();
        $query = DB::select(
            self::$_table_name . '.shop_id',
            self::$_table_name . '.order_start_date',
            self::$_table_name . '.order_end_date',
            self::$_table_name . '.seat_id'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
            ->where(self::table() . '.disable', '0')
            ->where(
                DB::expr("
                    IFNULL({$table}.is_cancel, 0) = 0
                    AND IFNULL({$table}.is_autocancel, 0) = 0
                ")
        	)
            ->and_where_open()
            ->where_open()
            ->where(self::$_table_name . '.order_start_date', '>=', $param['order_start_date'])
            ->where(self::$_table_name . '.order_start_date', '<', $param['order_end_date'])
            ->where_close()
            ->or_where_open()
            ->where(self::$_table_name . '.order_end_date', '>', $param['order_start_date'])
            ->where(self::$_table_name . '.order_end_date', '<=', $param['order_end_date'])
            ->or_where_close()
            ->and_where_close()
            ->order_by(self::$_table_name . '.seat_id');
        if (!empty($param['not_in_order_id'])) {
            $query->where(self::table() . '.id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
        }
        $orders = $query->execute()->as_array();
        $vacancy = array();
        $max_seat = $shop->get('max_seat');
        $hp_max_seat = $shop->get('hp_max_seat');

        // get vacancy of HP
        for ($i = $max_seat+1; $i <= ($max_seat+$hp_max_seat); $i++) {
            $find = array_search($i, array_column($orders, 'seat_id'));
            if ($find === false) {
                $vacancy[$i] = $i;
            }
        }

        // get vacancy of FN
        for ($i = 1; $i <= $shop->get('max_seat'); $i ++) {
            $find = array_search($i, array_column($orders, 'seat_id'));
            if ($find === false) {
                $vacancy[$i] = $i;
            }
        }

        // check by time
        list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list(array(
                'shop_id' => $param['shop_id'],
                'date' => date('Y-m-d', $param['order_start_date'])
            )
        );

        // find min vacancy position
        $minLimit = count($vacancy);
        for ($i = $param['order_start_date']; $i <= $param['order_end_date']; $i+=15*60) {
            if (isset($order_timely_limit[date('H:i', $i)]['limit'])) {
                if ($minLimit > $order_timely_limit[date('H:i', $i)]['limit']) {
                    $minLimit = $order_timely_limit[date('H:i', $i)]['limit'];
                }
            }
        }

        // has vacancy
        if (count($vacancy) > 0) {
            return array_slice($vacancy, 0, $minLimit);
        }

        // has not vacancy
        $seats = array_map(function($arr) {
            return $arr['seat_id'];
        }, $orders);
        return array(max($seats) + 1);
    }

    /**
     * Check duplicate order date for seat
     *
     * @author diennvt
     * @param array $param Input data
     * @return bool True if duplicate | no vacancy otherwise False
     */
    public static function check_duplicate_order_date_seatlist($param, $order_id = '')
    {
    	if(empty($param['is_seat'])){
    		return false;
    	} 	
    	$vacancy = self::check_duplicated_seat_ignore_block(array(
    			'shop_id' => $param['shop_id'],
    			'not_in_order_id' => $order_id,
    			'order_start_date' => $param['order_start_date'],
    			'order_end_date' => $param['order_end_date'],
    			'is_seat' => !empty($param['is_seat']) ? $param['is_seat'] : 0,
    			'hp_order_code' => $param['hp_order_code'],
    	));
        
        if (empty($vacancy)) {
            return true;
        }
    	if (!empty($param['seat_id']) && !in_array($param['seat_id'], $vacancy)) {
    		return true;
    	}
    	return false;
    }

    /**
    * Update order by counter_code
    *
    * @author truongnn
    * @param array $param Input data
    * @return boolean true if update successfully and otherwise.
    */
    public static function update_order_by_counter_code($param) {
        $date = date('Y-m-d');
        if ($order = self::check_exist_order_by_couter_code($param)) {
            $order = Model_Order::find($order['id']);
            $user = Model_User::find($param['user_id']);
            $order->set('user_id', $user->get('id'));
            $order->set('user_code', $user->get('code'));
            $order->set('visit_element', $user->get('visit_element'));
            $order->set('email', $user->get('email'));
            $order->set('user_name', $user->get('name'));
            $order->set('kana', $user->get('kana'));
            $order->set('phone', $user->get('phone'));
            $order->set('sex', $user->get('sex'));
            $order->set('birthday', $user->get('birthday'));
            $order->set('prefecture_id', $user->get('prefecture_id'));
            $order->set('address1', $user->get('address1'));
            $order->set('address2', $user->get('address2'));
            if (!$order->save()) {
                \LogLib::warning('Can not update orders.user_id by shop_counter_code', __METHOD__, $param);
                return false;
            }
            return $order;
        }
        static::errorNotExist('shop_counter_code');
        return false;
    }

    /**
     * Check exist order.
     *
     * @param array $param Input array.
     * @return array Order info
     */
    public static function check_exist_order_by_couter_code($param) {
        $date = date('Y-m-d');
        $order = DB::select()
            ->from(self::$_table_name)
            ->where('shop_counter_code', $param['shop_counter_code'])
            ->where('disable', '0')
            ->where(DB::expr("
                FROM_UNIXTIME(reservation_date, '%Y-%m-%d') = '{$date}'
                AND IFNULL(user_id, 0) = 0
            "))
//            ->where(DB::expr("
//                reservation_date >= UNIX_TIMESTAMP()
//                AND IFNULL(orders.user_id, 0) = 0
//            "))
            ->execute()
            ->offsetGet(0);
        return $order;
    }

    /**
     * Generate shop counter code
     *
     * @author Le Tuan Tu
     * @param int $date Input data
     * @return string shop counter code
     */
    public static function generate_shop_counter_code($date)
    {       
        return '';
        if (!is_numeric($date)) {
            $date = self::time_to_val($date);
        }
        $shop_counter_code = Lib\Arr::field(DB::select(               
                'shop_counter_code'               
            )
            ->from(self::$_table_name)
            ->where(DB::expr("FROM_UNIXTIME(reservation_date, '%Y-%m-%d') = FROM_UNIXTIME({$date}, '%Y-%m-%d')"))            
            ->where(DB::expr("IFNULL(shop_counter_code,'') <> ''"))
            ->order_by('shop_counter_code', 'ASC')
            ->execute()
            ->as_array(), 'shop_counter_code');          
        if (!empty($shop_counter_code)) {
            $end_shop_counter_code = end($shop_counter_code);
            $code = substr($end_shop_counter_code, 0, 1) . (intval(substr($end_shop_counter_code, -3)) + 1);
        }
        while (!isset($code) || (isset($code) && in_array($code, $shop_counter_code))) {
            $code = Lib\str::randomStr(1, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') . Lib\str::randomStr(3, '0123456789');
        }     
        return $code;
        
        $date = date('Y-m-d', $date);
        $checkExist = true;
        $i = 0;
        
        while ($checkExist) {
            $i++;
            $code = Lib\str::randomStr(1, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') . Lib\str::randomStr(3, '0123456789');
            $checkExist = DB::select(
                    'id',
                    'hp_order_code',
                    'reservation_date'
                )
                ->from(self::$_table_name)
                ->where(DB::expr("DATE(FROM_UNIXTIME(reservation_date)) = '" . $date . "'"))
                ->where('shop_counter_code', '=', $code)
                ->execute()
                ->offsetGet(1);
            \LogLib::info('check_sql['.$i.'] ', __METHOD__, DB::last_query());
        }
        return $code;
    }


    /**
     * Send mail thanks
     *
     * @author CaoLP
     * @param
     * @return
     */
    public static function send_mail_thanks()
    {
        $orders = DB::select(
                array(self::$_table_name . '.id', 'id'),
                array(self::$_table_name . '.id', 'order_id'),
                array(self::$_table_name . '.user_id', 'user_id'),
                array(self::$_table_name . '.user_name', 'user_name'),
                array(self::$_table_name . '.phone', 'phone'),
                array(self::$_table_name . '.email', 'email'),
                array(self::$_table_name . '.shop_id', 'shop_id'),
                array(self::$_table_name . '.request', 'request'),
                array(self::$_table_name . '.total_price', 'total_price'),
                array(self::$_table_name . '.total_tax_price', 'total_tax_price')
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.disable', '=' , 0)
            ->where(self::$_table_name . '.is_paid', '=' , 1)
            ->where(DB::expr(self::$_table_name . ".reservation_date >= (UNIX_TIMESTAMP() - 1*60*60)"))
            ->where(DB::expr(self::$_table_name . ".id NOT IN ( SELECT order_id
                                                                FROM mail_send_logs
                                                                WHERE type = 2)
                    AND IFNULL(email,'') <> ''
                ")
            )
            ->order_by(self::$_table_name . '.reservation_date', 'ASC')
            ->limit(\Config::get('limit_email_thanks'))
            ->execute()
            ->as_array();
        $result = array();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                \LogLib::info('BEGIN: Send thanks email:', __METHOD__, $order['email']);
                if (!\Lib\Email::sendThanksEmail($order)) {
                    $result[$order['email']] = 'FAIL';
                    \LogLib::warning('Can not send thanks email:', __METHOD__, $order['email']);
                } else {
                    $result[$order['email']] = 'OK';
                    \LogLib::info('END: Send thanks email:', __METHOD__, $order['email']);
                }
            }
        }
        return $result;
    }

    /**
     * Send mail thanks
     *
     * @author CaoLP
     * @param
     * @return
     */
    public static function send_mail_reminders()
    {
        $orders = DB::select(
                array(self::$_table_name . '.id', 'order_id'),
                array(self::$_table_name . '.user_id', 'user_id'),
                array(self::$_table_name . '.user_name', 'user_name'),
                array(self::$_table_name . '.phone', 'phone'),
                array(self::$_table_name . '.email', 'email'),
                array(self::$_table_name . '.shop_id', 'shop_id'),
                array(self::$_table_name . '.request', 'request'),
                array(self::$_table_name . '.total_price', 'total_price'),
                array(self::$_table_name . '.total_tax_price', 'total_tax_price'),
                array('order_logs.nailists', 'nailists'),
                array('shops.name', 'shop_name'),
                array('shops.address', 'shop_address'),
                array('shops.map_url', 'shop_map_url'),
                DB::expr("FROM_UNIXTIME(" . self::$_table_name . ".reservation_date,  '%Y年%m月%d日') AS day"),
                DB::expr("FROM_UNIXTIME(" . self::$_table_name . ".reservation_date,  '%W') AS day_of_week"),
                DB::expr("FROM_UNIXTIME(" . self::$_table_name . ".reservation_date,  '%k:%i') AS hour")
            )
            ->from(self::$_table_name)
            ->join("shops", 'LEFT')
            ->on(self::$_table_name . '.shop_id', '=', 'shops.id')
            ->join(DB::expr("(
                SELECT  order_logs.order_id,
                        GROUP_CONCAT(nailists.name SEPARATOR  ', ') AS nailists
                FROM    order_logs JOIN nailists ON order_logs.nailist_id = nailists.id
                WHERE   nailists.disable = 0
                    AND order_logs.disable = 0
                GROUP BY order_logs.order_id
            ) AS order_logs"), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'order_logs.order_id')
            ->where(self::$_table_name . '.disable', 0)           
            ->where(self::$_table_name . '.is_cancel', 0)
            ->where(DB::expr("FROM_UNIXTIME(" . self::$_table_name . ".reservation_date, '%Y-%m-%d') = adddate(CURDATE(),1)"))
            ->where(DB::expr(
                    self::$_table_name . ".id NOT IN (SELECT order_id
                                                      FROM mail_send_logs
                                                      WHERE type = 3)
                    AND IFNULL(email,'') <> ''
                ")
            )
            ->order_by(self::$_table_name . '.reservation_date', 'ASC')
            ->limit(\Config::get('limit_email_reminder'))
            ->execute()
            ->as_array();
        $result = array();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                switch ($order['day_of_week']) {
                    case "Sunday":
                        $order['day_of_week'] = '日';
                        break;
                    case "Monday":
                        $order['day_of_week'] = '月';
                        break;
                    case "Tuesday":
                        $order['day_of_week'] = '火';
                        break;
                    case "Wednesday":
                        $order['day_of_week'] = '水';
                        break;
                    case "Thursday":
                        $order['day_of_week'] = '木';
                        break;
                    case "Friday":
                        $order['day_of_week'] = '金';
                        break;
                    case "Saturday":
                        $order['day_of_week'] = '土';
                        break;
                }
                $order['reservation_date'] = $order['day'].'（'.$order['day_of_week'].'）'.$order['hour'];
                unset($order['day']);
                unset($order['day_of_week']);
                unset($order['hour']);
                \LogLib::info('BEGIN: Send reminder email:', __METHOD__, $order['email']);
                if (!\Lib\Email::sendReminderEmail($order)) {
                    $result[$order['email']] = 'FAIL';
                    \LogLib::warning('Can not send reminder email:', __METHOD__, $order['email']);
                } else {
                    $result[$order['email']] = 'OK';
                    \LogLib::info('END: Send reminder email:', __METHOD__, $order['email']);
                }
            }
        }
        return $result;
    }

    /**
     * Check exist order.
     *
     * @param array $param Input array.
     * @return array Order info
     */
    public static function get_max_seat_realtime($param) {
        if (empty($param['shop_id']) || empty($param['date'])) {
            return 0;
        }
        $query = DB::select(DB::expr("
                            CASE
                                WHEN MAX(seat_id) <= max_seat + hp_max_seat OR orders.id IS NULL THEN max_seat + hp_max_seat
                                ELSE MAX(seat_id)
                            END AS real_max_seat
                    FROM    shops LEFT JOIN orders ON shops.id = orders.shop_id
                    WHERE   orders.disable = 0
                            AND IFNULL(orders.is_cancel, 0) = 0
                            AND shops.id = " . $param['shop_id'] . "
                            AND DATE(FROM_UNIXTIME(reservation_date)) = '" . $param['date'] . "'
            ")
        );
        $total = $query->execute()->offsetGet(0)['real_max_seat'];
        return !empty($total) ? $total : 0;
    }

    /**
     * Add or update info for Order by admin
     *
     * @author KhoaTX
     * @param array $param Input data
     * @return int|bool Order id or false if error, -1 if reservation_date is duplicated
     */
    public static function admin_add_update($param) {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $order = new self;
        // check exist
        if (!empty($id)) {
            $param['not_in_order_id'] = $id;
            $order = self::find($id);
            if (empty($order)) {
                self::errorNotExist('order_id');
                return false;
            }
        }
        if (isset($param['off_nails']) && $param['off_nails'] !== '') {
            $order->set('off_nails', $param['off_nails']);
        }
        if (isset($param['nail_type']) && $param['nail_type'] !== '') {
            $order->set('nail_type', $param['nail_type']);
        }
        if (isset($param['nail_length']) && $param['nail_length'] !== '') {
            $order->set('nail_length', $param['nail_length']);
        }
        if (isset($param['nail_add_length']) && $param['nail_add_length'] !== '') {
            $order->set('nail_add_length', $param['nail_add_length']);
        } 
        if (isset($param['service']) && $param['service'] !== '') {
            $order->set('service', $param['service']);
        }
        if (isset($param['problem'])) {
            $order->set('problem', $param['problem']);
        }
        if (isset($param['request'])) {
            $order->set('request', $param['request']);
        }
        // save to database
        if ($order->save()) {
            if (empty($order->id)) {
                $order->id = self::cached_object($order)->_original['id'];
            }
            // save order's items
            if (isset($param['items_id'])) {
                if (!Model_Order_Item::add_update_by_order_id(array(
                    'order_id' => $order->id,
                    'item_id' => $param['items_id'],
                    'item_quantity' => isset($param['items_quantity']) ? $param['items_quantity'] : ''
                ))) {
                    return false;
                }
                // re-update order's price
                Model_Order::updatePrice(array(
                    'order_id' => $order->id
                ));
                if (self::error()) {
                    return false;
                }
            }
            \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
            \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
            return $order->id;
        }
        return false;
    }

    /**
     * Update order info when select FE's nail from medical chart
     *
     * @author thailh
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function fe_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;

        // check exist
        $order = self::find($id);
        if (empty($order)) {
            self::errorNotExist('order_id');
            return false;
        }

        $user = Model_User::find($param['login_user_id']);
        if (empty($user) || !empty($user->get('disable'))) {
            self::errorNotExist('user_id');
            return false;
        }
        $order->set('user_id', $user->get('id'));
        $order->set('user_code', $user->get('code'));
        $order->set('visit_element', $user->get('visit_element'));
        $order->set('email', $user->get('email'));
        $order->set('user_name', $user->get('name'));
        $order->set('kana', $user->get('kana'));
        $order->set('phone', $user->get('phone'));
        $order->set('sex', $user->get('sex'));
        $order->set('birthday', $user->get('birthday'));
        $order->set('prefecture_id', $user->get('prefecture_id'));
        $order->set('address1', $user->get('address1'));
        $order->set('address2', $user->get('address2'));

        // save to database
        if (!$order->update()) {
           \LogLib::warning('Can not order for medical chart', __METHOD__, $param);
            return false;
        }
        // re-update order's price
        Model_Order::updatePrice(array(
            'order_id' => $order->id
        ));
        \Lib\Cache::delete('orders_count_add_nail_' . $order->get('shop_id'));
        \Lib\Cache::delete('orders_count_without_nailist_' . $order->get('shop_id'));
        return true;
    }
    //Mobile
    /**
     * get element for mobile
     *
     * @author CaoLP
     * @param array $param Input data
     * @return array
     */
    public static function get_mobile_element($param)
    {
        $order_param = array();
        $nail_param = array();
        if (isset($param['login_user_id']) && $param['login_user_id'] !== '') {
            $order_param['login_user_id'] = $param['login_user_id'];
            $nail_param['login_user_id'] = $param['login_user_id'];
        }
        $nail_detail = $order_detail = array();
        if (isset($param['order_id']) && $param['order_id'] !== '') {
            $order_detail = self::get_detail(array(
                'id' => $param['order_id'] 
            )); 
            if (!empty($order_detail)) {
                $order_detail = $order_detail->to_array();         
                $nails = Model_Order_Nail::get_all(array(
                    'order_id' => $param['order_id'] 
                ));
                if (!empty($nails[0]['nail_id'])) {
                    $param['nail_id'] = $nails[0]['nail_id'];
                }               
            }
        }
        if (isset($param['nail_id']) && $param['nail_id'] !== '') { 
            $nail_detail = Model_Nail::get_detail(array('id' => $param['nail_id']));            
        }
        $result = array(
            'service' => \Config::get('order_service'),
            'off_nails' => \Config::get('order_off_nails'),
            'off_nails_price' => \Config::get('order_off_nails_price'),
            'nail_type' => \Config::get('order_nail_type'),
            'nail_length' => \Config::get('order_nail_length'),
            'nail_add_length' => \Config::get('order_nail_add_length'),
            'nail_add_length_price' => \Config::get('order_nail_add_length_price'),
            'areas' => Model_Area::get_all(array('get_shop' => 1)),
            'order_detail' => $order_detail,
            'nail_detail' => $nail_detail
        );
        return $result;
    }

    /**
     * Undo Stopwatch
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Order id or false if error, -1 if reservation_date is duplicated
     */
    public static function undo_stopwatch($param)
    {
        $order = self::find($param['order_id']);
        if (empty($order)) {
            self::errorNotExist('order_id');
            return false;
        }
        
        $order_before_undo = array(
        		'order_id' => $order['id'],
        		'start_date' => !empty($order['start_date']) ? $order['start_date'] : '',
        		'consult_start_date' => !empty($order['consult_start_date']) ? $order['consult_start_date'] : '',
        		'consult_end_date' => !empty($order['consult_end_date']) ? $order['consult_end_date'] : '',
        		'off_start_date' => !empty($order['off_start_date']) ? $order['off_start_date'] : '',
        		'off_end_date' => !empty($order['off_end_date']) ? $order['off_end_date'] : '',
        		'sr_start_date' => !empty($order['sr_start_date']) ? $order['sr_start_date'] : '',
        		'sr_end_date' => !empty($order['sr_end_date']) ? $order['sr_end_date'] : '',
        		'is_paid' => !empty($order['is_paid']) ? $order['is_paid'] : '',
        		'undo' => $param['undo']
        );
        
        if (isset($param['start_date'])) {
            $order->set('start_date', $param['start_date'] !=='' ? intval($param['start_date']) : 0);
        }
        if (isset($param['consult_start_date'])) {
            $order->set('consult_start_date', $param['consult_start_date']!==''? intval($param['consult_start_date']) : 0);
        }
        if (isset($param['consult_end_date'])) {
            $order->set('consult_end_date',  $param['consult_end_date'] !== '' ?  intval($param['consult_end_date']) : 0);
        }
        if (isset($param['off_start_date'])) {
            $order->set('off_start_date', $param['off_start_date'] !== '' ? intval($param['off_start_date']) : 0);
        }
        if (isset($param['off_end_date']) ) {
            $order->set('off_end_date', $param['off_end_date'] !== '' ? intval($param['off_end_date']) : 0);
        }
        if (isset($param['sr_start_date'])) {
            $order->set('sr_start_date', $param['sr_start_date'] !== '' ? intval($param['sr_start_date']) : 0);
        }
        if (isset($param['sr_end_date'])) {
            $order->set('sr_end_date', $param['sr_end_date'] !== '' ? intval($param['sr_end_date']) : 0);
        }
        if (isset($param['is_paid'])) {
            $order->set('is_paid', $param['is_paid'] !== '' ? $param['is_paid'] : 0);
        }
        // save to database
        if ($order->save()) {
            $type = 0;
            switch ($param['undo']) {
                case 'start_date':
                    $type = 1;
                    break;
                case 'consult_start_date':
                    $type = 7;
                    break;
                case 'consult_end_date':
                    $type = 8;
                    break;
                case 'off_start_date':
                    $type = 2;
                    break;
                case 'off_end_date':
                    $type = 3;
                    break;
                case 'sr_start_date':
                    $type = 4;
                    break;
                case 'sr_end_date':
                    $type = 5;
                    break;
                case 'is_paid':
                    $type = 6;
                    break;
            }
            
            if (Model_Order_Nailist_Log::disable_by_type(array(
                'order_id' => $param['order_id'],
                'type'     => $type,
            ))) {
            	
            	// Update point
            	if($param['undo'] == 'is_paid' &&  !empty($param['is_update_point'])){
            		$r_point = intval($order->get('r_point'));
            		if($r_point > 0){
            			$option['where'] = array(
            					'id' => $order->get('user_id'),
            					'disable' => '0'
            			);
            			$user = Model_User::find('first', $option);
            			if (empty($user)) {
            				\LogLib::info('User not exists ', __METHOD__, $option);
            				return false;
            			}
            			$update_point = $user['point'] + $r_point;
            			if(!Model_User::update_point(array('user_id'=>$order->get('user_id'), 'point' => $update_point))){
            				\LogLib::info('Update_point failed ', __METHOD__, $update_point);
            				return false;
            			}
            		}
            	}
                return array('before' => $order_before_undo, 
                		'after' =>array(
	                    'order_id' => $order->get('id'),
	                    'start_date' => $order->get('start_date'),
	                    'consult_start_date' => $order->get('consult_start_date'),
	                    'consult_end_date' => $order->get('consult_end_date'),
	                    'off_start_date' => $order->get('off_start_date'),
	                    'off_end_date' => $order->get('off_end_date'),
	                    'sr_start_date' => $order->get('sr_start_date'),
	                    'sr_end_date' => $order->get('sr_end_date'),
	                    'is_paid' => $order->get('is_paid'),
	                	'undo' => 	$param['undo']
	                ) );
            }
        }
        return false;
    }
    
    /**
     * Get nail's attributes of order
     *
     * @author thailh
     * @param array $param Input data
     * @return array List attributes
     */
    public static function get_nail_attributes($param)
    { 
        $attributeTable = array(            
            'bullion_id' => array(
                'bullions',
                'order_nail_bullions', 
                'nail_bullions',                 
            ),
            'color_jell_id' => array(
                'color_jells',
                'order_nail_color_jells', 
                'nail_color_jells',                 
            ),
            'flower_id' => array(
                'flowers',
                'order_nail_flowers', 
                'nail_flowers',                 
            ),
            'shell_id' => array(
                'shells',
                'order_nail_shells', 
                'nail_shells',                 
            ),           
            'hologram_id' => array(
                'holograms',
                'order_nail_holograms', 
                'nail_holograms',                 
            ),
            'paint_id' => array(
                'paints',
                'order_nail_paints', 
                'nail_paints',                 
            ),
            'stone_id' => array(
                'stones',
                'order_nail_stones', 
                'nail_stones',                 
            ),
            'powder_id' => array(
                'powders',
                'order_nail_powders', 
                'nail_powders',                
            ),
            'rame_jell_id' => array(
                'rame_jells',
                'order_nail_rame_jells', 
                'nail_rame_jells',                 
            ),
        ); 
        
        if (empty($param['order_id'])) {
            self::errorParamInvalid('order_id');
            return false;
        }
        
        // check exist order nail
        $options['where'] = array(
            'order_id' => $param['order_id'],               
            'disable' => '0'
        );
        $order_nail = Model_Order_Nail::find('first', $options);
        if (empty($order_nail)) {
            static::errorNotExist('order_id_or_nail_id');
            return false;           
        }
       
        $attrIdField = $param['attribute_id'];
        if (empty($attributeTable[$attrIdField])) {            
            return array();           
        }       
        $order_nail_attribute = DB::select()
                ->from($attributeTable[$attrIdField][1])               
                ->where("order_id", $order_nail->get('order_id'))
                ->where("nail_id", $order_nail->get('nail_id'))
                ->limit(1)  
                ->execute()   
                ->offsetGet(0);
        $attrNailTableName = $attributeTable[$attrIdField][0]; 
        $data = array();
        if (!empty($order_nail_attribute)) {
            // is change nail's attributes
            $attrOrderNailTableName = $attributeTable[$attrIdField][1];
            $data = DB::select(                    
                    "{$attrOrderNailTableName}.order_id",
                    "{$attrNailTableName}.id",
                    "{$attrNailTableName}.name",
                    DB::expr("IF(ISNULL({$attrOrderNailTableName}.id),0,1) AS checked")
                )
                ->from($attrNailTableName)
                ->join(DB::expr("(
                        SELECT *
                        FROM {$attrOrderNailTableName}
                        WHERE order_id = {$order_nail->get('order_id')}
                            AND nail_id = {$order_nail->get('nail_id')}
                            AND disable = 0                       
                        ) {$attrOrderNailTableName}"), 'LEFT') 
                ->on("{$attrOrderNailTableName}.{$attrIdField}", "=", "{$attrNailTableName}.id")
                ->where("{$attrNailTableName}.disable", "0")
                ->order_by("{$attrNailTableName}.name", "ASC")
                ->execute()
                ->as_array();
        } else {
            // not change yet nail's attributes
            $attrOrderNailTableName = $attributeTable[$attrIdField][2];
            $data = DB::select(                    
                    "{$attrNailTableName}.id",
                    "{$attrNailTableName}.name",
                    DB::expr("IF(ISNULL({$attrOrderNailTableName}.id),0,1) AS checked")
                )
                ->from($attrNailTableName)
                ->join(DB::expr("(
                        SELECT *
                        FROM {$attrOrderNailTableName}
                        WHERE nail_id = {$order_nail->get('nail_id')}
                            AND disable = 0                        
                        ) {$attrOrderNailTableName}"), 'LEFT')               
                ->on("{$attrOrderNailTableName}.{$attrIdField}", "=", "{$attrNailTableName}.id")               
                ->where("{$attrNailTableName}.disable", "0")
                ->order_by("{$attrNailTableName}.name", "ASC")
                ->execute()
                ->as_array();
        }
        if (!empty($param['checked'])) {
            $data = \Lib\Arr::filter($data, 'checked', 1);
        }
        return $data;
    }
    
    /**
     * Update nail's attributes of order
     *
     * @author thailh
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_nail_attributes($param)
    { 
        if (empty($param['order_id'])) {
            self::errorParamInvalid('order_id');
            return false;
        }
        
        $attributeTable = array(            
            'bullion_id' => 'order_nail_bullions',
            'color_jell_id' => 'order_nail_color_jells',
            'flower_id' => 'order_nail_flowers',
            'shell_id' => 'order_nail_shells',           
            'hologram_id' => 'order_nail_holograms',
            'paint_id' => 'order_nail_paints',
            'stone_id' => 'order_nail_stones',
            'powder_id' => 'order_nail_powders',
            'rame_jell_id' => 'order_nail_rame_jells',
        );
        
        // attribute field name
        foreach ($param as $field => $value) {
            if (isset($attributeTable[$field])) {
                $attrFieldName = $field;
                $attrTableName = $attributeTable[$field];
            }
        }       
        
        // check exist order nail
        $options['where'] = array(
            'order_id' => $param['order_id'],           
            'disable' => '0'
        );
        $order_nail = Model_Order_Nail::find('first', $options);
        if (empty($order_nail)) {
            static::errorNotExist('order_id_or_nail_id');
            return false;
        }
      
        // disable all if attr is empty
        if (empty($param[$attrFieldName])) {
            $result = DB::update($attrTableName)
                ->value('disable', '1')
                ->where('order_id', '=', $order_nail->get('order_id'))
                ->where('nail_id', '=', $order_nail->get('nail_id'))
                ->execute(); 
            if (isset($param['get_atttibute'])) {
                return array();
            }
            return $result;
        }
        
        // get existing attrs
        $attrs = \Lib\Arr::key_values(
                DB::select()
                    ->from($attrTableName)
                    ->where('order_id', '=', $order_nail->get('order_id'))
                    ->where('nail_id', $order_nail->get('nail_id'))
                    ->execute()
                    ->as_array(),
                $attrFieldName
            );
        
        // prepare data for insert/update
        $attrIds = explode(',', $param[$attrFieldName]);        
        $dataUpdate = array();
        foreach ($attrIds as $attrId) {
            if (!empty($attrId)) {                 
                $dataUpdate[] = array(                   
                    'id' => !empty($attrs[$attrId]['id']) ? $attrs[$attrId]['id'] : 0,
                    'order_id' => $order_nail->get('order_id'),
                    'nail_id' => $order_nail->get('nail_id'),
                    $attrFieldName => $attrId,
                    'disable' => 0,
                );               
            }
        }        
        foreach ($attrs as $attrId => $attr) {
            if (!in_array($attrId, $attrIds)) { 
                $dataUpdate[] = array(                   
                    'id' => $attr['id'],
                    'order_id' => $order_nail->get('order_id'),
                    'nail_id' => $order_nail->get('nail_id'),
                    $attrFieldName => $attrId,
                    'disable' => 1,
                );
            }
        }
        
        // execute insert/update
        if (!empty($dataUpdate) && !parent::batchInsert(
            $attrTableName,
            $dataUpdate,
            array('disable' => DB::expr('VALUES(disable)')),
            false
        )) {
            \LogLib::warning('Can not update ' . $attrTableName, __METHOD__, $param);
            return false;
        }        
        if (isset($param['get_atttibute'])) {
            $param['checked'] = 1;
            return self::get_nail_attributes($param);                    
        }
        return true;
    }
    
    /**
     * Update nail's attributes of order
     *
     * @author thailh
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function get_all_by_user_id($param)
    {
        $user = Model_User::find($param['user_id']);
        if (empty($user)) {
            static::errorNotExist('user_id');
            return false;
        }
        
        $query = DB::select()
        ->from(self::$_table_name)
        ->where('disable', '0')
        ->where(self::$_table_name.".user_id","=",$param['user_id'])
        ->order_by('created', 'ASC');
        
        $data = $query->execute()->as_array();	
        return $data;
    }
    
    /**
     * Check many order is blocked seat or not
     *
     * @author diennvt
     * @param array $orders 
     * @param int $shop_id
     * @param str $date (Y-m-d)
     * @return bool Success or otherwise
     */
    public static function check_many_orders_is_blocked_seat(&$orders, $shop_id, $date)
    {
    	list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list(array(
            'shop_id' => $shop_id,
            'date' => $date,
    	));
        
        foreach($orders as $k => $order) {
            for ($i = $order['order_start_date']; $i < $order['order_end_date']; $i+=15*60) {
                if (isset($order_timely_limit[date('H:i', $i)])) {
                    $limit = '';
                    if(!empty($order['hp_order_code'])) { 
                        $limit = $order_timely_limit[date('H:i', $i)]['hp_limit'];
                        if(!empty($order['seat_id'])){
                            $orders[$k]['is_blocked_seat'] = ($order['seat_id'] - $order['max_seat']) > $limit ? 1 : 0;
                        }
                    }else {
                        $limit = $order_timely_limit[date('H:i', $i)]['limit'];
                        if(!empty($order['seat_id'])){
                            $orders[$k]['is_blocked_seat'] = $order['seat_id'] > $limit ? 1 : 0;
                        }
                        
                    }
                    if(!empty($orders[$k]['is_blocked_seat'])) {
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * Check 1 order is blocked seat or not
     *
     * @author diennvt
     * @param array $order 
     * @return bool Success or otherwise
     */
    public static function check_one_order_is_blocked_seat($order)
    {
    	list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list(array(
            'shop_id' => $order['shop_id'],
            'date' => date('Y-m-d', $order['order_start_date']),
    	));
        
        for ($i = $order['order_start_date']; $i < $order['order_end_date']; $i+=15*60) {
            if (isset($order_timely_limit[date('H:i', $i)]['limit'])) {
                $limit = $order_timely_limit[date('H:i', $i)]['limit'];
                if(!empty($order['seat_id']) && ($order['seat_id'] > $limit)){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Get list Order (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order
     */
    public static function batch_update_counter_code()
    {       
         $orders = self::find('all', array(
            'where' => array(
                array('reservation_date', '>=', self::date_from_val(date('Y-m-d'))),
                array('reservation_date', '<=', self::date_to_val(date('Y-m-d'))),
//                array('disable', '=', '0'),
//                array('is_cancel', '=', '0'),
//                array('is_autocancel', '=', '0'),
//                array('shop_counter_code', '=', NULL),
//                'or' => array(
//                    array('shop_counter_code', ''),
//                ),
            ),        
        ));       
        $result = array();
        foreach ($orders as $order) {
            $order->set('shop_counter_code', Model_Shop_Counter_Code::get_code(true));
            if (!empty($order->get('shop_counter_code'))) {    
                $result[$order->get('id')] = ($order->update()) ? 'OK' : 'FAIL';
            } else {
                $result[$order->get('id')] = 'EMPTY';
            }
        }
        return $result;
    }
    
    /**
     * Get calendar data for seat
     *
     * @author thailh
     * @param array $param Input data
     * @return array List Order
     */
    public static function date_calendar_for_seat($param)
    {  
        $shop = Model_Shop::find($param['shop_id']);
        if (empty($shop) || empty($param['shop_id'])) {
            static::errorNotExist('shop_id');
            return false;
        }
        if (empty($param['date'])) {
            $param['date'] = date('Y-m-d');
        } else {
            $param['date'] = date('Y-m-d', strtotime($param['date']));
        }
        if (!isset($param['is_seat'])) {
            $subQuery = "
                SELECT  orders.id,
                        orders.shop_id,
                        orders.user_name,
                        orders.kana,
                        GROUP_CONCAT(CONCAT('n_',order_logs.nailist_id) SEPARATOR  ',') as stylist_seat_keys,
                        orders.reservation_date,
                        orders.start_date,
                        orders.order_start_date,
                        orders.order_end_date,
                        orders.is_paid,
                        orders.is_cancel,
                        orders.is_autocancel,
                        orders.seat_id,
                        orders.visit_section,
                        orders.service,
                        IFNULL(orders.hp_order_code,0) as hp_order_code
                FROM orders
                    LEFT JOIN order_logs ON (order_logs.order_id = orders.id)
                WHERE order_logs.disable = 0
                    AND orders.disable = 0
                    AND IFNULL(orders.is_cancel,0) = 0
                    AND orders.shop_id = {$param['shop_id']}
                    AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
                GROUP BY orders.id
            ";
        } else {
            $subQuery = "
                SELECT  orders.id,
                        orders.shop_id,
                        orders.user_name,
                        orders.kana,
                        CONCAT('s_', seat_id) as stylist_seat_keys,
                        orders.reservation_date,
                        orders.start_date,
                        orders.order_start_date,
                        orders.order_end_date,
                        orders.is_paid,
                        orders.is_cancel,
                        orders.is_autocancel,
                        orders.seat_id,
                        orders.visit_section,
                        orders.service,
                        IFNULL(orders.hp_order_code,0) as hp_order_code
                FROM orders
                WHERE orders.disable = 0
                    AND IFNULL(orders.is_cancel,0) = 0
                    AND orders.shop_id = {$param['shop_id']}
                    AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
            ";
        }
        $query = DB::select(DB::expr("
                A.id,
                shop_id,
                user_name,
                kana,
                GROUP_CONCAT(stylist_seat_keys SEPARATOR  ',') as stylist_seat_keys,
        		reservation_date,
                start_date,
                order_start_date,
                order_end_date,
        		IFNULL(is_paid,0) AS is_paid,
                IFNULL(is_cancel,0) AS is_cancel,
                IFNULL(is_autocancel,0) AS is_autocancel,
                seat_id,
        		visit_section,
        		service,
        		hp_order_code,
                shops.max_seat,
                shops.hp_max_seat
            "),
            DB::expr("CASE WHEN seat_id > (shops.max_seat + shops.hp_max_seat) THEN 'extra-seat' ELSE ''  END AS status "),
        	DB::expr("IF(EXISTS(SELECT order_nails.id FROM order_nails WHERE order_nails.order_id = A.id), 1,0) AS has_nail "))
            ->from(DB::expr("(
                (
                    {$subQuery}
                )
                UNION
                (
                    SELECT  orders.id,
                            orders.shop_id,
                            orders.user_name,
                            orders.kana,
                            GROUP_CONCAT(CONCAT('d_',order_devices.device_id) SEPARATOR  ',') as stylist_seat_keys,
            				orders.reservation_date,
                            orders.start_date,
                            orders.order_start_date,
                            orders.order_end_date,
            				orders.is_paid,
                            orders.is_cancel,
                            orders.is_autocancel,
                            orders.seat_id,
                            orders.visit_section,
                            orders.service,
                            IFNULL(orders.hp_order_code,0) as hp_order_code
                    FROM orders
                        LEFT JOIN order_devices ON (order_devices.order_id = orders.id)
                    WHERE order_devices.disable = 0
                        AND orders.disable = 0
                        AND IFNULL(orders.is_cancel,0) = 0
                        AND orders.shop_id = {$param['shop_id']}
                        AND DATE(FROM_UNIXTIME(reservation_date)) = '{$param['date']}'
                    GROUP BY orders.id
                )) AS A
            "
        ))
        ->join('shops','LEFT')
        ->on('shop_id','=','shops.id')
        ->where('shop_id', '=', $param['shop_id'])
        ->where(DB::expr("DATE(FROM_UNIXTIME(reservation_date)) = " . "'" . $param['date'] . "'"));
        if (!empty($param['order_id'])) {
        	$query->where('A.id', '=', $param['order_id']);
        }
        if (isset($param['is_seat'])) {
            $query->where(DB::expr("(
                    IFNULL(seat_id,0) > 0
                    OR IFNULL(stylist_seat_keys,'') <> ''
                )")
            );
        } else {
            $query->where(DB::expr("IFNULL(stylist_seat_keys,'') <> ''"));
        }
        $query->group_by('id');
        $data = $query->execute()->as_array();
        self::check_many_orders_is_blocked_seat($data, $param['shop_id'], $param['date']);
        $arr_calendar = array();
        if (!empty($data)) {
            $i = 0;
            $curtime = time();
            foreach ($data as $value) {
                if (isset($value['id'])){
                    $arr_calendar[$i]['id'] = $value['id'];
                }
                if (isset($value['id'])){
                    $arr_calendar[$i]['serial_num'] = $value['id'];
                }
                if (isset($value['order_start_date'])){
                	$arr_calendar[$i]['start_at'] = $value['order_start_date'];
                    $arr_calendar[$i]['start_at_epoch'] = intval($value['order_start_date']);
                }
                if(!empty($value['is_blocked_seat'])){
                	$arr_calendar[$i]['is_blocked'] = 1;
                }else{
                	$arr_calendar[$i]['is_blocked'] = 0;
                }
                if(!empty($value['hp_order_code'])){
                	$arr_calendar[$i]['is_hp'] = 1;
                }else{
                	$arr_calendar[$i]['is_hp'] = 0;
                }
                if (!empty($value['is_paid'])) {
                    $arr_calendar[$i]['status'] = 'is_paid';
                } elseif (!empty($value['start_date'])) {
                    $arr_calendar[$i]['status'] = 'start_date';
                } elseif (!empty($value['is_autocancel'])) { //$value['order_end_date'] < $curtime
                    $arr_calendar[$i]['status'] = 'not_come';
                } else {
                    $arr_calendar[$i]['status'] = !empty($value['status'])? $value['status']:'accepted';
                }
                if (isset($value['order_start_date']) && isset($value['order_end_date'])){
                    $arr_calendar[$i]['duration'] = $value['order_end_date'] - $value['order_start_date'];
                }
                if (!empty($value['user_name'])) {
//                     if (\Lib\Str::is_japanese($value['user_name'])) {
//                         $arr_calendar[$i]['customer_name'] = mb_substr($value['user_name'], 0, 2, "UTF-8");
//                     } else {
//                         $arr_calendar[$i]['customer_name'] = \Lib\Str::truncate($value['user_name'], 10);
//                     }
                	$arr_calendar[$i]['customer_name'] = $value['user_name'];
                }
                if (!empty($value['kana'])) {
                    $arr_calendar[$i]['customer_kana'] = $value['kana'];
                }
                if(isset($value['visit_section'])){
                	$arr_calendar[$i]['visit_section'] = !empty($value['visit_section']) ? 1: 0;
                }
                if(!empty($value['id'])){
                    $services = \Model_Order_Service::get_all(array('order_id' => $value['id']));
                    $arr_calendar[$i]['service_name']  = \Lib\Arr::field($services, 'name', true);
                }
                if(isset($value['has_nail'])){
                	$arr_calendar[$i]['has_nail'] = $value['has_nail'];
                }
                if (isset($value['stylist_seat_keys'])) {
                    $stylist_seat_keys = explode(',', $value['stylist_seat_keys']);
                    $arr = array();
                    foreach ($stylist_seat_keys as $value) {
                        $arr[] = $value ;
                    }
                    $arr_calendar[$i]['stylist_seat_keys'] = $arr;
                }
                $arr_calendar[$i]['points'] = '';
                $arr_calendar[$i]['customer_slug'] = '';
                $arr_calendar[$i]['orders'] = array();
                $arr_calendar[$i]['has_memo'] = false;
                $arr_calendar[$i]['has_requested_stylist'] = false;
                $arr_calendar[$i]['is_online'] = false;
                $arr_calendar[$i]['is_online_or_provider'] = false;
                $arr_calendar[$i]['error'] = false;
                $i++;
            }
        }
        $calendar_json = json_encode($arr_calendar);   
        list($cnt, $order_timely_limit) = Model_Order_Timely_Limit::get_list($param);
        return array(
            'shop' => $shop,
            'calendar_json' => $calendar_json,
            'max_real_seat' => self::get_max_seat_realtime($param),
            'order_timely_limit' => $order_timely_limit,
        );
    }
    
    public static function check_duplicated_seat_ignore_block($param){
    	$shop = Model_Shop::find($param['shop_id']);
    	if (empty($shop) || empty($param['shop_id'])) {
    		static::errorNotExist('shop_id');
    		return false;
    	}
    	if (!is_numeric($param['order_start_date'])) {
    		$param['order_start_date'] = self::time_to_val($param['order_start_date']);
    	}
    	if (!is_numeric($param['order_end_date'])) {
    		$param['order_end_date'] = self::time_to_val($param['order_end_date']);
    	}
    	if ($param['order_end_date'] <= $param['order_start_date']) {
    		$param['order_end_date'] = $param['order_start_date'] + 1;
    	}
    	$table = self::table();
    	$query = DB::select(
    			self::$_table_name . '.shop_id',
    			self::$_table_name . '.order_start_date',
    			self::$_table_name . '.order_end_date',
    			self::$_table_name . '.seat_id'
    	)
    	->from(self::$_table_name)
    	->where(self::$_table_name . '.shop_id', '=', $param['shop_id'])
    	->where(self::table() . '.disable', '0')
    	->where(
    			DB::expr("
    					IFNULL({$table}.is_cancel, 0) = 0
    					AND IFNULL({$table}.is_autocancel, 0) = 0
    					")
    	)
    	->and_where_open()
    	->where_open()
    	->where(self::$_table_name . '.order_start_date', '>=', $param['order_start_date'])
    	->where(self::$_table_name . '.order_start_date', '<', $param['order_end_date'])
    	->where_close()
    	->or_where_open()
    	->where(self::$_table_name . '.order_end_date', '>', $param['order_start_date'])
    	->where(self::$_table_name . '.order_end_date', '<=', $param['order_end_date'])
    	->or_where_close()
        ->or_where_open()
    	->where(self::$_table_name . '.order_start_date', '<', $param['order_start_date'])
        ->where(self::$_table_name . '.order_end_date', '>', $param['order_end_date'])
    	->or_where_close()
    	->and_where_close()
    	->order_by(self::$_table_name . '.seat_id');
    	if (!empty($param['not_in_order_id'])) {
    		$query->where(self::table() . '.id', 'NOT IN', DB::expr("({$param['not_in_order_id']})"));
    	}
    
    	if (!empty($param['is_seat'])) {
    		$query->where(DB::expr("(
                    IFNULL(orders.seat_id,0) > 0
                )")
    		);
    	}
    	$orders = $query->execute()->as_array();
    	
        if (empty($param['hp_order_code'])) {
            $max = $shop->get('max_seat');
        } else {
            $max = self::get_max_seat_realtime(array(
                'shop_id' => $param['shop_id'],
                'date' => date('Y-m-d', $param['order_start_date']),
            ));            
        }
        $vacancy = array();
        $seat_ids = array_column($orders, 'seat_id');
    	for ($i = 1; $i <= $max; $i ++) {
    		$find = array_search($i, $seat_ids);
    		if ($find === false) {
    			$vacancy[$i] = $i;
    		}
    	}
        return $vacancy;
    }
    
    /**
     * Get list Order (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Order
     */
    public static function get_export($param)
    {
        $query = DB::select(
            'orders.id',
            DB::expr("
                CASE
                    WHEN orders.is_paid = 1 THEN 1
                    WHEN orders.is_cancel = 1 THEN 2
                    WHEN orders.is_autocancel = 1 THEN 3
                    WHEN orders.start_date IS NOT NULL THEN 4
                    ELSE 5
                END AS status
            "),
            'users.code',
            array('shops.id', 'shop_id'),
            array('shops.name', 'shop_name'),
            'orders.user_name',
            'orders.kana',
            'orders.sex',
            'orders.phone',
            'orders.email',
            'users.is_magazine',
            DB::expr("DATE(FROM_UNIXTIME(users.birthday)) AS birthday"),
            array('prefectures.name', 'prefecture_name'),
            'users.address1',
            'users.address2',
            DB::expr("DATE(FROM_UNIXTIME(orders.reservation_date)) AS reservation_date"),
            DB::expr("TIME(FROM_UNIXTIME(orders.reservation_date)) AS reservation_time"),
            DB::expr("DATE(FROM_UNIXTIME(orders.start_date)) AS start_date"),
            DB::expr("TIME(FROM_UNIXTIME(orders.start_date)) AS start_time"),
            array('users.visit_element','user_visit_element'),
            'orders.reservation_type',
            array('orders.visit_element','order_visit_element'),
            'orders.request',
            DB::expr("FROM_UNIXTIME(last_order.reservation_date) AS last_time"),
            DB::expr("FROM_UNIXTIME(second_last_order.reservation_date) AS second_last_time"),
            array('last_3_months_order.total', 'last_3_months'),
            array('last_6_months_order.total', 'last_6_months'),
            array('last_12_months_order.total', 'last_12_months'),
            'orders.is_cancel',
            array('auto_cancel_order.total', 'auto_cancel'),
            'orders.purpose',
            'orders.shop_counter_code',
            array('total_order.total', 'total_order'),
            'orders.total_price',
            'nails.photo_cd',
            array('order_nails.price', 'order_nail_price'),
            array('item_section_1_2.total_price', 'item_section_1_2'),
            array('item_section_3.total_price', 'item_section_3'),
            array('item_section_4.total_price', 'item_section_4'),
            'orders.r_point',
            array('users.point', 'user_point'),
            'orders.fn_point',
            DB::expr("
                CASE
                    WHEN new_or_repeat.total = 1 THEN 1
                    WHEN new_or_repeat.total > 1 THEN 2
                    ELSE 0
                END AS new_or_repeat
            "),
            DB::expr("
                CASE
                    WHEN orders.order_method = 1 THEN 1
                    ELSE 0
                END AS order_by_design
            "),
            DB::expr("
                CASE
                    WHEN orders.order_method = 2 THEN 1
                    ELSE 0
                END AS order_by_day
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(consultant_end.id, 0) <> 0 THEN consultant_end.nailist_id
                    ELSE consultant_start.nailist_id
                END AS consultant_nailist_id
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(consultant_end.id, 0) <> 0 THEN consultant_end.name
                    ELSE consultant_start.name
                END AS consultant_nailist_name
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(nail_off_end.id, 0) <> 0 THEN nail_off_end.nailist_id
                    ELSE nail_off_start.nailist_id
                END AS nail_off_nailist_id
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(nail_off_end.id, 0) <> 0 THEN nail_off_end.name
                    ELSE nail_off_start.name
                END AS nail_off_nailist_name
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(attend_end.id, 0) <> 0 THEN attend_end.nailist_id
                    ELSE attend_start.nailist_id
                END AS attend_nailist_id
            "),
            DB::expr("
                CASE
                    WHEN IFNULL(attend_end.id, 0) <> 0 THEN attend_end.name
                    ELSE attend_start.name
                END AS attend_nailist_name
            "),
            DB::expr("(SEC_TO_TIME(orders.sr_end_date - orders.start_date) + 0)/100 AS all_time"),
            DB::expr("TIME(FROM_UNIXTIME(orders.start_date)) AS start_time"),
            DB::expr("FROM_UNIXTIME(orders.consult_start_date) AS consult_start_date"),
            DB::expr("FROM_UNIXTIME(orders.consult_end_date) AS consult_end_date"),
            DB::expr("FROM_UNIXTIME(orders.off_start_date) AS off_start_date"),
            DB::expr("FROM_UNIXTIME(orders.off_end_date) AS off_end_date"),
            DB::expr("FROM_UNIXTIME(orders.sr_start_date) AS sr_start_date"),
            DB::expr("FROM_UNIXTIME(orders.sr_end_date) AS sr_end_date"),
            DB::expr("FROM_UNIXTIME(users.last_login) AS last_login"),
            'orders.user_id'
        )
            ->from(self::$_table_name)

            ->join('shops', 'LEFT')
            ->on(self::$_table_name.'.shop_id', '=', 'shops.id')

            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')

            ->join('order_nails', 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'order_nails.order_id')

            ->join('nails', 'LEFT')
            ->on('order_nails.nail_id', '=', 'nails.id')

            ->join('prefectures', 'LEFT')
            ->on('users.prefecture_id', '=', 'prefectures.id')

            ->join(DB::expr("
                (SELECT user_id,
                        MAX(reservation_date) AS reservation_date
                FROM orders
                WHERE disable = 0
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS last_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'last_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        MAX(reservation_date) AS reservation_date
                FROM
                    ( SELECT *
                     FROM orders
                     WHERE id NOT IN
                             ( SELECT A.id
                              FROM orders AS A
                              INNER JOIN
                                  (SELECT user_id,
                                          max(reservation_date) AS reservation_date
                                   FROM orders
                                   WHERE disable = 0
                                       AND user_id > 0
                                       AND is_paid = 1
                                   GROUP BY user_id
                                   ORDER BY reservation_date) AS B ON B.user_id = A.user_id
                              AND A.reservation_date = B.reservation_date )
                         AND orders.disable = '0'
                         AND orders.user_id > 0
                         AND is_paid = 1 ) orders
                WHERE disable = 0
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS second_last_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'second_last_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND DATE(FROM_UNIXTIME(reservation_date)) >= DATE(NOW() - INTERVAL 3 MONTH)
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS last_3_months_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'last_3_months_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND DATE(FROM_UNIXTIME(reservation_date)) >= DATE(NOW() - INTERVAL 6 MONTH)
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS last_6_months_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'last_6_months_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND DATE(FROM_UNIXTIME(reservation_date)) >= DATE(NOW() - INTERVAL 12 MONTH)
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS last_12_months_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'last_12_months_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND is_autocancel = 1
                    AND user_id > 0
                GROUP BY user_id) AS auto_cancel_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'auto_cancel_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND user_id > 0
                GROUP BY user_id) AS total_order
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'total_order.user_id')

            ->join(DB::expr("
                (SELECT user_id,
                        COUNT(*) AS total
                FROM orders
                WHERE orders.disable = 0
                    AND DATE(FROM_UNIXTIME(reservation_date)) <= DATE(NOW())
                    AND is_paid = 1
                    AND user_id > 0
                GROUP BY user_id) AS new_or_repeat
            "), 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'new_or_repeat.user_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 7) AS consultant_start
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'consultant_start.order_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 8) AS consultant_end
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'consultant_end.order_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 2) AS nail_off_start
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'nail_off_start.order_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 3) AS nail_off_end
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'nail_off_end.order_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 4) AS attend_start
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'attend_start.order_id')

            ->join(DB::expr("
                (SELECT order_nailist_logs.id,
                        order_id,
                        nailist_id,
                        name
                FROM order_nailist_logs
                LEFT JOIN nailists ON order_nailist_logs.nailist_id = nailists.id
                WHERE type = 5) AS attend_end
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'attend_end.order_id')

            ->join(DB::expr("
                (SELECT order_id,
                       SUM(order_items.item_price) AS total_price
                FROM order_items
                JOIN items ON order_items.item_id = items.id
                WHERE items.item_section IN (1,2)
                GROUP BY order_id) AS item_section_1_2
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'item_section_1_2.order_id')

            ->join(DB::expr("
                (SELECT order_id,
                       SUM(order_items.item_price) AS total_price
                FROM order_items
                JOIN items ON order_items.item_id = items.id
                WHERE items.item_section = 3
                GROUP BY order_id) AS item_section_3
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'item_section_3.order_id')

            ->join(DB::expr("
                (SELECT order_id,
                       SUM(order_items.item_price) AS total_price
                FROM order_items
                JOIN items ON order_items.item_id = items.id
                WHERE items.item_section = 4
                GROUP BY order_id) AS item_section_4
            "), 'LEFT')
            ->on(self::$_table_name.'.id', '=', 'item_section_4.order_id')

            ->where(self::$_table_name.'.disable', '=', '0');

        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name.'.reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name.'.reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        @ini_set('memory_limit', '-1');
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
} 
