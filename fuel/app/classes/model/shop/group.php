<?php

class Model_Shop_Group extends Model_Abstract
{
	/** @var array $_properties field of table */
    protected static $_properties = array(
		
		'id',
		'name',
        'address',
		'created',
		'updated',
		'disable',
	);
    /** @var array $_observers field of table */
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
    /** @var array $_table_name name of table */

	protected static $_table_name = 'shop_groups';
	/**
     * Add and update info of group.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $group = new self;
        if (!empty($id)) {
            $group = self::find($id);
            if (empty($group)) {
                static::errorNotExist('group_id', $id);

                return false;
            }
        }

        if (!empty($param['name'])) {
            $group->set('name', $param['name']);
        }
        if (!empty($param['address'])) {
            $group->set('address', $param['address']);
        }
        if ($group->save()) {
            if (empty($group->id)) {
                $group->id = self::cached_object($group)->_original['id'];
            }

            return !empty($group->id) ? $group->id : 0;
        }

        return false;
    }

    /**
     * Get detail group.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array|bool Detail  or false if error.
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('group_id');
            return false;
        }
        return $data;
    }

    /**
     * Disable/enable group.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $group = self::find($id);
            if ($group) {
                $group->set('disable', $param['disable']);
                if (!$group->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('group_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all group.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'id',
                'name'
            )
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get list tags.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (empty($param['page']) ) 
        {
            $param['page'] = 1;
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }
}
