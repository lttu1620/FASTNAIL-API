<?php

class Model_Shop_Counter_Code extends Model_Abstract
{
	/** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'code',        
        'is_used'
	);
    
    /** @var array $_observers field of table */
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
    
    /** @var array $_table_name name of table */
	protected static $_table_name = 'shop_counter_codes';
    	
    /**
     * get one counter code
     *
     * @author thailh
     * @param array $update_is_used Update is_used | or 
     * @return string code.
     */
    public static function get_code($update_is_used = false)
    {
        $self = self::find('first', array(
            'where' => array(
                array('is_used', '=', '0')
            )
        ));
        if (empty($self)) {
            return '';          
        }
        if ($update_is_used) {
            $self->set('is_used', '1');
            $self->update();
        }
        return $self->get('code');
    }
    
    /**
     * generate code
     *
     * @author thailh     
     * @return void
     */
    public static function create_code()
    {  
        for ($i = 0; $i < 26000; $i++) {
            $self = new self;
            $self->set('is_used', '0');
            $self->create();
        }        
    }
    
    /**
     * update is_used = 0 on begin of new day
     *
     * @author thailh     
     * @return void
     */
    public static function reset_code()
    {  
        return DB::update(self::$_table_name)
            ->value('is_used', '0')
            ->where('is_used', '1')
            ->where(DB::expr("code NOT IN (
                SELECT shop_counter_code 
                FROM orders 
                WHERE CURDATE() = FROM_UNIXTIME(reservation_date, '%Y-%m-%d')
            )"))
            ->execute();
    }

}
