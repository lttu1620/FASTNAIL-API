<?php

/**
 * Any query in Model Shop
 *
 * @package Model
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Shop extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'shop_group_id',
        'prefecture_id',
    	'area_id',
        'admin_id',
        'reservation_interval',
        'name',
        'phone',
        'address',
        'map_url',
        'open_time',
        'close_time',
        'is_designate',
        'is_plus',
        'order_daily_limit',
        'max_seat',
        'hp_max_seat',
        'created',
        'updated',
        'disable',
        'region',
        'zipcode',
        'workingtime',
        'offday',
        'image_url',
        'hpb_name',
        'is_franchise',
        'priority'
    );

    protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(               
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'shops';

    /**
     * Add or update info for Shop
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Shop id or false if error
     */
    public static function add_update($param)
    {   
        $id = !empty($param['id']) ? $param['id'] : 0;
        $shop = new self;
        $shop->set('prefecture_id', '0');
        // check exist
        if (!empty($id)) {
            $shop = self::find($id);
            if (empty($shop)) {
                self::errorNotExist('shop_id');
                return false;
            }
        }
        if (isset($param['shop_group_id'])) {
            $shop->set('shop_group_id', $param['shop_group_id']);
        }
        if (isset($param['area_id'])) {
            $shop->set('area_id', $param['area_id']);
        }
        if (isset($param['admin_id'])) {
            $shop->set('admin_id', $param['admin_id']);
        }
        $shop->set('reservation_interval', empty($param['reservation_interval']) ? 0 : $param['reservation_interval']);
        // set value
        if (!empty($param['name'])) {
            $shop->set('name', $param['name']);
        }
        if (isset($param['phone'])) {
            $shop->set('phone', $param['phone']);
        }
        if (isset($param['address'])) {
            $shop->set('address', $param['address']);
        }
        if (isset($param['map_url'])) {
            $shop->set('map_url', $param['map_url']);
        }
        if (isset($param['open_time']) && $param['open_time'] != '') {
            $shop->set('open_time', $param['open_time']);
        }
        if (isset($param['close_time']) && $param['close_time'] != '') {
            $shop->set('close_time', $param['close_time']);
        }        
        if (isset($param['is_plus']) && $param['is_plus'] == '1') {
            $shop->set('is_plus', 1);
            $shop->set('is_designate', '1');
        } else {
            $shop->set('is_plus', '0');
            $shop->set('is_designate', '0');
        }
        if (isset($param['max_seat']) && $param['max_seat'] != '') {
            $shop->set('max_seat', $param['max_seat']);
        }
        if (isset($param['hp_max_seat']) && $param['hp_max_seat'] != '') {
            $shop->set('hp_max_seat', $param['hp_max_seat']);
        }
        if (isset($param['region']) && $param['region'] != '') {
            $shop->set('region', $param['region']);
        }
        if (isset($param['zipcode']) && $param['zipcode'] != '') {
            $shop->set('zipcode', $param['zipcode']);
        }
        if (isset($param['workingtime']) && $param['workingtime'] != '') {
            $shop->set('workingtime', $param['workingtime']);
        }
        if (!empty($param['image_url']) && is_string($param['image_url'])) {
            $shop->set('image_url', $param['image_url']);
        }
        if (isset($param['offday']) && $param['offday'] != '') {
            $shop->set('offday', $param['offday']);
        }
        if (isset($param['priority']) && $param['priority'] !== '') {
            $shop->set('priority', $param['priority']);
        }
        if (isset($param['is_franchise']) && $param['is_franchise'] == '1')
            $shop->set('is_franchise', 1);
        else
            $shop->set('is_franchise', 0);
        // save to database
        if ($shop->save()) {
            if (empty($shop->id)) {
                $shop->id = self::cached_object($shop)->_original['id'];
            }
            return !empty($shop->id) ? $shop->id : 0;
        }
        return false;
    }

    /**
     * Get list Shop (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Shop
     */
    public static function get_list($param)
    {
        $query = DB::select(
            'shops.id',
            'shops.shop_group_id',
            'shops.prefecture_id',
            'shops.area_id',
            'shops.admin_id',
            'shops.reservation_interval',
            'shops.name',
            'shops.phone',
            'shops.address',
            'shops.map_url',
            'shops.open_time',
            'shops.close_time',
            'shops.is_designate',
            'shops.is_plus',
            'shops.order_daily_limit',
            'shops.max_seat',
            'shops.hp_max_seat',
            'shops.created',
            'shops.updated',
            'shops.disable',
            'shops.region',
            'shops.zipcode',
            'shops.workingtime',
            'shops.offday',
            'shops.is_franchise',
            DB::expr(
                "IFNULL(IF(shops.image_url='',NULL,shops.image_url),'".\Config::get(
                    'no_image_other'
                )."') AS image_url"
            ),
            array('admins.name', 'admin_name'),
            array('shop_groups.name', 'shop_group_name'),
        	array('areas.name', 'area_name')
        )
            ->from(self::$_table_name)
            ->join("admins", "LEFT")
            ->on(self::$_table_name . ".admin_id", '=', 'admins.id')
            ->join("areas", "LEFT")
            ->on(self::$_table_name . ".area_id", '=', 'areas.id')
            ->join("shop_groups", "LEFT")
            ->on(self::$_table_name . ".shop_group_id", '=', 'shop_groups.id');
        // filter by keyword
        if (isset($param['max_seat']) && $param['max_seat'] != '') {
            $query->where(self::$_table_name . '.max_seat', '=', $param['max_seat']);
        }
        if (isset($param['hp_max_seat']) && $param['hp_max_seat'] != '') {
            $query->where(self::$_table_name . '.hp_max_seat', '=', $param['hp_max_seat']);
        }
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['phone'])) {
            $query->where(self::$_table_name . '.phone', '=', $param['phone']);
        }
        if (!empty($param['address'])) {
            $query->where(self::$_table_name . '.address', 'LIKE', "%{$param['address']}%");
        }
        if (isset($param['area_id']) && $param['area_id'] != '') {
        	$query->where(self::$_table_name . '.area_id', '=', $param['area_id']);
        }
        if (isset($param['shop_group_id']) && $param['shop_group_id'] != '') {
            $query->where(self::$_table_name . '.shop_group_id', '=', $param['shop_group_id']);
        }
         if (!empty($param['open_time'])) {
            $query->where(self::$_table_name . '.open_time', '>=', $param['open_time']);
        }
        if (!empty($param['close_time'])) {
            $query->where(self::$_table_name . '.close_time', '<=', $param['close_time']);
        }
        if (isset($param['is_plus']) && $param['is_plus'] != '') {
            $query->where(self::$_table_name . '.is_plus', $param['is_plus']);
        }
        if (isset($param['region']) && $param['region'] != '') {
            $query->where(self::$_table_name . '.region', $param['region']);
        }
        if (isset($param['workingtime']) && $param['workingtime'] != '') {
            $query->where(self::$_table_name . '.workingtime', $param['workingtime']);
        }
        if (isset($param['zipcode']) && $param['zipcode'] != '') {
            $query->where(self::$_table_name . '.zipcode', $param['zipcode']);
        }
        if (isset($param['offday']) && $param['offday'] != '') {
            $query->where(self::$_table_name . '.offday', $param['offday']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (isset($param['is_franchise']) && $param['is_franchise'] != '') {
            $query->where(self::$_table_name . '.is_franchise', '=', $param['is_franchise']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $query->order_by(self::$_table_name . '.priority', 'DESC');
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Shop (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Shop
     */
    public static function get_all($param)
    {
        $query = DB::select(
            array(self::$_table_name.'.id', 'id'),
            array(self::$_table_name.'.shop_group_id', 'shop_group_id'),
            array(self::$_table_name.'.admin_id', 'admin_id'),
            array(self::$_table_name.'.reservation_interval', 'reservation_interval'),
            array(self::$_table_name.'.name', 'name'),
            array(self::$_table_name.'.phone', 'phone'),
            array(self::$_table_name.'.address', 'address'),
            array(self::$_table_name.'.is_designate', 'is_designate'),
            array(self::$_table_name.'.is_plus', 'is_plus'),
            array(self::$_table_name.'.max_seat', 'max_seat'),
            array(self::$_table_name.'.hp_max_seat', 'hp_max_seat'),
            array(self::$_table_name.'.zipcode', 'zipcode'),
            array(self::$_table_name.'.workingtime', 'workingtime'),
            array(self::$_table_name.'.offday', 'offday'),
            array(self::$_table_name.'.region', 'region'),
            array(self::$_table_name.'.open_time', 'open_time'),
            array(self::$_table_name.'.close_time', 'close_time'),
            array(self::$_table_name.'.map_url', 'map_url'),
            array(self::$_table_name.'.is_franchise', 'is_franchise'),
            array(self::$_table_name.'.priority', 'priority'),
            DB::expr(
                "IFNULL(IF(shops.image_url='',NULL,shops.image_url),'".\Config::get(
                    'no_image_other'
                )."') AS image_url"
            ),
            array('areas.id', 'area_id'),
            array('areas.name', 'area_name')
        )
            ->from(self::$_table_name)
            ->join("areas", "LEFT")
            ->on(self::$_table_name.".area_id", '=', 'areas.id')
            ->where(self::$_table_name.'.disable', '0');
        if (isset($param['shop_group_id']) && $param['shop_group_id'] !== '') {
            $query->where(self::$_table_name . '.shop_group_id', $param['shop_group_id']);
        }
        if (isset($param['region']) && $param['region'] !== '') {
            $query->where(self::$_table_name . '.region', $param['region']);
        }
        if (isset($param['area_id']) && $param['area_id'] !== '') {
            if (!is_array($param['area_id'])) {
                $param['area_id'] = array($param['area_id']);
            }
            $query->where(self::$_table_name . '.area_id', 'IN', $param['area_id']);
        }
        $query->order_by(self::$_table_name . '.priority', 'DESC');
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Shop
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $shop = self::find($id);
            if ($shop) {
                $shop->set('disable', $param['disable']);
                if (!$shop->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('shop_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Shop
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Shop or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('shop_id');
            return false;
        }
        return $data;
    }

    /**
     * Get list Shop's customer
     *
     * @author thailh
     * @param array $param Input data
     * @return array List Customer
     */
    public static function get_customer($param)
    {
        if (!empty($param['member'])) {
            $query = DB::select(
                    array('users.id', 'id'),
                    array('users.id', 'user_id'),
                    array('users.name', 'user_name'),
                    array('users.kana', 'kana'),
                    array('users.sex', 'sex'),
                    array('users.phone', 'phone'),
                    array('users.email', 'email'),
                    array('orders.id', 'order_id'),
                    array('orders.reservation_date', 'reservation_date'),
                    array(self::$_table_name . '.name', 'shop_name'),                  
                    array(self::$_table_name . '.id', 'shop_id'),  
                    array('order_nailists.nailists_name', 'nailists'),
                    DB::expr("IF(claims.order_id IS NULL,'無','有') AS as_claim_id")
                )                               
                ->from(DB::expr("(
                    SELECT user_id, max(id) AS order_id
                    FROM orders
                    WHERE disable = 0 
                    AND IFNULL(user_id, 0) > 0             
                    AND shop_id = {$param['shop_id']}             
                    GROUP BY user_id) AS max_orders
                "))
                ->join(DB::expr("(
                    SELECT DISTINCT order_id
                    FROM claims
                    WHERE disable = 0) AS claims
                "), 'LEFT')                        
                ->on('claims.order_id', '=', 'max_orders.order_id')                            
                ->join('users')
                ->on('users.id', '=', 'max_orders.user_id')
                ->join('orders')
                ->on('orders.id', '=', 'max_orders.order_id')
                ->join(self::$_table_name)    
                ->on(self::$_table_name . '.id', '=', 'orders.shop_id')
                ->join(DB::expr("(
                        SELECT  order_logs.order_id,
                                GROUP_CONCAT(nailists.name SEPARATOR  ', ') AS nailists_name
                        FROM (  SELECT DISTINCT order_id, nailist_id
                                FROM order_logs
                                WHERE order_logs.disable = 0                         
                                ORDER BY id DESC
                            ) order_logs
                        JOIN nailists ON order_logs.nailist_id = nailists.id
                        WHERE nailists.disable = 0
                        GROUP BY order_logs.order_id
                    ) AS order_nailists"), 'LEFT')
                ->on('order_nailists.order_id', '=', "max_orders.order_id");                    
            
            if (!empty($param['keyword'])) {
                $query->where(DB::expr("(
                        users.name LIKE '%{$param['keyword']}%' OR              
                        users.kana LIKE '%{$param['keyword']}%' OR                       
                        users.phone LIKE '%{$param['keyword']}%' OR                       
                        users.email LIKE '%{$param['keyword']}%'
                    )")
                );
            }            
        } else {
            $query = DB::select(                    
                    array('orders.user_id', 'id'),
                    array('orders.user_id', 'user_id'),
                    array('orders.user_name', 'user_name'),
                    array('orders.kana', 'kana'),
                    array('orders.sex', 'sex'),
                    array('orders.phone', 'phone'),
                    array('orders.email', 'email'),
                    array('orders.id', 'order_id'),
                    array('orders.reservation_date', 'reservation_date'),
                    array(self::$_table_name . '.name', 'shop_name'),                  
                    array(self::$_table_name . '.id', 'shop_id'),  
                    array('order_nailists.nailists_name', 'nailists'),
                    DB::expr("IF(claims.order_id IS NULL,'無','有') AS as_claim_id")
                )                
                ->from('orders')
                ->join(DB::expr("(
                    SELECT DISTINCT user_name, kana, sex, phone, email, max(id) AS order_id
                    FROM orders
                    WHERE disable = 0 
                    AND IFNULL(user_id, 0) = 0             
                    AND shop_id = {$param['shop_id']}             
                    AND (
                        IFNULL(user_name, '') <> ''   
                        OR IFNULL(kana, '') <> '' 
                        OR IFNULL(email, '') <> ''   
                    )
                    GROUP BY user_name, kana, sex, phone, email) AS max_orders
                "))
                ->on('orders.id', '=', 'max_orders.order_id')   
                ->join(DB::expr("(
                    SELECT DISTINCT order_id
                    FROM claims
                    WHERE disable = 0) AS claims
                "), 'LEFT')                        
                ->on('claims.order_id', '=', 'orders.id')               
                ->join(self::$_table_name)    
                ->on(self::$_table_name . '.id', '=', 'orders.shop_id')
                ->join(DB::expr("(
                        SELECT  order_logs.order_id,
                                GROUP_CONCAT(nailists.name SEPARATOR  ', ') AS nailists_name
                        FROM (  SELECT DISTINCT order_id, nailist_id
                                FROM order_logs
                                WHERE order_logs.disable = 0                         
                                ORDER BY id DESC
                            ) order_logs
                        JOIN nailists ON order_logs.nailist_id = nailists.id
                        WHERE nailists.disable = 0
                        GROUP BY order_logs.order_id
                    ) AS order_nailists"), 'LEFT')
                ->on('order_nailists.order_id', '=', "orders.id")
                ->where('orders.disable', '=', '0')                    
                ->where(DB::expr("IFNULL(orders.user_id, 0) = 0")); 
            if (!empty($param['keyword'])) {
                $query->where(DB::expr("(
                        orders.user_name LIKE '%{$param['keyword']}%' OR 
                        orders.kana LIKE '%{$param['keyword']}%' OR  
                        orders.phone LIKE '%{$param['keyword']}%' OR                       
                        orders.email LIKE '%{$param['keyword']}%'
                    )")
                );
            }  
        }
        if (!empty($param['date_from'])) {
            $query->where('orders.reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where('orders.reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        $query->order_by('orders.created', 'DESC');        
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();      
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);            
    }

     /**
     * Execute is_plus list Shop
     *
     * @author truongnn
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function plus($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $shop = self::find($id);
            if ($shop) {
                $shop->set('is_plus', $param['is_plus']);
                if (!$shop->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('is_plus');
                return false;
            }
        }
        return true;
    }

    /**
     * Get list order daily limits
     *
     * @param array $param Input data
     * @author thailh
     * @return array List order daily limits
     */
    public static function get_order_daily_limits($param) {
        if (empty($param['shop_id'])) {
            return false;
        }
        $query = DB::select(
                    "order_daily_limits.shop_id",
                    "order_daily_limits.date",                    
                    DB::expr("IF(IFNULL(`limit`,0) = 0, order_daily_limit, `limit`) AS `limit`"),
                    DB::expr("DATE(FROM_UNIXTIME(order_daily_limits.date)) AS date_ymd")
                )
                ->from('order_daily_limits')
                ->join('shops')
                ->on('order_daily_limits.shop_id', '=', 'shops.id')
                ->where('order_daily_limits.disable', '=', '0')
                ->where('order_daily_limits.shop_id', '=', $param['shop_id'])
                ->where('order_daily_limits.date', 'BETWEEN', array($param['date_from'], $param['date_to']));
        $data = $query->execute()->as_array();       
        return \Lib\Arr::key_values($data, 'date_ymd');        
    }
    
     /**
     * Get list order timely limits
     *
     * @param array $param Input data
     * @author thailh
     * @return array List order timely limits
     */
    public static function get_order_timely_limits($param) {
        if (empty($param['shop_id'])) {
            return false;
        }
        $query = DB::select(
                    "shop_id",
                    "time",                    
                    "limit",                    
                    "hp_limit",
                    DB::expr("FROM_UNIXTIME(time, '%Y-%d-%m %H:%i') AS ymdhi")                    
                )
                ->from('order_timely_limits') 
                ->where('disable', '=', '0')
                ->where('shop_id', '=', $param['shop_id'])
                ->where('time', 'BETWEEN', array($param['date_from'], $param['date_to']));
        $data = $query->execute()->as_array();        
        return \Lib\Arr::key_values($data, 'time');       
    }
    
}
