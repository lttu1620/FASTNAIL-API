<?php

/**
 * Any query in Model Point Get
 *
 * @package Model
 * @created 2015-08-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Point_Get extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'point',       
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'point_gets';

    /**
     * Add or update info for Point Get
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Point Get id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $self = new self;
        // check exist
        if (!empty($id)) {
            $self = self::find($id);
            if (empty($self)) {
                self::errorNotExist('point_get_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $self->set('name', $param['name']);
        }
        if (isset($param['point']) && $param['point'] != '') {
            $self->set('point', $param['point']);
        }        
        // save to database
        if ($self->save()) {
            if (empty($self->id)) {
                $self->id = self::cached_object($self)->_original['id'];
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Get list Point Get (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Point Get
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where(self::$_table_name.'.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['point'])) {
            $query->where(self::$_table_name.'.point', '=', $param['point']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Point Get (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Point Get
     */
    public static function get_all()
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.disable', '=', '0')
            ->order_by(self::$_table_name.'.id', 'ASC');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Point Get
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $self = self::find($id);
            if ($self) {
                $self->set('disable', $param['disable']);
                if (!$self->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('point_get_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Point Get
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Point Get or false if error
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name.'.id', '=', $param['id']);
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('point_get_id');
            return false;
        }
        return $data;
    }
    
}
