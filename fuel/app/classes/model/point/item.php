<?php

/**
 * Any query in Model Item Point
 *
 * @package Model
 * @created 2015-06-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Point_Item extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',        
        'name',
        'description',
        'image_url',
        'point',
        'limit_quantity',
        'start_date',
        'end_date',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'point_items';

    /**
     * Add or update info for Item Point
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Item Point id or false if error
     */
    public static function add_update($param)
    {        
        $id = !empty($param['id']) ? $param['id'] : 0;
        $point = new self;
        // check exist
        if (!empty($id)) {
            $point = self::find($id);
            if (empty($point)) {
                self::errorNotExist('id');
                return false;
            }
        }      
        // set value 
        if (isset($param['name'])) {
            $point->set('name', $param['name']);
        }
        if (isset($param['description'])) {
            $point->set('description', $param['description']);
        }
        if (isset($param['image_url']) && is_string($param['image_url'])) {
            $point->set('image_url', $param['image_url']);
        }
        if (isset($param['point'])) {
            if (empty($param['point'])) $param['point'] = 0;
            $point->set('point', $param['point']);
        }
        if (isset($param['start_date']) && $param['start_date'] !== '') {
            $point->set('start_date', self::time_to_val($param['start_date']));
        }
        if (isset($param['end_date']) && $param['end_date'] !== '') {
            $point->set('end_date', self::time_to_val($param['end_date']));
        }
        if (isset($param['limit_quantity'])) {
            if (empty($param['limit_quantity'])) $param['limit_quantity'] = 0;
            $point->set('limit_quantity', $param['limit_quantity']);
        }
        // save to database
        if ($point->save()) {
            if (empty($point->id)) {
                $point->id = self::cached_object($point)->_original['id'];
            }
            return !empty($point->id) ? $point->id : 0;
        }
        return false;
    }

    /**
     * Get list Item Point (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Item Point
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', '=', $param['id']);
        }        
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['start_date'])) {
            $query->where(DB::expr("
                DATE(FROM_UNIXTIME(item_points.start_date)) = '{$param['start_date']}'
            "));
        }
        if (!empty($param['end_date'])) {
            $query->where(DB::expr("
                DATE(FROM_UNIXTIME(item_points.end_date)) = '{$param['end_date']}'
            "));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.id', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }


    /**
     * Get list Item Point (using array count)
     *
     * @author Caolp
     * @param array $param Input data
     * @return array List Item Point
     */
    public static function get_list_for_mobile($param)
    {
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }                
        $query = DB::select(
                self::$_table_name . '.id',                
                self::$_table_name . '.name',
                self::$_table_name . '.description',
                self::$_table_name . '.image_url',
                self::$_table_name . '.point',                        
                DB::expr("IFNULL(limit_quantity,0) AS limit_quantity"),
                DB::expr("IFNULL(bought_quantity,0) AS bought_quantity"),
                DB::expr("IFNULL(limit_quantity,0) - IFNULL(bought_quantity,0) AS remain_quantity")
            )
            ->from(self::$_table_name)
            ->join(
				DB::expr("(
                        SELECT point_item_id, IFNULL(sum(quantity),0) AS bought_quantity
                        FROM user_bought_items
                        WHERE user_id = {$param['login_user_id']}
                            AND canceled IS NULL   
                        GROUP BY point_item_id                        
                    ) AS user_bought_items          
				"),
				'LEFT'
			)
            ->on(self::$_table_name . '.id', '=', 'user_bought_items.point_item_id')
            ->where(self::$_table_name . '.disable', 0)
            ->where(DB::expr("
                start_date <= UNIX_TIMESTAMP() 
                AND end_date >= UNIX_TIMESTAMP()
            "))
            ->order_by(self::$_table_name . '.id', 'DESC');        
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array('total' => $total, 'data' => $data);
    }

    
    /**
     * Get detail Item Point
     *
     * @author thailh
     * @param array $param Input data
     * @return array Item Point
     */
    public static function get_detail_for_mobile($param)
    {
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }                
        $query = DB::select(
                self::$_table_name . '.id',                
                self::$_table_name . '.name',
                self::$_table_name . '.description',
                self::$_table_name . '.image_url',
                self::$_table_name . '.point',                        
                DB::expr("IFNULL(limit_quantity,0) AS limit_quantity"),
                DB::expr("IFNULL(bought_quantity,0) AS bought_quantity"),
                DB::expr("IFNULL(limit_quantity,0) - IFNULL(bought_quantity,0) AS remain_quantity")
            )
            ->from(self::$_table_name)
            ->join(
				DB::expr("(
                        SELECT point_item_id, IFNULL(sum(quantity),0) AS bought_quantity
                        FROM user_bought_items
                        WHERE user_id = {$param['login_user_id']}
                            AND canceled IS NULL   
                        GROUP BY point_item_id
                    ) AS user_bought_items          
				"),
				'LEFT'
			)
            ->on(self::$_table_name . '.id', '=', 'user_bought_items.point_item_id')
            ->where(self::$_table_name . '.disable', 0)            
            ->where(self::$_table_name . '.id', $param['id']);
        $data = $query->execute()->offsetGet(0);       
        return $data;
    }
    
    /**
     * Get all Item Point (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Item Point
     */
    public static function get_all($param)
    {
        $query = DB::select(
                self::$_table_name . '.id',               
                self::$_table_name . '.name',
                self::$_table_name . '.description',
                self::$_table_name . '.image_url',
                self::$_table_name . '.point',
                self::$_table_name . '.limit_quantity'
            )
            ->from(self::$_table_name)
            ->where('disable', 0)            
            ->where(DB::expr("
                start_date <= UNIX_TIMESTAMP() 
                AND end_date >= UNIX_TIMESTAMP()
            ")); 
        if (!empty($param['limit'])) {            
            $query->limit($param['limit'])->offset(1);
        }
        $query->order_by(self::$_table_name . '.id', 'ASC');     
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Item Point
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $point = self::find($id);
            if ($point) {
                $point->set('disable', $param['disable']);
                if (!$point->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Item Point
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Item Point or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('id');
            return false;
        }
        return $data;
    }
    
}
