<?php
/**
 * Model_Service - Model to operate to Services's functions
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Service extends Model_Abstract
{
	/** @var array $_properties field of table */
    protected static $_properties = array(		
		'id',
		'name',
		'time',
        'device_type',
		'created',
		'updated',
		'disable',
	);
    /** @var array $_observers field of table */
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
    /** @var array $_table_name name of table */
    protected static $_table_name = 'services';
    
     /**
     * Add and update info of service.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {   
        $id = !empty($param['id']) ? $param['id'] : 0;
        $service = new self;
        if (!empty($id)) {
            $service = self::find($id);
            if (empty($service)) {
                static::errorNotExist('service_id', $id);
                return false;
            }
        }
        if (!empty($param['name'])) {
            $service->set('name', $param['name']);
        }
        if (isset($param['time']) && $param['time'] != '') {
            $service->set('time', $param['time']);
        }
        if (isset($param['device_type']) && $param['device_type'] != '') {
            $service->set('device_type', $param['device_type']);
        }
        if ($service->save()) {
            if (empty($service->id)) {
                $service->id = self::cached_object($service)->_original['id'];
            }
            return !empty($service->id) ? $service->id : 0;
        }
        return false;
    }

    /**
     * Get detail service.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return array|bool Detail  or false if error.
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('service_id');
            return false;
        }
        return $data;
    }

    /**
     * Disable/enable service.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $service = self::find($id);
            if ($service) {
                $service->set('disable', $param['disable']);
                if (!$service->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('service_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all service.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param = array())
    {
        $query = DB::select(
        		"id",
        		"name",
        		DB::expr(" time*60 as time"),
        		"device_type",
        		"created",
        		"updated",
        		"disable"
        		)
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get all service.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all_for_frontend($param)
    {
        $data = DB::select(
        		"id",
        		array("name_front", "name")
            )
            ->from(self::$_table_name)
            ->where('disable', '0')
            ->execute()
            ->as_array();
        return $data;
    }
    
    /**
     * Get list services.
     *
     * @author trungnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['time']) && $param['time'] != '') {
            $query->where(self::$_table_name .'.time', $param['time']);
        }
        if (isset($param['device_type']) && $param['device_type']) {
            $query->where(self::$_table_name .'.device_type', $param['device_type']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (empty($param['page']) ) 
        {
            $param['page'] = 1;
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }
}
