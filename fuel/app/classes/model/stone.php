<?php
/**
 * Any query in Model Stone.
 *
 * @package Model
 * @version 1.0
 * @author Vu LTH
 * @copyright Oceanize INC
 */
class Model_Stone extends Model_Abstract
{
	/** @var array $_properties field of table */
    protected static $_properties = array(
		
		'id',
		'name',
		'created',
		'updated',
		'disable',
	);
    /** @var array $_observers field of table */
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
    /** @var array $_table_name name of table */
	protected static $_table_name = 'stones';

	/**
     * Add and update info of stone.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $stone = new self;
        if (!empty($id)) {
            $stone = self::find($id);
            if (empty($stone)) {
                static::errorNotExist('stone_id', $id);

                return false;
            }
        }

        if (!empty($param['name'])) {
            $stone->set('name', $param['name']);
        }
        if ($stone->save()) {
            if (empty($stone->id)) {
                $stone->id = self::cached_object($stone)->_original['id'];
            }

            return !empty($stone->id) ? $stone->id : 0;
        }

        return false;
    }

    /**
     * Get detail stone.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array|bool Detail  or false if error.
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('stone_id');
            return false;
        }
        return $data;
    }

    /**
     * Disable/enable stone.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $stone = self::find($id);
            if ($stone) {
                $stone->set('disable', $param['disable']);
                if (!$stone->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('stone_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all stone.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'id',
                'name'
            )
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get list tags.
     *
     * @author Vu LTH
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (empty($param['page']) ) 
        {
            $param['page'] = 1;
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }
}
