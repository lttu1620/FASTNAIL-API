<?php

use Fuel\Core\DB;
use Lib\Util;
/**
 * Any query in Model Admin.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Admin extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'name',
		'login_id',
		'password',
        'admin_type',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load', 
                'after_create', 
                'after_update', 
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'admins';

    /**
     * Get list admin by login_id LIKE $param['login_id'].
     *
     * @author Le Tuan Tu
     * @param array array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
                    self::$_table_name . '.*',
                    array('shops.id','shop_id'),
                    array('shops.name','shop_name')
                )
                ->from(self::$_table_name);        
       
        $query->join("shops", "LEFT");
        $query->on(self::$_table_name.".id",'=','shops.admin_id');
       
        if (!empty($param['login_id'])) {
            $query->where('login_id', 'LIKE', "%{$param['login_id']}%");
        }
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', '=', $param['disable']);
        }
        if (isset($param['admin_type']) && $param['admin_type'] != '') {
            $query->where('admin_type', '=', $param['admin_type']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Disable/Enable a admin.
     *
     * @author Le Tuan Tu
     * @param array array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $admin = self::find($id);
            if ($admin) {
                $admin->set('disable', $param['disable']);
                if (!$admin->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('admin_id');
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for admin.
     *
     * @author Le Tuan Tu
     * @param array array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $options['where'] = array(
            'login_id' => $param['login_id'],
        );
        if (!empty($id)) {
            $admin = self::find($id);
            if (empty($admin)) {
                return false;
            }  
            $options['where'][] = array(
                'id', '<>', $id
            );
        }       
        $check = self::find('first', $options);             
        if (!empty($check) && $check->get('login_id') == $param['login_id']) {
            static::errorDuplicate('login_id', $param['login_id']);
            return false;
        }
        if (empty($admin)) {
            $admin = new self;  
        }
        if (isset($param['name'])) {
            $admin->set('name', $param['name']);
        }
        if (isset($param['login_id'])) {
            $admin->set('login_id', $param['login_id']);
        }
        if (isset($param['admin_type']) && $param['admin_type'] != '') {
            $admin->set('admin_type', $param['admin_type']);
        }
        if (isset($param['password'])) {
            $admin->set('password', Util::encodePassword($param['password'], $param['login_id']));
        }
        if ($admin->save()) {
            if (empty($admin->id)) {
                $admin->id = self::cached_object($admin)->_original['id'];
            }
            return !empty($admin->id) ? $admin->id : 0;
        }
        return false;
    }
    
    /**
     * Login for admin.
     *
     * @author diennvt - VuLTH
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function login($param)
    {  
        $param['password'] = Util::encodePassword($param['password'], $param['login_id']);
        $query = DB::select(
                    'admins.id',
                    'admins.name',
                    'admins.login_id',
                    'admins.admin_type',
                    'admins.created',
                    'shops.shop_group_id', 
                    array('shops.area_id' , 'shop_area_id'),
                    array('shops.prefecture_id' , 'shop_prefecture_id'),
                    array('shops.reservation_interval' ,'shop_reservation_interval'),
                    array('shops.id', 'shop_id'),
                    array('shops.name', 'shop_name'),
                    array('shops.phone', 'shop_phone'),
                    array('shops.address', 'shop_address'),
                    array('shops.is_designate', 'shop_is_designate'),
                    array('shops.open_time', 'shop_open_time'),
                    array('shops.close_time', 'shop_close_time'),                    
                    array('shops.is_plus', 'shop_is_plus'),
                    array('shops.disable', 'shop_disable'),
                    array('shops.max_seat', 'shop_max_seat'),
                    array('shops.hp_max_seat', 'shop_hp_max_seat'),
                    DB::expr("(IFNULL(shops.max_seat,0) + IFNULL(shops.hp_max_seat,0)) AS total_max_seat"),
                    array('shops.is_franchise', 'shop_is_franchise')
                )
                ->from(self::$_table_name)
                ->join("shops", "LEFT")
                ->on(self::$_table_name.".id", '=', 'shops.admin_id')
                ->where("admins.disable", '0')
                ->where(self::$_table_name.".login_id",'=',$param['login_id'])
                ->where(self::$_table_name.".password",'=',$param['password']);
        $data = $query->execute()->offsetGet(0);
        if (!empty($data) && $data['admin_type'] == 1 && empty($data['shop_id'])) {
            $shop = Model_Shop::find('first', array(
                'where' => array('disable' => '0'),
                'order_by' => array('id' => 'ASC'),
            ));
            if (!empty($shop)) {
                $data['shop_group_id'] = $shop->get('shop_group_id');
                $data['shop_area_id'] = $shop->get('area_id');
                $data['shop_prefecture_id'] = $shop->get('prefecture_id');
                $data['shop_reservation_interval'] = $shop->get('reservation_interval');
                $data['shop_id'] = $shop->get('id');
                $data['shop_name'] = $shop->get('name');
                $data['shop_phone'] = $shop->get('phone');
                $data['shop_address'] = $shop->get('address');
                $data['shop_is_designate'] = $shop->get('is_designate');
                $data['shop_open_time'] = $shop->get('open_time');
                $data['shop_close_time'] = $shop->get('close_time');
                $data['shop_is_plus'] = $shop->get('is_plus');
                $data['shop_disable'] = $shop->get('disable');
                $data['shop_max_seat'] = !empty($shop->get('max_seat')) ? $shop->get('max_seat') : 0;
                $data['shop_hp_max_seat'] = !empty($shop->get('hp_max_seat')) ? $shop->get('hp_max_seat') : 0;
                $data['total_max_seat'] = $data['shop_max_seat'] + $data['total_max_seat'];
            }
        }
        return $data;
    }

    /**
     * Get detail for admin.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail($param)
    {
        $param['password'] = Util::encodePassword($param['password'], $param['login_id']);
        $query = DB::select(
                self::$_table_name.".*",
                'authenticates.token'
                )
                ->from(self::$_table_name)
                ->join("authenticates", "LEFT")
                ->on(self::$_table_name.".id",'=','authenticates.user_id')
                ->where(self::$_table_name.".login_id",'=',$param['login_id'])
                ->where(self::$_table_name.".password",'=',$param['password'])
                ->where("authenticates.regist_type",'=','admin');
        
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $param['login_id']);
        }
        return !empty($data) ? $data : array();
    }

    /**
     * Update password for admin.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * 
     * @return bool Returns the boolean.
     */
    public static function update_password($param)
    {
        $admin = self::find($param['id']);
        if ($admin) {
            $admin->set('password', Util::encodePassword($param['password'], $admin->get('login_id')));
            if ($admin->update()) {
                return true;
            }
        }
        self::errorNotExist('admin_id');
        return false;
    }
    
    /**
     * Get all Admin (without array count)
     *
     * @author Hoang Gia Thong
     * @return array List Admin
     */
    public static function get_all($param) {
        $query = DB::select()
                ->from(self::$_table_name)
                ->where('disable', '=', '0')               
                ->where(self::$_table_name.".admin_type",'=',$param['admin_type']);
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }
}
