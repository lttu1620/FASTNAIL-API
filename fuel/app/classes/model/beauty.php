<?php

/**
 * Any query in Model User
 *
 * @package Model
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Beauty extends Model_Abstract
{
	/** @var array $_properties field of table */
	protected static $_properties = array(
		'id',
		'title',
		'description',
		'image_url',
		'home_url',
		'point_get_id',
		'created',
		'updated',
		'disable',
		'start_date',
		'end_date',
		'mission_type',
		'segment_sex',
		'segment_device',
		'segment_prefecture',
		'segment_user_generation',
		'detail_page_url',
		'distribution_limit',
		'popup',
		'popup_priority',
		'display_no',
		'token'
	);

	protected static $_observers = array(
        'Model\Observer_Log' => array(
            'events' => array(
                'after_load',               
                'after_create', 
                'after_update',
                'after_delete'
            ),
            'mysql_timestamp' => false,
        ),
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	/** @var array $_table_name name of table */
	protected static $_table_name = 'beauties';

	/**
	 * Add and update info of beauty.
	 *
	 * @author diennvt
	 * @param array $param Input data.
	 * @return bool|int Returns the boolean or the integer.
	 */
	public static function add_update($param)
	{
        if (empty($param['mission_type'])) {
            $param['point'] = 0;
            $param['point_get_id'] = 0;
        }
		$id = !empty($param['id']) ? $param['id'] : 0;		
		if (!empty($id)) {
			$beauty = self::find($id);
			if (empty($beauty)) {
				static::errorNotExist('beauty_id', $id);
				return false;
			}
            if (!empty($param['point'])) {
                $param['point_get_id'] = Model_Point_Get::add_update(array(
                    'id' => !empty($beauty['point_get_id']) ? $beauty['point_get_id'] : 0,
                    'name' => isset($param['title']) ? $param['title'] : '',
                    'point' => $param['point'],
                ));
            }
		} else {            
            $beauty = new self;
            if (!empty($param['point'])) {
                $param['point_get_id'] = Model_Point_Get::add_update(array(
                    'name' => isset($param['title']) ? $param['title'] : '',
                    'point' => $param['point'],
                ));
            }
        }
		if (isset($param['title'])) {
			$beauty->set('title', $param['title']);
		}
		if (isset($param['description'])) {
			$beauty->set('description', $param['description']);
		}
		if (isset($param['image_url'])) {
			$beauty->set('image_url', $param['image_url']);
		}
		if (isset($param['home_url'])) {
			$beauty->set('home_url', $param['home_url']);
		}
		if (isset($param['point_get_id'])) {
			$beauty->set('point_get_id', $param['point_get_id']);
		}
		if (isset($param['disable'])) {
			$beauty->set('disable', $param['disable']);
		}
        if (isset($param['start_date']) && $param['start_date'] !== '') {
            $beauty->set('start_date', self::time_to_val($param['start_date']));
        }
        if (isset($param['end_date']) && $param['end_date'] !== '') {
            $beauty->set('end_date', self::time_to_val($param['end_date']));
        }
        if (isset($param['mission_type']) && $param['mission_type'] !== '') {
            $beauty->set('mission_type', $param['mission_type']);
            if (empty($id) 
                && ($param['mission_type'] == \Config::get('beauties.type.answer_survey') 
                    || $param['mission_type'] == \Config::get('beauties.type.register_from_other_site'))) {
                $token = \Lib\str::generate_token_for_survey();
                $beauty->set('token', $token);
            }
        }
		if (isset($param['segment_sex']) && $param['segment_sex'] !== '') {
			$beauty->set('segment_sex', $param['segment_sex']);
		}
		if (isset($param['display_no']) && $param['display_no'] !== '') {
			$beauty->set('display_no', $param['display_no']);
		}
        if (isset($param['mission_type']) && $param['mission_type'] !== '') {
            $beauty->set('mission_type', $param['mission_type']);
        }
        if (isset($param['segment_device']) && $param['segment_device'] !== '') {
            $beauty->set('segment_device', $param['segment_device']);
        }
        if (!empty($param['segment_prefecture'])) {
            $param['segment_prefecture'] = implode(',', array_map(function($input) { 
                return str_pad($input, 2, 0, STR_PAD_LEFT);
            }, explode(',', $param['segment_prefecture'])));             
            $beauty->set('segment_prefecture', $param['segment_prefecture']);
        }
        if (!empty($param['segment_user_generation'])) {
            $param['segment_user_generation'] = implode(',', array_map(function($input) { 
                return str_pad($input, 2, 0, STR_PAD_LEFT);
            }, explode(',', $param['segment_user_generation']))); 
            $beauty->set('segment_user_generation', $param['segment_user_generation']);
        }
        if (isset($param['detail_page_url']) && $param['detail_page_url'] !== '') {
            $beauty->set('detail_page_url', $param['detail_page_url']);
        }
        if (isset($param['distribution_limit']) && $param['distribution_limit'] !== '') {
            $beauty->set('distribution_limit', $param['distribution_limit']);
        }        
		if ($beauty->save()) {
			if (empty($beauty->id)) {
				$beauty->id = self::cached_object($beauty)->_original['id'];
			}
			return !empty($beauty->id) ? $beauty->id : 0;
		}
		return false;
	}

	/**
	 * Get all beauty.
	 *
	 * @author diennvt
	 * @param array $param Input data.
	 * @return array Returns the array.
	 */
	public static function get_all($param)
	{
		if (empty($param['login_user_id'])) {
			$param['login_user_id'] = 0;
		}
		$options['where'] = array(
			'id'      => $param['login_user_id'],
			'disable' => '0',
		);
		$user = Model_User::find('first', $options);
		$deviceId = \Lib\Util::deviceId($param['os']);

		// create sql when not login
		$sexSql = "IFNULL(segment_sex, 0) = 0";
		$prefectureSql = "IFNULL(segment_prefecture, '') = ''";
		$generationSql = "IFNULL(segment_user_generation, '') = ''";

		// update sql when login
		if ($user) {			
            $sexSql .= " OR (IFNULL(segment_sex, 0) <> 0 AND segment_sex = '{$user->get('sex')}')";
			$prefectureId = str_pad($user->get('prefecture_id'), 2, 0, STR_PAD_LEFT);
            $prefectureSql .= " OR (IFNULL(segment_prefecture, '') <> '' AND LOCATE('{$prefectureId}', segment_prefecture) > 0)";			
            $age = str_pad(date('Y', time()) - date('Y', $user->get('birthday')), 2, 0, STR_PAD_LEFT);
            $generationSql .= " OR (IFNULL(segment_user_generation, '') <> '' AND LOCATE('{$age}', segment_user_generation) > 0)";           
		}
        $mission_type = implode(',', array(
            \Config::get('user_point_logs.type.access_link'),
            \Config::get('user_point_logs.type.answer_survey'),
            \Config::get('user_point_logs.type.capture_photo'),
            \Config::get('user_point_logs.type.register_from_other_site'),
        ));
		$query = DB::select(
                self::$_table_name.'.id',
                self::$_table_name.'.title',
                self::$_table_name.'.description',
                self::$_table_name.'.image_url',
                self::$_table_name.'.home_url',
                self::$_table_name.'.point_get_id',
                self::$_table_name.'.start_date',
                self::$_table_name.'.end_date',
                self::$_table_name.'.mission_type',
                self::$_table_name.'.segment_sex',
                self::$_table_name.'.segment_device',
                self::$_table_name.'.segment_prefecture',
                self::$_table_name.'.segment_user_generation',
                self::$_table_name.'.detail_page_url',
                self::$_table_name.'.popup',
                self::$_table_name.'.popup_priority',
				self::$_table_name.'.display_no',
                self::$_table_name.'.distribution_limit',
                self::$_table_name.'.token',
                'point_gets.point',
                DB::expr("IF(ISNULL(user_point_logs.point_get_id), 0, 1) AS is_used")                             
            )
			->from(self::$_table_name)
			->join('point_gets', 'LEFT')
			->on(self::$_table_name.'.point_get_id', '=', 'point_gets.id')
			->join(
				DB::expr("(
                        SELECT point_get_id, sum(point) AS point
                        FROM user_point_logs
                        WHERE user_id = {$param['login_user_id']}
                        AND type IN ({$mission_type})
                        GROUP BY point_get_id
                    ) AS user_point_logs          
				"),
				'LEFT'
			)
			->on(self::$_table_name.'.point_get_id', '=', 'user_point_logs.point_get_id')
			->join(
				DB::expr("
					(SELECT COUNT(*) AS count,
							point_get_id
					 FROM user_point_logs
					 GROUP BY point_get_id) AS user_point_logs_count
				"),
				'LEFT'
			)
			->on(self::$_table_name.'.point_get_id', '=', 'user_point_logs_count.point_get_id')
			->where(self::$_table_name.'.disable', '0')
			->where(
				DB::expr("
				(
					beauties.start_date IS NULL
				 	OR beauties.end_date IS NULL
				 	OR
				 	(
                        beauties.start_date IS NOT NULL
                        AND beauties.end_date IS NOT NULL
				 		AND beauties.start_date <= UNIX_TIMESTAMP()
					 	AND beauties.end_date >= UNIX_TIMESTAMP()
					)
				)
                AND
				(
					IFNULL(segment_device, 0) = 0
					OR (IFNULL(segment_device, 0) <> 0 AND segment_device = {$deviceId})
				)
				AND
				(
					{$sexSql}
				)				
				AND
				(
					{$generationSql}
				)
				AND
				(
					{$prefectureSql}
				)
				AND
				(
					IFNULL(distribution_limit, 0) = 0
					OR distribution_limit > user_point_logs_count.count
				)
            "))
			->order_by(self::$_table_name.'.created', 'DESC');
		$data = $query->execute()->as_array();
        foreach ($data as &$row) {
            if (!empty($row['token']) && !empty($param['login_user_id'])) {
                $row['set_track_url'] = str_replace(array('{user_id}', '{mission_id}'), 
                                                    array($param['login_user_id'], $row['id']), 
                                                    \Config::get('beauties.set_track_url'));
                $row['get_track_url'] = str_replace(array('{token}'), 
                                                    array($row['token']), 
                                                    \Config::get('beauties.get_track_url'));
            }
        }
        unset($row);
		return $data;
	}

	/**
	 * Get detail beauty.
	 *
	 * @author diennvt
	 * @param array $param Input data.
	 * @return array|bool Detail  or false if error.
	 */
	public static function get_detail($param)
	{  
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $mission_type = implode(',', array(
            \Config::get('user_point_logs.type.access_link'),
            \Config::get('user_point_logs.type.answer_survey'),
            \Config::get('user_point_logs.type.capture_photo'),
            \Config::get('user_point_logs.type.register_from_other_site'),
        ));
        $query = DB::select(
                self::$_table_name . '.id',
				self::$_table_name . '.title',
				self::$_table_name . '.description',
				self::$_table_name . '.image_url',
				self::$_table_name . '.home_url',
				self::$_table_name . '.point_get_id',
				self::$_table_name . '.start_date',
				self::$_table_name . '.end_date',
                self::$_table_name . '.mission_type',
                self::$_table_name . '.segment_sex',
                self::$_table_name . '.segment_device',
                self::$_table_name . '.segment_prefecture',
                self::$_table_name . '.segment_user_generation',
                self::$_table_name . '.detail_page_url',
                self::$_table_name . '.popup',
                self::$_table_name . '.popup_priority',
				self::$_table_name . '.display_no',
                self::$_table_name . '.distribution_limit',
                self::$_table_name . '.token',
                'point_gets.point',
                DB::expr("IF(ISNULL(user_point_logs.point_get_id), 0, 1) AS is_used")
            )
            ->from(self::$_table_name)
            ->join('point_gets', 'LEFT')
            ->on(self::$_table_name . '.point_get_id', '=', 'point_gets.id')
            ->join(
				DB::expr("(
                        SELECT point_get_id, sum(point) AS point
                        FROM user_point_logs
                        WHERE user_id = {$param['login_user_id']}
                        AND type IN ({$mission_type})
                        GROUP BY point_get_id
                    ) AS user_point_logs          
				"),
				'LEFT'
			)
            ->on(self::$_table_name . '.point_get_id', '=', 'user_point_logs.point_get_id') 
			->where(self::$_table_name.'.id', $param['id'])
            ->limit(1);        
        if (isset($param['for_mission'])) {
            $query->where_open();
            $query->where(DB::expr("(
                beauties.start_date IS NULL 
                OR beauties.end_date IS NULL
                OR (beauties.start_date <= UNIX_TIMESTAMP() AND beauties.end_date >= UNIX_TIMESTAMP())
            )"));           
            $query->where_close();
        }
        $data = $query->execute()->offsetGet(0);        
        if (!empty($data['token']) && !empty($param['login_user_id'])) {
            $data['set_track_url'] = str_replace(array('{user_id}', '{mission_id}'), 
                                                array($param['login_user_id'], $data['id']), 
                                                \Config::get('beauties.set_track_url'));
            $data['get_track_url'] = str_replace(array('{token}'), 
                                                array($data['token']), 
                                                \Config::get('beauties.get_track_url'));
        }
        if (!empty($data) 
            && !empty($param['login_user_id']) 
            && isset($param['write_log_view'])) {
            Model_User_Beauty_View_Log::addLog(array(
                'login_user_id' => $param['login_user_id'],
                'beauty_id' => $data['id'],
            ));
        }
		return $data ? $data : array();
	}

	/**
	 * Disable/enable beauty.
	 *
	 * @author diennvt
	 * @param array $param Input data.
	 * @return bool Returns the boolean.
	 */
	public static function disable($param)
	{
		$ids = explode(',', $param['id']);
		foreach ($ids as $id) {
			$beauty = self::find($id);
			if ($beauty) {
				$beauty->set('disable', $param['disable']);
				if (!$beauty->update()) {
					return false;
				}
			} else {
				static::errorNotExist('beauty_id', $id);
				return false;
			}
		}
		return true;
	}

	/**
	 * Get list tags.
	 *
	 * @author diennvt
	 * @param array $param Input data.
	 * @return array Returns array(total, data).
	 */
	public static function get_list($param)
	{
        if (empty($param['page'])) {
            $param['page'] = 1;
        }
		$query = DB::select(
			self::$_table_name.'.id',
			self::$_table_name.'.title',
			self::$_table_name.'.description',
			self::$_table_name.'.image_url',
			self::$_table_name.'.home_url',
			self::$_table_name.'.point_get_id',
			self::$_table_name.'.created',
			self::$_table_name.'.updated',
			self::$_table_name.'.disable',
			self::$_table_name.'.start_date',
			self::$_table_name.'.end_date',
			self::$_table_name.'.mission_type',
			self::$_table_name.'.segment_sex',
			self::$_table_name.'.segment_device',
			self::$_table_name.'.segment_prefecture',
			self::$_table_name.'.segment_user_generation',
			self::$_table_name.'.detail_page_url',
			self::$_table_name.'.popup',
			self::$_table_name.'.popup_priority',
			self::$_table_name.'.display_no',
			'point_gets.point'
		)
            ->from(self::$_table_name)
            ->join('point_gets', 'LEFT')
            ->on(self::$_table_name . '.point_get_id', '=', 'point_gets.id');
		if (!empty($param['title'])) {
			$query->where(self::$_table_name . '.title', 'LIKE', "%{$param['title']}%");
		}
		if (!empty($param['mission_type'])) {
			$query->where(self::$_table_name . '.mission_type', $param['mission_type']);
		}
        if (!empty($param['point_get_id'])) {
			$query->where(self::$_table_name . '.point_get_id', $param['point_get_id']);
		}
		if (isset($param['disable']) && $param['disable'] !== '') {
			$query->where(self::$_table_name . '.disable', $param['disable']);
		}
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			$query->order_by($sortExplode[0], $sortExplode[1]);
		} else {
			$query->order_by(self::$_table_name . '.created', 'DESC');
		}		
        if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;
		return array($total, $data);
	}

    /**
     * Get random beauty
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array Random beauty
     */
    public static function get_random($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '=', '0')
            ->order_by(DB::expr("
                RAND()
            "))
            ->limit(1);
        $data = $query->execute()->offsetGet(0);
        return $data;
    }
    
}
