<?php

/**
 * Any query in Model Survey Log
 *
 * @package Model
 * @created 2015-03-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Survey_Log extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'survey_id',
        'created'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'survey_logs';

    /**
     * Add info for Survey Log
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Survey Log id or false if error
     */
    public static function add($param)
    {
        $log = new self;
        // set value
        if (!empty($param['user_id'])) {
            $log->set('user_id', $param['user_id']);
        }
        if (!empty($param['survey_id'])) {
            $log->set('survey_id', $param['survey_id']);
        }
        // save to database
        if ($log->save()) {
            if (empty($log->id)) {
                $log->id = self::cached_object($log)->_original['id'];
            }
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

    /**
     * Get list Survey Log (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Survey Log
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            'users.first_name',
            'users.last_name',
            'surveys.title'
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name .'.user_id', '=', 'users.id')
            ->join('surveys')
            ->on(self::$_table_name .'.survey_id', '=', 'surveys.id');
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['survey_id'])) {
            $query->where('survey_id', '=', $param['survey_id']);
        }
        if (!empty($param['date_from'])) {
            $query->where('surveys.finished', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where('surveys.started', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
}
