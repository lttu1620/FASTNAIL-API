<html lang="ja">
<head>
    <meta charset="utf-8">
    <style>
        .contain {
            text-align: center;
        }
        
        .title {
            margin-top: 5px;
            margin-bottom: 50px;
        }
        
        .content {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <div class='contain'>
        <div class='content'>
            Invalid mission !
        </div>
    </div>
</body> 

</html>