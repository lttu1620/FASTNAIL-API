<html lang="ja">

<head>
    <meta charset="utf-8">
    <style>
        .contain {
            text-align: center;
        }
        
        .title {
            margin-top: 5px;
            margin-bottom: 50px;
            text-align: left;
        }
        
        .content {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <div class='contain'>

        <div class='title'>↑ここから戻ってね</div>

        <div class='content'>
            アンケートにご協力いただき<br>
            ありがとうございました
        </div>
        
    </div>
</body> 

</html>