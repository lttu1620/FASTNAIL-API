<?php
return array(
    'driver' => 'file',
    'expiration' => null,
    'file' => array(
		'path'  =>	APPPATH . 'cache' . DS
	),   
	'key' => array(		
		'setting_all' => 24*60*60,
	)
);
