<?php
$envConf = array(
	'img_url' => 'https://img.fastnail.town/',
    'fe_url' => 'https://fastnail.town/',
	'adm_url' => 'https://admin.fastnail.town/',
    'facebook' => array(
        'app_id' => '1634581676778094',
        'app_secret' => '4fbe51895082fd60eddb10375b54d95e',
    ),
    'item_img_url' => array(
        'prices' => 'https://img.fastnail.town/fastnail/prices/',
        'nails' => 'https://img.fastnail.town/fastnail/nails/',
        'genres' => 'https://img.fastnail.town/fastnail/genres/',
        'colors' => 'https://img.fastnail.town/fastnail/colors/',
        'designs' => 'https://img.fastnail.town/fastnail/designs/',
        'scenes' => 'https://img.fastnail.town/fastnail/scenes/',
        'color_jells' => 'https://img.fastnail.town/fastnail/color_jells/',
    ),
    'send_email' => true,
    'test_email' => '', // will send to this email for testing
    'report_bug_email' => 'webmaster@oceanize.co.jp', // will send to this email when bug happen
    'limit_email_thanks' => 50,
    'limit_email_reminder' => 50,
    'fn_check_security' => true,
    'log_threshold' => Fuel::L_WARNING,   
    'locale' => '',
    
    'db.redis.default' => array(
        //'hostname' => 'fastnail-cache.8ni1y1.0001.apne1.cache.amazonaws.com',
        'hostname' => 'fastnailredis.8ni1y1.0001.apne1.cache.amazonaws.com',
        'port' => '6379',        
    )
);

$envConf['nail_price_icon'] = array(
    array(
        'key' => '2990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc2990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc2990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '2990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm2990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm2990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '2990.png',
    ),
    array(
        'key' => '3490',
        'icon' => $envConf['item_img_url']['prices'] . 'pc3490.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc3490active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '3490.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm3490.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm3490active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '3490.png',
    ),
    array(
        'key' => '3990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc3990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc3990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '3990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm3990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm3990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '3990.png',
    ),
    array(
        'key' => '4990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc4990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc4990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '4990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm4990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm4990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '4990.png',
    ),
    array(
        'key' => '5990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc5990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc5990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '5990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm5990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm5990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '5990.png',
    ),
    array(
        'key' => '6990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc6990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc6990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '6990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm6990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm6990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '6990.png',
    ),
    array(
        'key' => '7990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc7990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc7990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '7990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm7990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm7990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '7990.png',
    ),
    array(
        'key' => '0000',
        'icon' => $envConf['item_img_url']['prices'] . 'pc0000.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc0000active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '0000.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm0000.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm0000active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '0000.png',
    ),
);

if (isset($_SERVER['SERVER_NAME'])) {
    if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php')) {
        include_once (__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php');
        $envConf = array_merge($envConf, $domainConf);
    }
}
return $envConf;