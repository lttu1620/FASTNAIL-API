<?php
return array(
//    'driver' => 'file',
//    'expiration' => null,
//    'file' => array(    
//    'path'  =>  '/var/www/api/fuel/app/cache',
//  ),
    'driver' => 'redis',
    'expiration' => null,   
    'redis' => array(
        'cache_id' => 'fn_pro_api_',
        'database' => 'default'
    ),
);
