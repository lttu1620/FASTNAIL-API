<?php
$envConf = array(
	'img_url' => 'http://sv4.evolable-asia.z-hosts.com:84/',
	'fe_url' => 'http://sv4.evolable-asia.z-hosts.com:8092/',
	'adm_url' => "http://sv4.evolable-asia.z-hosts.com:8091/",
    'facebook' => array(
        'app_id' => '945848175446201',
        'app_secret' => 'fe612c106056f2784406c497805b0fe3',
    ),
	'item_img_url' => array(
        'prices' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/prices/',
        'nails' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/nails/',
        'genres' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/genres/',
        'colors' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/colors/',
        'designs' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/designs/',
        'scenes' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/scenes/',
        'color_jells' => 'http://sv4.evolable-asia.z-hosts.com:84/fastnail/color_jells/',
    ),
    'send_email' => true,
    'test_email' => '', // will send to this email for testing
    'report_bug_email' => 'thailh@evolableasia.vn', // will send to this email when bug happen
    'limit_email_thanks' => 30,
    'limit_email_reminder' => 30,    
    'fn_check_security' => false,
    'point_expire_days' => 2,
    'db.redis.default' => array(
        'hostname' => 'localhost',
        'port' => '6379',        
    )
);

$envConf['nail_price_icon'] = array(
    array(
        'key' => '2990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc2990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc2990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '2990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm2990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm2990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '2990.png',
    ),
    array(
        'key' => '3490',
        'icon' => $envConf['item_img_url']['prices'] . 'pc3490.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc3490active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '3490.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm3490.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm3490active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '3490.png',
    ),
    array(
        'key' => '3990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc3990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc3990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '3990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm3990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm3990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '3990.png',
    ),
    array(
        'key' => '4990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc4990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc4990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '4990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm4990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm4990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '4990.png',
    ),
    array(
        'key' => '5990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc5990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc5990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '5990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm5990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm5990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '5990.png',
    ),
    array(
        'key' => '6990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc6990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc6990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '6990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm6990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm6990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '6990.png',
    ),
    array(
        'key' => '7990',
        'icon' => $envConf['item_img_url']['prices'] . 'pc7990.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc7990active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '7990.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm7990.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm7990active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '7990.png',
    ),
    array(
        'key' => '0000',
        'icon' => $envConf['item_img_url']['prices'] . 'pc0000.png',
        'active_icon' => $envConf['item_img_url']['prices'] . 'pc0000active.png',
        'header_icon' => $envConf['item_img_url']['prices'] . '0000.png',
        'mobile_icon' => $envConf['item_img_url']['prices'] . 'm0000.png',
        'mobile_active_icon' => $envConf['item_img_url']['prices'] . 'm0000active.png',
        'mobile_header_icon' => $envConf['item_img_url']['prices'] . '0000.png',
    ),
);

if (isset($_SERVER['SERVER_NAME'])) {
    if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php')) {
        include_once (__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php');
        $envConf = array_merge($envConf, $domainConf);
    }
}
return $envConf;