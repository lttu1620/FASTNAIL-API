<?php
return array(
//    'driver' => 'file',
//    'expiration' => null,    
//    'file' => array(
//		'path'  =>	'/var/www/html/oceanize/fastnail/cache/api/',
//	), 
    'driver' => 'redis',
    'expiration' => null,   
    'redis' => array(
        'cache_id' => 'fn',
        'database' => 'default'
    ),
);
