<?php
$domainConf = array(
	'img_url' => 'http://devimg.fastnail.town/',
    'fe_url' => 'http://dev.fastnail.town/',
	'adm_url' => 'http://devadmin.fastnail.town/',        
    'facebook' => array(
        'app_id' => '1634581676778094',
        'app_secret' => '4fbe51895082fd60eddb10375b54d95e',
    ),
    'item_img_url' => array(
        'prices' => 'http://devimg.fastnail.town/fastnail/prices/',
        'nails' => 'http://devimg.fastnail.town/fastnail/nails/',
        'genres' => 'http://devimg.fastnail.town/fastnail/genres/',
        'colors' => 'http://devimg.fastnail.town/fastnail/colors/',
        'designs' => 'http://devimg.fastnail.town/fastnail/designs/',
        'scenes' => 'http://devimg.fastnail.town/fastnail/scenes/',
        'color_jells' => 'http://devimg.fastnail.town/fastnail/color_jells/',
    ),
    'beauties' => array(
        'set_track_url' => 'http://devapi.fastnail.town/set_track?user_id={user_id}&mission_id={mission_id}',
        'get_track_url' => 'http://devapi.fastnail.town/get_track?token={token}'
    ),
    'user_bought_items' => array(
        'qr_link' => 'http://devapi.fastnail.town/userboughtitems/scanqr/{token}'
    ),
);