<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;

/**
 * Update monthly rank of nail
 *
 * @package             Tasks
 * @create              Jun 18 2015
 * @version             1.0
 * @author              Le Tuan Tu
 * @run                 php oil refine updatemonthlyrank
 * @run                 FUEL_ENV=test php oil refine updatemonthlyrank
 * @run                 FUEL_ENV=production php oil refine updatemonthlyrank
 * @copyright           Oceanize INC
 */
class UpdateMonthlyRank
{
    public static function run()
    {
        \LogLib::info('BEGIN [UpdateMonthlyRank] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("BEGIN [UpdateMonthlyRank] " . date('Y-m-d H:i:s') . "\n\n PROCESSING . . . . ! \n");
        $result = \Model_Nail_Monthly_Rank::update_rank_nail_monthly();
        \LogLib::info('END [UpdateMonthlyRank] ' . date('Y-m-d H:i:s'), __METHOD__, $result);
        Cli::write("END [UpdateMonthlyRank] " . date('Y-m-d H:i:s') . "\n");
    }

}
