<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

/**
 * Update orders.shop_counter_code for today's orders on begin of new day
 *
 * @package             Tasks
 * @create              2015-08-15
 * @version             1.0
 * @author              thailh
 * @run                 php oil refine updateshopcountercode
 * @run                 FUEL_ENV=test php oil refine updateshopcountercode
 * @run                 FUEL_ENV=production php oil refine updateshopcountercode
 * @copyright           Oceanize INC
 */
class UpdateShopCounterCode
{ 
    public static function run()
    {
        try {
            \LogLib::info('BEGIN [Update shop_counter_code] ' . date('Y-m-d H:i:s'), __METHOD__, array());
            Cli::write("BEGIN [Update shop_counter_code] " . date('Y-m-d H:i:s') . "\n\nPROCESSING . . . . ! \n");
            Cli::write("Reset shop_counter_code...");
            \Model_Shop_Counter_Code::reset_code();      
            $result = \Model_Order::batch_update_counter_code();
            foreach ($result as $order_id => $status) {
                Cli::write("{$order_id} -> {$status}\n");
            }
            \LogLib::info('END [Update shop_counter_code] ' . date('Y-m-d H:i:s'), __METHOD__, $result);        
            Cli::write("END [Update shop_counter_code] " . date('Y-m-d H:i:s') . "\n");
        } catch (\Exception $e) {
            $message = sprintf("Exception\n"
                            . " - Message : %s\n"
                            . " - Code : %s\n"
                            . " - File : %s\n"
                            . " - Line : %d\n"
                            . " - Stack trace : \n"
                            . "%s",
                            $e->getMessage(),
                            $e->getCode(),
                            $e->getFile(),
                            $e->getLine(),
                            $e->getTraceAsString());
            \LogLib::error($message, __METHOD__);
            \Lib\Email::sendReportBug($message);
        }
    }

}
