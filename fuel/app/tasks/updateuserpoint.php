<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

/**
 * Update monthly rank of nail
 *
 * @package             Tasks
 * @create              Aug 13 2015
 * @version             1.0
 * @author              CaoLP
 * @run                 php oil refine updateuserpoint
 * @run                 FUEL_ENV=test php oil refine updateuserpoint
 * @run                 FUEL_ENV=production php oil refine updateuserpoint
 * @copyright           Oceanize INC
 */
class UpdateUserPoint
{
    public function __construct(){
        date_default_timezone_set('Asia/Tokyo');
    }
    
    public static function run()
    {
        \LogLib::info('BEGIN [Update user point] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("BEGIN [Update user point] " . date('Y-m-d H:i:s') . "\n\nPROCESSING . . . . ! \n");
        $result = array();
        $res = \Model_User_Point_Log::get_list_for_batch(
            array (
                'expired_yesterday' => true,
                'set_expire' => true
            )
        );
        foreach ($res as $point) {
            Cli::write('POINT ID [' . $point['id'] .'] WAS EXPIRED');
            $expired_point = $point['point'] - $point['minus'];
            $param = array(
                'login_user_id' => $point['user_id'],
                'type' => \Config::get('user_point_logs.type.point_expired'),
                'user_bought_item_id' => 0,
                'order_id' => 0,
                'is_paid' => 0,
                'point' => $expired_point,
            );
            $inserted_id = \Model_User_Point_Log::add($param);
            if ($inserted_id) {
                $result['inserted'][$inserted_id] = array(
                    'point' => $point['point'],
                    'point_used' => $point['minus']
                );
                Cli::write("{$inserted_id} -> OK\n");
            } else {
                $result['failed'][] = $point['id'];
                Cli::write("{$point['id']} -> Failed\n");
            }
        }
        \LogLib::info('END [Update user point] ' . date('Y-m-d H:i:s'), __METHOD__, $result);
        Cli::write("END [Update user point] " . date('Y-m-d H:i:s') . "\n");
    }

}
