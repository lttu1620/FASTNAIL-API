<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

include(VENDORPATH . 'php-imap/__autoload.php');
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;
use PhpImap\ParseMail;

/**
 * Import orders from Hot Pepper Service
 *
 * @package             Tasks
 * @create              May 15 2015
 * @version             1.0
 * @author             <Caolp>
 * @run                 php oil refine hpordersimport
 * @run                 FUEL_ENV=test php oil refine hpordersimport
 * @run                 FUEL_ENV=production php oil refine hpordersimport
 * @copyright           Oceanize INC
 */
class HPOrdersImport {

    public static function run() { //return self::test();
        try {
            \LogLib::info('BEGIN [Get mail data from Hot Pepper] ' . date('Y-m-d H:i:s'), __METHOD__, array());
            Cli::write("BEGIN [PARSE MAIL HOT PEPPER] ".date('Y-m-d H:i:s')." PROCESSING \n\n . . . . ! \n");
            // Configure your imap mailboxes
            $mailboxes = Config::get('hot_pepper_mailboxes');
            $from = $mailboxes['from_email'];
            $craw_time = date('d-M-Y', strtotime("-1 days"));
            $connection = $mailboxes['connection'];
            $configKey = array(
                'shop_name' => 'shop_name',
                'is_cancel' => 'is_cancel',
                '■予約番号' => 'hp_order_code',
                '■氏名' => 'user_name',
                'kana' => 'kana',
                '■来店日時' => 'reservation_date',
                '■指名スタッフ' => 'nailist_name',
                '■合計金額' => 'total_price',
                '■ご要望・ご相談' => 'request',
                'created' => 'created',
                'r_point' => 'r_point',
                'minute' => 'minute',
            );
            $conf = $mailboxes[$connection];
            $mailbox = new ImapMailbox(
                $conf['mailbox'], $conf['username'], $conf['password'], __DIR__
            );
            $parseMail = new ParseMail();
            $mailsIds = $mailbox->searchMailBox("SINCE {$craw_time} FROM {$from}");
            if (!$mailsIds) {
                \LogLib::warning('[PARSE MAIL] Mailbox is empty', __METHOD__, $conf);
                Cli::write('[PARSE MAIL] Mailbox is empty\n');
                Cli::write('END [Get mail data from Hot Pepper]\n');
                die;
            }

            $mailParsed = array();
            $textPlains = array();
            foreach ($mailsIds as $mailId) {             
                Cli::write("[PARSE MAIL] BEGIN parse -> mail_id: {$mailId} \n");
                $cacheKey = 'hot_pepper_email_' . $mailId;
                $textPlain = \Lib\Cache::get($cacheKey);
                if ($textPlain === false) { 
                    $mail = $mailbox->getMail($mailId);
                    if (!empty($mail)) {
                        \Lib\Cache::set($cacheKey, $textPlain = $mail->textPlain, 2*24*60*60); // cache 2 days                
                    }
                }
                $mailParsed[] = $parseMail->parse_mail($textPlain, $mailId);            
                $textPlains[] = $textPlain;   
                Cli::write("[PARSE MAIL] END parse -> mail_id: {$mailId} \n");
            }

            \Lib\Cache::set('hot_pepper_email', $textPlains, 12*60*60);

            $hpOrders = array();
            foreach ($mailParsed as $maildata) {
                $hpOrder = array();
                foreach ($maildata as $key => $value) {
                    $k = trim($key);
                    if (isset($configKey[$k])) {
                        $field = $configKey[$k];
                        $hpOrder[$field] = $value;
                        if (!empty($value)) {
                            switch ($field) {
                                case 'user_name':
                                    $nameParsed = $parseMail->parseName($value);
                                    $hpOrder['user_name'] = !empty($nameParsed[1]) ? $nameParsed[1] : '';
                                    $hpOrder['kana'] = !empty($nameParsed[2]) ? $nameParsed[2] : '';
                                    break;
                                case 'total_price':
                                    $hpOrder[$field] = $parseMail->parseMoney($value);
                                    break;
                                case 'reservation_date':
                                case 'created':
                                    $hpOrder[$field] = $parseMail->parseDate($value);
                                    break;
                            }
                        }
                    }
                }
                if (empty($hpOrder['hp_order_code'])) {
                    \LogLib::info('[PARSE MAIL] hp_order_code does not exists ', __METHOD__, $maildata);
                    continue;
                }
                $hpOrders[$hpOrder['hp_order_code']] = $hpOrder;            
            }
            Cli::write("Result:\n");
            $importedResult = \Model_Order::importHpOrders($hpOrders);
            if (!empty($importedResult)) {
                foreach ($importedResult as $hpOrderCode => $status) {
                    Cli::write("{$hpOrderCode} -> {$status}\n");
                }
            } else {
                Cli::write("All HpOrders have already imported\n");
            }
            \LogLib::info('END [Get mail data from Hot Pepper] ' . date('Y-m-d H:i:s'), __METHOD__, array());
            Cli::write("END [PARSE MAIL HOT PEPPER] ".date('Y-m-d H:i:s')."\n");
            exit;
        } catch (\Exception $e) {
            $message = sprintf("Exception\n"
                            . " - Message : %s\n"
                            . " - Code : %s\n"
                            . " - File : %s\n"
                            . " - Line : %d\n"
                            . " - Stack trace : \n"
                            . "%s",
                            $e->getMessage(),
                            $e->getCode(),
                            $e->getFile(),
                            $e->getLine(),
                            $e->getTraceAsString());
            \LogLib::error($message, __METHOD__);
            \Lib\Email::sendReportBug($message);
        }
    }
    
    public static function test() {
        $textPlain = "
            From:	SALON BOARD <yoyaku_system@salonboard.com>
            To:	fastnail.town@gmail.com
            日付:	2015年8月6日 12:32
            件名:	【当日】予約連絡

            FAST NAIL 京都烏丸店様

            Hot Pepper Beauty「SALON BOARD」にお客様から
            ご予約が入りました。

            ◇ご予約内容
            ■予約番号
              B072675837
            ■氏名
              世一 啓子（ヨイチ ヒロコ）
            ■来店日時
              2015年08月06日（木）15:00
            ■指名スタッフ
              指名なし
            ■メニュー
              【定額・オフ無料】フレンチ・カラグラ　3769円　(Total：60分)
                ★オフィスでの好感度もUP☆派手なネイルができない方にオススメ♪【ファストネイル京都烏丸店】
              （所要時間目安：20分）
            ■ご利用クーポン
              利用クーポンなし
            ■合計金額
              予約時合計金額　3,769円
              今回の利用ポイント　利用なし
              お支払い予定金額　3,769円
              今回の利用ポイント1000ポイント

            ◇ご予約付加情報
            ■なりたいイメージ
              なし
            ■ご要望・ご相談
              -

            PC版SALON BOARD
            https://salonboard.com/login/
            スマートフォン版SALON BOARD
            https://salonboard.com/login_sp/

            予約受付日時：2015年08月06日（木）14:32



            =============================================
            ホットペッパービューティー　ヘルプデスク
            電話：0120-36-0493（サロン　ノ　レスキューサン）
            受付時間：10:00～21:00（年中無休）
            =============================================
        ";
        
        $textPlain = '                
            FAST NAIL 渋谷店様

Hot Pepper Beauty「SALON BOARD」にお客様から
ご予約が入りました。

◇ご予約内容
■予約番号
　B075723171
■氏名
　横山 恭平（ヨコヤマ キョウヘイ）
■来店日時
　2015年09月01日（火）13:00
■指名スタッフ
　指名なし
■メニュー
　ジェル＋アート
　【ハンド・付け替え時】　ソフトジェルネイルオフ　10本　無料
　　【オフのみの方も大歓迎】オフもお手ごろにスピーディー仕上げ♪【ファストネイル渋谷店】
　（所要時間目安：35分）
■ご利用クーポン
　【定額・オフ無料・アート付】 2940円 人気No.1フレンチ (Total:60分)
　　派手なネイルができない方に◎カラー変更OK！オーダーの際『ホットペッパークーポンをご利用のお客様』へお進み下さい！ソフトジェルなら付け替え時は他店オフも無料☆※他クーポン併用不可
■合計金額
　予約時合計金額　2,940円
　今回の利用ポイント　利用なし
　お支払い予定金額　2,940円

◇ご予約付加情報
■なりたいイメージ
　なし
■ご要望・ご相談
　-

PC版SALON BOARD
https://salonboard.com/login/
スマートフォン版SALON BOARD
https://salonboard.com/login_sp/

予約受付日時：2015年09月01日（火）10:44

=============================================
ホットペッパービューティー　ヘルプデスク
電話：0120-36-0493（サロン　ノ　レスキューサン）
受付時間：10:00～21:00（年中無休）
=============================================
        ';
        
        $configKey = array(
            'shop_name' => 'shop_name',
            'is_cancel' => 'is_cancel',
            '■予約番号' => 'hp_order_code',
            '■氏名' => 'user_name',
            'kana' => 'kana',
            '■来店日時' => 'reservation_date',
            '■指名スタッフ' => 'nailist_name',
            '■合計金額' => 'total_price',
            '■ご要望・ご相談' => 'request',
            'created' => 'created',
            'r_point' => 'r_point',
            'minute' => 'minute',
        );
        $parseMail = new ParseMail();
        $mailParsed = array();      
        $mailParsed[] = $parseMail->parse_mail($textPlain, 1);
        
        $hpOrders = array();
        foreach ($mailParsed as $maildata) {
            $hpOrder = array();
            foreach ($maildata as $key => $value) {
                $k = trim($key);
                if (isset($configKey[$k])) {
                    $field = $configKey[$k];
                    $hpOrder[$field] = $value;
                    if (!empty($value)) {
                        switch ($field) {
                            case 'user_name':
                                $nameParsed = $parseMail->parseName($value);
                                $hpOrder['user_name'] = !empty($nameParsed[1]) ? $nameParsed[1] : '';
                                $hpOrder['kana'] = !empty($nameParsed[2]) ? $nameParsed[2] : '';
                                break;
                            case 'total_price':
                                $hpOrder[$field] = $parseMail->parseMoney($value);
                                break;
                            case 'reservation_date':
                            case 'created':
                                $hpOrder[$field] = $parseMail->parseDate($value);
                                break;                            
                        }
                    }
                }
            }
            if (empty($hpOrder['hp_order_code'])) {
                \LogLib::warning('[PARSE MAIL] hp_order_code does not exists ', __METHOD__, $hpOrder);
                continue;
            }
            $hpOrders[$hpOrder['hp_order_code']] = $hpOrder;
        } 
        //print_r($hpOrders); exit;
        Cli::write("Result:\n");
        $importedResult = \Model_Order::importHpOrders($hpOrders);
        if (!empty($importedResult)) {
            foreach ($importedResult as $hpOrderCode => $status) {
                Cli::write("{$hpOrderCode} -> {$status}\n");
            }
        } else {
            Cli::write("All HpOrders have already imported\n");
        }
        \LogLib::info('END [Get mail data from Hot Pepper] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("END [PARSE MAIL HOT PEPPER] ".date('Y-m-d H:i:s')."\n");
        exit;
    }    

}
