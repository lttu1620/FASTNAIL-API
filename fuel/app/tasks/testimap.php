<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

include(VENDORPATH . 'php-imap/__autoload.php');
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

/**
 * Test i-map connection
 *
 * @package             Tasks
 * @create              Aug 13 2015
 * @version             1.0
 * @author              CaoLP
 * @run                 php oil refine testimap
 * @run                 FUEL_ENV=test php oil refine testimap
 * @run                 FUEL_ENV=production php oil refine testimap
 * @copyright           Oceanize INC
 */
class TestImap
{ 
    public static function run()
    {        
        error_reporting(E_ALL);
        Cli::write("BEGIN: " . date('Y-m-d H:i:s') . "\n\nPROCESSING . . . . ! \n");
        $mailboxes = Config::get('hot_pepper_mailboxes');
        $from = $mailboxes['from_email'];
        $craw_time = date('d-M-Y', strtotime("-1 days"));
        $connection = $mailboxes['connection'];
        $conf = $mailboxes[$connection];
       
        $conf = array(
            'label'    => 'Gmail',
            'enable'   => true,
            'mailbox'  => '{imap.gmail.com:993/imap/ssl}INBOX',
            'username' => 'developer.php.vn@gmail.com',
            'password' => 'dev@123456'
        );
        $conf = array(
            'label'    => 'Gmail',
            'enable'   => true,
            'mailbox'  => '{imap.gmail.com:993/imap/ssl/novalidate-cert}',
            'username' => 'fastnail.town@gmail.com',
            'password' => '73FursUAF3'
        );
        $mbox = imap_open($conf['mailbox'], $conf['username'], $conf['password']) or die('Cannot connect: ' . print_r(imap_errors(), true));
        exit;
                
        $mbox = imap_open(
            $conf['mailbox'], 
            $conf['username'], 
            $conf['password'],
            NULL, 
            1,
            array('DISABLE_AUTHENTICATOR' => 'GSSAPI')
        );
        if (!$mbox) {
            echo 'Could not connect';
            exit;
        }
        try {
            $emails = imap_search($mbox, 'SINCE ' . date('d-M-Y', strtotime("-1 week")));
        } catch (\PhpErrorException $e) {
            print_r($e);
            exit;
        }
        imap_close($mbox);
        echo 'Total: ' . count($emails);
        exit;
        $mailbox = new \PhpImap\Mailbox(
            $conf['mailbox'],
            $conf['username'], 
            $conf['password'], 
            __DIR__
        );
        $from = 'mailer-daemon@googlemail.com';
        $mailsIds = $mailbox->searchMailBox("SINCE {$craw_time} FROM {$from}");
        //$mailsIds = $mailbox->searchMailBox('ALL');
        if (!$mailsIds) {
            die('Mailbox is empty');
        }
        Cli::write("Email total: " . count($mailsIds) . "\n");
        Cli::write("END: " . date('Y-m-d H:i:s') . "\n");
    }

}
