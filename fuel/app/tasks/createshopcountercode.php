<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

/**
 * Generate shop_counter_code
 *
 * @package             Tasks
 * @create              2015-08-15
 * @version             1.0
 * @author              thailh
 * @run                 php oil refine createshopcountercode
 * @run                 FUEL_ENV=test php oil refine createshopcountercode
 * @run                 FUEL_ENV=production php oil refine createshopcountercode
 * @copyright           Oceanize INC
 */

/*
CREATE TABLE `shop_counter_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) DEFAULT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TRIGGER `before_insert_shop_counter_codes` BEFORE INSERT ON `shop_counter_codes` FOR EACH ROW BEGIN
	DECLARE v_exists INT DEFAULT 1; 
    DECLARE v_shop_counter_code VARCHAR(4);	
	WHILE (v_exists = 1) DO	
		SET v_shop_counter_code = CONCAT(CHAR(ROUND(RAND()*25)+65), CHAR(ROUND(RAND()*9)+48), CHAR(ROUND(RAND()*9)+48), CHAR(ROUND(RAND()*9)+48));
		IF (!EXISTS(SELECT 1
								FROM shop_counter_codes 	
								WHERE `code` = v_shop_counter_code 								
								LIMIT 1)) THEN		
			SET v_exists = 0;
			SET new.`code` = v_shop_counter_code;
		END IF;
	END WHILE;	
END;
 * 
 */

class CreateShopCounterCode
{ 
    public static function run()
    {
        \LogLib::info('BEGIN [Generate shop_counter_code] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("BEGIN [Generate shop_counter_code] " . date('Y-m-d H:i:s') . "\n\nPROCESSING . . . . ! \n");
        //\Model_Shop_Counter_Code::create_code();
        Cli::write("END [Generate shop_counter_code] " . date('Y-m-d H:i:s') . "\n");
    }
    
}