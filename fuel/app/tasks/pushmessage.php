<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;

/**
 * Push message to device
 *
 * @package             Tasks
 * @create              2015-07-27
 * @version             1.0
 * @author              thailh
 * @run                 php oil refine pushmessage
 * @run                 FUEL_ENV=test php oil refine pushmessage
 * @run                 FUEL_ENV=production php oil refine pushmessage
 * @copyright           Oceanize INC
 */
class PushMessage {

    public static function run() {
        \Package::load('gcm');   
        \Package::load('apns'); 
        \LogLib::info('BEGIN [Push message] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("BEGIN [Push message] ".date('Y-m-d H:i:s')." PROCESSING \n\n . . . . ! \n");
        $messages = \Model_Push_Message::get_for_task();
        if (empty($messages)) {
            Cli::write('There are no message to sent');
            return false;
        }
        $user_settings = array();
        foreach ($messages as $message) {
            if (!isset($user_settings[$message['receive_user_id']])) {
                $user_settings[$message['receive_user_id']] = \Lib\Arr::key_value(
                    \Model_User_Setting::mobile_get_list(array(
                        'user_id' => $message['receive_user_id']
                     )),
                    'name',
                    'value'
                );
            }
            $user_setting = $user_settings[$message['receive_user_id']];
            if (($message['type'] != \Config::get('push_messages.type.reminder_order'))
                || ($message['type'] == \Config::get('push_messages.type.reminder_order') 
                        && empty($user_setting['send_reminder_device']))) {
                continue;
            }      
            foreach ($message->user_notifications as $user_notification) {
                $apple_regid = $user_notification->get('apple_regid');
                $google_regid = $user_notification->get('google_regid');
                $is_sent = 0;                
                $param = json_decode($message->get('param'), true);
                $data = array(
                    'notification_id' => $message->get('id'),
                    'message' => $message->get('message'),             
                    'receive_user_id' => $message->get('receive_user_id'),
                );
                switch ($message->get('type')) {
                    case \Config::get('push_messages.type.reminder_order'): //1
                        $data['type'] = 'reminder';
                        $data['order_id'] = !empty($param['order_id']) ? $param['order_id'] : 0;
                        break;
                    case \Config::get('push_messages.type.cancel_order'): //2
                        $data['type'] = 'cancel_order';
                        break;
                    case \Config::get('push_messages.type.cancel_bought_item'): //3
                        $data['type'] = 'cancel_bought_item';
                        break;
                    case \Config::get('push_messages.type.approve_mission_take_photo'): //4
                        $data['type'] = 'approve_image';
                        $data['mission_id'] = !empty($param['beauty_id']) ? $param['beauty_id'] : 0;
                        break;
                }

                if (!empty($google_regid)) {
                    Cli::write('Send message to Android device ' . $google_regid);                    
                    if (!\Gcm::sendMessage(array(
                        'google_regid' => $google_regid,
                        'data' => $data
                    ))) {
                        \LogLib::error('Can not send message to Android device ', __METHOD__, $google_regid);                       
                    } else {
                        $is_sent = 1;
                    }
                }
                if (!empty($apple_regid)) {
                    Cli::write('Send message to iOS device ' . $apple_regid);
                    unset($data['message']);
                    if (!\Apns::sendMessage(array(
                        'apple_regid' => $apple_regid,
                        'message' => $message->get('message'),                
                        'properties' => $data,                
                    ))) {
                        \LogLib::error('Can not send message to iOS device ', __METHOD__, $apple_regid);                       
                    } else {
                        $is_sent = 1;
                    }
                }
                if ($is_sent) {               
                    $message->set('is_sent', '1');
                    $message->set('sent_date', time());
                    $message->save();
                }
            }
        }
        \LogLib::info('END [Push message] ' . date('Y-m-d H:i:s'), __METHOD__, array());
        Cli::write("END [Push message] ".date('Y-m-d H:i:s')."\n");
        exit;
    }

}
