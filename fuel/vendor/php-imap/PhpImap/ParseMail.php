<?php

namespace PhpImap;

use Fuel\Core\Cli;
use LogLib\LogLib;

/**
 * @author caolp
 * 
 */
class ParseMail {

    public function parse_mail($mail, $mailId) {
       // mb_internal_encoding("UTF-8");
        preg_match_all('/(■.+\n.+)/', $mail, $re);
        $result = array();
        foreach ($re[0] as $item) {
            $string = trim(preg_replace('/(\n　|\n)/', '!', $item));
            $t = @split('!', $string);
            if (count($t) > 0) {
                $result[$t[0]] = trim($t[1]);
            }
        }
        
        // parse is_cancel
        preg_match('/ご予約.+/', $mail, $type);
        if (isset($type[0])) {
            if (trim($type[0]) == "ご予約のキャンセルがありました。")
                $result['is_cancel'] = 1;
            else
                $result['is_cancel'] = 0;
        }  
        
        // parse shop_name
        preg_match('/(FAST NAIL.+)様/', $mail, $shop_name);
        if (isset($shop_name[1])) {            
            $result['shop_name'] = trim($shop_name[1], '　 '); // trim space 1 byte & 2 bytes 
        }
       
        // parse created date
        preg_match('/予約受付日時：(.+)/', $mail, $created);
        if (isset($created[1])) {
            $result['created'] = $created[1];
        }
        
        // parse r_point
        preg_match('/今回の利用ポイント(.+)ポイント/', $mail, $r_point);
        if (isset($r_point[1])) {
            $result['r_point'] = trim($r_point[1], '　 ');
            $result['r_point'] = str_replace(',', '', $result['r_point']);
        }
        if (isset($result['r_point']) && !is_numeric($result['r_point'])) {
            $result['r_point'] = 0;
        }  
        
         // parse minute
        preg_match('/（所要時間目安：(.+)分）/', $mail, $minute);       
        if (isset($minute[1])) {
            $result['minute'] = trim($minute[1], '　 ');           
        }
        
        $result['mail_id'] = $mailId;
        return $result;
    }

    public function parseDate($date) {
        $date = trim(preg_replace('/（.+）/', ' ', $date));
        $date = date_create_from_format('Y年m月d日 H:i', $date);
        return $date->format('Y-m-d H:i');
    }

    public function parseMoney($money) {
        $money = str_replace(',', '', $money);
        preg_match('/\d+円/', $money, $re);
        if (isset($re[0]))
            return str_replace('円', '', $re[0]);
        else
            return 0;
    }

    public function parseName($name) {
        preg_match('/(.+)（(.+)）/', $name, $parseName);
        return $parseName;
    }
    
}
