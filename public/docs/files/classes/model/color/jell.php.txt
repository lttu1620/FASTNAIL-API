<?php

/**
 * Any query in Model Color Jell
 *
 * @package Model
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Color_Jell extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'icon',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'color_jells';

    /**
     * Add or update info for Color Jell
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Color Jell id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $color = new self;
        // check exist
        if (!empty($id)) {
            $color = self::find($id);
            if (empty($color)) {
                self::errorNotExist('color_jell_id');
                return false;
            }
        }
        // set value
        if (isset($param['name'])) {
            $color->set('name', $param['name']);
        }
        if (isset($param['icon'])) {
            $color->set('icon', $param['icon']);
        }
        // save to database
        if ($color->save()) {
            if (empty($color->id)) {
                $color->id = self::cached_object($color)->_original['id'];
            }
            return !empty($color->id) ? $color->id : 0;
        }
        return false;
    }

    /**
     * Get list Color Jell (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Color Jell
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Color Jell (without array count)
     *
     * @author Le Tuan Tu
     * @return array List Color Jell
     */
    public static function get_all()
    {
        $query = DB::select(
                'id',
                'name',
                'icon'       
            )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Color Jell
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $color = self::find($id);
            if ($color) {
                $color->set('disable', $param['disable']);
                if (!$color->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('color_jell_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Color Jell
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Color Jell or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('color_jell_id');
            return false;
        }
        return $data;
    }
}

