<?php

class Model_Nail_Image extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'user_id',
		'nail_id',
		'image_url',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'nail_images';

}

