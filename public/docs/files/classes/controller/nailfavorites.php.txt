<?php

/**
 * Controller for actions on Nail Favorite
 *
 * @package Controller
 * @created 2015-03-23
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NailFavorites extends \Controller_App
{
    /**
     * Add info for Nail Favorite
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\NailFavorites_Add::getInstance()->execute();
    }

    /**
     * Get list Nail Favorite (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\NailFavorites_List::getInstance()->execute();
    }

    /**
     * Disable/Enable a Nail Favorite
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\NailFavorites_Disable::getInstance()->execute();
    }
}

