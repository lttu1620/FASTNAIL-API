<?php

/**
 * Controller_Paints - Controller for actions on Paints
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author VuLTH
 * @copyright Oceanize INC
 */
class Controller_Paints extends \Controller_App {

    /**
     *  Get list paint by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Paints_List::getInstance()->execute();
    }

    /**
     *  Get detail of paint
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Paints_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for paint
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Paints_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new paint
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Paints_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all paint
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Paints_All::getInstance()->execute();
    }
}

