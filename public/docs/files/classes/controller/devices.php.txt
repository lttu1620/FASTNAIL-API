<?php

/**
 * Controller for actions on Devices
 *
 * @package Controller
 * @created 2015-04-01
 * @version 1.0
 * @author Hoang Gia Thong
 * @copyright Oceanize INC
 */
class Controller_Devices extends \Controller_App
{
    /**
     * Add and update info for Devices
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Devices_AddUpdate::getInstance()->execute();
    }
   
    /**
     * Get list Devices (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Devices_List::getInstance()->execute();
    }

    /**
     * Get all Devices (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Devices_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Devices
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Devices_Disable::getInstance()->execute();
    }

    /**
     * Get detail Devices
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Devices_Detail::getInstance()->execute();
    }    
}

