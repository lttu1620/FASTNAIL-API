<?php

/**
 * Controller_NailRameJells - Controller for actions on Nail RameJells
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author Tran Xuan Khoa
 * @copyright Oceanize INC
 */
class Controller_NailRameJells extends \Controller_App {

    /**
     *  Get list nail's rame_jells by condition
     * 
     * @return array 
     */
    public function action_list() {
        return \Bus\NailRameJells_List::getInstance()->execute();
    }
    
    /**
     *  Get all nail's rame_jells
     * 
     * @return array 
     */
    public function action_all() {
        return \Bus\NailRameJells_All::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for rame_jell
     * 
     * @return boolean 
     */
    public function action_addupdatebyramejellid() {
        return \Bus\NailRameJells_AddUpdateByRameJellId::getInstance()->execute();
    }
    
    /**
     *  Update or add new nails for rame_jell
     * 
     * @return boolean 
     */
    public function action_addupdatebynailid() {
        return \Bus\NailRameJells_AddUpdateByNailId::getInstance()->execute();
    }

}

