<?php

/**
 * Controller for actions on Order Nail
 *
 * @package Controller
 * @created 2015-03-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_OrderNails extends \Controller_App
{
    /**
     * Add info for Order Nail
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\OrderNails_Add::getInstance()->execute();
    }

    /**
     * Get all Order Nail (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\OrderNails_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Order Nail
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\OrderNails_Disable::getInstance()->execute();
    }
}

