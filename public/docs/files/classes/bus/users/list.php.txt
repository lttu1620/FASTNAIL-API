<?php

namespace Bus;

/**
 * Get list User (with array count)
 *
 * @package Bus
 * @created 2015-03-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'first_name'         => array(1, 64),
        'last_name'         => array(1, 64),
        'code'              => array(1, 64),
        'sex'               => 1,
        'phone'             => array(1, 64),
        'email'             => array(1, 255),
        'disable'           => 1,
        'disable_by_user'   => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'sex',
        'disable',
        'disable_by_user'
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email'
    );

    /**
     * Call function get_list() from model User
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::get_list($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}

