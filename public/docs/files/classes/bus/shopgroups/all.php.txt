<?php

namespace Bus;

/**
 * <ShopGroups_All - Model to operate to ShopGroups's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author Vulth
 * @copyright Oceanize INC
 */
class ShopGroups_All extends BusAbstract 
{
    /**
     * call function operateDB()
     *     
     * @author Vulth
     * @param $data
     * @return bool    
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Shop_Group::get_all($data);
            return $this->result(\Model_Shop_Group::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

