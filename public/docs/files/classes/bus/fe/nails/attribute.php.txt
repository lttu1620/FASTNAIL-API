<?php

namespace Bus;

/**
 * Get list Nails (using array count)
 *
 * @package Bus
 * @created 2015-04-06
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Fe_Nails_Attribute extends BusAbstract
{

    /** @var array $_required field require */
    protected $_required = array(
        'nail_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'nail_id' => array(1, 11),        
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'nail_id',        
    );

    /**
     * Call function get_top() from model Nail
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {                   
            $this->_response = \Model_Nail::get_attribute_for_frontend($data);
            return $this->result(\Model_Nail::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}

