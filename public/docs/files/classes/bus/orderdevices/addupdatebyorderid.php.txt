<?php

namespace Bus;

/**
 * Add and update info for Order Device
 *
 * @package Bus
 * @created 2015-04-14
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderDevices_AddUpdateByOrderId extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'order_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'order_id' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'order_id'
    );

    /**
     * Validate device id list format: d,d,d,d
     *
     * @author diennnvt
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function checkDataFormat($data)
    {
        //Validate device_ id's format
        $field = 'device_id';
        if (!empty($data[$field])
            && !preg_match('/^\d(?:,\d)*$/', $data[$field])
        ) {//device_id's format: digital,digital,digital
            $this->_addError(self::ERROR_CODE_INVALED_PARAMETER, $field, $data[$field]);
            $this->_invalid_parameter = $field;
            return false;
        }
        return true;
    }

    /**
     * Call function add_update_by_order_id() from model Order Device
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Device::add_update_by_order_id($data);
            return $this->result(\Model_Order_Device::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}

