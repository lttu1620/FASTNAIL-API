<?php

namespace Bus;

/**
 * Increase field limit for Order Timely Limit
 *
 * @package Bus
 * @created 2015-05-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class OrderTimelyLimits_IncreaseLimit extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'shop_id',
        'time_from',
        'time_to',       
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'shop_id'  => array(1, 11),
        'increase_fn' => array(1, 11),
        'increase_hp' => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'shop_id',
        'increase_fn',
        'increase_hp'
    );

    /** @var array $_date_format field date */
    protected $_date_format = array(
        'time_from' => 'Y-m-d H:i:s',
        'time_to'   => 'Y-m-d H:i:s'
    );

    /**
     * Call function Increase_limit() from model Order Timely Limit
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Order_Timely_Limit::increase_limit($data);
            return $this->result(\Model_Order_Timely_Limit::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}

