<?php

/**
 * The production database settings. These get merged with the global settings.
 */
return array(
    'default' => array(
        'connection' => array(
            'dsn' => 'mysql:host=fastanaildb.csliptt4607j.ap-northeast-1.rds.amazonaws.com;dbname=fastnail',
            'username' => 'fastnailuser',
            'password' => 'fastnailpass123',
        ),
        'timezone' => '+9:00'
    ),
);

