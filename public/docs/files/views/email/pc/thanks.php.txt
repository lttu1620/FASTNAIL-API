<?php
echo "
{$user_name} 様<br>
<br>
□■□■□■□■□■□■<br>
ファストネイルへ<br>
ご来店ありがとうございました<br>
□■□■□■□■□■□■<br>
<br>
ご来店いただきましてありがとうございました。<br>
従業員一同、更なるサービス向上を目指していきますので、今後ともよろしくお願い致します。<br>
<br>
<br>
ファストネイルでは、会員の方だけが受けられるお得なキャンペーンや最新情報を随時お送りしております。<br>
<br>
次はお友達と一緒に～<br>
↓友達に教える↓<br>
￣￣￣￣￣￣￣￣￣￣￣<br>
http://www.fastnail.jp/fn-mobile/201212_friends.html<br>
<br>
<br>
◆◆◆◆◆◆◆◆◆◆◆◆<br>
お客様のお声をください<br>
◆◆◆◆◆◆◆◆◆◆◆◆<br>
<br>
お客様になお一層ご満足をいただけるサービスをご提供するために、ご意見・ご要望をお聞かせください。<br>
<br>
↓アンケートはコチラからご入力お願いします。<br>
https://jp.surveymonkey.com/s/fastnail_TY?c={$order_id}<br>
<br>
お客様から選ばれるネイルサロンとなることを目標としサービス向上に役立てていく所存でございますのでご協力くださいますようお願い申し上げます。<br>
<br>
<br>
<br>
<br>
★アナタのための★<br>
￣￣￣￣￣￣￣￣￣￣￣￣￣￣<br>
研修あり！アナタもネイリストに<br>
♪正社員、アルバイト情報♪<br>
http://www.fastnail.jp/fn-mobile/fn-recruit-2013-a.html<br>
<br>
<br>
<br>
<br>
◆FASTNAILオフィシャルサイト◆<br><br>
￣￣￣￣￣￣￣￣￣￣￣￣￣￣<br>
http://www.fastnail.jp/fn-mobile/fn-2013-a.html<br><br>

◆FASTNAILネット予約サイト◆<br><br>
￣￣￣￣￣￣￣￣￣￣￣￣￣￣<br>
https://fastnail.town/<br>
<br>
<br>
<br>
<br>
ATTENTION━━━━━━━━━<br>

◆個人情報の取扱いについてはHP個人情報保護方針をご覧ください。<br>
http://www.fastnail.jp/company/privacy.html<br>
<br>
◆このメールは、送信専用アドレスから配信されています。ご返信いただいてもお答えできませんので、ご了承ください。<br>
<br>
━━━━━━━━━━━━━━<br>
";
