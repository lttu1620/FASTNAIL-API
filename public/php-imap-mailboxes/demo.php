<?php
// configure your imap mailboxes
$current_mailbox = array(
    'label'    => 'Gmail',
    'enable'   => true,
    'mailbox'  => '{imap.gmail.com:993/imap/ssl}INBOX',
    'username' => 'quannqjp@gmail.com',
    'password' => 'quannq@jp'

//        'label'    => 'My Cpanel website',
//        'enable'   => FALSE,
//        'mailbox'  => '{mail.yourdomain.com:143/notls}INBOX',
//        'username' => 'info+yourdomain.com',
//        'password' => 'yourpassword'
);

$current_mailbox = array(
    'label'    => 'Gmail',
    'enable'   => true,
    'mailbox'  => '{imap.gmail.com:993/imap/ssl}INBOX',
    'username' => 'developer.php.vn@gmail.com',
    'password' => 'dev@123456'
);

// a function to decode MIME message header extensions and get the text
function decode_imap_text($str) {
    $result = '';
    $decode_header = imap_mime_header_decode($str);
    foreach ($decode_header AS $obj) {
        $result .= htmlspecialchars(rtrim($obj->text, "\t"));
    }
    return $result;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Demo PHP mail</title>
        <link href="css/demo.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <div id="main">
                <div id="mailboxes">
                    <div class="mailbox">
                        <h2><?php echo $current_mailbox['label'] ?></h2>
                        <?php $stream = imap_open($current_mailbox['mailbox'], $current_mailbox['username'], $current_mailbox['password']); ?>
                        <?php if (!$stream): ?>
                            <p>Could not connect to: <?php echo $current_mailbox['label'] ?>. Error: <?php echo imap_last_error() ?></p>
                        <?php else: ?>
                            <?php $emails = imap_search($stream, 'SINCE ' . date('d-M-Y', strtotime("-1 week"))); ?>
                            <?php if (!count($emails)): ?>
                                <p>No e-mails found.</p>
                            <?php else: ?>
                                <?php rsort($emails); ?>
                                <?php foreach ($emails as $email_id): ?>
                                    <?php $overview = imap_fetch_overview($stream, $email_id, 0); ?>
                                    <div class="email_item clearfix <?php echo $overview[0]->seen ? 'read' : 'unread' ?>"> <?php // add a different class for seperating read and unread e-mails               ?>
                                        <div class="mailbox" style="clear: both;">
                                            <span class="subject" title="<?php echo decode_imap_text($overview[0]->subject) ?>"><?php echo decode_imap_text($overview[0]->subject) ?></span>
                                            <span class="from" title="<?php echo decode_imap_text($overview[0]->from) ?>"><?php echo decode_imap_text($overview[0]->from) ?></span>
                                            <span class="date"><?php echo $overview[0]->date ?></span>
                                        </div>
                                        <pre class="mailbox" style="clear: both;"><?php echo htmlentities(imap_body($stream, $email_id)); ?></pre>
                                    </div>
                                <?php endforeach; ?>
                                <?php imap_close($stream); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- #main -->
        </div><!-- #wrapper -->
    </body>
</html>
