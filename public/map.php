<?php
$address = $_GET['address'];
$phone = $_GET['phone'];
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/map.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    </head>
    <body>
        <div id="map">Hello</div>
    </body>
</html>
<script>
$(function() {	
	var page_title = "A";
	var address = "<?php echo $address;?>";
	var phone = "<?php echo $phone;?>";	
	var geocoder = new google.maps.Geocoder();
    var latitude = 0;
    var longitude = 0;
    var info = '<div style="line-height:20px;">';
	info += '<strong>' + page_title + '</strong><br/>';
	info += 'Address: ' + address + '<br/>';
	info += 'Phone: ' + phone + '<br/>';	
	info += '</div>';
	infowindow = new google.maps.InfoWindow({
		content: ''
	}); 
	geocoder.geocode({'address': address}, function(results, status) {               
        if (status == google.maps.GeocoderStatus.OK) {
            latitude=results[0].geometry.location.lat();
            longitude=results[0].geometry.location.lng();
            var latLng = new google.maps.LatLng(latitude, longitude);
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                "info": info,
				position: latLng,
                title: address,
                map: map,
                draggable: true,
				
            });
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(this.info)
				infowindow.open(map, this);
			});			
        } else {
            document.getElementById('map').innerHTML('');
        }
    });    
});
</script>

